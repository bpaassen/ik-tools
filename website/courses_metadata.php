<?php
/*
This API grabs the interdisciplinary college course metadata
from https://interdisciplinary-college.org/program/ and provides it
as pure json. Note that this API expects the json data to be wrapped
in a script tag and the script tag to have the following structure:

start_date = "2020-03-13";
dinner_date = "2020-03-17";
end_date = "2020-03-20";
courses = [
 ... one object per course
];

This javascript content will then be offered as json in the structure

{
  "start_date" : "2020-03-13",
  "dinner_date" : "2020-03-17",
  "end_date" : "2020-03-20",
  "courses" : [
    ... one object per course
  ]
}

Created 2020 by Benjamin Paassen - bpaassen@techfak.uni-bielefeld.de
*/

// We permit this API to be openly accessible because it only offers
// a small bandwidth of public data in a static format. Still, please handle
// responsibly.
header("Access-Control-Allow-Origin: *");
// load the html content from https://interdisciplinary-college.org/program/
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://interdisciplinary-college.org/program/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$html = curl_exec($ch);
curl_close($ch);
//$html = file_get_contents('https://interdisciplinary-college.org/program/');
// look for the script tag with the desired id
$start_pos  = strpos($html, '<script id="courses_metadata"');
if($start_pos === false) {
    echo 'Internal error: did not find script tag with id "courses_metadata"';
    return;
}
// look for the closing tag, starting from $pos
$end_pos = strpos($html, '</script>', $start_pos);
if($end_pos === false) {
    echo 'Internal error: did not find closing script tag';
    return;
}
// get the correct substring, containing only the javascript content
$offset = strlen('<script id="courses_metadata"');
$js = substr($html, $start_pos + $offset, $end_pos - $start_pos - $offset);
// remove comments; importantly, this expression matches greedily so that
// only the first closing */ is matched
$js = preg_replace('/[\n\r]*\/\*.+?\*\/[\n\r]*/s', '', $js);

// extract the javascript fields, i.e. start_data, dinner_date, end_date,
// and courses and reformat them as JSON content
$out = '{';
foreach(array('start_date', 'dinner_date', 'end_date', 'motto') as $key) {
    $pattern = '/' . $key . '\s*=\s*(\"[^"]+\")\s*;/';
    $found = preg_match($pattern, $js, $matches);
    if($found === 0 || $found === false) {
        continue;
    }
    $out .= '  "' . $key . '" : ' . $matches[1] . ',';
}
// extract the courses
$found = preg_match('/courses\s*=\s*\[/', $js, $matches, PREG_OFFSET_CAPTURE);
if($found === 0 || $found === false) {
    echo 'Internal error: did not find courses';
    return;
}
$courses_pos = $matches[0][1] + strlen($matches[0][0]) - 1;
// find position of last semicolon
$semi_pos = strrpos($js, ';');
if($semi_pos === false || $semi_pos <= $courses_pos) {
    echo 'Internal error: did not find semicolon at the end of the javascript data';
    return;
}

$courses_json = substr($js, $courses_pos, $semi_pos - $courses_pos);
$out .= '  "courses" : ' . $courses_json . '}';

echo $out;

?>
