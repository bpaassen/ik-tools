<?php
/*

This API loads calendar data from a google calendar and provides it as ical raw data.
Note that this API must be updated every year

Created 2019-2022 by Benjamin Paassen - bpaassen@techfak.uni-bielefeld.de

*/

// We permit this API to be openly accessible because it only offers
// a small bandwidth of public data in a static format. Still, please handle
// responsibly.
header("Access-Control-Allow-Origin: *");
header("Content-Type: text/calendar; charset=utf-8");

// loads the .ical data from google calendar
$cal = file_get_contents('https://ics.teamup.com/feed/ksq72jqw6aetd7b5zb/0.ics');

// Regex to match timestamps, with hours as separate group.
// This will exclude the marker for UTC at the end.
define('REGEX_TIMESTAMP', '/(2022\d{4}T)(\d{2})(\d{4})Z/');

// only apply timestamp hack if parameter is given
// Note: This hack _will_ break, if the hour is 0 (midnight).
if(isset($_GET['utc'])) {
    $cal = preg_replace_callback(REGEX_TIMESTAMP, function ($matches) {
        $time = intval($matches[2]) + 1;
        return $matches[1] . $time . $matches[3];
    }, $cal);
}

// echos the .ical data
echo $cal;

?>
