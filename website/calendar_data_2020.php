<?php
/*

This API loads calendar data from a google calendar and provides it as ical raw data.
Note that this API must be updated every year

Created 2019-2020 by Benjamin Paassen - bpaassen@techfak.uni-bielefeld.de

*/

// We permit this API to be openly accessible because it only offers
// a small bandwidth of public data in a static format. Still, please handle
// responsibly.
header("Access-Control-Allow-Origin: *");
// loads the .ical data from google calendar
$cal = file_get_contents('https://calendar.google.com/calendar/ical/87u6i1ooqr6kiubg0uq57n5jkk%40group.calendar.google.com/private-6ecf6ba57c6f0edf2f7ef0f4a197f4a6/basic.ics');
// echos the .ical data
echo $cal;
?>
