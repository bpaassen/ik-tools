
const DISCIPLINES = ['neurobiology', 'neural computation', 'cognitive science', 'psychology', 'artificial intelligence', 'machine learning', 'robotics', 'philosophy'];
const DISC_POINT_LABELS = ['NB', 'NC', 'CS', 'Psy', 'AI', 'ML', 'Rob', 'Phi'];
const SLIDER_MIN = 0;
const SLIDER_MAX = 100;


// normal distribution between 0 and 1
// from: https://stackoverflow.com/a/49434653/8952900
function randn_bm() {
  let u = 0, v = 0;
  while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random();
  let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
  num = num / 10.0 + 0.5; // Translate to 0 -> 1
  if (num > 1 || num < 0) return randn_bm() // resample between 0 and 1
  return num
}

function generateRandomValues(count) {
    // use Math.random() instead of randn_bm() for uniform distribution
    return [...Array(count)].map(_ => SLIDER_MIN + (SLIDER_MAX - SLIDER_MIN) * randn_bm());
}

function debounce(callback, timeout) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => callback.apply(this, args), timeout);
  };
}

function exportChart() {
    chartImageElem.src = chart.toBase64Image('image/png', 1);
}

const exportChartDebounced = debounce(() => exportChart(), 1000);

function updateChart(slider, index) {
    chart.data.datasets[0].data[index] = Number.parseInt(slider.value) + 1;
    chart.update('none');
    if(chartImageElem.src != "") {
        chartImageElem.src = "";
    }
    exportChartDebounced();
}

function updateColor() {
    const hex = colorInputElem.value;
    const r = parseInt(hex.slice(1, 3), 16);
    const g = parseInt(hex.slice(3, 5), 16);
    const b = parseInt(hex.slice(5, 7), 16);

    chart.config.data.datasets[0].backgroundColor = `rgba(${r}, ${g}, ${b}, 0.2)`;
    chart.config.data.datasets[0].borderColor = `rgb(${r}, ${g}, ${b})`;
    chart.update('none');
    exportChart();
}

function int2hex(integer) {
    const str = Number(integer).toString(16);
    return str.length == 1 ? "0" + str : str;
}

function randomColor() {
    colorInputElem.value = "#" + [0, 0, 0].map(_ => Math.floor(256 * Math.random())).map(int2hex).join('').toUpperCase();
    //const hue = Math.floor(360 * Math.random());
    //colorInputElem.value = `hsl(${hue}, 100%, 50%)`;
    updateColor();
}



const slidersElem = document.getElementById('sliders');
const initialDataValues = generateRandomValues(DISCIPLINES.length);

slidersElem.innerHTML = DISCIPLINES.map((discipline, index) => `
<div class="slider">
  <label for="slider${index+1}">${discipline}</label>
  <span>0%</span>
  <input type="range" name="slider${index+1}" min="${SLIDER_MIN}" max="${SLIDER_MAX}" value="${initialDataValues[index]}" oninput="updateChart(this, ${index});">
  <span>100%</span>  
</div>
`).join('\n');


const data = {
        labels: DISCIPLINES,
        datasets: [{
            label: 'Research Profile',
            data: initialDataValues,
            borderWidth: 3,
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgb(255, 99, 132)',
            fill: true,
            pointBackgroundColor: undefined,
            pointBorderWidth: 0,
            pointHoverBorderWidth: undefined,
        }]
};

const config = {
    type: 'radar',
    data: data,
    options: {
        animation: false,
        responsive: true,
        plugins: {
            title: {
                display: false,
                text: 'Research Profile',
            },
            legend: {
                display: false,
            },
            tooltip: {
                enabled: false,
            },
        },
        scales: {
            r: {
                min: SLIDER_MIN,
                max: SLIDER_MAX,
                ticks: {
                    display: false,
                },
                pointLabels: {
                    font: {
                        size: 32,
                    },
                    callback: (_, index) => DISC_POINT_LABELS[index],
                },
                backgroundColor: '#ffffff',
            },
        },
    },
};

const colorInputElem = document.getElementById('color-selection');
const chartImageElem = document.getElementById('chart-image');
const context = document.getElementById('chart').getContext('2d');
const chart = new Chart(context, config);

exportChart();
randomColor();


// Chrome only
function copyChart() {
  const canvas = document.getElementById('chart');
  canvas.toBlob(function (blob) {
    let data = [new ClipboardItem({ [blob.type]: blob })];

    navigator.clipboard.write(data).then(function () {
      console.log("yay");
    }, function (err) {
      console.log(err);
    })
  });
}

// API supported?
if(typeof ClipboardItem !== 'undefined') {
    const canvasWrapper = document.getElementById('canvas-wrapper');
    const moreToolsElem = document.createElement('div');
    moreToolsElem.innerHTML = "<p>You can also click/tap <button onclick=\"copyChart();\">here</button> or on the chart to copy to clipboard!</p>";
    canvasWrapper.parentElement.insertBefore(moreToolsElem, canvasWrapper);
    chartImageElem.onclick = copyChart;
}
