<?php
/* Template Name: Reimbursement Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, March 2020 -->
			<?php

				// definition of virtual IK conference fees
				$virtual_fees = [
					"Waiver" => 0,
					"Student" => 50,
					"Phd" => 120,
					"Full" => 250,
				];

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// first name
				if(!isset($_POST['firstname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the first name was not given. Please check your form again and re-submit.';
					exit;
				}
				$first_name = sanitize_text_field( $_POST['firstname'] );

				// last name
				if(!isset($_POST['lastname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the last name was not given. Please check your form again and re-submit.';
					exit;
				}
				$last_name = sanitize_text_field( $_POST['lastname'] );

				// phone number
				if(!isset($_POST['phone'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the phone number was not given. Please check your form again and re-submit.';
					exit;
				}
				$phone = sanitize_text_field( $_POST['phone'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// Fee Information
				if(!isset($_POST['conference_fee'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the conference fee was not given. Please check your form again and re-submit.';
					exit;
				}
				$conference_fee = sanitize_text_field( $_POST['conference_fee'] );

				if(!isset($_POST['accommodation_fee'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the accommodation fee was not given. Please check your form again and re-submit.';
					exit;
				}
				$accommodation_fee = sanitize_text_field( $_POST['accommodation_fee'] );

				// Bank account
				if(!isset($_POST['no_iban'])) {
					if(!isset($_POST['account_holder'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the bank account holder was not given. Please check your form again and re-submit.';
						exit;
					}
					$account_holder = sanitize_text_field( $_POST['account_holder'] );

					if(!isset($_POST['account_iban'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the bank account IBAN was not given. Please check your form again and re-submit.';
						exit;
					}
					$account_iban = sanitize_text_field( $_POST['account_iban'] );

					if(!isset($_POST['account_bic'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the bank account BIC was not given. Please check your form again and re-submit.';
						exit;
					}
					$account_bic = sanitize_text_field( $_POST['account_bic'] );
				} else {
					$account_holder = "n.a.";
					$account_iban = "n.a.";
					$account_bic = "n.a.";
				}

				// Virtual IK registration
				if(!isset($_POST['virtual_fee'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the virtual IK fee information was not specified. Please check your form again and re-submit.';
					exit;
				}
				$fee_string = sanitize_text_field( $_POST['virtual_fee'] );

				if(!($fee_string === 'Waiver' || $fee_string === 'Student' || $fee_string === 'Phd' || $fee_string === 'Full' || $fee_string === 'None')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the fee information had an invalid value (must be Waiver, Student, Phd, Full, or None). Please check your form again and re-submit.';
					exit;
				}

				// construct reimbursement e-mail from user data
				$reimbursement_mail  = "Last name:          $last_name\n";
				$reimbursement_mail .= "First name:         $first_name\n";
				$reimbursement_mail .= "Phone:              $phone\n";
				$reimbursement_mail .= "E-Mail:             $email\n\n";

				$reimbursement_mail .= "I wish to have the following costs reimbursed.\n";
				$reimbursement_mail .= "Conference Fee:     $conference_fee EUR\n";
				$reimbursement_mail .= "Accommodation Fee:  $accommodation_fee EUR\n\n";

				if(!isset($_POST['no_iban'])) {
					$reimbursement_mail .= "The reimbursement should be paid to:\n";
					$reimbursement_mail .= "Account Holder:     $account_holder\n";
					$reimbursement_mail .= "IBAN:               $account_iban\n";
					$reimbursement_mail .= "BIC:                $account_bic\n\n";
				} else {
					$reimbursement_mail .= "I do not have a European bank account. I will get in touch with\nregistration@interdisciplinary-college.org to arrange reimbursement.\n\n";
				}

				if($fee_string === 'None') {
					$virtual_fee = 0;
					$reimbursement_mail .= "I do not wish to participate in Virtual IK.";
				} elseif($fee_string === 'Waiver') {
					$virtual_fee = 0;
					$reimbursement_mail .= "I wish to participate in Virtual IK, but do not have institutional funding\nand so would like to have my registration fee waived.";
				} else {
					$virtual_fee = $virtual_fees[$fee_string];
					$reimbursement_mail .= "I wish to participate in Virtual IK with a conference fee of $virtual_fee EUR ($fee_string).\nI authorize you to deduct the virtual IK registration fee from my\nreimbursement.";
				}

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				if(strlen($reimbursement_mail) > 5000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the data was too long (> 5000 characters). Please check your form again and re-submit.';
					exit;
				}

				// create a machine readable version in JSON format
				$reimbursement_object = (object)[];
				$reimbursement_object->first_name = $first_name;
				$reimbursement_object->last_name = $last_name;
				$reimbursement_object->phone = $phone;
				$reimbursement_object->email = $email;
				$reimbursement_object->conference_fee = $conference_fee;
				$reimbursement_object->accommodation_fee = $accommodation_fee;
				$reimbursement_object->account_holder = $account_holder;
				$reimbursement_object->account_iban = $account_iban;
				$reimbursement_object->account_bic = $account_bic;
				$reimbursement_object->fee_string = $fee_string;
				$reimbursement_object->virtual_fee = $virtual_fee;

				// special code for debug input
				if($first_name === 'Debug') {
					echo "<p>Your reimbursement request was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that the reimbursement process may take time. For your own archive: The following data was transmitted:</p> <pre>$reimbursement_mail</pre>";
					$json_data = json_encode($reimbursement_object);
					echo "<p>JSON data</p><pre>$json_data</pre>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the reimbursement data
				 */
				if(!wp_mail('registration@interdisciplinary-college.org', 'IK Reimbursement Request', '<pre>' . $reimbursement_mail . '</pre>', $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your reimbursement request has failed. Please send your reimbursement request manually to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$reimbursement_mail</pre>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p>Your reimbursement request was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that the reimbursement process may take time. For your own archive: The following data was transmitted:</p> <pre>$reimbursement_mail</pre>";
				}

				$headers = array('Content-Type: text/plain; charset=UTF-8');

				// send a machine-readable backup copy to the webmaster
				wp_mail('webmaster@interdisciplinary-college.org', 'IK Reimbursement Request (machine readable copy)', json_encode($reimbursement_object), $headers)

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
