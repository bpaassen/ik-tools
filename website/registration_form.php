<?php
/* Template Name: Registration Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, October 2019 -->
			<?php
				// definition of conference fee constants
				// NOTE: THIS TABLE MUST BE UPDATED EVERY YEAR
				$conference_fees = [
					"EarlyBirdMemberStudent" => 110,
					"EarlyBirdNonMemberStudent" => 160,
					"EarlyBirdMemberPhd" => 280,
					"EarlyBirdNonMemberPhd" => 385,
					"EarlyBirdMemberFull" => 600,
					"EarlyBirdNonMemberFull" => 950,
					"RegularMemberStudent" => 140,
					"RegularNonMemberStudent" => 200,
					"RegularMemberPhd" => 360,
					"RegularNonMemberPhd" => 475,
					"RegularMemberFull" => 725,
					"RegularNonMemberFull" => 1150,
				];
				// definition of accommodation fee constants
				// NOTE: THIS TABLE MUST BE UPDATED EVERY YEAR
				$accommodation_fees = [
					"External" => 310,
					"Single" => 650,
					"Double" => 550,
					"Triple" => 390,
				];
				// check whether this registration is early bird.
				// NOTE: THIS DATE HAS TO BE UPDATED EVERY YEAR
				date_default_timezone_set('Europe/Berlin');
				$registration_date = date("m-d");
				if($registration_date >= "06-01" || $registration_date <= "01-08") {
					$conference_fee_string = 'EarlyBird';
				} else {
					$conference_fee_string = 'Regular';
				}

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) && $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// name
				if(!isset($_POST['first_name'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the first name was not given. Please check your form again and re-submit.';
					exit;
				}
				$first_name = sanitize_text_field( $_POST['first_name'] );

				if(!isset($_POST['last_name'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the öast name was not given. Please check your form again and re-submit.';
					exit;
				}
				$last_name = sanitize_text_field( $_POST['last_name'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// affiliation
				if(!isset($_POST['affiliation'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the affiliation was not given. Please check your form again and re-submit.';
					exit;
				}
				$affiliation = sanitize_text_field( $_POST['affiliation'] );

				// billing_name
				if(!isset($_POST['billing_name'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the name for the invoice was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_name = sanitize_text_field( $_POST['billing_name'] );

				// billing_institution
				if(!isset($_POST['billing_institution'])) {
					$billing_institution = "";
				} else {
					$billing_institution = sanitize_text_field( $_POST['billing_institution'] );
				}

				// billing_department
				if(!isset($_POST['billing_department'])) {
					$billing_department = "";
				} else {
					$billing_department = sanitize_text_field( $_POST['billing_department'] );
				}

				// billing_street
				if(!isset($_POST['billing_street'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the street of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_street = sanitize_text_field( $_POST['billing_street'] );

				// house no.
				if(!isset($_POST['billing_no'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the house number of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_no = sanitize_text_field( $_POST['billing_no'] );

				// billing_zip
				if(!isset($_POST['billing_zip'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the zip code of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_zip = sanitize_text_field( $_POST['billing_zip'] );

				// billing_city
				if(!isset($_POST['billing_city'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the city of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_city = sanitize_text_field( $_POST['billing_city'] );

				// billing_country
				if(!isset($_POST['billing_country'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the country of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_country = sanitize_text_field( $_POST['billing_country'] );


				// conference_fee
				if(!isset($_POST['conference_fee'])) {
					echo 'Unfortunately, your form data was invalid. In particular, your conference fee was not given. Please check your form again and re-submit.';
					exit;
				}
				$fee_class = sanitize_text_field( $_POST['conference_fee'] );
				if(!($fee_class === 'MemberStudent' || $fee_class === 'MemberPhd' || $fee_class === 'MemberFull' ||
					 $fee_class === 'NonMemberStudent' || $fee_class === 'NonMemberPhd' || $fee_class === 'NonMemberFull')) {
					echo 'Unfortunately, your form data was invalid. In particular, the conference fee had an invalid value (must be MemberStudent, MemberPhd, MemberFull, NonMemberStudent, NonMemberPhd, or NonMemberFull). Please check your form again and re-submit.';
					exit;
				}
				$conference_fee_string .= $fee_class;

				// GI membership number
				if(!isset($_POST['gi_number'])) {
					$gi_number = "";
				} else {
					$gi_number = sanitize_text_field( $_POST['gi_number'] );
				}

				// accommodation
				if(!isset($_POST['room'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the accommodation was not given. Please check your form again and re-submit.';
					exit;
				}
				$room = sanitize_text_field( $_POST['room'] );
				if(!($room === 'External' || $room === 'Single' || $room === 'Double' || $room === 'Triple')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the accommodation had an invalid value (must be External, Single, Double, or Triple). Please check your form again and re-submit.';
					exit;
				}
				if(($room === 'Double' || $room === 'Triple')) {
					if(!isset($_POST['roommates'])) {
						$roommates = '';
					} else {
						$roommates = sanitize_text_field( $_POST['roommates'] );
					}
				} else {
					$roommates = '';
				}

				if(!isset($_POST['room_requests'])) {
					$room_requests = '';
				} else {
					$room_requests = sanitize_text_field( $_POST['room_requests'] );
				}

				// payment type
				if(!isset($_POST['payment'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type was not given. Please check your form again and re-submit.';
					exit;
				}
				$payment = sanitize_text_field( $_POST['payment'] );
				if(!($payment === 'Bank transfer' || $payment === 'Credit card')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type had an invalid value (must be either Bank transfer or Credit card). Please check your form again and re-submit.';
					exit;
				}

				// dietary restrictions
				if(!isset($_POST['diet'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the dietary restrictions were not given. Please check your form again and re-submit.';
					exit;
				}
				$diet = sanitize_text_field( $_POST['diet'] );
				if(!($diet === 'Vegetarian' || $diet === 'Vegan' || $diet === 'Other' || $diet === 'None')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the dietary restrictions had an invalid value (must be either Vegetarian, Vegan, Other, or None). Please check your form again and re-submit.';
					exit;
				}
				if($diet === 'Other') {
					if(!isset($_POST['diet_other_text'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you specified Other dietary restrictions but not which one. Please check your form again and re-submit.';
						exit;
					}
					$diet_other_text = sanitize_text_field( $_POST['diet_other_text'] );
				} else {
					$diet_other_text = '';
				}

				// course preferences
				if(!isset($_POST['courses'])) {
					$courses = '';
				} else {
					$courses = sanitize_text_field( $_POST['courses'] );
				}

				if(!isset($_POST['consent_participant_list'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you did not indicate whether you consent to your data being included in the participant list. Please check your form again and re-submit.';
					exit;
				}
				$consent_participant_list = sanitize_text_field( $_POST['consent_participant_list'] );
				if(!($consent_participant_list === 'Yes' || $consent_participant_list === 'No')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your consent value to the participant list must be either Yes or No. Please check your form again and re-submit.';
					exit;
				}

				if(!isset($_POST['consent_alumni_list'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you did not indicate whether you consent to your data being included in the alumni database. Please check your form again and re-submit.';
					exit;
				}
				$consent_alumni_list = sanitize_text_field( $_POST['consent_alumni_list'] );
				if(!($consent_alumni_list === 'Yes' || $consent_alumni_list === 'No')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your consent value to the alumni data base must be either Yes or No. Please check your form again and re-submit.';
					exit;
				}

				// now, check stipend application data
				if(isset($_POST['stipend_application'])) {
/*
					// organisations
					if(isset($_POST['stipend_organisations'])) {
						$stipend_organisations = sanitize_text_field( $_POST['stipend_organisations'] );
					} else {
						$stipend_organisations = '';
					}*/

					$stipend_gender = sanitize_text_field( $_POST['stipend_gender'] );
					if(!($stipend_gender === 'Male' || $stipend_gender === 'Female' || $stipend_gender === 'NonBinary' || $stipend_gender === 'NoAnswer')) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the gender in your stipend application had a non-permitted value (must be either Male, Female, NonBinary, or NoAnswer). Please check your form again and re-submit.';
						exit;
					}

					// neuro_relation
					if(!(isset($_POST['neuro_relation']))) {
						echo 'Unfortunately, your form data was invalid. In particular, the relation to neuroscience was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$neuro_relation = sanitize_text_field( $_POST['neuro_relation'] );

					if(strlen($neuro_relation) > 500) {
						echo 'Unfortunately, your form data was invalid. In particular, the relation to neuroscience in the stipend application form was too long (> 500 characters). Please check your form again and re-submit.';
						exit;
					}

					// motivation letter
					if(!(isset($_POST['motivation']))) {
						echo 'Unfortunately, your form data was invalid. In particular, the academic motivation was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$motivation = sanitize_text_field( $_POST['motivation'] );

					if(strlen($motivation) > 4100) {
						echo 'Unfortunately, your form data was invalid. In particular, the academic motivation in the stipend application form was too long (> 4000 characters). Please check your form again and re-submit.';
						exit;
					}

					// poster planned
					if(isset($_POST['poster'])) {
						$poster = 'true';
					} else {
						$poster = 'false';
					}

					// spotlight talk planned
					if(isset($_POST['spotlight'])) {
						$spotlight = 'true';
					} else {
						$spotlight = 'false';
					}

					// lecture planned
					if(isset($_POST['lecture'])) {
						$lecture = 'true';
					} else {
						$lecture = 'false';
					}

					// contribution_title
					if(!(isset($_POST['contribution_title']))) {
						echo 'Unfortunately, your form data was invalid. In particular, the title of your planned contribution
						 was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$contribution_title = sanitize_text_field( $_POST['contribution_title'] );

					if(strlen($contribution_title) > 500) {
						echo 'Unfortunately, your form data was invalid. In particular, the title of your contribution in the stipend application form was too long (> 500 characters). Please check your form again and re-submit.';
						exit;
					}

					// ects
					if(isset($_POST['ects'])) {
						$ects = 'true';
					} else {
						$ects = 'false';
					}

					if(!(isset($_POST['travel_location']))) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the travel location was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$travel_location = sanitize_text_field( $_POST['travel_location'] );

					if(!(isset($_POST['travel_cost']))) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the travel cost was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$travel_cost     = sanitize_text_field( $_POST['travel_cost'] );

					// retrieve student status from conference fee string
					if(substr($conference_fee_string, -strlen('Student')) === 'Student') {
						$status = 'Student';
					} else if(substr($conference_fee_string, -strlen('Phd')) === 'Phd') {
						$status = 'PhD Student';
					} else {
						$status = 'Other';
					}
				}

				// sunshine volunteer
				if(isset($_POST['sunshine'])) {
					$sunshine = 'true';
					if(!isset($_POST['sunshine_lecturers'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the lecturers for which you wish to be sunshine were not given in the stipend application form. Please check your form again and re-submit.';
					}
					$sunshine_lecturers = sanitize_text_field( $_POST['sunshine_lecturers'] );
				} else {
					$sunshine = 'false';
					$sunshine_lecturers = '';
				}

				// moonshine volunteer
				if(isset($_POST['moonlight'])) {
					$moonlight = 'true';
				} else {
					$moonlight = 'false';
				}

				// compute conference fee and accommodation fee
				$conference_fee = $conference_fees[$conference_fee_string];
				$accommodation_fee = $accommodation_fees[$room];

				// construct object
				$registration_object = (object)[];
				$registration_object->first_name = $first_name;
				$registration_object->last_name = $last_name;
				$registration_object->email = $email;
				$registration_object->affiliation = $affiliation;
				$registration_object->billing_name = $billing_name;
				$registration_object->billing_institution = $billing_institution;
				$registration_object->billing_department = $billing_department;
				$registration_object->billing_street = $billing_street;
				$registration_object->billing_no = $billing_no;
				$registration_object->billing_zip = $billing_zip;
				$registration_object->billing_city = $billing_city;
				$registration_object->billing_country = $billing_country;
				$registration_object->conference_fee_string = $conference_fee_string;
				$registration_object->gi_number = $gi_number;
				$registration_object->room = $room;
				$registration_object->room_no = 'room number not yet given';
				$registration_object->roommates = $roommates;
				$registration_object->room_requests = $room_requests;
				$registration_object->payment = $payment;
				$registration_object->conference_fee = $conference_fee;
				$registration_object->accommodation_fee = $accommodation_fee;
				$registration_object->total_fee = $conference_fee + $accommodation_fee;
				$registration_object->diet = $diet;
				$registration_object->diet_other_text = $diet_other_text;
				$registration_object->courses = $courses;
				$registration_object->sunshine = $sunshine;
				$registration_object->sunshine_lecturers = $sunshine_lecturers;
				$registration_object->moonlight = $moonlight;
				$registration_object->consent_participant_list = $consent_participant_list;
				$registration_object->consent_alumni_list = $consent_alumni_list;
				$registration_object->interaction_group_no = '';
				$registration_object->registration_date = date("Y-m-d H:i:s");

				if(isset($_POST['stipend_application'])) {
					// append the object representation of the stipend application data to the
					// registration object
					$registration_object->stipend_application = 'Yes';
//					$registration_object->stipend_organisations = $stipend_organisations;
					$registration_object->stipend_gender = $stipend_gender;
					$registration_object->neuro_relation = $neuro_relation;
					$registration_object->motivation = $motivation;
					$registration_object->poster = $poster;
					$registration_object->spotlight = $spotlight;
					$registration_object->lecture = $lecture;
					$registration_object->contribution_title = $contribution_title;
					$registration_object->ects = $ects;
					$registration_object->travel_location = $travel_location;
					$registration_object->travel_cost = $travel_cost;
				} else {
					$registration_object->stipend_application = 'No';
				}

				// construct registration e-mail from user data
				$registration_mail  = "$first_name $last_name ($email) just registered. Please find the data below.\n\n<pre>" . json_encode($registration_object, JSON_PRETTY_PRINT) . "</pre>";

				// special code for debug input
				if($first_name === 'Debug') {
					echo "<p id=\"success\">Your waiting list entry was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that your registration is only confirmed once you received your invoice. For your own archive: The following data was transmitted:</p>$registration_mail";
					echo "<p>Your stipend application was sent successfully to <a href=\"mailto:stipends@interdisciplinary-college.org\">stipends@interdisciplinary-college.org</a>. You should receive the decision regarding the stipends via e-mail within a few weeks after the stipend deadline has passed.</p>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8', "Reply-To: $first_name $last_name <$email>");

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				 // NOTE THIS TITLE MUST BE UPDATED EVERY YEAR
				$conf_identifier = 'IK25';
				if(!wp_mail('registration@interdisciplinary-college.org', $conf_identifier . ' Waiting List Entry', $registration_mail, $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your waiting list entry has failed. Please send your waiting list entry manually to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p>$registration_mail";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p id=\"success\">Your waiting list entry was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that your registration is only confirmed once you received your invoice. For your own archive: The following data was transmitted:</p>$registration_mail";
				}

				// send the stipend application if there was one
				if(isset($_POST['stipend_application'])) {
					if(!wp_mail('stipends@interdisciplinary-college.org', $conf_identifier . ' Stipend Application', $registration_mail, $headers)) {
						// if sending the stipend application failed, inform the user.
						echo "<p>We are very sorry, but unfortunately sending your stipend application has failed. Please send your stipend application manually to <a href=\"mailto:stipends@interdisciplinary-college.org\">stipends@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p>$stipend_mail";
						exit;
					} else {
						// otherwise, display a success message
						echo "<p>Your stipend application was sent successfully to <a href=\"mailto:stipends@interdisciplinary-college.org\">stipends@interdisciplinary-college.org</a>. You should receive the decision regarding the stipends via e-mail within a few weeks after the stipend deadline has passed.</p>";
					}
				}

				// send a machine-readable backup copy to the webmaster
				wp_mail('webmaster@interdisciplinary-college.org', $conf_identifier . ' Waiting List Entry (copy)', $registration_mail, $headers);

				$confirmation_Mail  = "Dear $first_name $last_name,\n\nthank you very much for your waiting list entry for the interdisciplinary college! Your waiting list entry was received. Please note that your registration is only fully confirmed once you receive an invoice from us. For questions, please refer to registration@interdisciplinary-college.org.\n\nFor your own archive, you can find a full copy of the data transmitted to the conference office below.\n\n<pre>" . json_encode($registration_object, JSON_PRETTY_PRINT) . "</pre>";
				// send a copy back to the registrant
				wp_mail($email, $conf_identifier . ' waiting list entry received', $confirmation_Mail, $headers);

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
