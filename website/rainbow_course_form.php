<?php
/* Template Name: Rainbow Course Submission Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, March 2020 -->
			<?php

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// title
				if(!isset($_POST['title'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the title was not given. Please check your form again and re-submit.';
					exit;
				}
				$title = sanitize_text_field( $_POST['title'] );

				// disciplines/fields
				if(!isset($_POST['fields'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the fields were not given. Please check your form again and re-submit.';
					exit;
				}
				$fields = sanitize_text_field( $_POST['fields'] );

				// abstract
				if(!isset($_POST['abstract'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the abstract was not given. Please check your form again and re-submit.';
					exit;
				}
				$abstract = sanitize_textarea_field( $_POST['abstract'] );

				// literature
				if(!isset($_POST['literature'])) {
					$literature = '';
				} else {
					$literature = sanitize_textarea_field( $_POST['literature'] );
				}

				// name
				if(!isset($_POST['instructor'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the name was not given. Please check your form again and re-submit.';
					exit;
				}
				$instructor = sanitize_text_field( $_POST['instructor'] );

				// affiliation
				if(!isset($_POST['affiliation'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the affiliation was not given. Please check your form again and re-submit.';
					exit;
				}
				$affiliation = sanitize_text_field( $_POST['affiliation'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// homepage
				if(!isset($_POST['homepage'])) {
					$homepage = '';
				} else {
					$homepage = sanitize_text_field( $_POST['homepage'] );
				}
/*
				// photo URL
				if(!isset($_POST['photo_url'])) {
					$photo_url = '';
				} else {
					$photo_url = sanitize_text_field( $_POST['photo_url'] );
				}
*/
				// photo
				if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] === UPLOAD_ERR_NO_FILE) {
					// a null value here indicates no image further down below
					$photo_filename = null;

				} else {
					if($_FILES["photo"]["error"] !== UPLOAD_ERR_OK) {
						echo 'General error while uploading image. Please try again or contact the administrators, if the problem persists.';
						exit;
					}

					$upload_max_size = 2 * 1024 * 1024;
					if($_FILES["photo"]["size"] > $upload_max_size) {
						echo 'Sorry, the uploaded file exceeded the maxiumum size of '.($upload_max_size/1024).' kb. Please try again with a lower resolution';
						exit;
					}

					$upload_extension = strtolower(pathinfo($_FILES["photo"]["name"], PATHINFO_EXTENSION));

					$upload_valid_extensions = array("jpg", "jpeg", "png");
					if(!in_array($upload_extension, $upload_valid_extensions)) {
						echo 'An invalid file type was uploaded. Please try one of the suggested ones: '.implode(", ", $upload_valid_extensions);
						exit;
					}

					$upload_file_info = getimagesize($_FILES["photo"]["tmp_name"]);
					if($upload_file_info === false) {
						echo 'Sorry, your uploaded image seems to be broken. Please try again and get in contact with the administrators, if the problem persists.';
						exit;
					}

					// Constraints regarding images sizes could be enforced
					// at this place.

					// alternative approaches:
					// - $_FILES["photo"]["type"]; 
					//   - unreliable, can be faked by client
					// - $info = finfo_open(FILEINFO_MIME_TYPE);
					//   $mime = finfo_file($finfo, $filename);
					//   - would work, but more general than necessary
					//   - not as portable

					$upload_valid_mimetypes = array("image/jpeg", "image/png");
					if(!in_array($upload_file_info['mime'], $upload_valid_mimetypes)) {
						echo 'The uploaded file seems to be in an unexpected format. Please try a different one.';
						exit;
					}

					// All sanity checks successful, hooray!
					// Store details for later use.

					// Next line could be changed to a uniform name based on
					// sanitised text input from other form fields.
					$photo_filename = sanitize_text_field(basename($_FILES["photo"]["name"]));
					$photo_mimetype = $upload_file_info['mime'];
					$photo_data = base64_encode(file_get_contents($_FILES["photo"]["tmp_name"]));
				}

				// short CV
				if(!isset($_POST['short_cv'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the short CV was not given. Please check your form again and re-submit.';
					exit;
				}
				$short_cv = sanitize_textarea_field( $_POST['short_cv'] );

				// preprocessing ends here

				// construct email to conference management
				$description_email = "<p>New Rainbow course submission from $email</p>\n";
				$description_email .= "<h2>$title</h2>\n";
				$description_email .= "<p><strong>Lecturer:</strong> <a href=\"#lecturer\">$instructor</a><br/>\n";
				$description_email .= "<strong>Fields:</strong> $fields</p>\n\n";

				$description_email .= "<h3>Content</h3>\n\n<p>";
				$description_email .= preg_replace(array('/\s*\n\s*\n\s*/', '/\s*\n\s*/'), array("</p><p>", "<br/>"), $abstract);
				$description_email .= "</p>\n\n";

				if($literature !== '') {
					$description_email .= "<h3>Literature</h3>\n\n<ul><li>";
					$description_email .= preg_replace('/\s*\n\s*\*\s*/', "</li><li>", $literature);
					$description_email .= "</li></ul>\n\n";
				}

				$description_email .= "<h3 id=\"lecturer\">Lecturer</h3>\n\n";
				/*
				if($photo_url !== '') {
					$description_email .= "<div class=\"wp-block-image\"><figure class=\"alignright is-resized\"><img loading=\"lazy\" src=\"$photo_url\" sizes=\"(max-width:300px) 100vw 300px\"/></figure></div>";
				}
				*/
				if($photo_filename) {
					$description_email .= "<div class=\"wp-block-image\"><figure class=\"alignright is-resized\">";
					$description_email .= "<img src=\"data:$photo_mimetype;base64,$photo_data\" alt=\"$instructor\" sizes=\"(max-width:300px) 100vw 300px\"/>";
					$description_email .= "</figure></div>";
				} else {
					$description_email .= "<p>No image provided.</p>\n";
				}
				$description_email .= "<p>$short_cv</p>";
				$description_email .= "\n\n<p><strong>Affiliation:</strong> $affiliation";
				if($homepage !== '') {
					$description_email .= "<br/>\n<strong>Homepage:</strong> <a href=\"$homepage\">$homepage</a>";
				}
				$description_email .= "</p>\n\n";

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				$description_length = strlen($description_email);
				if($photo_filename) {
					// Do not count the size of an embedded image, if one was
					// uploaded.
					// FIXME This is a cheap fix. There should be smarter ways
					// to check for valid input sizes.
					$description_length -= strlen($photo_data);
				}
				if($description_length > 15000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the data was too long (> 15000 characters). Please check your form again and re-submit.';
					exit;
				}

				// create a machine readable version in JSON format
				$description_object = (object)[];
				$description_object->identifier = "RC1";
				$description_object->title = $title;
				$description_object->fields = $fields;
				$description_object->url = "https://interdisciplinary-college.org/2021-rc1/";
				$description_object->type = "Rainbow Course";
				$description_object->instructor = $instructor;
				$description_object->description = $abstract;

				$json_data = json_encode($description_object);

				// special code for debug input
				if($instructor === 'Debug') {
					echo "<p id=\"success\">The debug is complete. The following email would be send to <a href=\"mailto:webmaster@interdisciplinary-college.org\">webmaster@interdisciplinary-college.org</a>:</p>$description_email";
					echo "<p>JSON data</p><pre>$json_data</pre>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8', "Reply-To: $email");

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				if(!wp_mail('poster@interdisciplinary-college.org', 'IK 2024 Rainbow Course Submission', $description_email, $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your course description has failed. Please send your course description manually to <a href=\"mailto:webmaster@interdisciplinary-college.org\">webmaster@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$description_email</pre>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p id=\"success\">Your rainbow course submission was sent successfully to <a href=\"mailto:poster@interdisciplinary-college.org\">poster@interdisciplinary-college.org</a>.</p>";
				}

				// add copy for registration desk
				wp_mail('registration@interdisciplinary-college.org', 'IK 2024 Rainbow Course Submission (copy)', $description_email, $headers);

				// add copy with machine readable format to webmaster
				wp_mail('webmaster@interdisciplinary-college.org', 'IK 2024 Rainbow Course Submission (machine-readable copy)', $description_email . "\n\n<pre>$json_data</pre>", $headers);

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
