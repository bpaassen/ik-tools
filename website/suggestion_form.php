<?php
/* Template Name: Suggestion Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, April 2021 -->
			<?php

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// name
				if(!isset($_POST['full_name'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the name was not given. Please check your form again and re-submit.';
					exit;
				}
				$full_name = sanitize_text_field( $_POST['full_name'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// process contact consent
				if(!isset($_POST['contact_consent'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you did not tell us whether you consent to being contacted for follow up questions.';
					exit;
				}
				$contact_consent = sanitize_text_field( $_POST['contact_consent'] );
				if(!($contact_consent === 'Yes' || $contact_consent === 'No')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your consent answer must be either Yes or No. Please check your form again and re-submit.';
					exit;
				}

				// process speaker suggestions
				if(isset($_POST['speakers'])) {
					$speakers = sanitize_textarea_field( $_POST['speakers'] );
				} else {
					$speakers = "";
				}

				// process topic suggestions
				if(isset($_POST['topics'])) {
					$topics = sanitize_textarea_field( $_POST['topics'] );
				} else {
					$topics = "";
				}

				// process further suggestions
				if(isset($_POST['further'])) {
					$further = sanitize_textarea_field( $_POST['further'] );
				} else {
					$further = "";
				}

				// preprocessing ends here

				// construct suggestion e-mail from user data
				$suggestion_mail  = "Name:         $full_name\n";
				$suggestion_mail .= "e-Mail:       $email\n";
				$suggestion_mail .= "consent to contact: $contact_consent\n\n";

				$suggestion_mail .= "Speaker suggestions: $speakers\n\n";
				$suggestion_mail .= "Topic suggestions: $topics\n\n";
				$suggestion_mail .= "Further suggestions: $further\n\n";

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				if(strlen($suggestion_mail) > 5000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the data suspiciously long (> 5000 characters). Please check your form again and re-submit.';
					exit;
				}

				// create a machine readable version in JSON format
				$suggestion_object = (object)[];
				$suggestion_object->name = $full_name;
				$suggestion_object->email = $email;
				$suggestion_object->contact_consent = $contact_consent;
				$suggestion_object->speakers = $speakers;
				$suggestion_object->topics = $topics;
				$suggestion_object->further = $further;

				$json_data = json_encode($suggestion_object);
				// special code for debug input
				if($full_name === 'Debug') {
					echo "<p id=\"success\">The debug is complete. The following email would be send to <a href=\"mailto:suggestions@interdisciplinary-college.org\">suggestions@interdisciplinary-college.org</a>:</p> <pre>$suggestion_mail</pre>";
					echo "<p>JSON data</p><pre>$json_data</pre>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				if(!wp_mail('suggestions@interdisciplinary-college.org', 'Suggestion for IK', '<pre>' . $suggestion_mail . '</pre><p>Machine readable version:</p><pre>' . $json_data . '</pre>', $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your suggestion has failed. Please send your suggestion manually to <a href=\"mailto:suggestions@interdisciplinary-college.org\">suggestions@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$suggestion_mail</pre>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p id=\"success\">Your suggestion was sent successfully to the conference organizers. For your own archive: The following data was transmitted:</p> <pre>$suggestion_mail</pre>";
				}

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
