<?php
/*

This API loads calendar data from a google calendar and provides it as ical raw data.
Note that this API must be updated every year

Created 2019-2023 by Benjamin Paassen - bpaassen@techfak.uni-bielefeld.de

*/

// We permit this API to be openly accessible because it only offers
// a small bandwidth of public data in a static format. Still, please handle
// responsibly.
header("Access-Control-Allow-Origin: *");
header("Content-Type: text/calendar; charset=utf-8");

// loads the .ical data from google calendar
$cal = file_get_contents('https://calendar.google.com/calendar/ical/5321c1d35e6f6e5f5921707ff668bc57f9ddf99dda5df524e9ec8a7102c3567c%40group.calendar.google.com/public/basic.ics');

// echos the .ical data
echo $cal;

?>
