<?php
/* Template Name: Course Description Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, March 2020 -->
			<?php

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// title
				if(!isset($_POST['title'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the course title was not given. Please check your form again and re-submit.';
					exit;
				}
				$title = sanitize_text_field( $_POST['title'] );

				// disciplines/fields
				if(!isset($_POST['fields'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the scientific fields of the course were not given. Please check your form again and re-submit.';
					exit;
				}
				$fields = sanitize_text_field( $_POST['fields'] );

				// abstract
				if(!isset($_POST['abstract'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the course abstract was not given. Please check your form again and re-submit.';
					exit;
				}
				$abstract = sanitize_textarea_field( $_POST['abstract'] );

				// literature
				if(!isset($_POST['literature'])) {
					$literature = '';
				} else {
					$literature = sanitize_textarea_field( $_POST['literature'] );
				}

				// length
				if(!isset($_POST['length'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the length was not given. Please check your form again and re-submit.';
					exit;
				}
				$length = sanitize_text_field( $_POST['length'] );
				if(!($length === '1' || $length === '3' || $length === '4')) {
					echo 'Unfortunately, your form data was invalid. In particular, the length was neither 1, nor 3, nor 4. Please check your form again and re-submit.';
					exit;
				}

				// availability
				if(!isset($_POST['availability'])) {
					echo 'Unfortunately, your form data was invalid. In particular, the availability was not given. Please check your form again and re-submit.';
					exit;
				}
				$availability = sanitize_text_field( $_POST['availability'] );

				// special requests
				if(!isset($_POST['special_requests'])) {
					$special_requests = '';
				} else {
					$special_requests = sanitize_textarea_field( $_POST['special_requests'] );
				}

				/* Process instructors and put them onto an array */
				$instructors = array();
				$photos      = array();
				$instructor_str = '';
				$instructor_ids = ['first', 'second', 'third', 'fourth', 'fifth'];
				foreach($instructor_ids as $inst_id) {
					// check if the current instructor was set by inspecting the name field
					if(!isset($_POST[$inst_id . '_first_name']) || $_POST[$inst_id . '_first_name'] === '') {
						// if it was not set, check if this is the first instructor. if so,
						// we throw an error
						if($inst_id == 'first') {
							echo 'Unfortunately, your form data was invalid. In particular, the first instructor was not defined. Please check your form again and re-submit.';
							exit;
						}
						// otherwise, just continue
						continue;
					}
					// retrieve the instructor name
					$first_name = sanitize_text_field( $_POST[$inst_id . '_first_name'] );

					if(!isset($_POST[$inst_id . '_last_name'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the last name for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$last_name = sanitize_text_field( $_POST[$inst_id . '_last_name'] );

					if($inst_id != 'first') {
						$instructor_str .= ', ';
					}
					$instructor_str .= $first_name . ' ' . $last_name;
					// if the name for this instructor was set, we also expect that
					// the other required fields are defined

					// email
					if(!isset($_POST[$inst_id . '_email'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the email for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$email = sanitize_text_field( $_POST[$inst_id . '_email'] );

					// email
					if(!isset($_POST[$inst_id . '_affiliation'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the affiliation for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$affiliation = sanitize_text_field( $_POST[$inst_id . '_affiliation'] );

					// homepage
					if(!isset($_POST[$inst_id . '_homepage'])) {
						$homepage = '';
					} else {
						$homepage = sanitize_text_field( $_POST[$inst_id . '_homepage'] );
						$homepage = str_replace('\/', '/', $homepage);
					}

					// cv
					if(!isset($_POST[$inst_id . '_cv'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the short CV for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$short_cv = sanitize_textarea_field( $_POST[$inst_id . '_cv'] );

					// billing info

					// name
					if(!isset($_POST[$inst_id . '_billing_name'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the name (for travel reimbursement correspondence) for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_name = sanitize_text_field( $_POST[$inst_id . '_billing_name'] );

					// institution
					if(!isset($_POST[$inst_id . '_institution'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the institution for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_institution = sanitize_text_field( $_POST[$inst_id . '_institution'] );

					// department
					if(!isset($_POST[$inst_id . '_department'])) {
						$billing_department = '';
					} else {
						$billing_department = sanitize_text_field( $_POST[$inst_id . '_department'] );
					}

					// street
					if(!isset($_POST[$inst_id . '_street'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the street for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_street = sanitize_text_field( $_POST[$inst_id . '_street'] );

					// house no
					if(!isset($_POST[$inst_id . '_no'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the house number for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_no = sanitize_text_field( $_POST[$inst_id . '_no'] );

					// zip
					if(!isset($_POST[$inst_id . '_zip'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the ZIP code for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_zip = sanitize_text_field( $_POST[$inst_id . '_zip'] );

					// city
					if(!isset($_POST[$inst_id . '_city'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the city for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_city = sanitize_text_field( $_POST[$inst_id . '_city'] );

					// country
					if(!isset($_POST[$inst_id . '_country'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the country for the ' . $inst_id . ' instructor was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_country = sanitize_text_field( $_POST[$inst_id . '_country'] );

					// dietary restrictions
					if(!isset($_POST[$inst_id . '_diet'])) {
						echo 'Unfortunately, your form data was invalid. In particular, the dietary restrictions for the ' . $inst_id . ' instructor were not given. Please check your form again and re-submit.';
						exit;
					}
					$diet = sanitize_text_field( $_POST[$inst_id . '_diet'] );
					if(!($diet === 'Vegetarian' || $diet === 'Vegan' || $diet === 'Other' || $diet === 'None')) {
						echo 'Unfortunately, your form data was invalid. In particular, the dietary restrictions for the ' . $inst_id . ' instructor had an invalid value (must be either Vegetarian, Vegan, Other, or None). Please check your form again and re-submit.';
						exit;
					}
					if($diet === 'Other' & isset($_POST['diet_other_text'])) {
						$diet_other_text = sanitize_text_field( $_POST[$inst_id . 'diet_other_text'] );
					} else {
						$diet_other_text = '';
					}

					// photo requires special handling
					$photo_data     = null;
					$photo_mimetype = null;
					if(!isset($_FILES[$inst_id . '_photo']) || $_FILES[$inst_id . '_photo']["error"] === UPLOAD_ERR_NO_FILE) {
						// a null value here indicates no image further down below
						$photo_filename = null;

					} else {
						if($_FILES[$inst_id . '_photo']["error"] !== UPLOAD_ERR_OK) {
							echo 'General error while uploading image. Please try again or contact the administrators, if the problem persists.';
							exit;
						}

						$upload_max_size = 2 * 1024 * 1024;
						if($_FILES[$inst_id . '_photo']["size"] > $upload_max_size) {
							echo 'Sorry, the uploaded file exceeded the maxiumum size of '.($upload_max_size/1024).' kb. Please try again with a lower resolution';
							exit;
						}

						$upload_extension = strtolower(pathinfo($_FILES[$inst_id . '_photo']["name"], PATHINFO_EXTENSION));

						$upload_valid_extensions = array("jpg", "jpeg", "png");
						if(!in_array($upload_extension, $upload_valid_extensions)) {
							echo 'An invalid file type was uploaded. Please try one of the suggested ones: '.implode(", ", $upload_valid_extensions);
							exit;
						}

						$upload_file_info = getimagesize($_FILES[$inst_id . '_photo']["tmp_name"]);
						if($upload_file_info === false) {
							echo 'Sorry, your uploaded image seems to be broken. Please try again and get in contact with the administrators, if the problem persists.';
							exit;
						}

						// Constraints regarding images sizes could be enforced
						// at this place.

						// alternative approaches:
						// - $_FILES[$inst_id . '_photo']["type"]; 
						//   - unreliable, can be faked by client
						// - $info = finfo_open(FILEINFO_MIME_TYPE);
						//   $mime = finfo_file($finfo, $filename);
						//   - would work, but more general than necessary
						//   - not as portable

						$upload_valid_mimetypes = array("image/jpeg", "image/png");
						if(!in_array($upload_file_info['mime'], $upload_valid_mimetypes)) {
							echo 'The uploaded file seems to be in an unexpected format. Please try a different one.';
							exit;
						}

						// All checks successful, hooray!
						// Store details for later use.

						// Next line could be changed to a uniform name based on
						// sanitised text input from other form fields.
						$photo_filename = sanitize_text_field(basename($_FILES[$inst_id . '_photo']["name"]));
						$photo_mimetype = $upload_file_info['mime'];
						// get actual image data, scale it to 300
						// pixel width, and then encode it as
						// base64 such that we can transmit it as
						// part of an HTML form
						if($photo_mimetype == 'image/jpeg') {
							$image = imagecreatefromjpeg($_FILES[$inst_id . '_photo']["tmp_name"]);
							$old_width = imageSX($image);
							if($old_width > 350) {
								$image = imagescale($image, 350, -1,  IMG_BICUBIC);
							}
							ob_start();
							imagejpeg($image);
							$photo_data = base64_encode(ob_get_contents());
							ob_end_clean();
							imagedestroy($image); 
						} else {
							$image = imagecreatefrompng($_FILES[$inst_id . '_photo']["tmp_name"]);
							$old_width = imageSX($image);
							if($old_width > 350) {
								$image = imagescale($image, 350, -1,  IMG_BICUBIC);
							}
							ob_start();
							imagepng($image);
							$photo_data = base64_encode(ob_get_contents());
							ob_end_clean();
							imagedestroy($image); 
						}
					}
					$photo_obj = (object)[];
					$photo_obj->filename = $photo_filename;
					$photo_obj->data = $photo_data;
					$photo_obj->mimetype = $photo_mimetype;
					array_push($photos, $photo_obj);

					// append all data for this instructor to the array
					$instructor_object = (object)[];
					$instructor_object->first_name = $first_name;
					$instructor_object->last_name = $last_name;
					$instructor_object->email = $email;
					$instructor_object->affiliation = $affiliation;
					$instructor_object->homepage = $homepage;
					$instructor_object->short_cv = $short_cv;

					$instructor_object->billing_name = $billing_name;
					$instructor_object->billing_institution = $billing_institution;
					$instructor_object->billing_department = $billing_department;
					$instructor_object->billing_street = $billing_street;
					$instructor_object->billing_no = $billing_no;
					$instructor_object->billing_zip = $billing_zip;
					$instructor_object->billing_city = $billing_city;
					$instructor_object->billing_country = $billing_country;

					$instructor_object->room = 'single';
					$instructor_object->room_no = 'room number not yet given';
					$instructor_object->roommates = '';
					$instructor_object->room_requests = '';
					$instructor_object->diet = $diet;
					$instructor_object->diet_other_text = $diet_other_text;
					$instructor_object->consent_participant_list = 'Yes';
					$instructor_object->availability = $availability;
					$instructor_object->registration_date = date("Y-m-d H:i:s");

					array_push($instructors, $instructor_object);
				}

				// generate an object representing the course
				$course_object = (object)[];
				$course_object->identifier = "TODO: ADD COURSE IDENTIFIER";
				$course_object->title = $title;
				$course_object->fields = $fields;
				$course_object->url = "TODO: ADD CORRECT URL";
				$course_object->type = "TODO: ADD CORRECT COURSE TYPE";
				$course_object->instructor = $instructor_str;
				$course_object->description = $abstract;

				// preprocessing ends here

				// construct an email with registration data for the
				// registration team
				$registration_mail = "The lecturer(s) for the course $title just registered. Please find the data below.\n\n<pre>" . json_encode($instructors, JSON_PRETTY_PRINT) . "</pre>";

				// construct HTML email for the web team
				$description_email  = "<h2>$title</h2>\n";
				$description_email .= "<p><strong>Lecturer:</strong> $instructor_str<br/>\n";
				$description_email .= "<strong>Fields:</strong> $fields</p>\n\n";

				$description_email .= "<h3>Content</h3>\n\n<p>";
				$description_email .= preg_replace(array('/\s*\n\s*\n\s*/', '/\s*\n\s*/'), array("</p><p>", "<br/>"), $abstract);
				$description_email .= "</p>\n\n";

				if($literature !== '') {
					$description_email .= "<h3>Literature</h3>\n\n<ul><li>";
					$description_email .= preg_replace('/\s*\n\s*/', "</li><li>", preg_replace('/\*/', '', $literature));
					$description_email .= "</li></ul>\n\n";
				}

				$description_email .= "<h3 id=\"lecturer\">Lecturer</h3>\n\n";

				// process lecturer array
				for($i = 0; $i < count($instructors); $i++) {
					$instructor = $instructors[$i];
					if($photos[$i]->data) {
						$description_email .= "<div class=\"wp-block-image\"><figure class=\"alignright is-resized\">";
						$description_email .= '<img src="data:' . $photos[$i]->mimetype . ';base64,' . $photos[$i]->data . '" alt="' . $instructor->preferred_name . '" sizes="(max-width:300px) 100vw 300px"/>';
						$description_email .= "</figure></div>";
					} else {
						$description_email .= "<p>No image provided.</p>\n";
					}
					$description_email .= '<p>' . $instructor->short_cv . '</p>';
					$description_email .= "\n\n<p><strong>Affiliation:</strong> " . $instructor->affiliation;
					if($instructor->homepage !== '') {
						$description_email .= "<br/>\n<strong>Homepage:</strong> <a href=\"" . $instructor->homepage . '" target="_blank">' . $instructor->homepage . '</a>';
					}
					$description_email .= "</p>\n\n";
				}

				$description_email .= "<h2>Organisational Information:</h2>";

				$description_email .= "<p><strong>Length:</strong> $length\n";
				$description_email .= "<p><strong>Available on days:</strong> $availability\n";

				$description_email .= "</p>\n\n<h4>Special requests</h4>\n\n<p>";
				$description_email .= preg_replace(array('/\s*\n\s*\n\s*/', '/\s*\n\s*/'), array("</p><p>", "<br/>"), $special_requests);
				$description_email .= "</p>\n\n";
				$description_email .= "<p>JSON data:</p><pre>\n" . json_encode($course_object, JSON_PRETTY_PRINT) . "</pre>";

				// special code for debug input
				if($title === 'Debug') {
					echo "<p id=\"success\">The debug is complete. The following email would be send to <a href=\"mailto:lecture-org@interdisciplinary-college.org\">lecture-org@interdisciplinary-college.org</a>:</p>$description_email";
					echo "<p id=\"success\">The following email would be send to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>:</p>$registration_mail";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				// NOTE This must be changed every year
				$conf_identifier = 'IK25';

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the course data
				 */
				if(!wp_mail('lecture-org@interdisciplinary-college.org', $conf_identifier . ' Course Description', $description_email, $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your course description has failed. Please send your course description manually to <a href=\"mailto:lecture-org@interdisciplinary-college.org\">lecture-org@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p>$description_email";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p id=\"success\">Your course description was sent successfully to <a href=\"mailto:lecture-org@interdisciplinary-college.org\">lecture-org@interdisciplinary-college.org</a>.</p>";
				}

				// and send the registration data
				if(!wp_mail('registration@interdisciplinary-college.org',  $conf_identifier . ' Lecturer Registration', $registration_mail, $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your lecturer registration has failed. Please send your lecturer data manually to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p>$registration_mail";
					exit;
				} else {
					// no success message required
				}

				// send a backup to the webmaster registration data
				wp_mail('webmaster@interdisciplinary-college.org',  $conf_identifier . ' Lecturer Registration (copy)', $registration_mail, $headers);
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
