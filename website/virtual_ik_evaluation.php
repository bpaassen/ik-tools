<?php
/* Template Name: Virtual IK 2021 Evaluation Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, November 2020 -->
			<?php

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// define all keys in the form
				$keys   = array('slides', 'videos', 'streams', 'lecture_discord', 'poster_session', 'poster_discord', 'hacks', 'welcome', 'fireplace', 'pub');
				// define the valid values in an array
				$values = array('very much', 'somewhat', 'not really', 'not at all', 'not used', 'unaware', 'na');

				// validate and sanitize form data
				foreach ($keys as &$key) {
					if(!isset($_POST[$key])) {
						echo "Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the response to the $key question was not given. Please check your form again and re-submit.";
						exit;
					}
					if (!in_array(sanitize_text_field($_POST[$key]), $values, true)) {
						echo "Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the response to the $key question had an invalid value (must be Yes, Maybe, No, or NA). Please check your form again and re-submit.";
					}
				}

				// check the background question
				$background = sanitize_text_field( $_POST['background'] );
				if(!($background === 'undergrad' || $background === 'grad' || $background === 'phd' || $background === 'postdoc' || $background === 'prof' || $background === 'industry' || $background === 'other' || $background === 'na')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the background question had an invalid value (must be either undergrad, grad, phd, postdoc, prof, industry, other, or na). Please check your form again and re-submit.';
					exit;
				}

				// sanitize the free text fields
				$like_free_text     = sanitize_text_field( $_POST['like_free_text'] );
				$improve_free_text  = sanitize_text_field( $_POST['improve_free_text'] );
				$comments_free_text = sanitize_text_field( $_POST['comments_free_text'] );

				// generate a human-readable version:
				$mail_text = "<p>At the virtual IK 2020, which of the following offers did you use and how much did you like them?</p>";
				foreach ($keys as &$key) {
					$value = sanitize_text_field( $_POST[$key] );
					$mail_text .= "$key: $value<br/>";
				}
				$mail_text .= "<p>Background: $background</p>";
				$mail_text .= "<p>What I liked best about the virtual IK 2020:<br/> $like_free_text</p>";
				$mail_text .= "<p>What I would have done different about the virtual IK 2020:<br/>$improve_free_text</p>";
				$mail_text .= "<p>Further comments:<br/>$comments_free_text</p>";

				// transform the responses into a JSON object
				$eval_object = (object)[];
				foreach ($keys as &$key) {
					$eval_object->$key = sanitize_text_field( $_POST[$key] );
				}
				$eval_object->background = $background;
				$eval_object->like_free_text = $like_free_text;
				$eval_object->improve_free_text = $improve_free_text;
				$eval_object->comments_free_text = $comments_free_text;
				$json_data = json_encode($eval_object);

				// special code for debug input
				if($like_free_text === 'Debug') {
					echo "<p>The debug was successful.</p>";
					echo "<p>Human-readable version:</p>$mail_text";
					echo "<p>JSON data</p><pre>$json_data</pre>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the evaluation data
				 */
				if(!wp_mail('virtual@interdisciplinary-college.org', 'Virtual IK 2020 Evaluation', $mail_text . '<pre>' . $json_data . '</pre>', $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your evaluation has failed. Please contact us at <a href=\"mailto:virtual@interdisciplinary-college.org\">virtual@interdisciplinary-college.org</a>.</p>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p>Your evaluation was sent successfully to the conference management. Thank you very much!</p>";
				}

				// send human readable copy to Christine
				wp_mail('christine.harms@ccha.de', 'Virtual IK 2020 Evaluation', $mail_text, $headers);

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
