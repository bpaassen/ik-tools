<script id="courses_metadata" type="text/javascript">

var start_date = "2024-03-01";
var end_date = "2024-03-08";
var dinner_date = "2024-03-06";
var motto = "Resilience, Robustness, Responsibility";

const courses = [
{
    "identifier": "ET1",
    "title": "Who wants to live forever?",
    "fields": "Digital Afterlife\/AI",
    "url": "https://interdisciplinary-college.org/2024-et1/",
    "type": "Evening Talk",
    "instructor": "Maggi Savin-Baden",
    "description": "The famous song by Queen, was written by Brian May for the 1986 film Highlander, the song is used to frame the scenes in the film where Connor MacLeod must endure his beloved wife Heather MacLeod growing old and dying while he, as an Immortal, remains forever young.  Who wants to live forever as a song and as a religious and philosophical question introduces uncomfortable queries about the value and purpose of this life. \r\n\r\nThe evening talk suggests that Digital afterlife has moved beyond digital memorialisation towards a desire to preserve oneself after death. Preserving oneself or being preserved by someone else may affect both the dying person\u2019s peace of mind and the well-being of the bereaved. Yet it is not clear whether the possibility of digital immortality and the use of digital media alters thoughts about the mind-body connection, and whether interaction with a digital immortal alters one\u2019s spiritual journey through grief. Afterlife and resurrection remain troublesome because they are couched in mystery and philosophical and theological discourse cannot explain resurrection of the body, because the human body itself is not reducible to simple description or ready comprehension."
},
{
    "identifier": "ET2",
    "title": "The End of Capitalism",
    "fields": "Economics/Politics",
    "url": "https://interdisciplinary-college.org/2024-et2/",
    "type": "Evening Talk",
    "instructor": "Ulrike Herrmann",
    "description": "Humankind is ruining the planet, and the climate crisis is especially threatening. Hence, politics and economics hope for “green growth”. But this is an illusion. Green energy from solar panels and wind turbines will be insufficient to support permanent growth. The economy must shrink. Yet, shrinking would be the end of capitalism, because it is only stable as long as there is growth. Economic journalist Ulrike Herrmann describes what the future looks like – without growth, profit, cars, airplanes, banks, insurance companies and almost without meat."
},
{
    "identifier": "ET3",
    "title": "Artificial Consciousness? You must be joking Mr. Steels",
    "fields": "Artificial Intelligence, Consciousness",
    "url": "https://interdisciplinary-college.org/2024-et3/",
    "type": "Evening Talk",
    "instructor": "Luc Steels",
    "description": "In 2021, Blake Lemoine, a software engineer at Google, and responsible for testing a chatbot LaMDA (Language Model for Dialog Applications) came to the conclusion that LaMDA was sentient, in the sense of being conscious. Since then many other users of generative AI systems, in particular ChatGPT, have reported similar experiences and the topic of machine consciousness suddenly became salonfaehig, with neuroscientists and philosophers pitching in to define requirements for machine consciousness and whether or not this might ever be possible – although mostly concluding that current AI systems do not qualify as conscious agents. What are we to make of all this? This talk intends to bring this discussion to IK, a forum exceptionally well adapted to have groundbreaking multi-disciplinary open discussions. I will argue for a vision of the mind as a dense network of complex adaptive networks, operating at different layers, from living embodiment to sentience, cognition, sapience and consciousness. The networks are autonomous and autopoietic. In other words, they develop on their own account and remain in a constant state of becoming, grounded in embodied action on the one hand and cooperation and competition with other agents on the other. We will then discuss consciousness from the point of view of this framework, reflecting on measures of awareness, the role of language, what consciousness might be for, non-ordinary states of consciousness, moral consciousness, a.o.  In this vision there is no single I, no physical conscious substance a la Penrose or Faggin, no intelligence without grounding or social interaction, no understanding without meaning. The goal of this talk is not to announce a new gospel but to stimulate the discussion and, in particular, become more disciplined and careful in attributing consciousness or other mental qualities to machines."
},
{
    "identifier": "BC1",
    "title": "Introduction to Machine Learning",
    "fields": "Machine Learning",
    "url": "https://interdisciplinary-college.org/2024-bc1",
    "type": "Basic Course",
    "instructor": "Benjamin Paa\u00dfen",
    "description": "Machine learning is concerned with automatically learning models (patterns, regularities, correlations) from known data which generalize to new data. To do so, it combines concepts from mathematics (esp. statistics, probability theory, linear algebra, and optimization), artificial intelligence, and computer science. This course will provide an introduction to machine learning for the un-initiated. While some math will be necessary, everything will be accompanied by pictures and examples to get the core intuition across \ud83d\ude42\r\n\r\nIn more detail, the course will have four sessions with the following topics:\r\n\r\n* Session 1: Basic Concepts: What is Machine learning and how does it relate to Artificial Intelligence? What are types of ML? What does \\'learning\\' mean in ML? We will also discuss the basic ingredients of an ML algorithm (loss function, model class, and optimization strategy), linear regression as an example for such an algorithm, underfitting, overfitting (and how to prevent it), how probabilities help us to make precise what \\'generalization means\\', and how to design a basic ML experiment.\r\n* Session 2: Classic machine learning tasks and methods to solve them: The distance perspective on ML, Regression, Classification, Dimensionality Reduction, Clustering, with respective methods for each task; and decision trees\/forests\r\n* Session 3: Artificial neural networks and deep learning: How to build artificial neural networks from single neurons to present-day transformers\r\n* Session 4: Reinforcement learning and ethics\r\n\r\nEach session is accompanied by a (voluntary) programming exercise in Python. Exercise sheets (and slides) can be found here: https:\/\/bpaassen.gitlab.io\/Teaching.html"
},
{
    "identifier": "BC2",
    "title": "Basic Course Neuroscience",
    "fields": "Neuroscience",
    "url": "https://interdisciplinary-college.org/2024-bc2/",
    "type": "Basic Course",
    "instructor": "Till Bockem\u00fchl",
    "description": "The brain, the cause of \u2013 and solution to \u2013 all of life\\'s problems. According to our brains it is the most fascinating structure in the known universe. Consisting of about 86 billion neurons where each can form thousands of connections to other neurons it is also the most complex structure in the known universe. In this course we would like to give you a rough guide and introduction to the basic principles, fundamental theories, and methods of neuroscience.\r\nWe will demonstrate that neuroscience can be seen as a multi-modal, multi-level, multi-disciplinary research framework that aims at addressing the challenges of this megalomaniac scientific endeavor. We will see that different frameworks and methods can lead to conflicting empirical evidence, theoretical assumptions, and heated debates. However, we argue that this might be the only way to uncover the mysteries of our brain.\r\nIn this course we will cover a variety of scopes and perspectives. We will teach some of the fundamentals of neuroscience in human and non-human animals, but we will also explore some explanatory gaps between the different levels of inference.\r\nOn a phenomenal level we will investigate the functions of individual neurons and small networks. We will discuss if and how we can learn from (genetically modified) model animals about neural functions. To what degree is this relevant for understanding human brain function, such as learning and decision making? On the other hand, we will also investigate the state of the art in human brain mapping and cognitive neuroscience. Can findings from neuroimaging tell us anything at all about neurobiology \u2013 or are they just fancy illustrations that are better suited for children\\'s books?\r\n\r\n\r\nObjectives: \r\n-\tTo understand the anatomy and function of neurons\r\n-\tTo understand the interaction of neurons in a functional network\r\n-\tTo understand central methods and theories used in neurobiology and human cognitive neuroscience\r\n-\tTo understand the scope of different methods and theoretical frameworks"
},
{
    "identifier": "BC4",
    "title": "Responsible AI Development: Theory and Practice(s)",
    "fields": "Artificial Intelligence, Machine Learning, AI Governance",
    "url" : "https://interdisciplinary-college.org/2024-bc4/",
    "type": "Basic Course",
"instructor": "Tarek R. Besold",
    "description": "In this course we will (try to) assess the current state of play as regards responsible AI development from both a theoretical\/principled (i.e., what are -- or can be -- the foundations of responsible AI systems and development practices?) and an applied (i.e., which part(s) of the theory carry over into practice and what are the challenges\/breaking points?) perspective.\r\n\r\nWe will have a look at (some of) the conceptual underpinnings of responsible tech development, at the particularities which AI as a domain adds to those, and at proposals for how to bring the chosen values and guidelines into application. We will also have a look at the pressures AI developers face in practice, what these mean for the implementation of responsible development approaches, and if there are (currently) hard-to-overcome breaking points between theory and application. Finally, we will map some of the governance and regulatory infrastructure and processes governments are now working to put into place in order to guarantee adherence to a minimal standard of good practices in the development of AI systems."
},
{
    "identifier": "MC1",
    "title": "The Free Energy Principle: modeling data, modeling the brain, modeling the mind?",
    "fields": "Cognitive Modeling, Data Modeling",
    "url": "https://interdisciplinary-college.org/2024-mc1/",
    "type": "Method Course",
    "instructor": "Ronald Sladky",
    "description": "Originally, the idea of free energy minimization has been used as a tool for data modeling, in particular, to model effective connectivity in the brain based on neuroimaging data. On top of this, Karl Friston has proposed the free energy principle as a general principle for understanding brain functions. A bold statement – but, if true, one of the biggest breakthroughs in cognitive science. In the form of active inference, the free energy principle could provide a neuro-computational explanation for predictive processing, the Bayesian brain hypothesis, and enactive cognition. It could provide a unified conceptual and computational framework to link previously distant and isolated research fields in the cognitive sciences that could be compatible with how we understand self-organization of living systems in a physical world. In this course we will talk about how we make sense of data using models. By looking at how cognitive neuroscientists study the human brain using fMRI and brain connectivity methods. We discuss how models shape the way we interpret the world. I will teach you how these models work so you will find out on your own what to do with them. To what degree is the free energy principle useful for you? Modeling data, modeling the brain – or modeling the mind? Session 1 covers how we turn fMRI data into brain activation and connectivity models. Session 2 focusses on dynamic causal modeling to study effective connectivity in the brain. Session 3 extends the formalism used in DCM to describe brain functions and life as we know it. Session 4 will review state of the art applications, theoretical developments, and empirical evidence for the free energy principle and active inference in action. As an illustration, I will use my own research on amygdala functions and dysfunctions and its connectivity. So, we will also talk about fear, trust, and other emotions – not just data, brains, and methods."
},
{
    "identifier": "MC2",
    "title": "Knowledge Graphs for Hybrid Intelligence",
    "fields": "Artificial Intelligence",
    "url": "https://interdisciplinary-college.org/2024-mc2/",
    "type": "Method Course",
    "instructor": "Ilaria Tiddi",
    "description": "Hybrid Intelligence (HI) is a rapidly growing field aiming at creating collaborative systems where humans and intelligent machines cooperate in mixed teams towards shared goals. In this course, we will get to know the field by discussing the vision, and basics, and the solutions that have been proposed so far. In particular, we will focus how symbolic AI techniques (knowledge graphs and semantic technologies) have been proposed as complementary building blocks to the subsymbolic (machine learning) methods, and how this combination has been used to help solving the main challenges in the field of Hybrid Intelligence.\r\n\r\nThe course comprehends 3 lectures:\r\n1) Introduction to Hybrid Intelligence (1.5h). Here we will introduce the main research questions for the field of HI, present some  of the solutions proposed so far, and finally discuss open challenges.  \r\n\r\n2) Introduction to Knowledge Graphs (1.5h). Here we will introduce the basics of Knowledge Engineering , including modelling information and reason about it using the RDF\/RDFS\/OWL languages, principles of knowledge\/ontology engineering, and methods to query knowledge graphs.     \r\n\r\n3) Knowledge Engineering for Hybrid Intelligence (1.5h). The last lecture will introduce how ontologies and knowledge engineering methods can be used to design Hybrid Intelligence applications.  \r\n\r\nLearning objectives:\r\n- familiarise with Hybrid Intelligence challenges and methods\r\n- get to know the basics of Knowledge Graphs (RDF, OWL, SPARQL, ML over graphs)\r\n- apply the principles of knowledge engineering for the design for Hybrid Intelligence applications"
},
{
    "identifier": "MC3",
    "title": "Attractive Magnets: Robust combination of TMS and fMRI",
    "fields": "Cognitive Neuroscience, Clinical Neuroscience, Neuroimaging",
    "url": "https://interdisciplinary-college.org/2024-mc3",
    "type": "Method Course",
    "instructor": "Martin Tik, Anna-Lisa Schuler",
    "description": "In this course we will discuss basics, applications and hot topics for two of the most popular magnets in brain research: TMS and MRI. While fMRI allows for the depiction of neural underpinning underlying task processing, TMS as a neuromodulation technique allows for the targeted manipulation of these processes. This course will be dedicated to give an overview about both of the techniques and the advantages of their combination.\r\n\r\nSession 1: In session one we will give an overview about the history of magnets in cognitive neuroscience and the evolution of the methods of interest (TMS, fMRI). We will furthermore explain the technical basics and the composition of these devices.\r\n\r\nSession 2: In session two we will discuss the physiological mechanisms of action underlying the techniques including blood oxygenation and neuronal action potentials. Then we will give an overview about different applications of TMS and fMRI including exemplary research.\r\n\r\nSession 3: In this session we will discuss specific applications of combining TMS with fMRI in cognitive neuroscience and clinical medicine. In the second part of this session, participants will have the opportunity to plan their hypothetical own TMS and fMRI experiments in small groups.\r\n\r\nSession 4: Participants will discuss their projects in a plenum. Finally, there will be a summary, question round and wrap up of the course.\r\n\r\nLearning goals:\r\n\r\n-\tBasic principles of TMS and fMRI\r\n-\tApplications of TMS and fMRI\r\n-\tDirect transfer of these contents to own research"
},
{
    "identifier": "MC4",
    "title": "Dynamical Systems: a Navigation Guide",
    "fields": "All IK disciplines",
    "url": "https://interdisciplinary-college.org/2024-mc4",
    "type": "Method Course",
    "instructor": "Herbert Jaeger",
    "description": "This is a crash course (4 sessions at 90 minutes each) on dynamical systems, held at the Interdisciplinary College (https:\/\/interdisciplinary-college.org) several times. The presentation is meant to be understandable for a general natural \/ neural \/ cognitive science audience. The slides from the 2023 presentation can be retrieved from https:\/\/www.ai.rug.nl\/minds\/uploads\/DynamicalSystemsPrimer_Jaeger.pdf."
},
{
    "identifier": "SC1",
    "title": "Resilience in Outdoor Internet of Things",
    "fields": "Computer Science",
    "type": "Special Course",
    "instructor": "Anna Förster",
    "url": "https://interdisciplinary-college.org/2024-sc1/",
    "special_category": "Environment and Climate",
    "description": "This course will offer an overview of the problems and challenges associated with outdoor deployments of internet of things (IoT) applications. After a short introduction to the field of IoT and the discussion of various outdoor applications, we will dive deeper into the threats IoT applications face in these environments. We will showcase some concrete threats and discuss possible solutions and approaches."
},
{
    "identifier": "SC2",
    "title": "Evolution in a complex world",
    "fields": "Ecology & Evolution, Behavioural Biology",
    "type": "Special Course",
    "url": "https://interdisciplinary-college.org/2024-sc2/",
    "instructor": "Franjo Weissing",
    "special_category": "Environment and Climate",
    "description": "Biological organisms have to cope with ever-changing environmental conditions. They have been \\'designed\\' for this task in a long evolutionary history, but how evolution by natural selection has achieved this is far from clear. Two properties are crucial for long-term survival in a changing world: \\'robustness\\' (the ability to build the same phenotype under very different conditions) and \\'evolvability\\' (the ability to rapidly respond to changing conditions by adaptive evolution). The conundrum is that these properties seem to be contradictory: doesn\\'t a robust design impede evolvability, and doesn\\'t evolvability require a flexible design? A second problem is that \\'evolutionary design\\' is fundamentally different from the \u2018engineered design\u2019. While an engineer has foresight, adaptive evolution resembles a \\'blind watchmaker\\' (Dawkins 1986) in that it is driven by short-term selection pressures. We all know that following short-term incentives often has negative implications in the longer term. How, then, can long-term properties like robustness and evolvability be shaped by a myopic process like natural selection?\r\nQuestions like these will be addressed in four sessions. The first two sessions will illustrate the dynamic complexity of apparently simple ecological and evolutionary systems. We will see that such systems can be \\'fundamentally unpredictable\\' and that adaptive evolution can, in principle, drive a population to extinction (\\'evolutionary suicide\\'). In the last two sessions, we will sketch a new way of evolutionary thinking that may (partly) resolve issues like these. We will see that the evolution of \\'responsive strategies\\' (strategies that respond to the local environmental conditions) is fundamentally different from the evolution of non-responsive strategies. The reciprocal causality inherent to these strategies speeds up evolution by orders of magnitude and leads to quite different evolutionary outcomes. In biological organisms, responsive strategies are often implemented via regulatory networks (e.g., gene regulation networks or neural networks). It turns out that the evolution of such networks shares various properties with learning (by machines or intelligent agents)."
},
{
    "identifier": "SC3",
    "title": "Resilience thinking for social-ecological systems",
    "fields": "Biology",
    "type": "Special Course",
    "instructor": "Romina Martin",
    "url": "https://interdisciplinary-college.org/2024-sc3/",
    "special_category": "Environment and Climate",
    "description": "Wicked problems and the polycrises of the Anthropocene are challenging contexts for laying out a perspective for resilient and equitable human well-being within the planetary boundaries. \\\"Humans are embedded in the biosphere\\\" is the underlying assumption for sustainability scientists working with resilience of social-ecological systems. This means that humans shape, for example, lakes, agricultural landscapes, forests and oceans which they in turn depend on. The emerging system dynamics are often non-linear and further influenced by shocks. How could and should this system persist, adapt or transform in order to continue providing life support functions?  \r\nThis course will introduce resilience principles, complex adaptive systems and demonstrate how simulation models together with complementary methods enable research on regime shifts, poverty traps and common pool resource problems. We will use case study examples to co-devolop conceptual models on paper in a participatory process. To reflect, we discuss the inter- and transdisciplinary challenges for using model simulations in resilience thinking."
},
{
    "identifier": "SC4",
    "title": "Systems thinking in ecology",
    "fields": "Biology",
    "type": "Special Course",
    "instructor": "Ferenc Jordan",
    "url": "https://interdisciplinary-college.org/2024-sc4/",
    "special_category": "Environment and Climate",
    "description": "Dr. Jordan will overview how network models help in describing various ecological systems (animal social networks, food webs, habitat networks). We will focus on key nodes and critically important links, and discuss how to connect vertically the above-mentioned, horizontal organisation levels."
},
{
    "identifier": "SC5",
    "title": "Social Epigenetics and Social Evolution",
    "fields": "Sociobiology, Genomics and Epigenetics of adpative traits",
    "type": "Special Course",
    "instructor": "Jürgen Gadau",
    "url": "https://interdisciplinary-college.org/2024-sc5/",
    "special_category": "Health and Well-Being",
    "description": "I plan to give an overview on social evolution in animals and humans from an evolutionary and genomic\/epigenetic point of view. I plan three sessions, the first will give a general overview on genotype\/epigenotyp - phenotype mapping, the second will highlight and introduce epigenetic regulations and inheritance and finally I will discus the similarities and differences between social insects and humans, i.e. understand the key processes and mechanisms that sustain cooperation in these hypersocial organisms. 1 From Genotype to social Phenotypes. 2 The role of epigenetics and phenotypic plasticity for individualisation and social evolution. 3 Humans the other animals."
},
{
    "identifier": "SC6",
    "title": "Robust and Responsible Neural Technology",
    "fields": "Health and Wellbeing, Engineering, Material Sciences, Neuroscience, Technology Assessment",
    "type": "Special Course",
    "instructor": "Thomas Stieglitz",
    "url": "https://interdisciplinary-college.org/2024-sc6/",
    "special_category": "Health and Well-Being",
    "description": "Miniaturized neural implants cover the wet interface between electronic and biological circuits and systems. They need to establish stable and reliable functional interfaces to the target structure in chronic application in neuroscientific experiments but especially in clinical applications in humans. Proper selection of substrate, insulation and electrode materials is of utmost importance to bring the interface in close contact with the neural target structures, minimize foreign body reaction after implantation and maintain functionality over the complete implantation period. Silicon and polymer substrates with integrated thin-film metallization as core of stiff and flexible neural interfaces have been established as well as silicone rubber substrates with metal sheets. Micromachining and laser structuring are the main technologies for electrode array manufacturing. Different design and development aspects from the first idea to first-in-human studies are presented and challenges in translational research are discussed. Reliability data from long-term ageing studies and chronic experiments show the applicability of thin-film implants for stimulation and recording and ceramic packages for electronics protection. Examples of sensory feedback after amputation trauma, vagal nerve stimulation to treat hypertension and chronic recordings from the brain display opportunities and challenges of these miniaturized implants. System assembly and interfacing microsystems to robust cables and connectors still is a major challenge in translational research and transition of research results into medical products. Clinical translation raises questions and concerns when applications go beyond treatment of serious medical conditions or rehabilitation purposes towards life-style applications. The four sessions within the topic of robust and responsible neural technology will cover (1) neuroscientific and clinical applications of neural technology, (2) fundamentals on optogenetics, recording of bioelectricity and electrical stimulation, (3) the challenges of neural implant longevity and (4) ethical and societal considerations in neural technology use."
},
{
    "identifier": "SC7",
    "title": "Changing bodies, changing minds",
    "fields": "Cognitive Neuroscience, Experimental psychology",
    "type": "Special Course",
    "instructor": "Andreas Kalckert",
    "special_category": "Health and Well-Being",
    "url": "https://interdisciplinary-college.org/2024-sc7/",
    "description": "Embodied approaches have inspired and yielded new perspectives in a variety of research disciplines. These approaches have emphasized the role of the body in cognitive functions, not only for interacting with the world, but also for perceptual experiences. Psychological experiments using bodily illusion have taken this notion a step further and have provided evidence that even temporary experiences of embodiment can directly alter perception, action, and cognition. Consequently, these tools create new ways to change individuals in unprecedented ways. In this course, I will provide an introduction into the perceptual and neuronal processes underlying bodily illusions. I will then illustrate how such illusions have demonstrated changes in the experience and attitudes in both healthy and patient populations, and discuss the potential of using these paradigms for therapeutic interventions. We will also touch on some ethical dimensions within this research that raise questions over its use in the future."
},
{
    "identifier": "SC8",
    "title": "Immunity and Information",
    "fields": "Immunology, Artificial Intelligence, Machine Learning",
    "type": "Special Course",
    "url" : "https://interdisciplinary-college.org/2024-sc8/",
    "instructor": "Johannes Textor",
    "special_category": "Health and Well-Being",
    "description": "Our body harbours two complex learning systems: the central nervous system (CNS), and the adaptive immune system (IS). Artificial neural network models of the CNS have contributed substantial insight to neuroscience and CNS-inspired \"deep learning\" has revolutionized artificial intelligence. In contrast, how the IS processes information is still much less understood. This course will therefore ask: how can we understand information processing, learning and adaptation in the IS through the lens of computer science? We will dive deep into several fascinating processes in the adaptive immune system such as negative selection, self-foreign discrimination, tolerance, and affinity maturation. I will cover the necessary immunological background to come to an understand of what we do and don't know about these processes, and discuss how computational and mathematical models have been instrumental in our quest to understand the immune system from a computational perspective. I hope you will leave this course as fascinated and inspired by the marvelous architecture of our immune system as I am, and that this inspiration will transform and broaden your view of fundamental concepts like learning, adaptation, and generalization."
},
{
    "identifier": "SC9",
    "title": "Looking for Function in Social Systems and Sometimes Finding It: Ants, Humans, and Beyond",
    "fields": "Behavioral Ecology, Game Theory, Modeling, Natural History, Collective Behavior, Ants, Bees, Wasps, Social Insects, Social Behavior",
    "url": "https://interdisciplinary-college.org/2024-sc9/",
    "type": "Special Course",
    "special_category": "The Individual in Society",
    "instructor": "Theodore Pavlic",
    "description": "In this 4-part course, we delve into the fascinating world of social systems, drawing parallels between the intricacies of social-insect colonies and the complexities of human societies. Through the lens of resilience, robustness, and responsibility, we explore the dual nature of collective behavior, examining how it can both empower and challenge the adaptability of societies.\r\n\r\nWe begin by unraveling the mechanisms that enable social-insect colonies (with a particular focus on ants and social bees) to flexibly respond to environmental changes, while also scrutinizing the vulnerabilities that arise from individuals valuing public information too much over private information. We will also show motivational examples of natural collective decision-making systems in ant colonies that have qualitatively different cognitive capabilities at the level of the collective than the individual, effectively reducing the burden of individual responsibility in a functioning society. \r\n\r\nWe then continue to highlight strengths, weaknesses, opportunities, and threats in social systems by reviewing fundamental results from game theory. We start with a review of the Prisoner\\'s Dilemma and ask whether it has much practical value. We then transition to a wider range of social games, such as the Hawk\u2013Dove and the Stag Hunt, that highlight how living in societies is challenged not only by alignment of agendas but also coordination of collective action when there is limited information. This sets us up to talk about N-person games and equilibrium concepts that apply to larger social groups, with examples from social foraging to make this more concrete. Empowered with these game-theoretic fundamentals, we can then discuss the problem of altruism in societies and pivot to discussing alternative explanations for altruism based less on social relatedness (responsibility) and more on risk management (resilience and robustness). \r\n\r\nIn an attempt to close on an optimistic note, we conclude this course with examples of the various benefits of highly integrated social life viewed through the lenses of resilience, robustness, and responsibility. We uncover profound benefits of colony life in many social insects and discuss the mechanisms that underly these adaptations. \r\n\r\nUltimately, this course provides an opportunity to navigate through the complexities of social organization, shedding light on the fundamental principles that underpin the resilience, robustness, and responsibility of societies across the biological spectrum to highlight both how societies can protect from disturbances from the outside while also introducing new risks to manage that come from within."
},
{
    "identifier": "SC10",
    "title": "Multisystemic Approaches to Individual and Collective Resilience",
    "fields": "Social Science; Psychology; Health Science; Community Development; Cultural Studies",
    "url": "https://interdisciplinary-college.org/2024-sc10/",
    "type": "Special Course",
    "instructor": "Michael Ungar",
    "special_category": "The Individual in Society",
    "description": "In this short course, Dr. Michael Ungar will explore the many different systems that contribute to experiences of individual and collective resilience, as well as the methods used to research multisystemic resilience. The intent is to integrate perspectives from studies of biological, psychological, social, institutional, and economic resilience, as well as those concerned with the built and natural environments. The course will also focus on how to research resilience in participatory ways to develop knowledge that informs policy and practice. An introduction to the theory of resilience will be followed by an overview of its application to populations under stress, as well as the tools used to assess resilience at individual and community levels. Using examples from studies conducted by Dr. Ungar and his colleagues at the Resilience Research Centre, students will have an opportunity to reflect on how multiple systems influence one another over time and in culturally nuanced ways. Discussion will include topics such as contextualization of the resilience concept, measure development to account for positive developmental processes, and the many aspects of resilience that need to be considered in designing research and developing programs and policies to improve the capacity of populations to cope with atypical stressors. Participants are encouraged to bring questions relating to their own research topics whether from the natural, biological, or social sciences."
},
{
    "identifier": "SC11",
    "title": "Artificial Social Intelligence for Robust and Efficient Human-Agent Interaction",
    "fields": "Artificial Intelligence, Cognitive Science, Human-Agent/Robot Interaction",
    "type": "Special Course",
    "instructor": "Stefan Kopp",
    "url": "https://interdisciplinary-college.org/2024-sc11/",
    "special_category": "The Individual in Society",
    "description": "With A.I. systems becoming more capable and being widely deployed in our everyday life and work environments, the question how human users can and should interact with intelligent systems becomes pivotal. The AAAI 20-year research roadmap (2019) pointed out that \\\"meaningful interaction\\\" between humans and AI should be a top research priority, encompassing questions like trust, transparency and responsibility, as well as online interaction, multiple interaction channels or collaboration. Going beyond \\\"promptification\\\", humans and AI systems should be able to engage in forms of interaction that encompass mixtures of instructions, question answering, explanations, argumentation, or negotiation -- all embedded in a flexible dialog, often multimodal and potentially even interwoven with physical action. In this course we will discuss how such advanced forms of interaction between AI systems and humans can be realized in an efficient and robust way. What they all have in common is that they hinge on the effect that systems with a certain degree of competence and autonomy are readily perceived and treated as having agency. Correspondingly, the interaction is framed as human-agent interaction (HAI), i.e. between two or more agentive entities, and applies to systems such as conversational personal assistants, socially assistive robots, conversational recommender systems, or autonomous vehicles.\r\n\r\nThe course will touch upon three questions: (1) What abilities do AI-based agents need to have in order to engage in robust and efficient HAI? (2) How can those abilities be modeled? (3) What are the possible effects on users?  In discussing these questions we will focus on socio-cognitive core abilities for recognizing, reasoning about, and engaging in interactions with other social agents -- what we call \\\"Artificial Social Intelligence\\\". We start by learning about the basics of human-agent interaction and the emerging state of the art in modeling Artificial Social Intelligence in AI-based agents. We will then look at how robust and efficient HAI can be achieved through multimodal communication. In communication, participants continuously adjust their behaviors based on their interlocutor\u2019s language, gestures, and facial expressions during social interaction. We will learn about modern approaches to create AI agents that can understand and participate in these dynamic, multimodal dialogue interactions. Finally, we will discuss computational approaches to realize \\\"theory of mind\\\" abilities such as recognizing or inferring another agent\\'s intentions, beliefs, emotions or other mental states. Such abilities are indispensable for robust and efficient cooperation and have moved into the focus of much recent research on AI-based agents. For each topic, we will draw connections between insights from Cognitive Science, Psychology or Linguistics, and the modeling approaches in AI by means of cognitive modeling, model-based machine learning, deep learning, or lately LLMs."
},
{
    "identifier": "SC12",
    "title": "Why Martial Arts are helpful to deal with the unforeseeable in everyday situations.",
    "fields": "Cognitive Psychology, Philosophy",
    "url": "https://interdisciplinary-college.org/2024-sc12/",
    "type": "Special Course",
    "instructor": "Thomas Christaller",
    "special_category": "The Individual in Society",
    "description": "Obviously, in competitive sports, fighting systems, and martial arts you encounter stressful situations in which you dont know if and how you can resolve them. Independently of the mastery of the techniques relevant for the specific body movement system the training always includes exercises which helps to overcome the stress in such situations and being able to act in an appropriate and meaningful way. The help to overcome fear as well as aggression. The basic insight is that with specific techniques with regard to breathing, relaxation, and posture you can foster your resilience when things dont happen as expected. This will be the core of the course. To understand why these techniques really work practically we will explore the neural, emotional basis for them. Our brain is mainly used as a forecasting system for the very next second as well as for hours, days, and weeks by rehearsing. But in stressful situations the brain doesnt have the resources to explore the alternatives making a plausible prediction. Another topic is to stay calm to reduce stress and avoid a narrowed view of the world. The main basis of this is breathing. Usually this is done unconsciously and changes according to your bodily effort and the expectation of the effort in the very next moment. Especially in the martial arts breathing is a core element to be able to deal with the unforeseeable. The usual reflex if one experiences an unexpected situation is to become bodily tense. But then you may not be able to act fast enough. Relaxation is the secret which is different from being weak. All these different systems are connected in our body posture. The main insight is, what you learn bodily for a possible physical fight or threat can be transmitted into non-physical conflicts like discussions, verbal assaults, or mobbing. Finally it may become your individual personality."
},
{
    "identifier": "SC13",
    "title": "Advancing Cognitive Systems: Leveraging Memristive Technologies in CMOS Circuit Design for Neuromorphic Edge Computing",
    "fields": "Emerging memory devices, Neuromorphic computing",
    "type": "Special Course",
    "instructor": "Erika Covi",
    "url": "https://interdisciplinary-college.org/2024-sc13/",
    "special_category": "AI and Beyond",
    "description": "In recent years, the cloud-based approach to data classification has been challenged by the edge computing paradigm, which has enabled real-time data processing at the network edge, ideally next to the sensor collecting data. This paradigm poses severe constraints on the systems in terms of power-efficiency, compactness, and latency [1, 2]. Therefore, we need to explore unconventional hardware solutions able to meet these stringent requirements.\r\nBrain-inspired architectures, particularly Spiking Neural Networks (SNNs), are promising candidates to achieve low-latency computation, and stateful, energy-efficient operations [3]. However, their current implementations primarily rely on digital or mixed-signal Complementary Metal-Oxide-Semiconductor (CMOS) technologies, which pose challenges in meeting the demanding memory, area, and power constraints of computing on the edge [1].\r\nIn this context, the integration of embedded memristive technologies holds a significant promise to enhance the capabilities of CMOS technology [2, 4] for the development of neuromorphic hardware [5]. Memristive devices are nanoscale devices able to change their conductivity upon application of proper electrical stimuli. They offer fast and energy-efficient tuneable volatile and non-volatile storage, and are therefore well-suited for storing SNN parameters. The exploitation of their unique properties, such as operation voltages compatible with current CMOS technology as well as analogue, neural-\/synaptic-like behaviour, offer an attractive opportunity for realizing energy-efficient and massively parallel computing architectures in conjunction with CMOS technology [3, 5]. Indeed, these features enable efficient computation, neural dynamics, and synaptic plasticity, which are essential traits for emulating the brain\\'s functionality in hardware [4, 6].\r\nIn this course, we explore the role of memristive devices in emulating the functionality of neural networks, enabling edge systems to classify and recognise patterns or process sensory inputs. We emphasize the need for co-developing memristive devices with CMOS circuits to enable seamless integration and to exploit the strengths of both technologies. Furthermore, we discuss how the co-development of memristor devices, CMOS circuits, and innovative learning algorithms can facilitate edge computing paradigms. Moreover, the intrinsic physical characteristics of memristive devices, if correctly exploited, enable analogue computing, thus offering a compelling alternative to traditional digital approaches. We also discuss the challenges and opportunities of developing memristive-CMOS hardware neuromorphic architectures. The co-design of devices, circuits, and algorithms indeed requires to identify and address issues related to device variability, scalability, and system integration.\r\nIn conclusion, the synergistic co-development of memristive devices, CMOS circuits, and innovative algorithms can pave the way for intelligent edge devices capable of performing complex cognitive tasks."
},
{
    "identifier": "SC14",
    "title": "Robust Language and its Responsible Generation",
    "fields": "Artificial Intelligence, Computational Linguistics, Language Models",
    "url": "https://interdisciplinary-college.org/2024-sc14/",
    "type": "Special Course",
    "instructor": "Philipp Wicke",
    "special_category": "AI and Beyond",
    "description": "Course Overview:\r\nMost of the thousands of languages in the world share some key properties that enable us to exchange information within our language communities, but also beyond them. At the same time, we experience a shift in artificial intelligence, which heavily relies on the development of large language models for certain languages, which exhibit emergent, general human-like properties such as reasoning and planning. This course has a bipartite structure that combines the theory of robustness in languages from the perspective of cognitive science with the practice of responsible applications of generative language models.\r\n\r\n* Session 1: Introduction to Language Robustness (2x 40min)\r\nIn this introductory session, students will gain a profound understanding of language universals and the fundamental role they play in human communication. We explore universal properties of language and linguistic relativity, examining how language shapes our perceptions and thoughts.\r\n\r\n* Session 2: Traits of Robust Language: Image Schemas and Embodiment (2x 40min)\r\nBuilding on the foundation laid in the previous session, we investigate the connection between language and conceptual understanding. Explore image schemas and embodiment as powerful mechanisms for robust concepts. Additionally, we introduce the concept of \\\"Robustness of Concepts\\\" and explore illustrative examples, including the fusion of language and emojis.\r\n\r\n* Session 3: Large Language Models and Image Generation (2x 40min)\r\nThis session opens up new horizons as we look at Large Language Models (LLMs). Learn about the possibilities and challenges of using these sophisticated models for various applications. We delve into multilingual studies involving Large Language Models, such as Glot500, and look at those concerned with embodiment and LLMs. We will also look at text-to-image generational models such as Midjourney, StableDiffusion or Dalle-2. Students will also gain insights into testing different models to identify their strengths and limitations.\r\n\r\n* Session 4: Responsible Language Generation (2x 40min)\r\nEthics takes center stage in this last session, as we focus on the responsible use of language generation technologies. We focus on the environmental impact of these models, considering factors such as emissions, energy consumption during training and inference, and e-waste generation and storage. Societal implications are also discussed, including bias in language models, data annotation through crowdsourcing, and the ethical challenges posed by deep fakes and fake news generation.\r\n\r\nBy the end of this course, students will have a comprehensive understanding of language universals, large language models, and ethical considerations."
},
{
    "identifier": "SC15",
    "title": "Understanding humans, advancing robotics: exploring the challenges and opportunities of human-robot interaction",
    "fields": "Artificial Intelligence, Human-Robot Interaction",
    "type": "Special Course",
    "url":"https://interdisciplinary-college.org/2024-sc15/",
    "instructor": "Giulia Belgiovine",
    "special_category": "AI and Beyond",
    "description": "The study of human-robot interaction is a vast and intricate field that adopts a multidisciplinary approach and encompasses numerous challenges. On the one hand, the quest to comprehend and model the intricate mechanisms underlying human cognitive and social abilities; on the other hand, the problem of how to replicate a comparable level of intelligence in cognitive interactive agents. Achieving this necessitates the integration of sensory and motor capabilities, along with memory, reasoning, and learning mechanisms, to develop artificial agents endowed with adaptation and generalization skills. \r\nIn these lectures, we will explore the interdisciplinary nature of this field, discussing the challenges and opportunities that lie ahead."
},
{
    "identifier": "PC1",
    "title": "Academic Writing",
    "fields": "Academic skills, Higher Education, Writing Research and Didactics",
    "type": "Practical Course",
    "instructor": "Brigitte Römmer-Nossek and Birgit Peterson",
    "url": "https://interdisciplinary-college.org/2024-pc1",
    "description": "Resilience, Robustness and Responsibility in Academic Writing\r\n\r\nThe process of academic working and particularly the processes of reading and writing in academia is demanding, between maintaining a resilient writing behaviour and creating a robust piece of text. It is important to develop a routine in academic writing, which enables the author to become a responsible member of the respective discourse community and satisfy all requirements of scientific quality. \r\n\r\nIn this practical course we are going to work on aspects important for successful academic writing from 4 different perspectives. Although all 4 parts are connected to each other, it is possible to join for only one session as well.\r\n\r\n\r\nA Robust Framework for Your Writing Project\r\n\r\nIn the first session we will introduce our approach to academic writing. We will provide a generic framework for orientation in writing projects and pinpoint the challenges many writers come across in the respective phases. We will use a writing exercise for \u201egetting to the point\u201c. \r\n\r\nWhat am I Doing anyway? Developing as a Writer and Researcher\r\n\r\nIn this session we will explore the pains of placing what we are doing and writing about it. Finding a research question, positioning in the field of research what we are working on, and deciding what we want to say is hard work. We will look at reasons for that and discuss some strategies. \r\n\r\nHow to develop a resilient voice as a responsible academic writer\r\n\r\nIn the third session we focus on how to develop a strong academic voice out of your individual position as academic author. Therefore, you will explore you writing behaviour and the process of gradual formulation of ideas and voice through writing by different mind writing and specific revision strategies useful to embody thoughts within written products. \r\n\r\nHow to maintain robust choreographies for your academic working\r\n\r\nIn the last session, the focus will be on the \u201cchoreography of academic working\u201d: how we use and adjust a variety of materialities, actions and spaces to our needs within the phases of the academic writing process. The juggling with diverse working materialities and spaces that we tend to involve for different purposes eases the rhythmic shift between different mindsets we need for an efficient and harmonic choreography of academic working."
},
{
    "identifier": "PC2",
    "title": "The Resilience and Robustness (and hopefully Responsibility) of Linear Algebra",
    "fields": "Mathematics",
    "url": "https://interdisciplinary-college.org/2024-pc2/",
    "type": "Practical Course",
    "instructor": "Emily King",
    "description": "Linear algebra has ancient roots, appearing even in cuneiform text millennia ago.  However, it has proven itself to be resilient and powers many modern techniques in machine learning, natural language processing, cognitive science, and more. Yet, many people use these tools without truly understanding why they work and when they should be used.  The purpose of this course is to provide a deep dive into the intuition behind the tools that linear algebra has to offer. It should be of interest both to students who have taken a course in linear algebra and those who have not.  We will also touch on the topic of the responsible use of linear algebra and other mathematical techniques."
},
{
    "identifier": "PC3",
    "type": "Practical Course",
    "title": "Organizational Improvisation - The Art and Science of the Here and Now",
    "fields": "Improvisation; Creativity; Innovation",
    "url": "https://interdisciplinary-college.org/2024-pc3/",
    "instructor": "Lukas Zenk",
    "description": "In today's complex and unpredictable world, the traditional homo economicus approach of careful planning and execution faces significant challenges. The demand for rapid decision making in complex situations requires a shift toward improvisation - an approach characterized by rapid problem identification, idea generation, and immediate implementation. Drawing a parallel with classical theater, where actors meticulously follow predefined scripts, improvisational theater introduces a different paradigm. Actors take the stage without a predetermined story or characters, relying on their ability to improvise in the present moment. This skill, essential to artistic performance, finds a counterpart in the professional world, which faces time pressures and unpredictable challenges. Organizational Improvisation, an emerging field, explores and fosters this creative skill in organizations. The workshop aims to immerse participants in improvisational mindsets and experience their applicability in a variety of scenarios, with a particular focus on co-creativity. By encouraging radical collaboration, dealing with uncertainty, and fostering co-creative learning environments, the workshop aims to inspire participants to rethink and reinvent their ways of behaving and thinking."
},
{
    "identifier": "PC4",
    "title": "Cultivate your resilience garden",
    "url": "https://interdisciplinary-college.org/2024-pc4/",
    "type": "Practical Course",
    "instructor": "Ana-Alexandra Moga",
    "description": "In the four session course, we'll take a walk together through the garden of resilience. Over the duration of the course, we'll explore some science-based knowledge nuggets to expand on how we think about our mind and body, practice some coaching methods that can anchor us in the present moment, develop our own tools for cultivating resilience and, hopefully, have some fun in the meantime. As we collectively experience the conference,  during the middle part of the course we'll explore ways to connect deeper and find inspiration and strength from each other and the group.The closing part of the course is aimed to highlight and review the tools and strategies that we can take home with us to further experiment with and continue to cultivate resilience in our every day life."
},
{
    "identifier": "RC1",
    "title": "Higher-order phenomena in complex systems with a special focus on the brain: an hands-on experience",
    "fields": "Complexity, Information theory, neuroscience",
    "url": "https://interdisciplinary-college.org/2024-rc1/",
    "type": "Rainbow Course",
    "instructor": "Matteo Neri, Mar Estarellas",
    "description": "Within complexity sciences, various approaches have been developed to investigate the intricate web of interactions underpinning the behavior of complex systems, such as society or the brain. In the last two decades, a great deal of success was reached by theoretical and computational frameworks based on the study of pairwise interactions [1]. Recently, a growing body of literature focused on the extension of these approaches to the study of higher-order interactions, i.e. between more than 2 units of a system [2, 3]. In this course, we will introduce some of the main concepts in the field (e.g. synergy and redundancy in terms of information content [4]), discuss results obtained by previous work, and show how concretely they can be employed in the study of real-world data, with a special focus on cognitive neuroscience and consciousness [5]. \r\n\r\nWe will present our work, focusing on the relationship between the human brain and its internal processes as well as its interaction with the world around us, adopting information-theoretic approaches to analyze neuroimaging data collected with different techniques, in particular MEG, EEG, and fMRI. \r\n\r\nAdditionally, we aim to offer a hands-on experience by providing access to a cutting-edge toolbox [6], making the session interactive and applicable to real-world scenarios.\r\n\r\nThe proposed syllabus will be the following: \r\n\r\n- Basic introduction to graph and information theoretical approaches, focusing on higher-order interactions.\r\n- State of the art in using these measures to investigate complexity from brain to economy, with a special focus on cognition and consciousness science.\r\n- Hands-on experience (bring your computer if you can!)"
},
{
    "identifier": "RC2",
    "title": "Adaptive Ambulatory Assessment in Digital Health and MORE: Presenting and Experimenting with the Modular Open Research Platform for Situated and Longitudinal Human-Subject Research",
    "short_title": "Modular Open Research Platform",
    "fields": "Psychology / Cognitive Science / Data Science",
    "url": "https://interdisciplinary-college.org/2024-rc2/",
    "type": "Rainbow Course",
    "instructor": "David Haag",
    "description": "Ambulatory assessment has been an emerging paradigm in psychological and health research, allowing for the intensive longitudinal study of individuals\\' behavior, physiology, and experiences in their natural environments. By capturing data in the flow of daily life, ambulatory assessment allows us to gain insights into the dynamic processes that shape an individual\u2019s behavior and to consecutively adapt interventions to the individual and their current context. The integration of digital technologies has revolutionized ambulatory assessment, offering robust methodologies for investigating complex systems ranging from individual behavior to broader socio-ecological interactions.\r\n\r\nThe course will provide an introduction into ambulatory assessment, its significance in current research, particularly with modern approaches such as Ecological Momentary Assessment (EMA) and its potential for future applications within psychology, digital health and beyond. By examining various studies, we will explore how this methodology contributes to our understanding of human behavior and system interactions in situ, furthering the discourse on adaptive capacities within complex environments.\r\n\r\nAdditionally, we will introduce the open-source platform MORE (Modular Open Research Platform; https://dhp.lbg.ac.at/more/?lang=en), an innovative tool developed at the Ludwig Boltzmann Institute for Digital Health and Prevention in Salzburg in a research and development effort led by Dr. Jan David Smeddinck. The system is aimed at establishing a digital infrastructure capable of handling the complexities of modern health studies enabling researchers to carry out ambitious ambulatory assessment studies. It addresses the integration of external devices, real-time data collection, participant administration, and data security standards. Notably, the platform contains near real-time data capture and interpretation features, thus allowing for the ecologically valid investigation of momentary antecedents of health behaviors or even testing interventions that adapt to an individual and their current context. This is crucial e.g. for research into personalization in digital health and further investigations along the concept of \u201cprecision health\u201d. MORE comprises a web application for study management and a smartphone app for participant engagement. The smartphone app is configured to guide participants through study procedures, enabling the collection of questionnaire data and sensor information over extended periods in a minimally intrusive and minimally burdensome manner. The app\\'s design reflects a commitment to sustainability, user-friendliness, and technological relevance.\r\n\r\nIn summary, this course will offer participants a blend of a theoretical introduction to ambulatory assessment and a comprehensive overview of the MORE platform, including its conception, key features, and examples of its capabilities to carry out ambulatory assessment studies. Additionally, attendees will have the opportunity for hands-on testing of the free and open-source system, providing a practical understanding of the platform."
},
{
    "identifier":"JE1",
    "title":"Welcome Event",
    "short":"Welcome",
    "url": "",
    "type": "Joint Event",
    "instructor": "",
    "description": "",
    "lecturer":""
},
{
    "identifier":"JE2",
    "title":"Interaction Groups",
    "short":"Interaction Groups",
    "url": "",
    "type": "Joint Event",
    "instructor": "",
    "description": "",
    "lecturer":""
},
{
    "identifier":"JE3",
    "title":"Poster Session",
    "short":"Poster Session",
    "url": "",
    "type": "Joint Event",
    "instructor": "",
    "description": "",
    "lecturer":""
},
{
    "identifier":"JE4",
    "title":"Conference Dinner",
    "short":"Conference Dinner",
    "url": "",
    "type": "Joint Event",
    "instructor": "",
    "description": "",
    "lecturer":""
}
];
</script>
