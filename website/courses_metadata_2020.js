<script id="courses_metadata">
/*
* This file defines the schedule data for an IK conference.
* In particular, this file should contain the following data:
* start_date: start date of the conference in the format "YYYY-MM-DD"
* dinner_date:the date of the conference dinner in the format "YYYY-MM-DD"
* end_date:   end date of the conference in the format "YYYY-MM-DD"
* courses:    a list of courses, each of which is supposed to be an
*             object with the following attributes:
*   "identifier" : Some unique string identifier for the course, e.g. BC1.
*                  The identifier must start with letters and end with at least
*                  one number. whitespaces and special characters are not
*                  permitted.
*   "title" :      The string title of the course.
*   "url" :        The URL to the course page.
*   "type" :       The type of the course, either "Basic Course"/"Introductory Course",
*                  "Method Course"/"Advanced Course", "Special Course"/"Focus Course",
*                  "Rainbow Course", "Evening Talk", "Additional Event", or "Hack".
*   "instructor" : The name of the instructor(s) as string.
*   "description": The course description as HTML string.
*/
start_date = "2020-03-13";
dinner_date = "2020-03-17";
end_date = "2020-03-20";
courses = [
/* Block 1: Friday to Sunday (morning and afternoon) */
{
	"identifier" : "IC2",
	"title" : "Introduction to Psychology",
	"url" : "https://interdisciplinary-college.org/2020-ic2/",
	"type" : "Introductory Course",
	"instructor" : "Katharina Krämer",
	"description" : "<p>This course is intended for all participants (psychologists and non-psychologists alike), who are curious about the human mind and its functions. And that is basically what psychology is about: Psychological research investigates the role of mental functions in individual and social behaviour and explores the physiological and biological processes that underlie cognitive functions and behaviours. As a social science, psychology aims to understand individuals and groups by establishing general principles and researching specific cases by using quantitative and qualitative research methods.</p><p>During this introduction to psychology we will get to know the major schools of thought, including behaviourism and cognitive psychology as well as psychoanalysis and psychodynamic psychology. Psychological research encompasses many subfields and includes different approaches to the study of mental processes and behaviour. In this course, we will focus in particular on the sub-disciplines of developmental psychology, social psychology, and clinical psychology.</p>"
},
{
	"identifier" : "FC09",
	"title" : "Using Robot Models to Explore the Exploratory Behaviour of Insects",
	"short_title" : "Robot Models of Insect Exploratory Behaviour",
	"url" : "https://interdisciplinary-college.org/2020-fc09/",
	"type" : "Focus Course",
	"instructor" : "Barbara Webb",
	"description" : "<p>Insects are often thought to show only fixed ‘robotic’ behaviours but in fact exhibit substantial flexibility, from maggots exploring their world to find which odours signal risk or reward, to ants and bees discovering and efficiently navigating between food sources scattered over a large environment. Yet insects also have small brains, providing the promise that we may be able to understand and model these aspects of intelligent behaviour down to the single neuron level. This course will describe the current state of research in insect exploration, emphasising an explicitly mechanistic view of explanation: to understand a system, we should (literally) try to build it. The final lecture will reflect on this methodology of modelling and what we can learn by implementing biological explanations as robots.</p>"
},
{
	"identifier" : "FC10",
	"title" : "Mindfulness as a Method to Explore your Mind-Wandering with Curiosity",
	"short_title" : "Mindfulness and Mind-Wandering",
	"url" : "https://interdisciplinary-college.org/2020-fc10/",
	"type" : "Focus Course",
	"instructor" : "Marieke Van Vugt",
	"description" : "<p>In the first session, we will introduce the methods of mindfulness, and discuss how mindfulness differs from mind-wandering. Contrary to popular belief, mindfulness is not the opposite of mind-wandering, but rather the cultivation of mindfulness involves becoming better friends with your mind so that you learn to become less stuck in thought processes. We will also review conceptual models of mindfulness and mind-wandering together with some research underpinnings. In addition, we will introduce the first and third-person perspective on studying the mind and basics of microphenomenology. We will also start a small experiment with our own mindfulness practice, which we will analyse in the last session of the course.</p><p>In the second session, we will continue our practice of mindfulness, and review research findings on the effects of mindfulness on cognitive function and brain activity.</p><p>In the third session, we will continue our practice of mindfulness. We will place mindfulness in the context of different meditation practices, discussing similarities and differences. We will also discuss in general how we can study mindfulness scientifically and how to do so rigorously.</p><p>In the fourth session, apart from practicing mindfulness, we will discuss the findings of our little experiments. There will also be ample space for questions and additional topics to discuss.</p>"
},
{
	"identifier" : "FC13",
	"title" : "Hominum-ex-Machina: About Artificial and Real Intelligence",
	"short_title" : "Hominum-ex-Machina",
	"url" : "https://interdisciplinary-college.org/2020-fc13/",
	"type" : "Focus Course",
	"instructor" : "Markus Krause",
	"description" : "<p>Modern computational systems have amazing capabilities. They can detect a face or fingerprint in millions of samples, find a search term in a sea of billions of documents, and control the flow of trillions of dollars. Some of these abilities seem almost supernatural and even frightening. Yet, our brains are still the architects of invention and might remain to be so for aeons to come. Understanding and utilising the difference between machine und human intelligence is one of the new frontiers of computer science. With the advent of the next AI winter integrating human intervention into almost autonomous systems will gain crucial importance in the near future.</p><p>In this course we aim at lifting a bit of the mystic shroud that surrounds artificial intelligence. We will uncover its abilities, unveil short comings, and even conjure a deep neural network from (almost) thin air. You do not need to be an experienced coder or mathematical genius. Basic python understanding, and 8 grade math skills are enough to follow the course and build your own “AI”. After this hopefully disillusionary exercise we take a refreshing dive into reality. We will investigate real intelligence and how our brains talent for strategic problem solving can fuse with the sheer calculation power of machines. We will explore how these socio-technical systems will shape the future and the risks and pitfalls of the Hominum-ex-Machina.</p>"
},
{
	"identifier" : "PrfC2",
	"title" : "Ethics in Science and Good Scientific Practice ",
	"url" : "https://interdisciplinary-college.org/2020-prfc2/",
	"type" : "Professional Course",
	"instructor" : "Hans-Joachim Pflüger",
	"description" : "<p>This course will focus on aspects of ethics in science and that “scientific curiosity” is not without limits although ethical standards may vary in different cultures. We shall focus on the relatively new field of neuro-ethics. Another important topic will be concerned with good scientific practice and its implications for the performance of science, and what kind of rules apply. To follow these rules of good scientific practice is among the key requirements for scientists worldwide as well as for any autonomous artificial intelligent systems and, thus, is one of the pillars of trust into scientific results. The participants will be confronted with examples of fraud in science, ethical problems and decisions as well as good scientific practice. The outcome of such ethical decisions and questions around good scientific practice will be discussed.</p>"
},
/* Block 2: Friday to Sunday (noon and late afternoon) */
{
	"identifier" : "IC4",
	"title" : "Introduction to Ethics in AI",
	"url" : "https://interdisciplinary-college.org/2020-ic4/",
	"type" : "Introductory Course",
	"instructor" : "Heike Felzmann",
	"description" : "<p>The last few years have seen an explosion of societal uses of AI technologies, but at the same time widespread public scepticism and fear about their use have emerged. In response to these concerns, a wide range of guidance documents for good practice in AI have been published by professional and societal actors recently. Both as researchers in AI and as consumers of AI it is helpful to understand ethical concepts and concerns associated with the use of AI and to be familiar with some of these guidance documents, in order to be able to reflect carefully on their ethical and social meaning and the balance of their benefits and risks and adapt one’s practices accordingly.</p><p>This course provides a general introduction to emergent ethical issues in the field of AI. It will be suitable for anyone with an interest in reflecting on how AI impacts on contemporary life and society.</p>"
},
{
	"identifier" : "MC1",
	"title" : "Applications of Bayesian Inference and the Free Energy Principle",
	"short_title" : "Bayesian Inference and Free Energy Principle",
	"url" : "https://interdisciplinary-college.org/2020-mc1/",
	"type" : "Method Course",
	"instructor" : "Christoph Mathys",
	"description" : "<p>We will start with a look at the fundamentals of Bayesian inference, model selection, and the free energy principle. We will then look at ways to reduce Bayesian inference to simple prediction adjustments based on precision-weighted prediction errors. This will provide a natural entry point to the field of active inference, a framework for modelling and programming the behaviour of agents negotiating their continued existence in a given environment. Under active inference, an agent uses Bayesian inference to choose its actions such that they minimize the free energy of its model of the environment. We will look at how an agent can infer the state of the environment and its own internal control states in order to generate appropriate actions.</p>"
},
{
	"identifier" : "FC07",
	"title" : "A Series of Interesting Choices: Risk, Reward, and Curiosity in Video Games",
	"short_title" : "CRR in Video Games",
	"url" : "https://interdisciplinary-college.org/2020-fc07/",
	"type" : "Focus Course",
	"instructor" : "Max Birk",
	"description" : "<p>Civilization’s Sid Meier defined video games as a series of interesting choices. Game-design aims to balance risk and reward for each choice made in a game, with the goal to create compelling experiences that draw people in and keep them spellbound. In this course you will create your own game and explore how modifying formal game elements applying psychological theory affects play experience.</p>"
},
{
	"identifier" : "FC12",
	"title" : "The Development of Curiosity",
	"url" : "https://interdisciplinary-college.org/2020-fc12/",
	"type" : "Focus Course",
	"instructor" : "Gert Westermann",
	"description" : "<p>Curiosity has been described as an important driver for learning from infancy onwards. But what is curiosity? How has it been conceptualized, and how has its role in infant learning been identified and characterized? This course will describe the main theories of what curiosity is and how it affects behaviour, and how recent developmental research has studied curiosity in infants and children. Here I will address children’s active role in their learning and in their language development, as well as their preference for specific types of information. I will also touch on the role of play in infant and child development. Computational modelling can help us to develop theories of the mechanisms underlying curiosity-based exploratory behaviour, and I will discuss some of these models.</p><p>This course does not require any prior knowledge and all topics will be introduced gently.</p>"
},
{
	"identifier" : "PC2",
	"title" : "Seeking Shaky Ground",
	"url" : "https://interdisciplinary-college.org/2020-pc2/",
	"type" : "Practical Course",
	"instructor" : "Claudia Muth & Elisabeth Zimmermann",
	"description" : "<p>Curiosity entails being able to delve into the unknown, to challenge habits of thinking, of acting, of reacting, of perceiving, – of sense-making. We can decide to let ourselves be challenged, we can seek uncertainty and the risk of not knowing what will come. This for example happens, when we try out a new physical activity we don’t master yet, e.g. an adult decides to learn to ride a horse. But it also happens, when we expose ourselves to art, challenging our patterns of perceiving.</p><p>In such situations we often loose and gain or regain stability. We thereby learn. Accepting moments of instability and uncertainty as part of each learning process can provide insight and even lead to experiencing such situations as pleasurable and rewarding.</p><p>Which preconditions and circumstances have to be met in order to develop an attitude of openness, of giving up anticipation and prediction, of letting go and letting come?</p><p>Becoming aware of our own patterns of moving, of perceiving, of relating to the world is one necessity. Establishing an atmosphere of trust, where “mistakes” are invited, another.</p><p>In this course we aim to put ourselves on “shaky ground” using exercises from dance/movement/contact improvisation, as well as techniques of design/art creation. We will try to become aware of, explore and play around with our habitual ways of interacting with the world and people around us, thereby challenge our habits and raise curiosity for the unknown.</p>"
},
/* Block 3: Sunday to Tuesday (morning and afternoon) */
{
	"identifier" : "IC3",
	"title" : "Introduction to Neuroscience",
	"url" : "https://interdisciplinary-college.org/2020-ic3/",
	"type" : "Introductory Course",
	"instructor" : "Till Bockemühl & Ronald Sladky",
	"description" : "<p>The brain, the cause of – and solution to – all of life’s problems. According to our brains it is the most fascinating structure in the known universe. Consisting of about 86 billion neurons of which each can form thousands of connections to other neurons it is also the most complex structure in the known universe. In this course we would like to give you a rough guide and introduction to the basic principles, fundamental theories, and methods of neuroscience.</p><p>We will demonstrate that neuroscience can be seen as a multi-modal, multi-level, multi-disciplinary research framework that aims at addressing the challenges of this megalomaniac scientific endeavor. We will see that different frameworks and methods can lead to conflicting empirical evidence, theoretical assumptions, and heated debates. However, we argue that this might be the only way to uncover the mysteries of our brain.</p><p>In this course we will cover a variety of scopes and perspectives. We will teach some of the fundamentals of neuroscience in human and non-human animals, but we will also explore some explanatory gaps between the different levels of inference.</p><p>On a phenomenal level we will investigate the functions of individual neurons and small networks. We will discuss if and how we can learn from (genetically modified) model animals about neural functions. To what degree is this relevant for understanding human brain function, such as learning and decision making? On the other hand, we will also investigate the state of the art in human brain mapping and cognitive neuroscience. Can findings from neuroimaging tell us anything at all about neurobiology – or are they just fancy illustrations that are better suited for children’s books?</p>"
},
/*{
	"identifier" : "FC03",
	"title" : "Arousal Interactions with Curiosity, Risk, and Reward from a Computational Cognitive Architecture Perspective",
	"short_title" : "Computational Cognitive Architectures of CRR",
	"url" : "https://interdisciplinary-college.org/2020-fc03/",
	"type" : "Focus Course",
	"instructor" : "Christopher Dancy",
	"description" : "<p>In this focused course, we will cover some of the many ways in which stress and arousal can modulate behaviors related to curiosity, risk, and reward through the interactions between physiological, affective, and cognitive processes. We will use the ACT-R/Phi architecture to think about this from a perspective of interacting mind and body processes. The Project Malmo (Minecraft) environment will also be used to show how we might implement some of the theoretical accounts as simulated agents in a virtual environment.</p>"
},*/
{
	"identifier" : "PC3",
	"title" : "Juggling - experience your brain at work",
	"url" : "https://interdisciplinary-college.org/2020-pc3/",
	"type" : "Practical Course",
	"instructor" : "Susan Wache & Julia Wache",
	"description" : "<p>In this course we will teach you how to juggle. Juggling is a motor activity that requires a lot of different skills. </p><p>The activity of juggling requires a lot of different abilities. Obviously, you need to learn the movement pattern and practice a lot to get the reward – being able to juggle! To learn such specific movement patterns requires a highly complex electrical and chemical circuitry in the brain, which becomes a more and more important field of neuroscience. Juggling seems to encourage nerve fiber growth and therefore scientist believe it not only promotes brain fitness in general but could also help with debilitating illnesses.</p><p>Nevertheless, learning to juggle requires attention, focus, concentration and persistence. As every juggler would agree, the key for success is repetition. We will teach juggling mainly practical. While training you can feel constant progress independently of your previous skill level. </p><p>In the last session you will also get an introduction to site swap, a mathematical description of juggling patterns you can notate, calculate and e.g. feed into a juggling simulator.</p>"
},
{
	"identifier" : "PC4",
	"title" : "Curious Making, Taking Fabrication Risks and Crafting Rewards",
	"short_title" : "Making, Fabrication, and Crafting",
	"url" : "https://interdisciplinary-college.org/2020-pc4/",
	"type" : "Practical Course",
	"instructor" : "Janis Meißner",
	"description" : "<p>This course is about getting hands-on curious with electronics and different crafts materials. Maker toolkits are a great way to get started with designing your own interactive sensor systems – but what if these designs could also integrate other (potentially more aesthetic) materials? E-textiles and paper circuits are good examples for how functional electronic systems can be recrafted with rewarding results. In principle, any every-day materials could be used with a bit of thinking outside the (tool)box. Let’s see what you will use to hack for your ideas!</p>"
},
{
	"identifier" : "PrfC1",
	"title" : "Curiosity, Risk, and Reward in Teaching in Higher Education",
	"short_title" : "Teaching in Higher Education",
	"url" : "https://interdisciplinary-college.org/2020-prfc1/",
	"type" : "Professional Course",
	"instructor" : "Ingrid Scharlau",
	"description" : "<p>Teaching is a very complex, multilayered activity, going far beyond transfer of knowledge. The three main topics of this IK help to understand important aspects of teaching in higher education: Teaching is risky (which is not a defect but at its core), teaching is a reciprocal, social and interpersonal practice, and teaching is often a gendered practice. The course covers these three aspects with a mix of theoretical input and reflection. In the first session, we will reflect on our cultural, disciplinary, and personal understanding of teaching and then cover the three aspects risk, reward, and, finally curiosity.</p>"
},
{
	"identifier" : "Pan1",
	"title" : "Lived Curiosity in Industry and Academia; panel; title TBA",
	"short_title" : "Industry & Academia Panel Discussion",
	"url" : "https://interdisciplinary-college.org/2020-pan1/",
	"type" : "Additional Event",
	"instructor" : "Becky Inkster",
	"description" : "<p>tba</p>"
},
/* Block 4: Monday to Tuesday (noon and late afternoon) */
{
	"identifier" : "IC1",
	"title" : "Introduction to Machine Learning",
	"url" : "https://interdisciplinary-college.org/2020-ic1/",
	"type" : "Introductory Course",
	"instructor" : "Benjamin Paassen",
	"description" : "<p>This course is intended for non-machine learners with little to no prior knowledge. It will provide many examples as well as accompanying exercises and limit the number of formulae to a bare minimum, while instead maximizing the number of meaningful images.</p>"
},
{
	"identifier" : "FC02",
	"title" : "Your Wit Is My Command",
	"url" : "https://interdisciplinary-college.org/2020-fc02/",
	"type" : "Focus Course",
	"instructor" : "Tony Veale",
	"description" : "<p>Until quite recently, AI was a scientific discipline defined more by its portrayal in science fiction than by its actual technical achievements. Real AI systems are now catching up to their fictional counterparts, and are as likely to be seen in news headlines as on the big screen. Yet as AI outperforms people on tasks that were once considered yardsticks of human intelligence, one area of human experience still remains unchallenged by technology: our sense of humour.</p><p>This is not for want of trying, as this course will show. The true nature of humour has intrigued scholars for millennia, but AI researchers can now go one step further than philosophers, linguists and psychologists once could: by building computer systems with a sense of humour, capable of appreciating the jokes of human users or even of generating their own, AI researchers can turn academic theories into practical realities that amuse, explain, provoke and delight.</p>"
},
{
	"identifier" : "FC08",
	"title" : "The Motivational Power of Curiosity – Information as Reward",
	"short_title" : "The Motivational Power of Curiosity",
	"url" : "https://interdisciplinary-college.org/2020-fc08/",
	"type" : "Focus Course",
	"instructor" : "Lily FitzGibbon",
	"description" : "<p>This course will provide an overview of research from a number of fields of psychology and neuroscience pertinent to the understanding of the motivational power of curiosity. In particular, we will discuss empirical findings from across the lifespan in the context of a reward learning framework of knowledge acquisition. We will consider where the subjective experiences of curiosity and interest fit into the model and how they might be differentiated. Finally, we will discuss and develop challenges, open questions, and testable predictions from the model, setting out a programme of work for the field. The aim of this final session is to generate and develop research ideas and foster new collaborations between course participants.</p>"
},
{
	"identifier" : "Db1",
	"title" : "Curiosity as a research field; debate; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-db1/",
	"type" : "Additional Event",
	"instructor" : "Praveen Paritosh",
	"description" : "<p>tba</p>"
},
/* Block 5: Tuesday to Thursday (morning and afternoon) */
{
	"identifier" : "MC2",
	"title" : "Symbolic Reasoning within Connectionist Systems",
	"url" : "https://interdisciplinary-college.org/2020-mc2/",
	"type" : "Method Course",
	"instructor" : "Klaus Greff",
	"description" : "<p>Our brains effortlessly organize our perception into objects which it uses to compose flexible mental models of the world. Objects are fundamental to our thinking and our brains are so good at forming them from raw perception, that it is hard to notice anything special happening at all. Yet, perceptual grouping is far from trivial and has puzzled neuroscientists, psychologists and AI researchers alike.</p><p>Current neural networks show impressive capacities in learning perceptual tasks but struggle with tasks that require a symbolic understanding. This ability to form high-level symbolic representations from raw data, I believe, is going to be a key ingredient of general AI.</p><p>During this course, I will try to share my fascination with this important but often neglected topic.</p><p>Within the context of neural networks, we will discuss the key challenges and how they may be addressed. Our main focus will be the so-called Binding Problem and how it prevents current neural networks from effectively dealing with multiple objects in a symbolic fashion.</p>"
},
{
	"identifier" : "FC05",
	"title" : "Confidence and Overconfidence",
	"url" : "https://interdisciplinary-college.org/2020-fc05/",
	"type" : "Focus Course",
	"instructor" : "Vivek Nityananda",
	"description" : "<p>Decision-making in human and animal societies often uses a confidence heuristic – trusting the decisions made by confident individuals. This could have the benefit of quick decision-making without having to explore risky options yourself. However, confidence is a good guide to decisions only if it reflects accuracy. When the trusted individuals are overconfident, this results in risky and often catastrophic decisions. Despite the possibility of these negative outcomes, overconfidence persists and is widespread. What are then the advantages of overconfidence? Using an evolutionary perspective demonstrates the individual and social rewards of overconfidence. This also helps us understand how we can make the most of confidence while avoiding the obvious costs of overconfidence.</p>"
},
/*{
	"identifier" : "FC06",
	"title" : "Cybersecurity/Curiosity, Risk, and Reward in Hacking; title TBA",
	"short_title" : "Cybersecurity",
	"url" : "https://interdisciplinary-college.org/2020-fc06/",
	"type" : "Focus Course",
	"instructor" : "Sadia Afroz</p>"
},*/
{
	"identifier" : "FC11",
	"title" : "Artificial curiosity for robot learning",
	"url" : "https://interdisciplinary-college.org/2020-fc11/",
	"type" : "Focus Course",
	"instructor" : "Nguyen Sao Mai",
	"description" : "<p>This course will provide an overview of research in machine learning and robotics of artificial curiosity. Also referred to as intrinsic motivation, this stream of algorithms inspired by theories of developmental psychology allow artificial agents to learn more autonomously, especially in stochastic high-dimensional environments, for redundant tasks, for multi-task, life-long or curriculum learning.</p>"
},
{
	"identifier" : "PC1",
	"title" : "Perceiving the World through Introspection",
	"url" : "https://interdisciplinary-college.org/2020-pc1/",
	"type" : "Practical Course",
	"instructor" : "Annekatrin Vetter & Sophia Reul",
	"description" : "<p>How do I experience the world around me? What might influence my decisions and actions in everyday life? How do I feel?  If you are curious to answer these questions we invite you to come to our course. In our four sessions we will focus on the broad field of self-experience. We are going to introduce you to different exercises and tools out of the range of self-awareness, mindfulness, body perception, biography reflection and interpersonal and intrapersonal communication. A curios mind is the only requirement to join our experiential-group and we are looking forward to welcome you at IK.</p>"
},
/* Block 6: Tuesday to Friday (noon and late afternoon) */
{
	"identifier" : "PrfC3",
	"title" : "Curiosity, Risk, and Reward in the Academic Job Search",
	"short_title" : "Academic Job Search",
	"url" : "https://interdisciplinary-college.org/2020-prfc3/",
	"type" : "Professional Course",
	"instructor" : "Emily King",
	"description" : "<p>So you’ve decided that you want a career or at least a job in academia; what’s next?  Imposter Syndrome is something that makes applying to lots of interesting jobs seem like a risk; when should you take the time to apply?  How can you leverage curiosity to broaden your job search? This single session course will cover some of the basics of the academic job search: how to decide whether to apply for a particular job and then how to make your application pop.  As the cover letter is the first thing that most hiring committees see, we will focus on how to make yours strong.  CVs, research statements, teaching portfolios, and interview questions will also be touched on.</p><p>This class will be discussion-based.  Please come with your questions! Also, if you want to submit a cover letter to be (constructively!) critiqued by the participants, please send it to me over email.</p>"
},
{
	"identifier" : "MC3",
	"title" : "Embodied Symbol Emergence",
	"url" : "https://interdisciplinary-college.org/2020-mc3/",
	"type" : "Method Course",
	"instructor" : "Malte Schilling & Michael Spranger",
	"description" : "<p>Symbols are the bedrock of human cognition. They play a role in planning, but are also crucial to understanding and modeling language. Since they are so important for human cognition, they are likely also vital for implementing similar abilities in software agents and robots.</p><p>The course will focus on symbols from two integrated perspectives. On the one hand, we look at the emergence of internal models through interaction with the environment and their role in sensorimotor behavior. This perspective is the embodied perspective. The first two lectures of the course concentrate on the emergence of internal models and grounded symbols in simple animals and agents and show how interaction with an environment requires internal models and how these are structured. Here we use robots to show how effective the discussed mechanisms are.</p><p>The second perspective is that symbols can also be socially constructed. In particular, we will focus on language and how it is grounded in embodiment but also social interaction. This will be the topic of the third and fourth lecture. We first investigate the emergence of grounded names and categories (and their terms) in social interactions between robots. The second two lectures of the course will focus on compositionality – that is the interaction of embodied categories in larger phrases or sentences and grammar.</p>"
},
/*{
	"identifier" : "MC4",
	"title" : "Learning Mappings via Symbolic, Probabilistic, and Connectionist Modeling",
	"short_title" : "Learning Mappings",
	"url" : "https://interdisciplinary-college.org/2020-mc4/",
	"type" : "Method Course",
	"instructor" : "Afsaneh Fazly</p>"
},*/
{
	"identifier" : "MC5",
	"title" : "Low Complexity Modeling in Data Analysis and Image Processing",
	"short_title" : "Low Complexity Modeling",
	"url" : "https://interdisciplinary-college.org/2020-mc5/",
	"type" : "Method Course",
	"instructor" : "Emily King",
	"description" : "<p>Are you curious about how to extract important information from a data set?  Very likely, you will be rewarded if you use some sort of low complexity model in your analysis and processing.  A low complexity model is a representation of data which is in some sense much simpler than what the original format of the data would suggest. For example, every time you take a picture with a phone, about 80% of the data is discarded when the image is saved as a JPEG file. The JPEG compression algorithm works due to the fact that discrete cosine functions yield a low complexity model for natural images that tricks human perception. As another example, linear bottlenecks, pooling, pruning, and dropout are all examples of enforcing a low complexity model on neural networks to prevent overfitting. </p>"
},
{
	"identifier" : "FC01",
	"title" : "Motifs for Neurocognitive Challenges from Individual to Evolutionary Time Scales",
	"short_title" : "Neurocognitive Challenges",
	"url" : "https://interdisciplinary-college.org/2020-fc01/",
	"type" : "Focus Course",
	"instructor" : "Wulf Haubensak",
	"description" : "<p>Brains are built to avoid threats and seize rewards – the basic behavioral challenges from survival in the wild to navigating complex societies. Here, we explore the underlying neuronal circuit motifs, from individual to evolutionary time scales. We will also discuss how network function is genetically programmed and how dysfunction might lead to psychiatric symptoms.</p>"
},
/*{
	"identifier" : "FC04",
	"title" : "Behaviour Modelling; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-fc04/",
	"type" : "Focus Course",
	"instructor" : "Fahim Kawsar",
	"description" : "<p>tba</p>"
},*/
/* Evening and Additional Events */
{
	"identifier" : "WA1",
	"title" : "Welcome Address",
	"url" : "https://interdisciplinary-college.org/program/#events",
	"type" : "Evening Talk",
	"instructor" : "Smeddinck, Rohlfing & Stewart"
},
{
	"identifier" : "ET1",
	"title" : "How to Know",
	"url" : "https://interdisciplinary-college.org/2020-et1/",
	"type" : "Evening Talk",
	"instructor" : "Celeste Kidd",
	"description" : "<p>This evening lecture will discuss Kidd’s research about how people come to know what they know. The world is a sea of information too vast for any one person to acquire entirely. How then do people navigate the information overload, and how do their decisions shape their knowledge and beliefs? In this talk, Kidd will discuss research from her lab about the core cognitive systems people use to guide their learning about the world—including attention, curiosity, and metacognition (thinking about thinking). The talk will discuss the evidence that people play an active role in their own learning, starting in infancy and continuing through adulthood. Kidd will explain why we are curious about some things but not others, and how our past experiences and existing knowledge shape our future interests. She will also discuss why people sometimes hold beliefs that are inconsistent with evidence available in the world, and how we might leverage our knowledge of human curiosity and learning to design systems that better support access to truth and reality.</p>"
},
{
	"identifier" : "PS1",
	"title" : "Poster Session",
	"url" : "https://interdisciplinary-college.org/contribute/#poster",
	"type" : "Evening Talk",
	"instructor" : ""
},
{
	"identifier" : "ET2",
	"title" : "Data-Driven Dynamical Models for Neuroscience and Neuroengineering",
	"url" : "https://interdisciplinary-college.org/2020-et2/",
	"type" : "Evening Talk",
	"instructor" : "Bing W. Brunton",
	"description" : "<p>Discoveries in modern neuroscience are increasingly driven by quantitative understanding of complex data. The work in my lab lies at an emerging, fertile intersection of computation and biology. I develop data-driven analytic methods that are applied to, and are inspired by, neuroscience questions. Projects in my lab explore neural computations in diverse organisms.  We work with theoretical collaborators on developing methods, and with experimental collaborators studying insects, rodents, and primates. The common theme in our work is the development of methods that leverage the escalating scale and complexity of neural and behavioural data to find interpretable patterns.</p>"
},
{
	"identifier" : "ET3",
	"title" : "Information as a Resource: How Organisms Deal with Uncertainty",
	"url" : "https://interdisciplinary-college.org/2020-et3/",
	"type" : "Evening Talk",
	"instructor" : "Alex Kacelnik",
	"description" : "<p>Organisms nearly always act with incomplete information about the outcome of possible actions. They can include unpredictability into their decision process (risk sensitivity), or allocate effort to reduce uncertainty (learning, sampling). In all cases, the consequences of uncertainty, and the cost of reducing it, affect the expected payoffs, and hence can be expected to play a role in the decision mechanisms. Similarly, designers of synthetic intelligences are starting to include information-seeking (i.e. curiosity) in the behaviour of autonomous artificial systems, including problem-solving robots. I will present several lines of behavioural research in this area.</p>"
},
{
	"identifier" : "ET4",
	"title" : "Biosignal Processing for Human-Machine Interaction",
	"url" : "https://interdisciplinary-college.org/2020-et4/",
	"type" : "Evening Talk",
	"instructor" : "Tanja Schultz",
	"description" : "<p>Human interaction is a complex process involving modalities such as speech, gestures, motion, and brain activities emitting a wide range of biosignals, which can be captured by a broad panoply of sensors. The processing and interpretation of these biosignals offer an inside perspective on human physical and mental activities and thus complement the traditional way of observing human interaction from the outside. As recent years have seen major advances in sensor technologies integrated into ubiquitous devices, and in machine learning methods to process and learn from the resulting data, the time is right to use of the full range of biosignals to gain further insights into the process of human-machine interaction.</p><p>In my talk I will present ongoing research at the Cognitive Systems Lab (CSL), where we explore interaction-related biosignals with the goal of advancing machine-mediated human communication and human-machine interaction. Several applications will be described such as Silent Speech Interfaces that rely on articulatory muscle movement captured by electromyography to recognize and synthesize silently produced speech, as well as Brain Computer Interfaces that use brain activity captured by electrocorticography to recognize speech (brain-to-text) and directly convert electrocortical signals into audible speech (brain-to-speech). I will also describe the recording, processing and automatic structuring of human everyday activities based on multimodal high-dimensional biosignals within the framework of EASE, a collaborative research center on cognition-enabled robotics. This work aims to establish an open-source biosignals corpus for investigations on how humans plan and execute interactions with the aim of facilitating robotic mastery of everyday activities.</p>"
},
/* Rainbow Courses */
{
	"identifier" : "RC1",
	"title" : "Homeostatically-driven behavioral architectures: How to model biological organisms throughout their life-cycle",
	"short_title" : "Homeostatically-driven behavioral architectures",
	"url" : "https://interdisciplinary-college.org/2020-rc1/",
	"type" : "Rainbow Course",
	"instructor" : "Panagiotis Sakagiannis",
	"description" : "<p>Why do organisms behave? When do they take risks and when do rewards matter to them? What is the nervous system's role in a successful life cycle and how does it relate to its evolutionary origins? In this course, we adopt a behavioral modeler's view integrating insights from systems neuroscience, ecological energetics, and layered robotic architectures in order to sketch a framework for dynamic mechanistic models of biological behavior. We address the advantages and shortcomings of region-specific biologically realistic neurocomputational models, of agent-based ecological simulations and of optimality-driven intelligent artificial agents and discuss ways of combining these powerful computational tools with a focus on the persisting individual homeostasis. Nested behaviors, recurrent neural networks, and entangled spatiotemporal scales are our main modeling challenges. An intensively studied organism, the drosophila fruit fly larva, will serve as our model agent for the whole course.</p>"
},
{
	"identifier" : "RC2",
	"title" : "Can patterns of word usage tell us what lemon and moon have in common? Analyzing the semantic content of distributional semantic models",
	"short_title" : "Analyzing the semantic content of distributional semantic models",
	"url" : "https://interdisciplinary-college.org/2020-rc2/",
	"type" : "Rainbow Course",
	"instructor" : "Pia Sommerauer",
	"description" : "<p>Can patterns of textual contexts in which words appear tell you (or your model) that both, a lemon and the moon are described as yellow and round but differ with respect to (almost) everything else? In other words: How much information about concepts is encoded in patterns of word usage (i.e. distributional data)?</p><p>In this course, I will take stock of what we know about the semantic content encoded in data-derrived meaning representations (e.g Word2Vec), which are commonly used in Natural Language Processing and cognitive modelling (e.g. metaphor interpretation).</p><p>I will focus on how we can find out whether (and what) semantic knowledge they represent (beyond a general sense of semantic word similarity and relatedness). Drawing on methods in the area of neural network interpretability, I will discuss how we can “diagnose” semantic knowledge to find out whether a model can in fact distinguish flying from non-flying birds or tell you what lemons and the moon have in common.</p>"
},
{
	"identifier" : "RC3",
	"title" : "Representing Uncertainties in Artificial Neural Networks",
	"url" : "https://interdisciplinary-college.org/2020-rc3/",
	"type" : "Rainbow Course",
	"instructor" : "Kai Standvoss",
	"description" : "<p>Tracking uncertainties is a key capacity of an intelligent system for successfully interacting within a changing environment. Representations of uncertainty are relevant to optimally weigh sensory evidence against prior expectations, to adjust learning rates accordingly, and importantly to trade-off exploitation and exploration. Thus, uncertainty is a crucial component of curiosity and reward driven behavior. Additionally, calibrated uncertainty estimates are relevant for human interaction as well as reliable artificial systems. However, it is not yet well understood how uncertainties are tracked in the brain.</p><p>Bayesian views on Deep Learning offer a way to specify distributions over model parameters and to learn generative models of data generation processes. Thereby, different levels and kind of uncertainties can be represented. In this course, we will discuss different Bayesian methods to track uncertainties in neural networks and speculate about possible links to neuroscience.</p>"
}
];
</script>
