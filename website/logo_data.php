<?php
/*
This API grabs the interdisciplinary college grabs the logo files
from https://interdisciplinary-college.org/schedule/logo_invoice.png
and https://interdisciplinary-college.org/schedule/logo_nametag.png
and provides it as base64 encoded JSON.

{
  "logo_invoice" : "<base64 content>",
  "logo_nametag" : "<base64 content>"
}

Created 2023 by Benjamin Paassen - bpaassen@techfak.uni-bielefeld.de
*/

// We permit this API to be openly accessible because it only offers
// a small bandwidth of public data in a static format. Still, please handle
// responsibly.
header("Access-Control-Allow-Origin: *");

// load logos
$logo_invoice = file_get_contents(
'https://interdisciplinary-college.org/schedule/logo_invoice.png');
$logo_nametag = file_get_contents(
'https://interdisciplinary-college.org/schedule/logo_nametag.png');
$logo_gi = file_get_contents(
'https://interdisciplinary-college.org/schedule/GI_schwarz.png');

// write to JSON
$out_data = '{ "logo_invoice" : "' . base64_encode($logo_invoice) . '", "logo_nametag" : "' . base64_encode($logo_nametag) . '", "logo_gi" : "' . base64_encode($logo_gi) . '" }';

echo $out_data;

?>
