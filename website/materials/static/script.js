
let linkElem;
let button;
let wrappers = document.getElementsByClassName("youtube-embed");

for(let wrapper of wrappers) {
    linkElem = wrapper.getElementsByTagName("a")[0];
    button = document.createElement("button");
    button.textContent = "Load embedded video here";
    button.videoURL = linkElem.href
        .replace("https://www.youtube.com/watch?v=", "https://www.youtube.com/embed/")
        // if pattern argument is a string, only first occurence is replaced
        .replace("&", "?");
    button.onclick = function() {
        wrapper.innerHTML = `<iframe width="560" height="315" src="${this.videoURL}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
    };
    wrapper.getElementsByClassName("info-actions")[0].appendChild(button);
//    linkElem.remove();
}
