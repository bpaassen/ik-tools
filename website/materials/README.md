# Generation of the IK materials page

The script `generate.py` can be used to generate an `index.html` file for the materials page. It is intended that the `static` directory can be uploaded as-is to the IK website via FTP. Course-related files should be placed in sub-directories named after their course ID.

The general idea is that the script checks reads basic course information (`src/lectures.tsv`), checks for each course whether course-specific files (`static/*/*.*`) and notes (`static/*/notes.md`) exist and produces a list of course based on that. Additionally, some static text can be included before (`src/header.md`) and after (`src/footer.md`) the generated list.

Run `./generate.py -h` to display some general usage information. 

