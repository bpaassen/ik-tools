## Poster abstracts

<a name="posters"></a>

### Sense of Ownership and Sense of Agency in Patients with Borderline Personality Disorder

*Tim J. Möller<sup>1\*</sup> , Niclas Braun<sup>2</sup>, Alexandra Philipsen<sup>2</sup>, Christoph S. Herrmann<sup>1</sup>*

<sup>1</sup>Department of Psychology, Experimental Psychology Lab, University of Oldenburg, Oldenburg, Germany

<sup>2</sup>Clinic and Policlinic for Psychiatry and Psychotherapy, University of Bonn, Bonn, Germany

<details markdown="1">
Patients with Borderline personality disorder (BPD) not only show a strong instability in their affect and interpersonal relations, but also a disturbed Sense of Self, including dissociative alienation symptoms [1]. Not yet understood is whether an altered Sense of Ownership (SoO) and Sense of Agency (SoA) may contribute to these alienation symptoms. The aim of the present study was to investigate whether patients with BPD have an altered SoO and SoA compared to healthy participants. To address this aim, an active variant of the rubber hand illusion (RHI) paradigm was used. Established subjective, electrodermal, and behavioral measures were used to assess the participant’s level of SoO and SoA. According to questionnaire data, SoO was reported to be higher under anatomical hand congruency than incongruency which stands in line with former studies. The feelings of Ownership are trend wise elevated in BPD patients, but statistically not signiﬁcant. Regarding SoA, illusory SoA over the anthropomorphic hand could be induced in both groups and conditions. Most importantly, BPD patients reported a higher SoA than the healthy controls. That indicates a stronger SoA is a feature elevated in BPD patients.
By adapting an intentional binding measurement that was used in earlier studies [2] we could show that participants clearly underestimated the time intervals in agency conditions that is argued to indicate levels of SoA [3]. A signiﬁcant group or position effect could, however, not be revealed. Given the present data, we suggest that causal inference rather than agentive inference accounts for the observed temporal binding effect. Looking at individual changes of electrodermal activity, the analysis of the tonic and phasic EDA did not indicate clear results. Data gives some preliminary indication that dissociation might be associated with an elevated body plasticity in BPD.

#### References

* [1] M. C. Zanarini, T. Ruser, F. Frankenburg, and J. Hennen. The dissociative experiences of
borderline patients. Comprehensive Psychiatry, 41(3):223–227, 2000. issn: 0010-440X. doi: [10.1016/S0010-440X(00)90051-8](https://doi.org/10.1016/S0010-440X(00)90051-8).
* [2] N. Braun, J. D. Thorne, H. Hildebrandt, and S. Debener. Interplay of agency and ownership: The intentional binding and rubber hand illusion Paradigm Combined. PLoS ONE, 9(11), 2014. issn: 19326203. doi: [10.1371/journal.pone.0111967](https://doi.org/10.1371/journal.pone.0111967).
* [3] H. Limerick, D. Coyle, and J. W. Moore. The experience of agency in human-computer interactions: a review. Frontiers in Human Neuroscience, 8(August):1–10, 2014. issn: 1662-5161. doi: [10.3389/fnhum.2014.00643](https://doi.org/10.3389/fnhum.2014.00643).
</details>

### Aesthetic perception of speech melody as a potential factor in the cultural evolution of language

*Theresa Matzinger, Eva Specker, Nikolaus Ritt, W. Tecumseh Fitch*

University of Vienna, Nicolaus Copernicus University Torun

<details markdown="1">
<strong>Keywords:</strong> aesthetics, language, speech melody, prosody, stress patterns, cultural evolution

All human cultures appreciate art and can perceive visual, verbal, or musical stimuli in terms of their aesthetic appeal. For example, aesthetic preferences regarding speech melody play a prominent role for the appeal of poetry (Nadal & Vartanian, 2019; Rastall, 2008). If such preferences also apply in the perception of spontaneous everyday speech, they may pose a constraint on language change (Rastall, 2008): we hypothesize that aesthetically appealing linguistic features are learned easily and used fre-quently, and will thus be culturally transmitted to future generations of speakers more successfully than less appealing features (cf. Smith & Kirby, 2008).
In our exploratory study, we investigated a crucial baseline for this hypothesis, namely if there were indeed differences in listener’s aesthetic judgements of linguistic features. Specifically, we focused on the aesthetic perception of temporal rhythmic patterns in polysyllabic words. On the one hand, words might be regarded as most aesthetic if their syllables are isochronous because isochrony has a facilita-tory effect on auditory processing, and people have a general propensity for regular patterns (Ravignani & Madison, 2017). On the other hand, listeners may also perceive irregular patterns as aesthetically appealing (Westphal-Fitch & Fitch, 2013). In that case, words with deviations from isochrony might be judged as more pleasing than isochronous stimuli.
To explore the potential link between words’ rhythmic patterns and aesthetic perception, we tested 180 native-German-speaking participants on their aesthetic evaluation of artificially generated trisyllabic pseudo-words. Each participant made valence ratings of 20 words that were each presented in 3 dif-ferent conditions in a random order: a) with isochronous syllables, b) with the initial, medial or final syllable lengthened and c) with the initial, medial or final syllable shortened by 50% of its original duration (400 ms). Each participant ranked each word three times, namely on its ‘likability’, on its ‘beauty’ and on its ‘naturalness’, which together served as indicators of ‘aesthetic appeal’.
Cumulative Link Mixed Models revealed that, overall, isochronous syllables were preferred over de-viations of isochrony. Especially, shortened syllables had a prominent negative effect on aesthetic ap-peal. The only modification that participants judged as more aesthetically appealing than isochrony was word-final lengthening. These results were similar for ‘likability’, ‘beauty’ and ‘naturalness’.
The positive ratings of rhythmic patterns are unlikely to have been influenced by their occurrence fre-quencies in the participants’ native language (Bybee, 2007) because word-medial syllables, which are typically stressed and thus lengthened in German (Domahs, Plag, & Carroll, 2014) have not been evaluated as aesthetically appealing when lengthened in our experiment.
Interestingly, the aesthetic appeal of prosodic patterns in this study corresponded to their effectiveness for speech segmentation in other experiments, where words with finally lengthened syllables could be extracted from continuous speech more successfully than words with finally shortened syllables (Matzinger, Ritt, & Fitch, 2021). Together, these findings indicate a potential connection between aes-thetics and language learning. Thus, overall, this study serves as an important starting point for testing the role of aesthetic perception of linguistic input for the cultural evolution of linguistic patterns.

<h4>References</h4>

* Bybee, J. (2007). Frequency of use and the organization of language. Oxford: Oxford University Press.
* Domahs, U., Plag, I., & Carroll, R. (2014). Word stress assignment in German, English and Dutch: Quantity-sensitivity and extrametricality revisited. The Journal of Comparative Germanic Linguistics, 17(1), 59–96. https://doi.org/10.1007/s10828-014-9063-9
* Matzinger, T., Ritt, N., & Fitch, W. T. (2021). The Influence of Different Prosodic Cues on Word Segmentation. Frontiers in Psychology, 12. https://doi.org/10.3389/fpsyg.2021.622042
* Nadal, M., & Vartanian, O. (Eds.). (2019). The Oxford Handbook of Empirical Aesthetics. Oxford: Oxford University Press.
* Rastall, P. (2008). Aesthetic responses and the “cloudiness” of language: Is there an aesthetic function of language? La Linguistique, 44, 103–132. https://doi.org/10.3917/ling.441.0103
* Ravignani, A., & Madison, G. (2017). The paradox of isochrony in the evolution of human rhythm. Frontiers in Psychology, 8, 1–13. https://doi.org/10.3389/fpsyg.2017.01820
* Smith, K., & Kirby, S. (2008). Cultural evolution: implications for understanding the human language faculty and its evolution. Philosophical Transactions of the Royal Society of London. Series B, Biological Sciences, 363(1509), 3591–3603. https://doi.org/10.1098/rstb.2008.0145
* Westphal-Fitch, G., & Fitch, W. T. (2013). Spatial Analysis of “Crazy Quilts”, a Class of Potentially Random Aesthetic Artefacts. PLoS ONE, 8(9). https://doi.org/10.1371/journal.pone.0074055
</details>

### High-accuracy neuronavigated TMS

*Elisabet Delgado Mas, Agata Patyczek, Maria Vasileiadi, Martin Tik, Christian Windischberger*

High Field MR Center, Center for Medical Physics and Biomedical Engineering, Medical University of Vienna, University of Vienna

<details markdown="1">
Transcranial Magnetic Stimulation (TMS) is an established non-invasive brain stimulation (NIBS) technique that enables to interact with structure–function relationships across the whole gamut of cognitive functions and their associated cortical brain regions. TMS is based on focal, strong magnetic field changes from brief, high-current pulses through the TMS coil that modulates (either increases or reduces) excitability.

Its effects and measurable outputs can be used for research and clinical purposes. Those include presurgical functional mapping, treating major depressive disorder (MDD), assisting in the diagnosis of diseases associated with motor dysfunction and promoting motor recovery after a stroke. Nevertheless, clinical applications typically show modest test-retest reliability.
Although TMS offers the advantage of unrivalled focality, common approaches do not take into account interindividual differences in the correspondence between scalp landmarks and underlying brain anatomy. Moreover, the standard placement of the TMS coil against the subject’s scalp is sensitive to movement and misalignment.

To prevent this, TMS neuronavigation systems link Magnetic Resonance (MR) images and real anatomy, allowing 3D interactive visual guidance [3]. In this study it is employed in conjunction with a tailored software to ensure optimal positioning of the coil and, as a proxy for the physiological changes from TMS application, peripheral electromyogram (EMG) is recorded placing two electrodes in the first dorsal interosseous muscle (FDI) during stimulation of the primary motor cortex (M1).

In order to assess whether an increase in neuronavigation accuracy will increase the reproducibility of EMG recordings, 15 healthy subjects will undergo MRI scanning and two neuronavigated TMS rounds with different accuracy settings. In each round, nine targets around the M1 hotspot will be stimulated 10 times each. Trial-to-trial variability will be assessed across the different accuracy setting sessions.
</details>

### Social Transparency in Network Monitoring and Security Systems

*Daria Soroko, Gian-Luca Savino, Nicholas Gray*

University of Bremen, University of St. Gallen, Julius Maximilian University of Würzburg

<details markdown="1">
System administrators (sysadmins) are a key to keeping the network systems safe and secure. As networks grow in size and complexity, and the number of cyber attacks increases, the task of securing the network becomes more challenging. Partial workflow automation with the help of Artificial Intelligence (AI) has become a recent remedy to address these concerns and help system administrators to tackle large volumes of data and keep the system safeguarded. However, although beneficial, AI-aided software solutions in the network security context often come at the cost of transparency and control. The algorithms in such software are opaque and do not provide enough information about the system\'s decision-making process to the admin. This results in the lack of understanding about the system and cultivates mistrust on the system administrator\'s behalf, which leads to the overall reluctance to use the new software solutions, slower response time in case of a security breach, and inability to resolve conflicts in the system. A recent study by Ehsan et al. focused on the concept of social transparency (ST) and showed that when applied to the cyber security context, peer support could help alleviate the issue of mistrust in the system and provide the missing knowledge to the sysadmin when faced with a novel situation. In our recent work, we tested the concept of social transparency and its application in this domain. The first results show that ST can indeed yield benefits for sysadmins but only when coupled with other contextual information available in the system and only when it adheres to certain quality standards in terms of content it provides. As we continue our analysis, we aim to compile a set of user requirements that are desirable for a successful AI system adoption in the network security domain.
</details>

### You know a neuron, you build a neuron: Octopus Cell Modelling

*Yidi Ke, Go Ashida*

Department of Neuroscience, University of Oldenburg

<details markdown="1">
Octopus cells (OCs) are known as monaural coincidence detectors in the mammalian auditory system. Located in the cochlear nucleus of the brainstem, OCs receive excitatory synaptic inputs from auditory nerve fibers (ANFs) in a broad range of frequency. Its thick dendrite tree that is tonotopically organized from high-to-low frequency compensates cochlear traveling wave delay. In addition, OCs have small membrane time constant and brief excitatory postsynaptic potentials (EPSPs), which cancel out asynchronous spontaneous synaptic inputs. Based on the propagation pathway and the known characteristics of OCs, a simple OC model was built by combining an auditory periphery model (BEZ2018model) and adaptive coincidence counting model (ACC model). The model replicated several crucial experimental results of (1) bimodal peri-stimulus time histogram (PSTH) patterns for different frequencies of tonal stimuli, (2) responding to broadband stimuli with sub-millisecond temporal precision, (3) a wide frequency tuning curve (FTC).
</details>
