
# Virtual IK 2022 Materials

These resources are intended for registered VIK 2022 participants only. Please do <strong>not</strong> redistribute access to this webpage and related resources! Do you think something is missing, broken, or other feedback? Please get in contact with the [organizers](mailto:webmaster@interdisciplinary-college.de)!

* [List of participants](ik-2022-participants.pdf)
* [Course Materials](#material)
* [Poster abstracts](#posters)

## List of participants

<a name="participants"></a>

You can download a list of participants [here](ik-2022-participants.pdf).

<!--## Interaction Group Locations

<a name="groups"></a>

On March 7, 8pm CET, you are invited to join the *interaction groups* to get to know IK and the other participants. You can find your interaction group number in the participant list. You can find your location and your guide in this table:

<table style="text-align: left;">
<tr><th>group</th><th>guides</th><th>location</th></tr>
<tr><td> 01 </td><td> Valerie Vaquet / Johannes Brinkrolf </td><td> hall 3 </td></tr>
<tr><td> 02 </td><td> Christina Zeller / Jochen Sprickerhof </td><td> soccer field </td></tr>
<tr><td> 03 </td><td> Simon Büchner / Jan Leininger </td><td> pub </td></tr>
<tr><td> 04 </td><td> Theresa Matzinger / Bettina Bläsing </td><td> pool </td></tr>
<tr><td> 05 </td><td> Birgit Peterson / Marie Sindermann </td><td> library </td></tr>
<tr><td> 06 </td><td> Jutta Kretzberg / Kayson Fakhar </td><td> hall 2 </td></tr>
<tr><td> 07 </td><td> Paulina Friemann / Marius Klug </td><td> fireplace </td></tr>
<tr><td> 08 </td><td> Tea Gjishti / Wanja Wiese </td><td> hall 1 </td></tr>
<tr><td> 09 </td><td> Terry Stewart / Sarah Pilz </td><td> cafeteria </td></tr>
<tr><td> 10 </td><td> Moritz Kriegleder / Ronald Sladky </td><td> forum </td></tr>
</table>-->

## Course materials

For the following lectures, additional resources like video recordings, slides, or other notes are available. Please click on the title of a lecture to find details. Poster abstracts and a list of participants can be found at the bottom of the page.

<a name="material"></a>
