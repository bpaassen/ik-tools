#!/bin/bash

{
  grep -v '^</\?script'  # processes stdin
  echo 'console.log(JSON.stringify(courses));'
} | node | {
  echo -e "lecturer\tcourse_id\tcourse_title\tvideo_url"
  jq -r '.[] | [.instructor, .identifier, .title, ""] | @tsv'
}

