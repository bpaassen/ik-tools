#!/usr/bin/env python3

import sys
from os.path import isdir, join, basename
from os import scandir, mkdir

import markdown
md = markdown.Markdown(extensions=['md_in_html'])


DEFAULT_ASSETS_DIR = './static'
DEFAULT_SOURCE_DIR = './src'

COURSE_NOTES_FILE_NAME = 'notes.md'
HEADER_MARKDOWN_FILE_NAME = 'header.md'
FOOTER_MARKDOWN_FILE_NAME = 'footer.md'
COURSES_TSV_FILE_NAME = 'lectures.tsv'
INDEX_HTML_FILE_NAME = 'index.html'  # silly, but let's stick to the pattern


HTML_DOCUMENT_TEMPLATE = """
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>IK Additional Materials</title>
        <link rel="stylesheet" media="all" href="style.css" />
    </head>
    <body>
        <div id="header">
            <img src="https://interdisciplinary-college.org/wp-content/uploads/2020/11/generic_header-3.png" alt="logo">
        </div>
        <div id="main">
{text_header}
            <div id="courses">
{sections}
            </div>
{text_footer}
            <script src="script.js"></script>
        </div>
    </body>
</html>"""

HTML_SECTION_TEMPLATE = """
<details>
<summary>
    <h3><span class="tag {css_class}">{course_id}</span> {course_title}</h3>
</summary>
<p>
  This lecture is held by <em>{lecturer}</em>. 
  <a href="{desc_link}" target="_blank">More information</a> on this lecture.
</p>
{notes}
{list_items}
{embedded_video}
</details>
"""

HTML_LIST_TEMPLATE = """
<p>The following files were provided by the lecturer for this course:</p>
<ul>
{list_items}
</ul>
"""

HTML_LIST_ITEM_TEMPLATE = """
<li><a href="{url}">{title}</a></li>
"""

HTML_VIDEO_EMBED_TEMPLATE = """
<div class="youtube-embed">
    <div class="info-embed">
        <p>To protect your privacy, the loading of embedded external content has been delayed.</p>
        <div class="info-actions"></div>
        <p><a href="{video_url}">Click here to open the video in YouTube.</a></p>
    </div>
</div>
"""


def read_markdown_safe(path):
    try:
        with open(path, 'r', encoding='utf-8') as f:
            text_md = f.read()
        text_html = md.convert(text_md)
    except FileNotFoundError:
        text_html = ''
    return text_html


def parse_section(line, assets_dir):
    items = [item.strip() for item in line.split('\t')]
    # TODO this breaks if new columns are added, should be smarter
    lecturer, course_id, course_title, desc_link, video_url = items
    course_dir = join(assets_dir, course_id)

    list_items = []
    notes = ""

    if isdir(course_dir):
        with scandir(course_dir) as it:
            for entry in it:
                if entry.name == COURSE_NOTES_FILE_NAME:
                    continue
                
                list_item = {
                    'title': entry.name,
                    'url': f"{course_id}/{entry.name}"
                }
                list_items.append(list_item)
        
        notes_path = join(course_dir, COURSE_NOTES_FILE_NAME)
        notes = read_markdown_safe(notes_path)
        list_items = sorted(list_items, key=lambda x: x['title'])
        
    video_url = video_url.strip()
    if video_url:
        if not video_url.startswith('https://www.youtube.com/watch?v='):
            raise ValueError(f"Error: Video URL for course {course_id} ({course_title}) has unexpected format.")
        # Show better video recommendations after the embedded video ends.
        # More information here:
        # https://digitalbard.com/how-to-stop-unrelated-videos-from-showing-up-at-the-end-of-your-youtube-video/
        video_url += '&rel=0'

    section = {
        'lecturer': lecturer,
        'course_id': course_id,
        'course_title': course_title,
        'video_url': video_url,
        'desc_link': desc_link,
        'files': list_items,
        'notes': notes,
    }
    
    return course_id, section
    

def parse_sections(tsv_path, assets_dir):
    sections = []

    with open(tsv_path, 'r') as f:
        # skip header, assuming fixed columns for now
        f.readline()

        for line in f:
            course_id, section = parse_section(line, assets_dir)

            if not course_id:
                sys.stderr.write(f"[SKIP]  Entry without course ID, skipping.\n")
                continue

            if not section['files'] and not section['notes'] and section['video_url'] == '':
                sys.stderr.write(f"[SKIP]  No data found for {course_id}.\n")
                continue
            
            notes_str = "files and notes" if section['notes'] else "files"
            sys.stderr.write(f"[ OK ]  Including {len(section['files'])} {notes_str} for {section['course_id']}.\n")

            sections.append(section)

    return sections
    

def htmlise_list_item(list_item):
    return HTML_LIST_ITEM_TEMPLATE.strip().format(**list_item)


def htmlise_list(list_items):
    html_items = "\n".join([htmlise_list_item(li) for li in list_items])
    variables = {
        'list_items': html_items
    }
    return HTML_LIST_TEMPLATE.strip().format(**variables)


def htmlise_section(section):
    css_class = ''.join(c for c in section['course_id'] if c.isalpha())
    css_class = "tag-" + css_class.lower()

    html_section = """
    <details>
    <summary>
        <h3><span class="tag %s">%s</span> %s</h3>
    </summary>
    <p>""" % (css_class, section['course_id'], section['course_title'])

    if section['lecturer']:
        html_section += "This lecture is held by <em>%s</em>." % section['lecturer']

    if section['desc_link']:
        html_section += """ <a href="%s" target="_blank">More information</a> on this lecture.""" % section['desc_link']

    html_section += section['notes'] if section['notes'] else ""
    html_section += htmlise_list(section['files']) if section['files'] else ""
    html_section += htmlise_video(section) if section['video_url'] else ""

    html_section += "\n</details>"
    
    return html_section


def htmlise_video(section):
    return HTML_VIDEO_EMBED_TEMPLATE.strip().format(**section)


def main(assets_dir, source_dir):
    sections = parse_sections(join(source_dir, COURSES_TSV_FILE_NAME), assets_dir)
    html_sections = [htmlise_section(s) for s in sections]
    html_section_block = "".join(html_sections)

    html_header = read_markdown_safe(join(source_dir, HEADER_MARKDOWN_FILE_NAME))
    html_footer = read_markdown_safe(join(source_dir, FOOTER_MARKDOWN_FILE_NAME))
    
    variables = {
        'text_header': html_header,
        'text_footer': html_footer,
        'sections': html_section_block
    }

    output_file_path = join(assets_dir, INDEX_HTML_FILE_NAME)
    html = HTML_DOCUMENT_TEMPLATE.strip().format(**variables)
    with open(output_file_path, 'w') as f:
        f.write(html)

    
def print_help():
    def prerr(text=""):
        sys.stderr.write(text + "\n")

    script_name = basename(sys.argv[0])

    prerr("Usage:")
    prerr(f"  {script_name} [ <assets-dir> [ <source-dir> ] ]")
    prerr()
    prerr("Reads certain files from <source-dir>. Checks contents of sub-directories in")
    prerr(f"<assets-dir>. Produces '{INDEX_HTML_FILE_NAME}' in <assets-dir>.")
    prerr()
    prerr(f"If no <assets-dir> is given, '{DEFAULT_ASSETS_DIR}' will be used.")
    prerr(f"If no <source-dir> is given, '{DEFAULT_SOURCE_DIR}' will be used.")
    prerr()
    prerr("This script makes use of a Markdown library for Python. Details here:")
    prerr()
    prerr("  https://python-markdown.github.io/")
    prerr()


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1].lower() in ['-h', '-help', '--h', '--help']:
        print_help()
    else:

        try:
            assets_dir = sys.argv[1]
        except IndexError:
            assets_dir = DEFAULT_ASSETS_DIR

        try:
            source_dir = sys.argv[2]
        except IndexError:
            source_dir = DEFAULT_SOURCE_DIR
            
        main(assets_dir, source_dir)
        
