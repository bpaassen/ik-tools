<?php
/*

This API loads calendar data from a google calendar and provides it as ical raw data.
Note that this API must be updated every year

Created 2019-2023 by Benjamin Paassen - bpaassen@techfak.uni-bielefeld.de

*/

// We permit this API to be openly accessible because it only offers
// a small bandwidth of public data in a static format. Still, please handle
// responsibly.
header("Access-Control-Allow-Origin: *");
header("Content-Type: text/calendar; charset=utf-8");

// loads the .ical data from google calendar
$cal = file_get_contents('https://calendar.google.com/calendar/ical/47bb9840f5d8c8b8c07e7448a12c63512e6e005bdef98bea61cff92f67254355%40group.calendar.google.com/public/basic.ics');

// echos the .ical data
echo $cal;

?>
