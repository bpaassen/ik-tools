<?php
/* Template Name: Poster Submission Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, March 2020 -->
			<?php

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// title
				if(!isset($_POST['title'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the title was not given. Please check your form again and re-submit.';
					exit;
				}
				$title = sanitize_text_field( $_POST['title'] );

				// presenting_author
				if(!isset($_POST['presenting_author'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the presenting_author was not given. Please check your form again and re-submit.';
					exit;
				}
				$presenting_author = sanitize_text_field( $_POST['presenting_author'] );

				// authors
				if(!isset($_POST['authors'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the author names were not given. Please check your form again and re-submit.';
					exit;
				}
				$authors = sanitize_text_field( $_POST['authors'] );

				// affiliations
				if(!isset($_POST['affiliations'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the affiliations were not given. Please check your form again and re-submit.';
					exit;
				}
				$affiliations = sanitize_text_field( $_POST['affiliations'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// abstract
				if(!isset($_POST['abstract'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the abstract was not given. Please check your form again and re-submit.';
					exit;
				}
				$abstract = sanitize_textarea_field( $_POST['abstract'] );


				if(!isset($_POST['special_requests'])) {
					$special_requests = "";
				} else {
					$special_requests = sanitize_textarea_field( $_POST['special_requests'] );
				}

				$submission_object = (object)[];
				$submission_object->presenting_author = $presenting_author;
				$submission_object->email = $email;
				$submission_object->authors = $authors;
				$submission_object->affiliations = $affiliations;
				$submission_object->title = $title;
				$submission_object->abstract = $abstract;
				$submission_object->special_requests = $special_requests;

				// preprocessing ends here

				// construct email to conference management
				$description_email = "<p>New poster submission from $email</p>\n\n";
				$description_email .= "<p>Special requests: $special_requests</p>\n\n";
				$description_email .= "<div class=\"poster_div\"><h2>$title</h2>\n";
				$description_email .= "<div class=\"author_div\"><p class=\"authors\">$authors</p>\n";
				$description_email .= "<p class=\"affiliations\">$affiliations</p></div>\n\n";

				$description_email .= "<div class=\"abstract\"><p>";
				$description_email .= preg_replace(array('/\s*\n\s*\n\s*/', '/\s*\n\s*/'), array("</p><p>", "<br/>"), $abstract);
				$description_email .= "</p></div></div>\n\n";

				$json_data = json_encode($submission_object, JSON_PRETTY_PRINT);

				$description_email .= "<p>JSON data</p><pre>$json_data</pre>";

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				$description_length = strlen($description_email);
				if($description_length > 15000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the data was too long (> 15000 characters). Please check your form again and re-submit.';
					exit;
				}

				// special code for debug input
				if($presenting_author === 'Debug') {
					echo "<p id=\"success\">The debug is complete. The following email would be send to <a href=\"mailto:poster@interdisciplinary-college.org\">poster@interdisciplinary-college.org</a>:</p>$description_email";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8', "Reply-To: $email");

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				if(!wp_mail('poster@interdisciplinary-college.org', 'IK 2025 Poster Submission', $description_email, $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your poster submission has failed. Please send your poster submission manually to <a href=\"mailto:webmaster@interdisciplinary-college.org\">webmaster@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$description_email</pre>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p id=\"success\">Your poster submission was sent successfully to <a href=\"mailto:poster@interdisciplinary-college.org\">poster@interdisciplinary-college.org</a>.</p>";
				}

				// add copy for registration desk
				wp_mail('registration@interdisciplinary-college.org', 'IK 2025 Poster Submission (copy)', $description_email, $headers);

				// add copy for the person itself
				wp_mail($email, 'IK 2025 Poster Submission (copy)', $description_email, $headers);
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
