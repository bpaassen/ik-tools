<?php
/* Template Name: KI 2020 Registration Form */

get_header(); ?>

<!--
Contains the frontend for the KI 2020 registration form.

Copyright (C) 2020
Benjamin Paaßen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, July 2020 -->
			<?php

				// definition of conference fee constants
				$conference_fees = [
					"member_author" => 100,
					"author" => 150,
					"member" => 0,
					"employee" => 0,
					"regular" => 50,
				];

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}

				// first name
				if(!isset($_POST['firstname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the first name was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$first_name = sanitize_text_field( $_POST['firstname'] );

				// last name
				if(!isset($_POST['lastname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the last name was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$last_name = sanitize_text_field( $_POST['lastname'] );

				// phone
				if(!isset($_POST['phone'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$phone = sanitize_text_field( $_POST['phone'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// Registration fee
				if(!isset($_POST['fee'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the fee information was not specified. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$fee_string = sanitize_text_field( $_POST['fee'] );

				if(!($fee_string === 'member_author' || $fee_string === 'author' || $fee_string === 'member' || $fee_string === 'employee' || $fee_string === 'regular')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the fee information had an invalid value (must be member_author, author, member, employee, or regular). Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}

				// process billing address

				// institution
				if(!isset($_POST['billing_institution'])) {
					$billing_institution = "";
				} else {
					$billing_institution = sanitize_text_field( $_POST['billing_institution'] );
				}

				// department
				if(!isset($_POST['billing_department'])) {
					$billing_department = "";
				} else {
					$billing_department = sanitize_text_field( $_POST['billing_department'] );
				}

				// street
				if(!isset($_POST['billing_street'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the street of your billing address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$billing_street = sanitize_text_field( $_POST['billing_street'] );

				// house no.
				if(!isset($_POST['billing_no'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the house number of your billing address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$billing_no = sanitize_text_field( $_POST['billing_no'] );

				// zip
				if(!isset($_POST['billing_zip'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the zip code of your billing address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$billing_zip = sanitize_text_field( $_POST['billing_zip'] );

				// city
				if(!isset($_POST['billing_city'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the city of your billing address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$billing_city = sanitize_text_field( $_POST['billing_city'] );

				// country
				if(!isset($_POST['billing_country'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the country of your billing address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}
				$billing_country = sanitize_text_field( $_POST['billing_country'] );

				// check paper ID
				if($fee_string === 'member_author' || $fee_string === 'author') {
					if(!isset($_POST['paper_id'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your paper ID was missing. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
						exit;
					}
					$paper_id = sanitize_text_field( $_POST['paper_id'] );
				}

				// check member number
				if($fee_string === 'member_author' || $fee_string === 'member') {
					if(!isset($_POST['member_orga'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your organization (GI, SI, or OEGI) was missing. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
						exit;
					}
					$member_orga = sanitize_text_field( $_POST['member_orga'] );
					if(!($member_orga === 'GI' || $member_orga === 'SI' || $member_orga === 'OEGI')) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the organization membership had an invalid value (must be GI, SI, or OEGI). Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
						exit;
					}
					if(!isset($_POST['member_number'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your member number was missing. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
						exit;
					}
					$member_number = sanitize_text_field( $_POST['member_number'] );
				}

				// check workshops
				$workshops = "";
				if(isset($_POST['w1'])) {
					$workshops .= "W1, ";
				}
				if(isset($_POST['w2'])) {
					$workshops .= "W2, ";
				}
				if(isset($_POST['w3'])) {
					$workshops .= "W3, ";
				}
				if(isset($_POST['w4'])) {
					$workshops .= "W4, ";
				}
				if(isset($_POST['w5'])) {
					$workshops .= "W5";
				}
				if(substr($workshops, -2) == ", ") {
					$workshops = substr($workshops, 0, -2);
				}

				// check tutorials
				$tutorials = "";
				if(isset($_POST['t1'])) {
					$tutorials .= "T1, ";
				}
				if(isset($_POST['t2'])) {
					$tutorials .= "T2, ";
				}
				if(isset($_POST['t3'])) {
					$tutorials .= "T3, ";
				}
				if(isset($_POST['t4'])) {
					$tutorials .= "T4, ";
				}
				if(isset($_POST['t5'])) {
					$tutorials .= "T5, ";
				}
				if(isset($_POST['t6'])) {
					$tutorials .= "T6, ";
				}
				if(isset($_POST['t7'])) {
					$tutorials .= "T7";
				}
				if(substr($tutorials, -2) == ", ") {
					$tutorials = substr($tutorials, 0, -2);
				}

				// check other
				$other = "";
				if(isset($_POST['dc'])) {
					$other .= "doctoral consortium, ";
				}
				if(isset($_POST['student_day'])) {
					$other .= "student day, ";
				}
				if(isset($_POST['printed_proceedings'])) {
					$other .= "printed proceedings, ";
				}
				if(substr($other, -2) == ", ") {
					$other = substr($other, 0, -2);
				}

				// check shipping address
				if(isset($_POST['printed_proceedings'])) {
					// if some shippable item is included, ensure that a shipping address is given
					if(isset($_POST['shipping_same'])) {
						// if shipping address and billing address should be the same, just copy the data
						$shipping_street = $billing_street;
						$shipping_no = $billing_no;
						$shipping_zip = $billing_zip;
						$shipping_city = $billing_city;
						$shipping_country = $billing_country;
					} else {
						// otherwise, extract them
						// street
						if(!isset($_POST['shipping_street'])) {
							echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the street of your shipping address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
							exit;
						}
						$shipping_street = sanitize_text_field( $_POST['shipping_street'] );
						// no
						if(!isset($_POST['shipping_no'])) {
							echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the house no. of your shipping address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
							exit;
						}
						$shipping_no = sanitize_text_field( $_POST['shipping_no'] );
						// zip
						if(!isset($_POST['shipping_zip'])) {
							echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the zip code of your shipping address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
							exit;
						}
						$shipping_zip = sanitize_text_field( $_POST['shipping_zip'] );
						// city
						if(!isset($_POST['shipping_city'])) {
							echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the city of your shipping address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
							exit;
						}
						$shipping_city = sanitize_text_field( $_POST['shipping_city'] );
						// country
						if(!isset($_POST['shipping_country'])) {
							echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the country of your shipping address was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
							exit;
						}
						$shipping_country = sanitize_text_field( $_POST['shipping_country'] );
					}
				}

				// check payment type
				$payment = sanitize_text_field( $_POST['payment'] );
				if(!($payment === 'Bank transfer' || $payment === 'Credit card')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type had an invalid value (must be either Bank transfer or Credit card). Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}

				// construct registration e-mail from user data
				$registration_mail  = "Last name:          $last_name\n";
				$registration_mail .= "First name:         $first_name\n";
				$registration_mail .= "Institution:        $billing_institution\n";
				$registration_mail .= "Faculty/Department: $billing_department\n";
				$registration_mail .= "Street/PO Box:      $billing_street $billing_no\n";
				$registration_mail .= "ZIP:                $billing_zip\n";
				$registration_mail .= "City:               $billing_city\n";
				$registration_mail .= "Country:            $billing_country\n";
				$registration_mail .= "Phone:              $phone\n";
				$registration_mail .= "E-Mail:             $email\n\n";

				$total_fee = $conference_fees[$fee_string];
				if(isset($_POST['printed_proceedings'])) {
					$total_fee += 70;
				}

				$registration_mail .= "Total fee:     $fee_string $total_fee Euro.";
				if($total_fee > 0) {
					$registration_mail .= "\nPayment type:  $payment\n";
					$registration_mail .= "You will receive an invoice via e-mail.";
				}

				$registration_mail .= "\n\nWorkshops: $workshops\n";
				$registration_mail .= "Tutorials: $tutorials\n";
				$registration_mail .= "Other: $other\n\n";


				if(isset($_POST['printed_proceedings'])) {
					$registration_mail .= "Shipping Address:\n";
					$registration_mail .= "$shipping_street $shipping_no\n";
					$registration_mail .= "$shipping_zip $shipping_city\n";
					$registration_mail .= "$shipping_country\n\n";
				}
				if($fee_string === 'member_author' || $fee_string === 'author') {
					$registration_mail .= "Paper ID: $paper_id\n";
				}
				if($fee_string === 'member_author' || $fee_string === 'member') {
					$registration_mail .= "$member_orga number: $member_number\n";
				}

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				if(strlen($registration_mail) > 5000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the data was too long (> 5000 characters). Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
					exit;
				}

				// create a machine readable version in JSON format
				$registration_object = (object)[];
				$registration_object->first_name = $first_name;
				$registration_object->last_name = $last_name;
				$registration_object->phone = $phone;
				$registration_object->email = $email;
				$registration_object->fee_string = $fee_string;
				$registration_object->total_fee = $total_fee;
				$registration_object->payment = $payment;
				$registration_object->billing_address = (object)[];
				$registration_object->billing_address->billing_institution = $billing_institution;
				$registration_object->billing_address->billing_department = $billing_department;
				$registration_object->billing_address->billing_street = $billing_street;
				$registration_object->billing_address->billing_no = $billing_no;
				$registration_object->billing_address->billing_zip = $billing_zip;
				$registration_object->billing_address->billing_city = $billing_city;
				$registration_object->billing_address->billing_country = $billing_country;
				if(isset($_POST['printed_proceedings'])) {
					$registration_object->shipping_address = (object)[];
					$registration_object->shipping_address->shipping_street = $shipping_street;
					$registration_object->shipping_address->shipping_no = $shipping_no;
					$registration_object->shipping_address->shipping_zip = $shipping_zip;
					$registration_object->shipping_address->shipping_city = $shipping_city;
					$registration_object->shipping_address->shipping_country = $shipping_country;
				}
				$registration_object->workshops = $workshops;
				$registration_object->tutorials = $tutorials;
				$registration_object->other = $other;
				if($fee_string === 'member_author' || $fee_string === 'author') {
					$registration_object->paper_id = $paper_id;
				}
				if($fee_string === 'member_author' || $fee_string === 'member') {
					$registration_object->member_orga = $member_orga;
					$registration_object->member_number = $member_number;
				}

				// special code for debug input
				if($first_name === 'Debug') {
					echo "<p>Your registration was sent successfully to the conference organizers. Please note that your registration is only confirmed once you received your invoice. For your own archive: The following data was transmitted:</p> <pre>$registration_mail</pre>";
					$json_data = json_encode($registration_object);
					echo "<p>JSON data</p><pre>$json_data</pre>";
					echo "<p><a href=\"https://ki2020.uni-bamberg.de/\">&rarr; Back to the KI 2020 page</a></p>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				if(!wp_mail('christine.harms@ccha.de', 'KI 2020 Registration', '<pre>' . $registration_mail . '</pre>', $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your registration has failed. Please send your registration manually to <a href=\"mailto:christine.harms@ccha.de\">christine.harms@ccha.de</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$registration_mail</pre>";
					echo "<p><a href=\"https://ki2020.uni-bamberg.de/\">&rarr; Back to the KI 2020 page</a></p>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p>Your registration was sent successfully to the conference organizers. Please note that your registration is only confirmed once you received your invoice. For your own archive: The following data was transmitted:</p> <pre>$registration_mail</pre>";
					echo "<p><a href=\"https://ki2020.uni-bamberg.de/\">&rarr; Back to the KI 2020 page</a></p>";
				}

				// Then send a copy to the conference organizers
				wp_mail('Franziska.Klugl@oru.se', 'KI 2020 Registration', '<pre>' . $registration_mail . '</pre>', $headers);

				$headers = array('Content-Type: text/plain; charset=UTF-8');

				// send a machine-readable backup copy to the webmaster
				wp_mail('ki2020@uni-bamberg.de', 'KI 2020 Registration (machine readable copy)', json_encode($registration_object), $headers);
				wp_mail('webmaster@interdisciplinary-college.org', 'KI 2020 Registration (machine readable copy)', json_encode($registration_object), $headers);

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
