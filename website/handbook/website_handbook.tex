\documentclass[a4paper,12pt]{article}

\RequirePackage{geometry}
\geometry{margin=2cm}

\usepackage{graphicx}
\usepackage[american]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}

% listings
\usepackage{listings}
\lstset{
basicstyle=\ttfamily\small,
tabsize=2,
language=php,
keywordstyle=\color{blue},
identifierstyle=\bfseries,
%backgroundcolor=\color{WhiteSmoke},
}

\addto\captionsamerican{%
\renewcommand{\contentsname}{How do I $\ldots$ ?}%
}

\usepackage{xcolor}

%opening
\title{Handbook for IK Website Changes}
\date{\vspace{-1cm}Version 2021-08-21}

\begin{document}

\maketitle

\section*{Checklist for a new IK}

\begin{enumerate}
\item Make sure all pages are hidden except for \emph{Startpage}, \emph{About IK}, \emph{Venue}, and \emph{Previous IKs} (after last IK; refer to Section~\ref{sec:hiding})
\item Set up a new startpage (summer; refer to Section~\ref{sec:startpage})
\item Add the course descriptions and metadata; make the \emph{Program} page visible (fall; refer to Section~\ref{sec:courses})
\item Set up the registration page (around October; refer to Section~\ref{sec:registration})
\item Close the registration and set to waiting list (around January; refer to Section~\ref{sec:waiting_list})
\item Set up the new schedule (December to January; refer to Section~\ref{sec:schedule})
\item Upload the course materials (after IK; refer to Section~\ref{sec:materials})
\end{enumerate}

\tableofcontents

\clearpage

\section{Log in to and out of the Wordpress Dashboard}

Please visit \url{https://interdisciplinary-college.org/wp-admin}
(ensure that you're using the https protocol in order to encrypt your password),
type in your username and password, and press 'Log in'.

If you have not yet received a username and password, please contact the
IK webmasters at \href{mailto:webmaster@interdisciplinary-college.org}{webmaster@interdisciplinary-college.org}.

\begin{center}
\includegraphics[width=0.5\textwidth]{login}
\end{center}

You can log out again by hovering over your username on the top right (1) and clicking 'Log Out' (2).

\begin{center}
\includegraphics[width=0.5\textwidth]{logout}
\end{center}


\clearpage

\section{Create a new user account}

In the Wordpress dashboard, click the 'Users' tab (1) on the left and then 'Add New' (2).
Fill out the new user's name (3) as well as the email address (4)
and select 'Administrator' (5) as role.
Make sure that 'Send User Notification' is checked (6). Then, click on 'Add New User' (7).
The new user should then receive an automatically generated mail containing a link to
set their own password. Please make sure that they also receive a copy of this handbook.

\vspace{1cm}
\begin{minipage}{0.45\textwidth}
\includegraphics[height=\textwidth]{new_user_1}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\includegraphics[height=\textwidth]{new_user_2}
\end{minipage}

\clearpage

\section{Edit a page}
\label{sec:pages}

In the Wordpress dashboard, click on 'Pages' (1) on the left and then on the page you wish
to edit (2). You will now enter the so-called \emph{Gutenberg editor} (3), which behaves
pretty much like a word processor, such as \emph{LibreOffice} or \emph{Word}. The main
difference is that the page content consists of 'blocks' and that there exist different
such blocks for different purposes (like headings, regular paragraphs, images,
and custom HTML blocks). If you wish to know more about the Gutenberg
editor, please consult an on-line tutorials, like \href{https://www.wpbeginner.com/beginners-guide/how-to-use-the-new-wordpress-block-editor/}{this one}.

Once you made your changes, you can preview them using the 'Preview' button (4) and publish
them to the website using the 'Update' (5) button.

\begin{center}
\includegraphics[width=0.6\textwidth]{edit_page_1}

\vspace{0.5cm}
\includegraphics[width=0.9\textwidth]{edit_page_2}
\end{center}

\clearpage

\section{Hide a page}
\label{sec:hiding}

To hide a page, click on it in the 'Pages' tab in the Wordpress dashboard.
To the right of the Gutenberg editor, you will then find a visibility option (1).
Click on it, select 'Private' (2) and then click 'Update' (3). The site will now
only be visible to logged in members of the IK team.

In preparation for a new IK, you only need to hide the 'Registration' page.

\begin{center}
\includegraphics[width=0.4\textwidth]{hiding}
\end{center}

\clearpage

\section{Add an image}
\label{sec:images}

If you wish to add an image, you have to upload it to the server first. To do so,
please click on 'Media' (1) on the left in the Wordpress dashboard, then
on 'Add New' (2), and then on 'Select Files' (3) to upload your image file.
Please ensure that your image is not excessively large (not more than about 2000px
in any dimension) to avoid long loading times for users of the website.

Once your image is uploaded, it will show up as new element in the Media Library (4).
Please click your new image file and use the newly opened window to add an alternative text (5),
which will show up on mouse hover over the image, a title (6), a caption (7), which
will be printed below the image whenever it is used on the website, and a
description (8). Typically, alternative text is a copy of the title and description
is a copy of the caption. \emph{Please give appropriate credit to designers/photographers
in the caption!}

You do \emph{not} need to explicitly save your information here.
Once your done, just close the window.

If you wish to delete an image, you can also do it in this window (9). \emph{Be advised
that a deletion can not be undone!}

\begin{center}
\includegraphics[width=0.9\textwidth]{new_image_1}

\vspace{0.5cm}
\includegraphics[width=0.5\textwidth]{new_image_2}
\end{center}

Now, after uploading your image, you can use it on the website. To do so, add an image
block in the Gutenberg editor (refer to Section~\ref{sec:pages}), click 
'Media Library' (10) and select your image from the library (11 and 12).
If you wish to re-scale the image to fit on the page, you can do so on the right (13).
Usually, 740px is the correct width in order to cover the entire width of the
text column. Note that you need to calculate the corresponding height yourself.

\vspace{0.5cm}
\begin{center}
\includegraphics[width=0.5\textwidth]{new_image_3}

\vspace{0.5cm}
\includegraphics[width=0.9\textwidth]{new_image_4}

\vspace{0.5cm}
\includegraphics[width=0.9\textwidth]{new_image_5}
\end{center}

\clearpage

\section{Set up the startpage for a new IK}
\label{sec:startpage}

First, some preparatory activities. You'll need five image files, which you'll need
to add to the media gallery first (refer to Section~\ref{sec:images}), namely:
\begin{itemize}
\item The logo for the next IK, as it should appear on the landing page (i.e.\ the first
thing people see when they visit \url{https://interdisciplinary-college.org}),
\item one portrait photo for each conference chair, and
\item a text header for the next IK, including the motto/title.
\end{itemize}
Further, you'll need to ask the conference chairs for a title and a description text for the next IK.

Once you have all this material, you can proceed. In the Wordpress dashboard, click
'Pages' on the left (1) and then 'Interdisciplinary College - Front Page' (2).
In the Editor, replace the old conference date with the new one (3),
replace the title with the new one (4), and replace the description text with the new
one (5).
Additionally, you need to update the pictures of the scientific chairs. This, unfortunately,
requires some manual HTML editing, but it is relatively straightforward. First, update the
links to the images of the chairs and put their names as title and alternative text (6).
\emph{Please make sure to use a link starting with 'https' in order to ensure proper encryption
of the website}.
Then, update the chairs' names, affiliations, and websites below the pictures (7).
Finally, you can preview and publish the page.

\begin{center}
\includegraphics[width=0.6\textwidth]{frontpage_1}

\vspace{0.5cm}
\includegraphics[width=0.7\textwidth]{frontpage_2}

\vspace{0.5cm}
\includegraphics[width=0.7\textwidth]{frontpage_3}
\end{center}

\clearpage

In a next step, we will update the website header. For that purpose, you need to
click on 'Appearance' on the left (8) and then on 'Customize' (9). In the newly
opened window, please select 'Additional CSS' (10). In the new window, please update
the 'background-image:' links to the new header (11). \emph{Again, ensure that you
use a 'https' link}. Afterwards, you can publish the changes (12) and go back to
the customization menu (13).

\vspace{0.5cm}
\hspace{-0.5cm}
\begin{minipage}{0.38\textwidth}
\includegraphics[height=10cm]{frontpage_4}
\end{minipage}
\begin{minipage}{0.34\textwidth}
\includegraphics[height=10cm]{frontpage_5}
\end{minipage}
\begin{minipage}{0.25\textwidth}
\includegraphics[height=10cm]{frontpage_6}
\end{minipage}

\vspace{0.5cm}
In a final step, please click on 'Header Media' (14) and select the IK logo as
it should appear on the landing page (15). After that, you can publish the
changes (16) and close the customization menu (17).

\begin{center}
\begin{minipage}{0.40\textwidth}
\includegraphics[height=9cm]{frontpage_7}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\includegraphics[height=9cm]{frontpage_8}
\end{minipage}
\end{center}

\clearpage

\section{Add a new course description}
\label{sec:courses}

This section describes how to add a course description based on the output of
a web form. If you wish to add a course description manually, please refer to
section~\ref{sec:legacy_courses}.

As a first step, you should make sure that the course description form is visible
and functional. For this purpose, you should set the visibility of the pages
\emph{Course Description Form} and \emph{Course Description Form Sent} to \emph{Public}.
Further, please make sure that the \texttt{course\_description\_form.php} file is
uploaded under 
\begin{center}
\texttt{/WordPress\_SecureMode\_01/wp-content/themes/twentyseventeen/course\_description\_form.php}
\end{center}

Then, the description form should then be available under the URL \url{https://interdisciplinary-college.org/course-description-form/}.
Please test the form by typing 'Debug' in the name field and sending it. Please also
check that the form still asks all information that you need this year. If not, please
adjust the form \emph{and} the PHP file accordingly.


Next, we need to prepare a category to group all course descriptions for the current
year. For that purpose, log in to the Wordpress dashboard, click on 'Posts' (1),
then on 'Categories' (2), put in a title for the current year (3), and click
'Add New Category' (4).

\begin{center}
\includegraphics[height=10cm]{courses_1}
\end{center}

Now you can ask the lecturers to fill out the form. The responses should automatically
be sent to the \texttt{lecture-org@interdisciplinary-college.org} mailing list (refer
to Section~\ref{sec:mailing_lists} to find out how to manage mailing lists).

Everytime a lecturer fills out the form, you should now get an e-mail that looks roughly
like this:

\begin{center}
\includegraphics[width=10cm]{courses_form_1.png}
\end{center}

We will now copy this data to the website. For each new course, click on 'Posts' (5) and on
'Add New' (6). Then, just copy all the course information up to 'Homepage', namely 
the title (including a course identifier that you have to generate yourself, 7),
the lecturer (8), the fields (8), the course description (9),
the literature list (11), the short bio of the lecturer, including a photo (12),
affiliation, and website (13). Make sure to check the formatting a bit because copy-paste
errors may occur.

Once all the info is in, assign a shortlink for
the course (the schema schould be: year, dash, course ID) (14),
add it to the current year's category (15) and publish the post (16).

\begin{center}
\includegraphics[width=0.5\textwidth]{courses_2}

\includegraphics[width=0.85\textwidth]{courses_3}

\end{center}

Now, the course description is listed in the system but it is not yet visible in
the program overview under \url{https://interdisciplinary-college.org/program/},
which is our next step.

Please click on 'Pages' on the left-hand-side menu (17), and then on 'Program' (18).
Here, you'll find two different course lists. One for users without javascript, and
one for users with java script. The first one is easy to update. You simply have
to click on it (19) and then select the category for the current year (20).
You only need to do this once per year.

\begin{center}
\includegraphics[width=0.5\textwidth]{courses_4}

\includegraphics[width=0.9\textwidth]{courses_5}
\end{center}

The javascript list is a bit more challenging. For this list, you have to select the
invisible 'IK Course metadata' block at the top of the page (21) and click on 'Edit' (22).
In this block, you'll find a bunch of javascript code, including a lot of documentation
at the start. First, please make sure that the \texttt{start\_date}, the
\texttt{dinner\_date}, and the \texttt{end\_date} fields are up to date. Then, scroll down
to the 'courses' array further below and remove all content from the previous year.
Next, for each new course, copy the javascript code from the e-mail above and adjust it
by updating the identifier, the URL, and the course type. It should look 
roughly as follows (23):

\clearpage

\begin{verbatim}
	{
		"identifier": "ID3",
		"title": "A pretty long course title that may not fit in the schedule later",
		"short_title": "A shorter course title",
		"url": "https://interdisciplinary-college.org/my-course-url",
		"type": "e.g. Introductory Course",
		"instructor": "Name of the lecturer"
	}
\end{verbatim}

Please make sure that the identifier always consists of a few characters (lower or upper case)
followed by at least one number. Special characters or whitespaces are no permitted.
Further, make sure that there is a comma after each line \emph{except} for the last line (instructor).

After you added the javascript data, scroll back up and save the changed block (24).
Then, you can also save the page. The course list should now display your new course.

\begin{center}
	\includegraphics[width=0.48\textwidth]{courses_6}
	\includegraphics[width=0.48\textwidth]{courses_7}
	
	\includegraphics[width=0.7\textwidth]{courses_8}
\end{center}

In a last step, you should also add the new lecturer to the ik-doz Mailing List (refer to Section~\ref{sec:mailing_lists}).

\clearpage

\section{Manage mailing lists}
\label{sec:mailing_lists}

Unfortunately, the mailing lists can not be managed via the Wordpress
interface. Instead, you have to log into the admin interface of the Strato
webspace (2) by entering the customer number and password (1). If you don't
know either of these, please contact \href{mailto:webmaster@interdisciplinary-college.org}{webmaster@interdisciplinary-college.org}.

Once you are logged in, please select 'E-Mail' (3) and 'Verwaltung' (4) on the left-hand side menu.

\begin{center}
\includegraphics[width=0.4\textwidth]{mailing_lists_1}
\hspace{0.05\textwidth}
\includegraphics[width=0.4\textwidth]{mailing_lists_2}
\end{center}

In the management overview, click on the 'three dots' for the mailing list you would like edit
(5), and select 'Filter bearbeiten' (6). Within the next window, select 'ändern' (7) on the
'Mailverteiler' filter.

\begin{center}
\includegraphics[width=0.8\textwidth]{mailing_lists_3}
\includegraphics[width=0.8\textwidth]{mailing_lists_4}
\end{center}

This should finally reveal the list of people on the mailing list. If you want to delete
someone, you can simply click on the 'x' sign next to the name (8). If you want to add a new
person, please click on the '+' sign next to the last name on the list (8), then select
'Umleiten nach' in the newly created dropdown menu (9), and enter the new person's email (10).

After you completed all your edits, click 'Speichern' (11). This concludes all necessary changes.

\begin{center}
\includegraphics[width=0.6\textwidth]{mailing_lists_5}

\vspace{0.5cm}
\includegraphics[width=0.9\textwidth]{mailing_lists_6}
\end{center}

\clearpage

\section{Set up the registration page}
\label{sec:registration}

In a first step, you'll need to update the deadlines for the current IK.
In particular, please visit the Wordpress dashboard, the 'Pages' overview (1) and
then the 'Registration page' (2). On that page, you'll find the early bird registration
deadline (3), the stipend application deadline (4), the payment deadline for regular
registration (5) and early-bird registration (6), as well as the deadline for
cancellations (7). Please update all these deadlines. Also make sure that the
conference fees (above 3), the payment delay fee (right of 6), the cancellation fee
(below 7), and the credit card fee (at the bottom of the page) are still up to date.

\begin{center}
\includegraphics[width=0.48\textwidth]{registration_1}
\includegraphics[width=0.48\textwidth]{registration_2}

\includegraphics[width=0.48\textwidth]{registration_3}
\includegraphics[width=0.48\textwidth]{registration_4}
\end{center}

Check the page \url{https://interdisciplinary-college.org/registration/}
and make sure that all fees and deadlines are correct (Early bird deadline,
stipend application deadline, money transfer deadline, cancellation deadline).

If you change any of the fees or the early bird deadline, you will also need
to adjust the php code for the registration form accordingly. The php script
is located at\\
\texttt{/WordPress\_SecureMode\_01/wp-content/themes/twentyseventeen/registration\_form.php}
on the IK Strato FTP server. Please download that script and first have a look at lines
41-54, where you should find the following lines of code:

\begin{lstlisting}
$conference_fees = [
	"EarlyBirdMemberFull" => 500,
	"EarlyBirdMemberPhd" => 240,
	"EarlyBirdMemberStudent" => 100,
	"EarlyBirdNonMemberFull" => 800,
	"EarlyBirdNonMemberPhd" => 320,
	"EarlyBirdNonMemberStudent" => 140,
	"RegularMemberFull" => 600,
	"RegularMemberPhd" => 300,
	"RegularMemberStudent" => 120,
	"RegularNonMemberFull" => 1000,
	"RegularNonMemberPhd" => 400,
	"RegularNonMemberStudent" => 170,
];
\end{lstlisting}

This code defines all possible participation fees that could apply. If
any of these change, please adjust the number.

Next, you should inspect lines 56-61, where you should find the following code:

\begin{lstlisting}
      $accommodation_fees = [
	      "External" => 300,
	      "Single" => 510,
	      "Double" => 430,
	      "Triple" => 310,
      ];
\end{lstlisting}

This defines the accommodation fees. If any of these change, please adjust the
number accordingly.

Finally, to adjust the early bird deadline, the relevant code snipped is in lines
66-72 and looks as follows:

\begin{lstlisting}
date_default_timezone_set('Europe/Berlin');
$registration_date = date("m-d");
if($registration_date >= "06-01" || $registration_date <= "01-07") {
	$conference_fee_string = 'EarlyBird';
} else {
	$conference_fee_string = 'Regular';
}
\end{lstlisting}

This code does the following: It retrieves the current date according to
the Europe/Berlin timezone and retrieves it in the format month-date.
Then, it checks if this date at least the first of june (assuming that
the registration will never be open before that time) or at most January
seventh. Note that these comparisons are actually string comparisons (i.e.\
lexicographic comparisons) and do \emph{not} compare time. In this particular
case, both things are equivalent, but this is worth keeping in mind if you change
anything here.

If the early bird deadline changes, you should change the second check
to another date.

Once you have adjusted the php script, please re-upload it to the server
at exactly the same location and override the old version. Your changes should then
apply immediately.

Per default, there should be a boldprint line \textbf{Registration is not open yet.}
at the beginning of the registration page. Once registration is open, you can replace that line
with a link to the registration form at
\url{https://interdisciplinary-college.org/registration-form/}
and set that 'page to public' (also refer to Section~\ref{sec:hiding}).
Otherwise, this link should be removed and the latter page should be set to private.

Once the registration page is updated, you can set it to 'public' (also refer to Section~\ref{sec:hiding}).

\paragraph{Note:} You should also check the poster and rainbow submission deadline
as well as the corresponding templates on the 'Participation' page (8).

\clearpage

\section{Close the registration (Waiting List)}
\label{sec:waiting_list}

Usually around January, the IK is fully booked and Christine will notify you that the
registration should be closed. In that case, we will leave the registration form available,
but convert it into a waiting list form. To do so, first visit the `Registration` page,
rename the Registration link/button to a waiting list button, and add a text that explains
that the IK is now fully booked but that you can still add your name to the waiting list.

Next, go to the `Registration Form` page and rename it to `Waiting List Form`.

Finally, open the php code for the registration page, which you can download from\\
\texttt{/WordPress\_SecureMode\_01/wp-content/themes/twentyseventeen/registration\_form.php}
on the IK FTP server. In this file, check lines 629, 640, and 658, which should
be (respectively):

\begin{lstlisting}
if(!wp_mail('registration@interdisciplinary-college.org',
  'IK Registration', '<pre>' . $registration_mail . '</pre>', $headers)) {
	...
	echo "<p>Your registration was sent successfully ...";
	...
	wp_mail('webmaster@interdisciplinary-college.org',
	'IK Registration (machine-readable copy)',
	json_encode($registration_object), $headers);
\end{lstlisting}

In the first and last line, replace the word `IK Registration` with `IK Waiting List Entry`.
This should change the subject line for registration emails and thus make them distinct.
To also make clear to the user that this was not a registration, change the phrase
`Your registration was sent successfully` to the phrase `Your waiting list entry was sent successfully`.

\paragraph{Note:} In some years, other changes to the registration form may be
necessary as well, like limiting accommodation selection or similar things. In these cases,
comment out the respective section on the `Registration Form` page, but always check
that the form is not broken and that the php code still works. You can do so
by typing the word `Debug` in the `First Name` field of the registration form,
in which case some javascript code should automatically fill in the rest of the form
and you can directly submit it. The php code is designed to not send an email for
such a debug registration but only check it and display the result.

\section{Set up the schedule}
\label{sec:schedule}

To set up the schedule for IK, we simply have to edit the course metadata
(refer to Section~\ref{sec:courses}) and add scheduling information to it.
To do so, visit the Wordpress dashboard, click on 'Pages' (1) and then on
'Daily Schedule' (2). In that page, you can now clock on the metadata block (3)
and then on 'edit' (4).

\begin{center}
\includegraphics[width=0.3\textwidth]{calendar_1}
\includegraphics[width=0.6\textwidth]{calendar_2}
\end{center}

First, add the start date and end date of the entire conference, as well as the date
of the conference dinner (5). Save the block after you did that.

\paragraph{Note:} Please also check the 'Special Events' section on the 'Program'
page and ensure that all timings still apply for the current year.

Next, we'll schedule the single sessions for every course. You can use any calendar
tool you like for this purpose, as long as it has an .ics/ical-output option. In this
handbook, we'll use Google calendar as an example.

For Google calendar specifically, we first need to set the time zone to
coordinated world time because the .ics-export converts everything to
that time zone. To do so, please click the gear in the top right corner (1)
and then 'Settings' (2). Inside the settings, please select 'GMT+00:00' as
your primary time zone (3). Then, leave the settings menu.

\begin{center}
\begin{minipage}{0.35\textwidth}
\includegraphics[height=5.5cm]{google_calendar_1}
\end{minipage}
\begin{minipage}{0.6\textwidth}
\includegraphics[height=5.5cm]{google_calendar_2}
\end{minipage}
\end{center}

In the left menu, click on the + symbol next to 'Other Calendars' (4)
and then on 'Create new calendar' (5). You can name the calendar as you
like (6); the name will not be displayed on the IK website. Then, press
'Create calendar' (7).

\begin{center}
\begin{minipage}{0.6\textwidth}
\includegraphics[height=6cm]{google_calendar_3}
\end{minipage}
\begin{minipage}{0.35\textwidth}
\includegraphics[height=6cm]{google_calendar_4}
\end{minipage}
\end{center}

Now, you can schedule your events by clicking in the
calendar and pulling the events to their desired time. Please make sure
that, for each event, the title/summary of the event begins with the courses
short code (e.g.\ IC1, MC3, or something like that) (8) and that the location
is set (9).

\begin{center}
\includegraphics[width=0.5\textwidth]{google_calendar_5}
\end{center}

Once you have added all sessions, the next step is to get your scheduling
data onto the IK website. In a first step, please click the three vertical
buttons next to your calendar in the left menu (10) and then
'Settings and sharing' (11). In that menu, you'll find two .ics links,
a public and a secret one. The first one only works if you set your calendar
to public, which is why we'll focus on the private one here (12). But either
link is fine.

\begin{center}
\begin{minipage}{0.3\textwidth}
\includegraphics[height=5.5cm]{google_calendar_6}
\end{minipage}
\begin{minipage}{0.6\textwidth}
\includegraphics[height=5.5cm]{google_calendar_7}
\end{minipage}
\end{center}

Unfortunately, we can not use this link directly to display calendar data on the
IK website because the
\href{https://en.wikipedia.org/wiki/Same-origin_policy}{same-origin policy}
forbids loading data form external sites (like Google calendar) if that side does
not explicitly permit it.

We solve this problem by writing a short php script which loads the data onto the
IK server and then makes it available. In particular, this php script should look
like this

\begin{lstlisting}
<?php
/*

This API loads calendar data from a google calendar and provides it as
ical raw data.
Note that this API must be updated every year

Created 2019-2020 by Benjamin Paassen - bpaassen@techfak.uni-bielefeld.de

*/

// We permit this API to be openly accessible because it only offers
// a small bandwidth of public data in a static format. Still, please handle
// responsibly.
header("Access-Control-Allow-Origin: *");
// loads the .ical data from google calendar
$cal = file_get_contents('<link to your google calendar>');
// echos the .ical data
echo $cal;
?>
\end{lstlisting}

We simply have to replace the link in there with our new ical link. Please save
your php script as a text file called \texttt{calendar\_data.php} on your computer.

Next, you'll have to upload this php script
via an SFTP tool of your choice (like \href{https://filezilla-project.org/}{FileZilla})
to the IK website in the folder 

\begin{center}
\texttt{/WordPress\_SecureMode\_01/schedule} (14).
\end{center}

\begin{center}
\includegraphics[width=0.8\textwidth]{google_calendar_9}
\end{center}

The file will then be available under the URL

\begin{center}
\texttt{https://interdisciplinary-college.org/schedule/calendar\_data.php}.
\end{center}

If you don't have SFTP access to the website yet, please contact the IK webmasters at
\href{mailto:webmaster@interdisciplinary-college.org}{webmaster@interdisciplinary-college.org}.

Once you uploaded the php script, you can visit the link and check that the ical data
for your calendar is indeed displayed.

\paragraph{Note:} You only have to do the php step \emph{once}. After that, every change
you apply in the Google calendar will automatically be transferred to the IK website.

\paragraph{Note:} There are several tools that may be helpful in generating a
schedule and which are accumulated in the git repository \url{https://gitlab.com/bpaassen/ik-tools/}.
First, there is the file \texttt{course\_preference\_statistics.ipynb}, which is
an ipython notebook that can load the JSON data of participant registrations and
extract the course preferences from the registrations. It will then create a histogram
of course popularities, a matrix of co-occurences (i.e.\ which courses should \emph{not}
be parallel), and a clustering of courses such that the least amound of co-occurences
applies.

Furthermore, there are the Python programs \texttt{generate\_schedule\_pdf.py} and\\
\texttt{generate\_traditional\_schedule\_pdf.py}, which generate PDF versions
of the schedule based on the currently available metadata on the website.

\clearpage

\section{Upload Course Materials}
\label{sec:materials}

To upload course materials, the best way to date is to create password-protected
zip files for each course and upload all the .zip files via SFTP to the folder

\begin{center}
\texttt{/WordPress\_SecureMode\_01/material/<year>}.
\end{center}

The files will then be available under the URL

\begin{center}
\texttt{https://interdisciplinary-college.org/material/<year>/<filename>.zip}.
\end{center}


Once everything is uploaded, write an e-mail with the links and the password to
all IK participants.
If the lecturers agree AND the materials do not contain copyrighted material,
you can also integrate these links in the course descriptions
(refer to Section~\ref{sec:courses}) and remove the password
protection.

\emph{Please avoid very big files whenever possible. Our webspace is limited to
50 Gigabytes!}

\clearpage

\section{Troubleshoot, i.e.\ Where do I find what?}

\begin{description}
\item[Course Descriptions] are implemented as blog posts in Wordpress. Also refer to Section~\ref{sec:courses}.
\item[Course Metadata] is managed in Wordpress via a re-usable custom HTML block in the sites 'Program', 'Week Schedule', and 'Daily Schedule'. Also refer to Sections~\ref{sec:courses} and~\ref{sec:schedule}. There is also a php script called \texttt{courses\_metadata.php}
which provides this data as JSON. This file is located in the folder \texttt{/WordPress\_SecureMode\_01/schedule} on the SFTP server and usually does not need
to be changed.
\item[Course Materials] are uploaded via SFTP in the directory
\begin{center}
\texttt{/WordPress\_SecureMode\_01/material/<year>}.
\end{center}
\item[E-Mail-Addresses,] including forwards, aliases, and mailing lists, such as \texttt{webmaster@interdisciplinary-college.org},
\texttt{registration@interdisciplinary-college.org},
\texttt{social@interdisciplinary-college.org},
\texttt{posters@interdisciplinary-college.org},
\texttt{ik-org@interdisciplinary-college.org},
\texttt{ik-doz@interdisciplinary-college.org},
\texttt{ik-pk@interdisciplinary-college.org}, and
\texttt{ik-pk-adv@interdisciplinary-college.org}, are managed via the
\href{https://www.strato.de/apps/CustomerService#/skl}{Strato customer page}.
If you do not yet have access to this page, please contact the IK webmasters under \href{mailto:webmaster@interdisciplinary-college.org}{webmaster@interdisciplinary-college.org}.
For Mailing lists, also refer to Section~\ref{sec:mailing_lists}.
\item[Images] are generally stored in the media library of Wordpress. Also refer to Section~\ref{sec:images}.
\item[Menus,] both the header and the footer menu, are managed via \texttt{Appearance} $\rightarrow$
\texttt{Menus} in Wordpress.
\item[Registration Code] The HTML and javascript code of the registration form can be found on the page
\url{https://interdisciplinary-college.org/registration-form/}. The php code for validating the user input
and sending the registration/stipend application email resides under

\vspace{-0.8cm}
\begin{center}
\texttt{/WordPress\_SecureMode\_01/wp-content/themes/twentyseventeen/registration\_form.php}
\end{center}
and can only be changed via SFTP.
\item[Schedule data] is managed via some external site, e.g.\ a Google calendar. Also refer to Section~\ref{sec:schedule}.
\item[Schedule code,] namely the javascript files \texttt{schedule\_functions.js}, \texttt{schedule\_view.js}, and the calendar interface \texttt{calendar\_data.php} are uploaded via SFTP in the directory
\begin{center}
\texttt{/WordPress\_SecureMode\_01/schedule}.
\end{center}
Additional code is contained in the Pages 'Week Schedule' and 'Daily Schedule' in Wordpress.
In case of unsolvable problems with the javascript code, please contact Benjamin Paaßen. The php code
should be generated according to Section~\ref{sec:schedule}.

\item[php backup:] In case php code gets lost (e.g.\ because themes have been updated),
you can find a backup of all php code at \url{https://gitlab.com/bpaassen/ik-tools/}.
\item[Style Issues] such as colors, dimensions, and background images under
\texttt{Appearance} $\rightarrow$ \texttt{Customize} $\rightarrow$ \texttt{Additional CSS}.
Also refer to steps 8-13 in Section~\ref{sec:startpage}.
The general page styling is listed on top, the CSS styles for the schedule in the
bottom part. Please note that in some occasions it may be necessary to use the
\texttt{!important} keyword in order to override native Wordpress css code.
\end{description}



\section{Manually adding course descriptions (legacy)}
\label{sec:legacy_courses}


As preparation for adding a new course, please upload the lecturer's portrait photo
to the media library (refer to Section~\ref{sec:images}).

Next, we need to prepare a category to group all course descriptions for the current
year. For that purpose, log in to the Wordpress dashboard, click on 'Posts' (1),
then on 'Categories' (2), put in a title for the current year (3), and click
'Add New Category' (4).

\begin{center}
\includegraphics[height=10cm]{courses_1}
\end{center}

Then, for each new course, click on 'Posts' (5) and on 'Add New' (6). In the Gutenberg
editor, insert all course information, starting with the title (including the course
identifier, 7), the lecturer (8), and the fields (8). After this step, select the
lecturer's name (8.1), and add a link (8.2) to the HTML anchor \texttt{\#lecturer} (8.3 and 8.4).
We will create this anchor in a moment.

Next, add the course description (9), the learning objectives (10),
the literature list (11),  and the short bio of the lecturer, including a photo (12).
The latter should be a 'Media with Text' block. After this step, select the
'Lecturer' header (12.1) and assign the anchor \texttt{lecturer} (without hashtag this time) (12.2) under 'Advanced'.

Finally, add the lecturer's affiliation and website (13), assign a shortlink for
the course (14), add it to the current year's category (15) and publish the post (16).

\begin{center}
\includegraphics[width=0.5\textwidth]{courses_2}

\includegraphics[width=0.85\textwidth]{courses_3}

\includegraphics[width=0.85\textwidth]{courses_3_1}
\includegraphics[width=0.85\textwidth]{courses_3_2}
\end{center}

Now, the course description is listed in the system but it is not yet visible in
the program overview under \url{https://interdisciplinary-college.org/program/},
which is our next step.

Please click on 'Pages' on the left-hand-side menu (17), and then on 'Program' (18).
Here, you'll find two different course lists. One for users without javascript, and
one for users with java script. The first one is easy to update. You simply have
to click on it (19) and then select the category for the current year (20).
You only need to do this once per year.

\begin{center}
\includegraphics[width=0.5\textwidth]{courses_4}

\includegraphics[width=0.9\textwidth]{courses_5}
\end{center}

The javascript list is a bit more challenging. For this list, you have to select the
invisible 'IK Course metadata' block at the top of the page (21) and click on 'Edit' (22).
In this block, you'll find a bunch of javascript code, including a lot of documentation
at the start. Important for you right now is the 'courses' array further down.
Please remove all content from the previous year from this array and then add a new
object for your new course, which should look roughly as follows (23):

\clearpage

\begin{verbatim}
{
	"identifier": "ID3",
	"title": "A pretty long course title that may not fit in the schedule later",
	"short_title": "A shorter course title",
	"url": "https://interdisciplinary-college.org/my-course-url",
	"type": "e.g. Introductory Course",
	"instructor": "Name of the lecturer"
}
\end{verbatim}

Please make sure that the identifier always consists of a few characters (lower or upper case)
followed by at least one number. Special characters or whitespaces are no permitted.

After you added the javascript data, scroll back up and save the changed block (24).
Then, you can also save the page. The course list should now display your new course.

\begin{center}
\includegraphics[width=0.48\textwidth]{courses_6}
\includegraphics[width=0.48\textwidth]{courses_7}

\includegraphics[width=0.7\textwidth]{courses_8}
\end{center}

In a last step, you should also add the new lecturer to the ik-doz Mailing List (refer to Section~\ref{sec:mailing_lists}).

\end{document}
