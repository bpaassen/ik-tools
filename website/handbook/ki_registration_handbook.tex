\documentclass[a4paper,12pt]{article}

\RequirePackage{geometry}
\geometry{margin=2cm}

\usepackage{graphicx}
\usepackage[american]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}

% listings
\usepackage{listings}
\lstset{
basicstyle=\ttfamily\small,
tabsize=2,
language=php,
keywordstyle=\color{blue},
identifierstyle=\bfseries,
%backgroundcolor=\color{WhiteSmoke},
}

\usepackage{xcolor}

%opening
\title{Handbook for the KI registration form}
\date{Version 2020-09-07}
\author{Benjamin Paaßen - \href{mailto:bpaassen@techfak.uni-bielefeld.de}{bpaassen@techfak.uni-bielefeld.de}}

\begin{document}

\maketitle

Before starting, please verify that you have all required files at hand. In particular,
the registration form consists of two files, the \lstinline!ki_2020_registration_form.html!
file and the\\ \lstinline!ki_2020_registration_form.php! file. Both are strictly needed.

\section{Basic functionality and architecture}

The basic idea behind the registration form is to 1) enable conference attendees to supply all
information needed for registration, 2) to validate this information, and 3) to transmit the validated
information to the conference organizers via e-mail. This form does \emph{not} manage payment
or maintains a database of registered attendees. All permanent data storage needs to be done by
the conference organizers. This is intended to avoid an online database that would have to be
maintained and could be hacked into.

The registration form is structured in two parts, 1) the \emph{frontend} which runs purely in the
user's browser, and 2) the \emph{backend} (written in php) which runs on the server.
The frontend, in turn, consists of three parts, namely a) the form structure (including the question text,
the response types, etc.) written in HTML, b) the form style (including colors, fonts, etc.) written
in CSS, and c) auxiliary functions (e.g.\ to show and hide parts of the form) written in javascript.
The basic structure of the frontend file is as follows.

\begin{lstlisting}
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>KI 2020 Registration</title>
<script>
// javascript functions
</script
</script>
<style>
// style information in CSS
</style>
</style>
</head>
<body>
<form id="registration_form" action="..."
  onsubmit="return check_credit_card_form()" method="post">
<!-- HTML form structure -->
<input type="submit" value="Submit"/>
</form>
</body>
</html>
\end{lstlisting}

The \lstinline!action! attribute of the form element contains the link to the backend php page.
Whenever the user clicks on the 'submit' button at the bottom of the file, all information in the
form is sent to the php program located at that link. This php file runs on the server and has the
purpose to make sure that all transmitted information is secure (and not spam) and then put it into
an e-mail to send it to the conference organizers. The php also generates a feedback message for the
user which is then displayed as another HTML page, in this case a page that informs the user that
their information has been successfully transmitted.

In the following we make the pipeline clear in the example of a single question of the form.

\section{An example question}

We now cover the example of a single question in the form. When adding, editing, or removing questions
of the form, all the involved elements will have to be edited as well.

\subsection{HTML}

The first step in integrating a question into the form is to add a fitting HTML element to the
form. For example, names are best modelled as text fields, while fee reductions are best modelled with
radio buttons, and workshop choices via checkboxes. Code examples and further explanations for all
typical HTML form elements are provided by w3schools at this page: \url{https://www.w3schools.com/html/html_form_elements.asp}.

Here, we consider the example of a text field, namely the 'firstname' field of the form. The
corresponding HTML snipped is this:

\begin{lstlisting}
<div class="table">
<p>
  <label for="firstname">First name: </label>
  <input type="text" id="firstname" name="firstname" required="required"
    onfocusout="check_debug()" onchange="update_firstname()"/>
</p>
</div>
\end{lstlisting}

There is quite a bit to unpack here. First, the actual text field for the user is only the
\lstinline!<input type="text"/>! element. However, to make clear to the user that the first name
should be put into this text field, we also provide a \lstinline!label! element, which contains
the text 'First name: '. The input element further has several attributes. The most important
one is \lstinline!name="firstname"!. This specifies that the name of this form field is 'firstname'.
Therefore, when the form is transmitted to the backend, we can retrieve the content of this text
field under the field name 'firstname'. We further use this identifier to connect the label to the
text field via the attribute \lstinline!for="firstname"!. This attribute is not strictly necessary
for functionaliy, but it makes the form structure easier to understand for machines, such as screen
readers for blind people.

Beyond the name attribute, we also need an identifier to refer to this field from javascript.
For this purpose, we use the \lstinline!id="firstname"! attribute. Note that we assign the same
id and name. This is to avoid confusion. Next, we notice the attribute \lstinline!required="required"!.
This means that the user will not be able to submit the form until some information has been put into
this field. Modern browsers also automatically provide highlights of required fields if the user has
forgotten to put information in.

Finally, we consider the attributes \lstinline!onfocusout="check_debug()"! and
\lstinline!onchange="update_firstname()"!. These tell the user's browser to call javascript functions
when certain conditions are fulfilled. In particular, whenever the user has clicked on the 'firstname'
field and then clicks somewhere else, the \lstinline!check_debug! function is triggered; and whenever the user
changes the content of the 'firstname' field, the function \lstinline!update_firstname()! is triggered.

\subsection{Javascript}

These functions are in the javascript part of the form and have the following content.

\begin{lstlisting}
function update_firstname() {
	document.getElementById("credit_card_participant").value = 
	document.getElementById("firstname").value + " " + 
	document.getElementById("lastname").value;
}

function check_debug() {
	if (document.getElementById("firstname").value === 'Debug') {
		// set a valid combination of registration fields
		document.getElementById("lastname").value = 'Last Name';
		document.getElementById("email").value = 'debug@debug.org';
		document.getElementById("fee_regular").click();
		document.getElementById("billing_institution").value = 'University of Debugtown';
		document.getElementById("billing_department").value = 'Faculty of Debugging';
		document.getElementById("billing_street").value = 'Debugway';
		document.getElementById("billing_no").value = '1';
		document.getElementById("billing_zip").value = '99999';
		document.getElementById("billing_city").value = 'Debugtown';
		document.getElementById("billing_country").value = 'Debugistan';
		document.getElementById("printed_proceedings").click();
		document.getElementById("virtual_beer_tasting").click();
		document.getElementById("shipping_same").click();
	}
}
\end{lstlisting}

In particular, the \lstinline!update_firstname! function copies the current information
from the firstname and lastname field to the credit card information. This is a convenience
feature to make it easier for the user to fill out their credit card info. Note that we use
the javascript function \lstinline!getElementById! here which retrieves an element by its 'id'
attribute. The underlying assumption is that no two elements in the HTML have the same 'id'.
We, as developers, have to make sure that this is true.

The \lstinline!check_debug! function is a convenience function for us as developers. Whenever
the first name is 'Debug', this function automatically fills in all other required information
for the form. So when we test the form we do not need to type in all the fields each time but
can instead just type in the first name 'Debug', then click somewhere else in the form, and let
this javascript function fill in the rest for us.

There are many other javascript functions, but the basic structure is the same as here: Some element
of the form has an \lstinline!onchange! attribute (or some other attribute) which calls the function
and the function then automates some stuff that the user would otherwise have to do.

\subsection{CSS}

The styling for our question looks as follows.

\begin{lstlisting}
#registration_form div.table {
display: table;
padding-bottom:20px;
}
#registration_form div.table p { display: table-row;  }
#registration_form label {
display: table-cell;
padding-right:10px;
}
#registration_form input {
display: table-cell;
}

#registration_form label {
font-weight:400;
}
\end{lstlisting}

This CSS code makes sure that any div element that belongs to the 'table' class is displayed
as a table. Further, any paragraph (or 'p') element inside such a div element is displayed as
the row of a table, and any 'label' as well as any 'input' element is displayed as a table cell.
This styling ensures that our label with the text 'First name: ' is positioned to the left of our
text field and not above. Further, the table layout ensures that all labels and text fields in the
same div element are nicely horizonally aligned.

\subsection{php}

As mentioned before, when the user clicks the 'submit' button and has filled in all required information,
the information is transmitted to the server, in particular to our php script. In principle, the structure
of our HTML form and our javascript functions already make sure that the user has to fill in all necessary
information and can not fill in rubbish. Unfortunately, though, we can not rely on this. A hacker could
also call our php script directly, without using our nicely designed HTML form, and could send malicious
information to our server. To prevent this, we need to validate all incoming data.
For the example of our 'firstname' field, the validation looks as follows.

\begin{lstlisting}
// first name
if(!isset($_POST['firstname'])) {
	echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the first name was not given. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
	exit;
}
$first_name = sanitize_text_field( $_POST['firstname'] );
\end{lstlisting}

This code implements two layers of security. If the data we receive contains no 'firstname' field at all,
we directly display an error message to the user.
If the 'firstname' field is contained, we first put the data through the \lstinline!sanitize_text_field!
function which makes sure that malicious code would be escaped. The result is stored in the variable
\lstinline!$first_name$!. Note that the \lstinline!sanitize_text_field! function is not written by us
but is a function provided by WordPress. If you want to run this registration form without WordPress,
you need to replace this function by another one, for example the sanitizing functions provided by php
itself: \url{https://www.php.net/manual/en/filter.filters.sanitize.php}.

Once the data is validated, we re-format it in two ways. First, in a human-readable format as feedback
to the user and for the e-mail for the conference organizers. Second, in a machine-readable format to
make the registration data easier to process for the technical administrators of the conference.

The respective php code for our 'firstname' example is as follows.

\begin{lstlisting}
// construct registration e-mail from user data
$registration_mail  = "Last name:          $last_name\n";
$registration_mail .= "First name:         $first_name\n";

// create a machine readable version in JSON format
$registration_object = (object)[];
$registration_object->first_name = $first_name;
\end{lstlisting}

The \lstinline!.=! operator performs string concatenation in php.
Note how we use the \lstinline!$first_name! variable from before as our interface to the data.

Next, we have a special code snippet in case the \lstinline!$first_name! variable is set to 'Debug'.
In that case, we do not want to send out the information via e-mail, because it is only debugging
information anyways.

\begin{lstlisting}
// special code for debug input
if($first_name === 'Debug') {
	echo "<p>Your registration was sent successfully ...</p>
	  <pre>$registration_mail</pre>";
	$json_data = json_encode($registration_object);
	echo "<p>JSON data</p><pre>$json_data</pre>";
	echo "<p><a href=\"https://ki2020.uni-bamberg.de/\">&rarr; Back to the KI 2020 page</a></p>";
	exit;
}
\end{lstlisting}
Note that twe still display the information that we \emph{would} have sent out via e-mail, for
debugging purposes.


As a final step in the php script, we send out the information to the conference
organizers and display it back to the user. For this purpose, we again use a WordPress function,
namely the \lstinline!wp_mail! function.

\begin{lstlisting}
$headers = array('Content-Type: text/html; charset=UTF-8');

/*
* At this point, we are reasonably certain that the input is valid and that we
* can risk sending it via e-mail. Sent the registration data
*/
if(!wp_mail('<email address of conference manager>', 'KI 2020 Registration',
  '<pre>' . $registration_mail . '</pre>', $headers)) {
	// if the mail sending has failed, inform the user
	echo "<p>We are very sorry, but unfortunately ... ";
	echo "<p><a href=\"https://ki2020.uni-bamberg.de/\">&rarr; Back to the KI 2020 page</a></p>";
	exit;
} else {
	// otherwise, display a success message
	echo "<p>Your registration was sent successfully ...";
	echo "<p><a href=\"https://ki2020.uni-bamberg.de/\">&rarr; Back to the KI 2020 page</a></p>";
}

// Then send a copy to the conference organizers
wp_mail('<email address of second conference manager>', 'KI 2020 Registration',
  '<pre>' . $registration_mail . '</pre>', $headers);

$headers = array('Content-Type: text/plain; charset=UTF-8');

// send a machine-readable backup copy to the webmaster
wp_mail('<email address of technical admin>', 'KI 2020 Registration (copy)',
  json_encode($registration_object), $headers);
\end{lstlisting}

Note that we display an error message again if the \lstinline!wp_mail! function fails.
In our prior use, this never happened. Unfortunately, even if it does not happen, 
this is \emph{not} a guarantee that the information was transmitted. If a
receiving e-mail account is full or has strict spam filters, the information may never arrive.
\textbf{Therefore, the registration form should be re-tested every year to make sure it is still functional.}

If the registration form should run without WordPress, another mailing function needs to
be used. An example tutorial on how to send e-mails via php is given here:
\url{https://blog.mailtrap.io/php-email-sending/}.

\section{How to adapt the form for a new conference}

For any new conference, all e-mail addresses and links both in the HTML file and
in the PHP file should be checked again and replaced with the up-to-date information. Additionally,
the php e-mail sending plugin should be re-configured to be up-to-date.
Then, all questions in the form should be checked with the current organizers and should be updated,
especially the registration prices, the workshop and tutorial lists, and the 'other' section.
Also make sure to update the contact data for the credit card form.

Further note that any change in the questions needs to be made in the HTML form, in the javascript
functions, \emph{and} in the php script.

\section{How to integrate into WordPress}

When this registration form should run in WordPress, the following operations need to be performed.
\begin{enumerate}
\item In the WordPress admin console, add a new site for the registration form itself with a 'Custom HTML' element that contains
everything in the 'script' element above as well as the entire form HTML.
\item Put all CSS content into the 'additional CSS' of your WordPress site. Refer to \url{https://www.wpbeginner.com/plugins/how-to-easily-add-custom-css-to-your-WordPress-site/} for more information.
\item Using an SFTP client, upload the php file, including the WordPress header and footer information, to the folder
\lstinline!wp-content/themes/<name of your theme>/!.
\item Back in the WordPress admin console, add a new site for the php script. This site should have no
content, but the option 'Page Attributes $\rightarrow$ Template' should be set to 'KI 2020 Registration Form'. This will make sure that this site loads the php code of the script. Then, set the 'URL slug' under the 'permalink' option to a value you like and put the link to this site in the 'action' attribute of the HTML form.
\item In the WordPress admin console, load the 'WP Mail SMTP' plugin and configure ot according to the
e-mail account you want to use to send the registration e-mails to the conference organizers.
\end{enumerate}

After all this, the form should run and you can test it.

\section{How to use without WordPress}

If the registration form is to be used without WordPress, you need to make sure that an up-to-date php
instance runs on your server (this should be the case for all typical website hosters) and that you
have a replacement for the WordPress \lstinline!sanitize_text_field! and \lstinline!wp_mail! functions.
Other than that, the integration is quite simple: You just have to upload both the HTML and the php
file to your website. In the php file, you first have to remove all WordPress specific code, namely:

\begin{lstlisting}
<?php
/* Template Name: KI 2020 Registration Form */

get_header(); ?>

<div class="wrap">
<div id="primary" class="content-area">
<main id="main" class="site-main" role="main">

<!-- Standard WordPress 2017 theme page template -->
<?php
while ( have_posts() ) :
the_post();

get_template_part( 'template-parts/page/content', 'page' );

// If comments are open or we have at least one comment
// load up the comment template.
if ( comments_open() || get_comments_number() ) :
comments_template();
endif;

endwhile; // End of the loop.
?>
<!-- End of theme page template -->
\end{lstlisting}

at the beginning of the file and

\begin{lstlisting}
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
\end{lstlisting}
should be removed.

Further, the 'action' attribute of the HTML form needs to be adjusted to link to the location
of the php script. It may also be nice add some HTML content to the php script in order to format
the feedback to the user in a nicer way. Other than that, form should be functional.

\section{Troubleshooting and known issues}

The most typical problem when working with this registration form is that one has made a change
to the HTML code but has not made the necessary changes to the javascript or php. Therefore,
try to get used to a firm pipeline where every change to the HTML is also checked in javascript
and php. Further, get used to testing the form after \textbf{every} change to the form.
This is made much easier with the 'Debug' function introduced above so it shouldn't take too much
of your time.

Another known issue is that the registration e-mail might have been sent but may have been filtered
out due to spam filters. To prevent this, make sure that you use a well-established host to send the
registration e-mails, that your e-mail sending function is well-configured, and that your headers
are set. Further, make sure you send the registration data not only to a single conference organizer
but to several ones to have redundancy.

Finally, if running the registration form in WordPress, it sometimes occurs that updates of WordPress
delete all custom php files in the theme folder. If that occurs, the registration form will not work
anymore and users will only see a white screen after registering, which is very unpleasant. Therefore,
whenever an automatic or manual WordPress update occurs, make sure that the php file is still in the
\lstinline!wp-content/themes/<name of your theme>/! folder and re-upload it if necessary.

\section{Security and privacy considerations}

Covering all potential web security and privacy risks is out of scope of this manual, but we can
at least explain the considerations that motivated the architecture of this form. When constructing
this form, we considered three risks: a) Leaks of attendee data via badly configured databases
b) Malicious code in text fields of the registration form, and c) spamming via bots that automatically
fill out the registration form thousands of times.

We addressed a) by not using an online database but instead transmitting data via e-mail. In this way
we circumvent all problems of having to secure an online database against hacking or leaks or having
to make regular backups of that database. All data storage is done by the e-mail servers of the
conference organizers. Further, provided that the php e-mail function supports SMTP, all transmitted
data is encrypted, adding additional security (the GDPR also requires us to transmit personal
data only over encrypted channels, to my knowledge). The drawback is that e-mails may get lost, that
we have to maintain an e-mail sending function, and that all e-mail addresses have to be re-configured
whenever the organizers change. Still, this seemed like the most pragmatic option to us.
\textbf{Importantly, when configuring your php e-mail sending function, make sure that the e-mail
user and password are not leaked in the PHP code but are hidden.}

We addressed b) by sanitizing any text field input. Additionally, we only
handle strings in php and maintain no database, such that there is only very limited mischief that a
hacker could do.

c) is hardest to address. We can not completely prevent getting spammed by bots. Still, there are
a few security features. First, we have an input field called 'email2'
in the form which is invisible to users but is still part of the HTML structure, such that a bot may
try to put information in. If that is the case, the php form stops processing this form
submission and returns an error message:

\begin{lstlisting}
if(isset($_POST['email2']) & $_POST['email2'] !== '') {
	echo 'You used the invisible second e-mail field ...';
	exit;
}
\end{lstlisting}

Furthermore, after reformatting data into a human-readable form, we make a length check such that
any spam bot who just puts a lot of garbage into the text fields should fail this check.

\begin{lstlisting}
// check that the data has not become too long overall, which
// would be an indication of some kind of hacking attack
if(strlen($registration_mail) > 5000) {
	echo 'Unfortunately, your form data was invalid ...';
	exit;
}
\end{lstlisting}

Finally, we do \emph{not} send an e-mail of the registration data back to the user. This has been
counterintuitive to users in the past (some were not sure whether they were really registered or not),
but we found that the risk of spam bots misusing our form to flood the e-mail accounts of users
is too high.

In our experience so far, these features have sufficed to prevent spamming attacks. But further
security features may be necessary in the future.

\end{document}
