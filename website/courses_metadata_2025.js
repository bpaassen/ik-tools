<script id="courses_metadata" type="text/javascript">

var start_date = "2025-03-14";
var end_date = "2025-03-21";
var dinner_date = "2025-03-18";
var motto = "Augmenting Minds and Bodies";

const courses = [ 
{
    "identifier": "BC1",
    "title": "Introduction to Machine Learning",
    "fields": "Machine Learning",
    "url": "https://interdisciplinary-college.org/2025-bc1/",
    "type": "Basic Course",
    "instructor": "Magda Gregorova",
    "description": "In this introductory course we will cover the basics of machine learning (ML) targeting specifically the un-initiated audience. If you are not sure what machine learning actually is, if you have never trained an ML model, if for you deep learning means learning in deep sleep and ChatGPT is a result of dark magic, then this course is meant for you. The course will be organized in four sessions where (1) we will begin from the basic concepts of learning from data reviewing the fundamental ideas and building stones of machine learning, (2) we shall discuss some classical ML algorithms which are still the workhorses for solving many practical problems, (3) we shall explore the more modern deep learning approaches based on neural network models, and (4) we shall uncover some of the magic behind ChatGPT by discussing the concepts of deep generative modelling. The field is vast and very fast paced combining mathematics with computer science while spicing it up with ideas coming from physics, neuroscience, and many other areas. While some math cannot be avoided, it is not my ambition to cover all the technicalities of ML. I will rather endeavor to help you build your own picture of the field based on basic understanding of the underlying fundamental ideas and nature your own intuition for data analysis hoping you will become more comfortable and confident when exploring the ML methods in your future work."
},
{
    "identifier": "BC2",
    "title": "A glimpse of the human nervous system (in pain)",
    "fields": "Neuropharmacology",
    "url": "https://interdisciplinary-college.org/2025-bc2/",
    "type": "Basic Course",
    "instructor": "Margot Ernst",
    "description": "The molecular foundation for NS function will be introduced in part 1, ranging from the smallest vital units (water, salt, fat) to the largest biochemicals (DNA, RNA, proteins). Selected so-called transmitter systems  will be introduced in order to convey a feeling for the molecular complexity and how food and drugs act on the NS. Molecules relevant for nociception and pain will be in the focus.\r\nIn part 2, the vast complexity of cells that comprise the NS will be introduced, alongside with the genetic basis of the cellular pre- and postnatal  development. The basis for adapting to the environment is provided by cellular and molecular traces made by physical, chemical and complex (e.g. social) stimuli that act on the organism. Cellular key players that contribute to pain perception and memory will be highlighted. \r\nIn part 3, the small parts will be put together and selected so-called \u201csystems\u201d will be introduced.  Nociception and pain, and the concept of \u201caversive responses\u201d will be examined in some detail, as well as the neural correlates of pain memory and phantom pain. \r\nIn part 4, selected means to interact with the NS are examined, and some of the challenges and chances for future development highlighted. Progressing from non invasive to invasive (e.g. implants), various selected means to influence nociception and pain perception, as well as the aversive reactions to painful stimuli, illustrate some basic principles."
},
{
    "identifier": "BC3",
    "title": "Situated Affectivity and its applications",
    "fields": "Philosophy (of emotions)",
    "url": "https://interdisciplinary-college.org/2025-bc3/",
    "type": "Basic Course",
    "instructor": "Achim Stephan",
    "description": "The course offers (1) an introduction to affective phenomena such as emotions and moods,  in general; (2) it also introduces to the key notions of situated affectivity such as 4E, user-resource interactions, mind shaping, mind invasion, and scaffolds; (3) next, we will apply these notions to the study of  cases of mind invasion from individuals to nations; (4) finally, we will explore whether we can trust our (own) emotions."
},
{
    "identifier": "BC4",
    "title": "Interfacing brain and body \u2013 Insights into opportunities, challenges and limitations from an engineering perspective",
    "short_title" : "Interfacing brain and body",
    "fields": "Brain-Body Interface, Engineering, Society",
    "url": "https://interdisciplinary-college.org/2025-bc4/",
    "type": "Basic Course",
    "instructor": "Thomas Stieglitz",
    "description": "Neural engineering addresses the wet interface between electronic and biological circuits and systems. There is a need to establish stable and reliable functional interfaces to neuronal and muscular target structure in chronic application in neuroscientific experiments but especially in chronic applications in humans. Here, the focus will be laid on medical applications rather than on fictional scenarios towards eternal storage of the mind in the cloud. The physiological mechanisms as well as basic technical concepts will be introduced to lay a common knowledge for informed decisions and discussions. From a neural engineering point of view, proper selection of substrate, insulation and electrode materials is of utmost importance to bring the interface in close contact with the neural target structures, minimize foreign body reaction after implantation and maintain functionality over the complete implantation period. Different materials and associated manufacturing technologies will be introduced and assessed with respect to their strengths and weaknesses for different application scenarios. Different design and development aspects from the first idea to first-in-human studies are presented and challenges in translational research are discussed. Reliability data from long-term ageing studies and chronic experiments show the applicability of thin-film implants for stimulation and recording and ceramic packages for electronics protection. Examples of sensory feedback after amputation trauma, vagal nerve stimulation to treat hypertension and chronic recordings from the brain display opportunities and challenges of these miniaturized implants. System assembly and interfacing microsystems to robust cables and connectors still is a major challenge in translational research and transition of research results into medical products. Clinical translation raises questions and concerns when applications go beyond treatment of serious medical conditions or rehabilitation purposes towards life-style applications. The four sessions within this course \u201cInterfacing brain and body\u201d will cover (1) physiological and engineering aspects of technical interfaces to brain and body: fundamentals on optogenetics, recording of bioelectricity and electrical stimulation, (2) neuroscientific and clinical applications of neural technology like (3) the challenges of neural implant longevity and (4) ethical and societal considerations in neural technology use."
},
{
    "identifier": "MC1",
    "title": "Neurons and the Dynamics of Cognition: How Neurons Compute",
    "short_title" : "Neurons and the Dynamics of Cognition",
    "fields": "Computational Neuroscience \/ Neuromorphic Computing",
    "url": "https://interdisciplinary-college.org/2025-mc1/",
    "type": "Method Course",
    "instructor": "Andreas St\u00f6ckel",
    "description": "While the brain does perform some sort of computation to produce cognition, it is clear that this sort of computation is wildly different from traditional computers, and indeed also wildly different from traditional machine learning neural networks. In this course, we identify the type of computation that biological neurons are good at (in particular, dynamical systems), and show how to build large-scale neural models that realize basic aspects of cognition (sensorimotor, memory, symbolic reasoning, action selection, learning, etc.). These models can either be made to be biologically realistic (to varying levels of detail) or mapped onto energy-efficient neuromorphic hardware."
},
{
    "identifier": "MC2",
    "title": "Social mechanisms and human compatible agents",
    "fields": "Artificial Intelligence",
    "url": "https://interdisciplinary-college.org/2025-mc2/",
    "type": "Method Course",
    "instructor": "Ralf M\u00f6ller",
    "description": "In the course we develop notions of human-compatibility in mechanisms in which humans and artificial agents interact (social mechanisms). Properties of social mechanisms are investigated in terms of AI alignment in general and assistance games in particular. Modeling formalisms needed to realise human-aligned agents are introduced."
},
{
    "identifier": "MC3",
    "title": "Augmented Thought: Language, Embodiment, and Agentic Language Models",
    "short_title": "Language, Embodiment, Agentic LMs",
    "fields": "Artificial Intelligence, Computational Linguistics",
    "url": "https://interdisciplinary-college.org/2025-mc3/",
    "type": "Method Course",
    "instructor": "Philipp Wicke",
    "description": "t.b.a."
},
{
    "identifier": "MC4",
    "title": "New bodies, new minds \u2013 A Practical Guide to Body Ownership Illusions",
    "short_title" : "Body Ownership Illusions",
    "fields": "Cognitive neuroscience, Experimental psychology",
    "url": "https://interdisciplinary-college.org/2025-mc4/",
    "type": "Method Course",
    "instructor": "Andreas Kalckert",
    "description": "The introduction of body ownership illusion experiments has revolutionized our ability to explore and manipulate the subjective experience of our own bodies. Innovative paradigms like the rubber hand illusion and body swapping have greatly expanded our understanding of the cognitive and neural mechanisms that shape our sense of self. Due to their relatively low technical and practical demands, these paradigms have surged in popularity, leading to hundreds of studies. However, this rapid expansion has also resulted in a variety of experimental approaches, often lacking standardized methods\u2014making it challenging to ensure comparability and replicability across studies.\r\nIn this methods course, we aim to tackle both the methodological and conceptual dimensions of these intriguing paradigms. We will dive deep into the existing literature, critically evaluating the theoretical and practical aspects of these experiments. We will also identify specific elements of experimental paradigms\u2014such as stimulation protocols and measurement techniques\u2014that are not only practically relevant but also crucial for refining our understanding of these illusions.\r\nThe course is designed to be both informative and interactive, consisting of two lectures and two practical sessions where we will explore body ownership illusions in both physical and virtual reality environments. By the end of the course, participants will have gained a clearer insight into the nuances and demands of these fascinating experiments and be equipped with a set of best practices for conducting replicable studies."
},
{
    "identifier": "SC1",
    "title": "Bionic Extremity Reconstruction",
    "fields": "Neuroscience, Medicine, Engineering",
    "url": "https://interdisciplinary-college.org/2025-sc1/",
    "type": "Special Course",
    "special_category":"Augmenting Bodies",
    "instructor": "Cosima Prahm",
    "description": "Although the hand represents only 1% of our body weight, most of our sensorimotor cortex is associated with its control. The loss of a hand therefore not only signifies the loss of the most important tool with which we can interact with our environment, but also leaves us with a drastic sensory-motor deficit that challenges our central nervous system. Restoring hand function is therefore not only an essential part of restoring physical integrity and functional employability, but also closes the neural circuit, thereby reducing phantom sensations and nerve pain. When there is no longer sufficient anatomy to restore meaningful function, we can resort to complex robotic replacements whose functional capabilities in some respects even surpass biological alternatives, such as conservative reconstructive measures or transplantation of a hand. However, as with replantation and transplantation, the challenge with bionic robotic replacements is to solidly attach it to the skeleton and connect the prosthesis to our neural and muscular system to achieve natural, intuitive control and also provide basic sensory feedback. This interdisciplinary course will discuss the progressive development of upper extremity robotic prosthetics in the fields of bioengineering, medicine, computer science, and neuroscience. We address the medical basis of biosignals, movement, amputation and restoration, and various systems of prosthetic limbs to restore physical integrity. We will discuss enhancement versus restoration and how to improve the man-machine-interface, exemplified with case studies. Sessions in detail: We will cover the fundamentals of bionic extremity reconstruction, including replantation, transplantation, and biological restoration, along with an overview of various upper limb prosthetic types. Participants will explore conventional and advanced prosthetic control methods, including Machine Learning applications and innovative human-machine interfaces like nerve transfers, osseointegration, and implanted electrodes. Cutting-edge concepts, such as self-contained neuromusculoskeletal prostheses and cyborg technologies, will also be discussed. The course will address virtual rehabilitation using mixed reality (XR) environments, focusing on traumatic hand injuries, phantom limb pain, and neural pain management."
},
{
    "identifier": "SC2",
    "title": "Phantoms in the mind: body and pain perception after limb amputation",
    "fields": "Psychology, Neuroscience, Rehabilitation",
    "url": "https://interdisciplinary-college.org/2025-sc2/",
    "type": "Special Course",
    "short_title" : "Body and pain perception after limb amputation",
    "special_category":"Augmenting Bodies",
    "instructor": "Robin Bekrater-Bodmann",
    "description": "The amputation of a limb represents the most serious breach of a person's physical integrity and requires extensive psychological and behavioral adjustment. In addition, many people are confronted with special perceptual phenomena after an amputation: the presence of a phantom of the lost body part, which in many cases is experienced as painful. Physiological and brain imaging results show that characteristic changes in the central nervous system correlate with the experience of pain. The perceived restoration of physical integrity, e.g. through prostheses, mirrors, or virtual reality, can normalize the functioning of the central nervous system which can have positive effects on the perception of pain. In this workshop, participants learn the sensory basics of bodily self-experience firsthand. The psychobiological changes after an amputation and their connections to the experience of post-amputation pain are examined. And finally, the implications of the findings for tailored treatments are discussed."
},
{
    "identifier": "SC3",
    "title": "Interfacing Spinal Motor Neurons in Humans for Highly Intuitive Neuromotor Interfaces",
    "short_title" : "Neuromotor Interfaces",
    "fields": "Engineering",
    "url": "https://interdisciplinary-college.org/2025-sc3/",
    "type": "Special Course",
    "special_category":"Augmenting Bodies",
    "instructor": "Alessandro del Vecchio",
    "description": "Spinal motor neurons represent the final gateway from neural intention to physical movement, making them crucial for any interface that aims to restore or augment motor function. In cases of spinal cord injury (SCI), paralysis of the hand muscles significantly impacts quality of life, as individuals lose the ability to perform fundamental tasks. However, our recent research demonstrates that even in individuals with motor complete SCI (C5\u2013C6), the activity of spinal motor neurons remains accessible and task-modulatable. Using a minimally-invasive electromygraphic interface, we tested eight SCI individuals and identified distinct groups of motor units under voluntary control that could encode specific hand movements, from grasping to individual finger flexion and extension. By mapping these motor unit discharges to a virtual hand interface, we enabled participants to proportionally control multiple degrees of freedom, successfully matching various cued hand postures. These findings underscore the potential of wearable muscle sensors to access voluntarily controlled motor neurons in SCI populations, presenting a pathway to restore lost motor functions through assistive technologies.\r\n\r\nAlongside this study, we explored the neural organization of motor unit activity in different muscle groups, focusing on the low-dimensional latent structures\u2014or motor unit modes\u2014that underlie the coordinated output of motor units. By applying factor analysis, we identified two primary motor unit modes that captured most of the variability in motor unit discharge rates across knee extensor and hand muscles. Interestingly, we observed a distinct pattern in the hand muscles, where motor unit modes were largely specific to individual muscles, whereas knee extensors displayed a more continuous distribution, with shared synaptic inputs leading to overlapping motor unit modes across muscle groups. Simulations with large populations of integrate-and-fire neurons confirmed the accuracy of these modes, shedding light on the common inputs that drive correlated activity in synergistic muscle groups.\r\n\r\nBuilding on these insights, we have now developed an open-source software platform that translates real-time EMG activity into controllable movement outputs. This software seamlessly integrates with both exoskeletons and prosthetics, allowing for precise and intuitive movement control that aligns with the user's intent. With this tool, we can now bring intuitive neuromotor interfaces closer to clinical reality, offering individuals with SCI and other neuromuscular impairments a new level of interaction and independence."
},
{
    "identifier": "SC5",
    "title": "Developmental Cognitive Science",
    "fields": "Cognitive Science",
    "url": "https://interdisciplinary-college.org/2025-sc5/",
    "type": "Special Course",
    "special_category":"Augmenting Minds",
    "instructor": "Celeste Kidd",
    "description": "In this lecture series, I will discuss our labs research about how people (and some non-human animals) come to know what they know about the world. The world is a sea of information too vast for anyone to acquire entirely. How do people navigate the information overload, and how do their decisions shape their knowledge and beliefs? We'll discuss recent empirical work about the core cognitive systems that people use to guide their learning about the world—including attention, curiosity, and metacognition (thinking about thinking). We discuss the evidence that people play an active role in their own learning, starting in infancy and continuing through adulthood, and how many of these mechanisms are shared with non-human animals. We'll talk about why we are curious about some things but not others, and how our past experiences and existing knowledge shape our future interests. We'll also discuss why people sometimes hold beliefs that are inconsistent with evidence available in the world, and how we might leverage our knowledge of human curiosity and learning to design systems that better support access to truth and reality. A running theme throughout this series will be the importance of uncertainty in guiding our learning and collaborative knowledge building. We will be taking a comparative approach in outlining the types of cognitive representations of uncertainty shared among biological intelligence, but lacking in artificial ones, to explain how current generative AI models cannot be trusted to disseminate information to people without problems. We'll discuss several core tenets of human psychology that can help build a bridge of understanding about what is at stake when discussing regulation and policy options to prevent widespread adoption of these AI technologies from permanently distorting human beliefs in problematic ways."
},
{
    "identifier": "SC6",
    "title": "Mirroring, alerting, guiding, on-loading, off-loading. How can (and should) adaptive technology support learning and teaching?",
    "short_title": "Adaptive Learning Techologies",
    "fields": "Educational Psychology, Learning Analytics, Learning Sciences",
    "url": "https://interdisciplinary-college.org/2025-sc6/",
    "type": "Special Course",
    "special_category":"Augmenting Minds",
    "instructor": "Sebastian Strau\u00df",
    "description": "Educational technology has come a long way since the Teaching Machines from the 1920s. While some commercial educational software can arguably still be classified as Teaching Machines, research and development in the learning sciences and technology-enhanced learning have produced educational technologies that can facilitate different aspects of teaching and learning alike.\r\n\r\nThe objective of this course is to examine the concept of learning from the standpoint of educational psychology, with a particular focus on the potential of adaptive educational technology to facilitate learning and teaching processes. We will do so by conceptualizing learning and teaching contexts as a relationship between (usually) one teacher and a group of students. Educational technology can now be leveraged as a tool to facilitate learning and teaching. For example, educational technology can offer insight into learning and teaching processes (mirroring), draw inferences, offer diagnoses (alerting), or take automated actions on behalf of the learners or the teacher (guiding).\r\n\r\nThe course will examine the perspectives of both learners and their teachers. We will explore how educational technology can enhance learners' cognitive and metacognitive abilities, beyond learning more efficiently. To this end, we first look at learning processes and learning outcomes in the context of school and higher education and discuss how they can be observed by humans (and machines). \r\n\r\nFrom the perspective of the learners, we then explore educational technologies that can provide us with information about the development of our skills and our learning behavior. As an example, we will look at learning analytics dashboards. Further, we look at technologies that provide learning tasks, assess the learning progress, and utilize this information to provide us with individualized support. Examples for such a technology are intelligent tutoring systems.  \r\n\r\nTaking the perspective of the teacher, we look at educational technologies that provide us with an overview of our learners\u2019 progress, for example teacher dashboards. Such tools may enhance our teacher vision, by providing information that is usually difficult to gather and to aggregate. This information, in turn, can provide valuable insights into the learning of the entire class and individual students which allows us to provide them with the assistance that they need. Going one step further, teacher dashboards may also process the data further and alert us of challenges that our learners face, or even suggest instructional support for the learners. \r\nAt the same time, data analytics approaches can also focus on the teachers and their teaching. For example, tools allow teachers to observe and adapt their own teaching, which offers benefits for teacher professional development.\r\n\r\nAs we look at these different perspectives, we'll explore the various levels of automation that educational technology can offer the classroom, the challenges that result from the need to utilize valid measurements of learning and teaching, from biased data sets, and from automation-induced complacency. Further, we will explore the consequences of (partially) offloading cognitive and metacognitive processes to automated systems. On the side of the students, this includes the question which tasks should be on-loaded or off-loaded to foster learning. With respect to teachers, we will discuss the concepts of hybrid intelligence and human-AI co-orchestration in the classroom."
},
{
    "identifier": "SC7",
    "title": "Contemplative science and the practice of contemplation",
    "short_title" : "Contemplation",
    "fields": "Psychology, neuroscience, cognitive science",
    "url": "https://interdisciplinary-college.org/2025-sc7/",
    "type": "Special Course",
    "special_category":"Augmenting Minds",
    "instructor": "Marieke van Vugt",
    "description": "Contemplative practices such as mindfulness are often marketed as methods to augment our minds: making us more concentrated, more happy, more efficient. But is that all these practices are about? And what is actually the evidence? In contemplative science we critically examine how contemplative practices affect mind, brain and body. In this course, we will blend contemplative science with actual practice of the relevant contemplative practice. It seeks to give a broad overview of different practices. In session 1, we will practice mindfulness and discuss evidence for effect of mindfulness on cognition. In session 2, we will practice analytical meditation and discuss how it affects mind and brain. In session 3, we will focus on embodied practices and use contemplative dance as an example. There is relatively little science on this topic, but we will focus on the science of embodied practices in general, and leave space to discuss your own ideas about how to study these practices and\/or how to use them in your own life."
},
{
    "identifier": "SC8",
    "title": "AI Ethics",
    "fields": "Artificial Intelligence, Ethics",
    "url": "https://interdisciplinary-college.org/2025-sc8/",
    "type": "Special Course",
    "special_category":"Augmenting AI",
    "instructor": "Heike Felzmann",
    "description": "The rapid development of digital technologies and more recently AI has changed our personal, professional and societal practices in ways that open up opportunities, but also come with risks. In this course we are going to discuss ethical concerns related to these developments with the help of selected ethical concepts and relevant examples. Unit 1: We will begin with a discussion of how values are embedded in technologies and how this can be taken into account in technology development, considering the responsibilities of the technology developer. We will then reflect on privacy and consent to explore whether privacy is obsolete and how we can think about privacy in a rapidly changing information environment in which we are leaving a constantly expanding digital footprint. Unit 2: We will begin by looking at datafication, the transformation of human actions and experiences into data, and the ethical significance of drawing inferences from such data. We will discuss the problem of algorithmic bias and challenges arising when decision-making is handed over to algorithms and artificial intelligence, addressing problems of transparency, algorithmic governance and accountability. Unit 3: We will begin by discussing the close relation between surveillance, personalised services and the exercise of power in the digital sphere and will then reflect on Zuboff's theory of surveillance capitalism with regard to problems of human freedom and manipulation. We may also consider emerging risks of market concentration in AI. Unit 4: This unit focuses on the tension between potential benefits and risks of AI. We will discuss the potential impacts of human replacement by AI, relating to increasing human-AI interaction, the rise of synthetic media, and the replacement of human labour by AI. We will also reflect on the sustainability of AI and will finish by considering whether we should be worried about existential risk of AI."
},
{
    "identifier": "SC9",
    "title": "Challenges and opportunities of incremental learning",
    "short_title": "Incremental Machine Learning",
    "fields": "Machine Learning",
    "special_category":"Augmenting AI",
    "url": "https://interdisciplinary-college.org/2025-sc9/",
    "type": "Special Course",
    "instructor": "Barbara Hammer, Fabian Hinder",
    "description": "Incremental learning refers to the notion of machine learning methods which learn continuously from a given stream of training data rather than a priorly available batch. It carries great promises for model personalization and model adaptation in case of a changing environment. Example applications are personalization of wearables or monitoring of critical infrastructure. In comparison to classical batch learning, incremental addresses two main challenges: How to solve the algorithmic problem to efficiently adapt a model incrementally given limited memory recourses? How to solve the learning problem that the underlying input distribution might change within the stream, i.e. drift occurs?\r\n\r\nThe course will be split into three main parts: (1) Fundamentals of incremental learning algorithms and its applications, dealing with prototypical algorithmic solutions and exemplary applications. (2) Drift detection, dealing with the question what exactly is referred to by drift, and algorithms to locate drift in time. (4) Monitoring change, dealing with the question how to locate drift in space and provide explanations what exactly has caused the observed drift."
},
{
    "identifier": "SC10",
    "title": "Uncertainty and Trustworthiness in Natural Language Processing",
    "short_title": "Uncertainty and Trustworthiness in NLP",
    "fields": "Artificial Intelligence, Natural Language Processing",
    "url": "https://interdisciplinary-college.org/2025-sc10/",
    "type": "Special Course",
    "instructor": "Barbara Plank",
    "special_category":"Augmenting AI",
    "description": "Despite the recent success of Natural Language Processing (NLP) driven by advances in large language models (LLMs), there are many challenges ahead to make NLP more trustworthy. In this course, we will look at trustworthiness by taking the lens of uncertainty in language, from uncertainty in inputs, in outputs and how models deal with uncertainty themselves."
},
{
    "identifier": "SC11",
    "title": "Beyond Boxes and Arrows: Bridging the gap between theoretical models of cognition and the biological realities of the brain",
    "short_title": "Cognitive Models and Biological Realities",
    "fields": "Cognitive Modeling, Neuroscience, Cognitive Architectures",
    "url": "https://interdisciplinary-college.org/2025-sc11/",
    "type": "Special Course",
    "special_category":"Augmenting AI",
    "instructor": "Catherine Sibert",
    "description": "The human brain is an incredibly complex system, and many approaches and perspectives are taken in an effort to better understand how it works. While this diversity of approaches has led to many insights, too often they are taken in isolation from one another, and it can be difficult to incorporate the findings from one perspective into others. Modeling-based approaches to understanding cognition, with their origins in computer science, artificial intelligence, and information processing, struggle to connect with researchers with a stronger neuroscience focus, as the models often do not incorporate or function at a higher level of abstraction than what is known about the biological brain. However, what we do understand about the brain is often at a lower level of abstraction than can be easily connected to the higher level cognitive components of models. In this course, we will discuss the strengths and weaknesses of the modeling approach, what kind of information we can actually extract from the brain, and some examples of how the two approaches can be combined to form a broader perspective of how to study the brain."
},
{
    "identifier": "PC1",
    "title": "Mind and body user interfaces",
    "fields": "Neuroscience, Computer Science, Cognitive Science, Biology, Human-Computer Interaction",
    "url": "https://interdisciplinary-college.org/2025-pc1/",
    "type": "Practical Course",
    "instructor": "Marius Klug, Michael Bressler",
    "description": "This is a hands-on course on the fascinating world of physiological user interfaces. A quick introduction to the body and brain signals that can be measured\u2014such as heart rate, muscle activity, and brain activity\u2014plus a short tutorial on the provided Unity game engine prefabs will get you up to speed. Equipped with this, the course will be a series of supervised hands-on hackathon-style practical sessions where you will explore how various body signals can be harnessed to create innovative applications. We provide devices like the Polar H10, Myo Armband, and Muse S, which can read different physiological signals. Teams of a maximum of three participants will then ideate, design, and prototype interactive systems that use the provided body and mind signals as input for applications in the Unity game engine. The applications can be for desktop, mobile, and even XR platforms (we provide a few Meta Quest devices) and they should be designed to be entertaining and fun. The focus is on fostering creativity and collaboration while gaining deeper insights into how our bodies can interact with technology. Provided and generated software is expected to be uploaded to public code repositories. Finally, teams will pitch their ideas and demo applications to a jury and to the other participants. Aspiring participants are encouraged to visit the Unity tutorials at https:\/\/learn.unity.com\/pathway\/unity-essentials."
},
{
    "identifier": "PC2",
    "title": "Hands-on Hardware: (No) Brain in Robots and Edge Computing? Put the brain on 'em!",
    "short_title": "Brain in Robots and Edge Computing",
    "fields": "Robotics, Sensor Data Processing, Edge Computing, Machine Learning",
    "url": "https://interdisciplinary-college.org/2025-pc2/",
    "type": "Practical Course",
    "instructor": "Tim Tiedemann",
    "description": "(this course description is under construction)\r\nIn this practical course, we will touch hardware -- robots and microcontrollers -- and we will see: there is no brain inside!  But as robots and lone small edge hardware out in the woods would really benefit from something like a brain, this course  combines hardware and cognitive sciences. And as we are nice guys and gals, we will do it on our own: Put it on 'em! Put the brain on 'em!\r\n\r\nAs it is currently planned, different hardware will be on site and\/or accessible:\r\n- mobile wheel-based robot\r\n- autonomous underwater vehicle (AUV)\r\n- multiple microcontroller boards with different sensors\r\n- (and as there need to be disappointments:  systems in simulation)\r\n\r\nWe will try to implement different findings from the cognitive sciences (ie. Biology and Cognitive Psychology) or Data Science on the (small!) systems (some were already implemented by the instructor in research projects, some are brand-new and unrevealed).\r\n\r\nThe course is planned as BYOD and detailed descriptions of what notebook\/installation should be brought to the course to participate hands-on, will follow."
},
{
    "identifier": "PC3",
    "title": "Augmenting Senses: Can We Feel Space?",
    "fields": "Cognitive Science, Psychology",
    "url": "https://interdisciplinary-college.org/2025-pc3/",
    "type": "Practical Course",
    "instructor": "Julia Wache, Lio Franz",
    "description": "Beyond the normal use of your senses, what else is possible? There have been different approaches to augment your senses. Here you will learn and experience more about space perception. How do you perceive space? What is space perception, and can it be changed? People’s experiences and skills vary greatly on how easily they can orient themselves in unknown environments. What happens if you always knew where north is, as well? You can try it with the feelSpace belt, which always points towards north via vibration. You will perform your own little experiments to aim for a new perception of space. Throughout the course and the whole IK it will be possible to lend out compass belts. Spacial perception can also be augmented through sound, e.g. as known from bats through echolocation. Also humans are capable of using echoes to learn a lot about their environment which has proven to be a helpful orientation skill for many blind people. But how do blind people learn to use echolocation? And can sighted people learn to use echolocation? We will present you the basics of how it is instructed. You will learn that you can hear more than you might guess. 1.     Session: Introduction You will learn what sensory substitution and augmentation is and what is currently done to enable the use of information via unusual paths. We will present some examples and introduce to you the compass belt from feelSpace and the research behind it that proves how using this tool can help to gain a new sense of space. We will try out the compass belt and do some small experiments to experience the new “north-sense”. 2.     Session: From research to startup: feelSpace You will learn more about the story of feelSpace, the startup that develops tactile belts to make navigation easy for everyone. In groups you will develop your own ideas on using the presented technology and plan little experiments that you conduct in this session and throughout the course. 3.     Session: Echolocation Some blind people use echolocation to “see” their environment. How do they do it and what is possible with this method? In this session we teach you the basics and we will do some practical exercises to show you how you can “sense” the space around you. This session can be visited independently to the others. 4.     Session: Finalizing and presenting your sensory augmentation experiments You have time to rap up your results and short presentations and present them to the group. We will discuss your ideas and approaches and finish with a feedback round to discuss the experiences of the course participants with the compass belt and the entrepreneurial approach."
},
{
    "identifier": "PC4",
    "title": "Solving the Global Learning Crisis: AI in Action for Early Education",
    "short_title": "AI for Early Education",
    "fields": "Education, Artificial Intelligence, Machine Learning",
    "url": "https://interdisciplinary-college.org/2025-pc4/",
    "type": "Practical Course",
    "instructor": "Logan Bentley, Harriet Crisp",
    "description": "Join us for an interactive, hands-on workshop where you\u2019ll design AI-powered interventions to tackle real-world challenges in early childhood education. In this course we will merge academic expertise with industry-driven insights - equipping and inspiring you to create solutions addressing the global learning crisis.\r\n\r\nThe course will begin with foundational instruction in core concepts and practices of early childhood education, emphasizing its critical importance in shaping equitable futures. Then we\u2019ll explore the transformative potential of AI in education, presenting case studies from EIDU (https:\/\/www.eidu.com\/) where traditional pedagogical interventions have been augmented with AI. These will include:\r\n- Structured Pedagogy: How we leverage large language models (LLMs) to generate unique and relevant lesson plans that empower teachers.\r\n- Personalized Learning: The use of machine learning to tailor individual students\u2019 learning paths and maximize their learning outcomes.\r\n\r\nWith this foundation, we\u2019ll move into an immersive design workshop where you prototype practical, AI-driven interventions targeting improvement for early learners\u2019 literacy and numeracy. You\u2019ll be guided with instruction on best practices, proven frameworks, and lessons from our own experience.\r\n\r\nThe learning crisis is a global challenge; solving it demands collaboration across disciplines, industries, and borders. Let\u2019s explore together how we all can make a lasting impact."
},
{
    "identifier": "PC5",
    "title": "Augmenting your career with a PhD?",
    "fields": "Personal development \/ career advice",
    "url": "https://interdisciplinary-college.org/2025-pc5/",
    "type": "Practical Course",
    "instructor": "Jutta Kretzberg",
    "description": "Are you a student? Are you thinking about a PhD? And maybe even about a career in academia? \r\nDoes \u201cdoing a PhD\u201d sound like fun? Or rather like pain? \r\nMany Master students struggle with the decision if a PhD would be the right choice for their career. And a considerable percentage of PhD students continue wondering if their decision was right until they graduate (or even beyond). \r\nThere is no general advice who should pursue a PhD. The decision for or against a PhD is a personal one \u2013 it depends on many factors including your personality, your personal situation and the available job opportunities. The goal of this workshop is to help you to develop a clearer personal perspective on this decision.\r\n\r\nSession 1: External perspectives\r\nIn the first session, I will start with a brief overview of different options how to do a PhD in Germany. After that, we will interactively explore the perspectives of different stakeholders: What do Master students expect from doing a PhD? What do PhD supervisors expect from their PhD students? What do employers expect from applicants with a PhD versus a Master\u2019s degree? And what is the perspective of family and friends? \r\n\r\nSession 2: Your personal perspective\r\nIn preparation of the second session, you will write cards with your hopes, neutral expectations, and fears concerning yourself doing  a PhD. Teaming up with one of the other participants, both of you will cluster these cards into the categories \u201ctasks \/ skills\u201d, \u201ctopics \/ scientific questions\u201d, \u201cworking environment\u201d, and \u201cpersonal factors\u201d. Explaining your thoughts (and listening to the thoughts of your teammate) can sharpen your personal perspective and help you to identify core aspects of your career decisions. \r\n\r\n Please note: This workshop consists of two sessions and will be offered two times for a maximum of 20 participants each. Please register for one of the two workshop iterations via the list at \u2018Glaskasten\u2019. \r\nThe main target group of this workshop are Master\u2019s and advanced Bachelor\u2019s students. However, the method to develop your personal perspective is also applicable to further career steps. PhD students, PhD holders, and positive non-PhDs who are willing to share their perspectives are highly welcome!"
},
{
    "identifier": "PC6",
    "title": "Career Panel: What's Next? Making the Leap from Academia to Industry",
    "short_title" : "Career Panel",
    "fields": "Personal development \/ career advice",
    "url": "https://interdisciplinary-college.org/2025-pc6/",
    "type": "Practical Course",
    "instructor": "Sarah Schulz",
    "description": "How do you go from writing papers to solving real-world problems? From deep academic dives to navigating corporate landscapes? If you've ever found yourself wondering how to make the jump from academia to industry, this panel is where you'll get answers – and maybe a little inspiration, too. We're gathering a crew of peers who've successfully transitioned into the trenches of industry. They've tackled the big questions: How do you translate your academic expertise into skills employers actually want? What's the best way to handle rejection – or worse, radio silence? And how do you hold onto the parts of academia you loved while embracing the fast-paced, team-driven culture of industry? I'll keep the conversation rolling, make sure we cover the important questions, and create space for you to chime in, too. So if you're ready to start mapping out your next career move, grab a seat - we'll give you a head start."
},
{
    "identifier": "ET1",
    "title": "From a Creative User and Professional Peer Counselor to a Researcher in Upper Limb Prosthetics",
    "fields": "Medicine/Engineering",
    "url": "https://interdisciplinary-college.org/2025-et1/",
    "type": "Evening Talk",
    "instructor": "Lara Wilkin",
    "description": "Growing beyond oneself as an expert in one's own field and looking beyond the familiar horizon is often initially overlooked until one's competence is proven. Research is interdisciplinary, and who, if not the users themselves, possesses the in-depth knowledge of everyday requirements and challenges?\r\n\r\nBy combining a natural-scientific and technical affinity with a creative background, Lara Wilkin is engaged both in research and through her voluntary work and networks to pave new ways, raise awareness, and advocate for those affected. Her journey spans from being a creative user and professional peer worker to becoming a researcher in the field of upper limb prosthetics."
},
{
    "identifier": "ET2",
    "title": "How can cognitive science help in understanding what artificial intelligence systems cannot yet do?",
    "fields": "Cognitive Science, Artificial Intelligence",
    "url": "https://interdisciplinary-college.org/2025-et2/",
    "type": "Evening Talk",
    "instructor": "Constantin Rothkopf",
    "description": "Recent advances in artificial intelligence based on deep learning have led to the discovery of new medical drugs, the development of new materials, and the optimization of fusion reactor designs. However, claims about fundamental limitations persist: unpredictable blunders, limited robustness, and lack of explainability. The talk will present recent examples and studies contributing to the current debate on what current AI systems can do and what they cannot yet do. A central topic will be how to leverage Cognitive Science to understand the properties of such AI systems. The systems discussed include large language models, neural network models of economic decision-making, visual-language foundation models and the considered tasks range from the classic Bongard problems to sensorimotor control and planning under uncertainty to deontological ethical judgments. Topics will cover the anthropomorphization of AI systems, problems of data contamination and bias, Clever-Hans phenomena, inherent limitations of benchmarks, and fundamental limitations of evaluations and comparisons in terms of performance measures of behavior."
},
{
    "identifier": "ET3",
    "title": "Visual Number Sense in Generative AI Models",
    "fields": "Artificial Intelligence, Machine Learning, Cognitive Science",
    "url": "https://interdisciplinary-college.org/2025-et3/",
    "type": "Evening Talk",
    "instructor": "Ivana Kajic",
    "description": "Recent years have seen a proliferation of machine learning models that are capable of producing high-quality images that faithfully depict concepts described using natural language.  Such models can generate images that represent arbitrary objects, object attributes, and complex relations between objects. In this talk, I will show that despite these impressive advancements, such models can still struggle with relatively simple tasks. Specifically, I will demonstrate that even the most advanced models have only a rudimentary notion of number sense. Their ability to correctly generate a number of objects in an image is limited to small numbers, and it is highly dependent on the linguistic context the number term appears in. I will further highlight challenges associated with evaluation of different model capabilities, including evaluation of numerical reasoning, and talk about different automated approaches that can be used to evaluate models in a more interpretable way by leveraging existing tools in machine learning and cognitive science."
},
{
    "identifier": "JE1",
    "title": "Welcome Event",
    "fields": "",
    "url": "",
    "type": "Joint Event",
    "instructor": "Benjamin Paaßen, Cosima Prahm, Terry Stewart",
    "description": ""
},
{
    "identifier": "JE2",
    "title": "Poster Session",
    "fields": "",
    "url": "https://interdisciplinary-college.org/contribute/#poster",
    "type": "Joint Event",
    "instructor": "",
    "description": ""
},
{
    "identifier": "JE3",
    "title": "Conference Dinner",
    "fields": "",
    "url": "",
    "type": "Joint Event",
    "instructor": "",
    "description": ""
},
{
    "identifier": "JE4",
    "title": "Conclusion Event",
    "fields": "",
    "url": "",
    "type": "Joint Event",
    "instructor": "Benjamin Paaßen, Cosima Prahm, Terry Stewart",
    "description": ""
},
{
    "identifier": "RS1",
    "title": "Scientific Spotlights Session 1",
    "fields": "",
    "url": "https://interdisciplinary-college.org/contribute/#spotlight",
    "type": "Research Spotlight",
    "instructor": "",
    "description": "First Scientific Spotlight Session"
},
{
    "identifier": "RS2",
    "title": "Scientific Spotlights Session 2",
    "fields": "",
    "url": "https://interdisciplinary-college.org/contribute/#spotlight",
    "type": "Research Spotlight",
    "instructor": "",
    "description": "Second Scientific Spotlight Session"
},
{
    "identifier": "FL1",
    "title": "Enhancing Human Potential: UX Design from Concept to Reality",
    "short_title": "UX Design",
    "fields": "UX Design",
    "url": "https://interdisciplinary-college.org/2025-fl1/",
    "type": "Featured Lecture",
    "instructor": "Filiz Resim",
    "description": "Join me for a fun, interactive session that takes you from brainstorm to launch. I'll share real-world projects from the industry, showcasing the transformative journey of UX design from initial concept to final release. You'll get a glimpse into the importance of UX and its effects, how we incorporate AI in everyday work, as well as an insider's look at company processes and the pace of corporate life."
},
{
    "identifier": "FL2",
    "title": "How to Augment a Tractor with a “Mind”?",
    "short_title": "Augment a Tractor",
    "fields": "Computer Science, Robotics, Systems Engineering",
    "url": "https://interdisciplinary-college.org/2025-fl2/",
    "type": "Featured Lecture",
    "instructor": "Felix Hülsmann",
    "description": "In this talk, I would like to guide you through the development of an autonomous tractor that shall be usable in a legal way in Europe. During this trip, I will give you insights into the Systems Engineering approach. Through the whole talk, we will use the extension of a standard tractor with high-level autonomy as an example. After the talk, you will have a first impression of one of the standard approaches in the development of large technical systems, for instance in agricultural, but also in the automotive, aviation and lots of further industries. And ideally, you directly want to start developing the machinery of tomorrow."
},
{
    "identifier": "FL3",
    "title": "Philosophy of Artificial Consciousness",
    "fields": "Philosophy",
    "url": "https://interdisciplinary-college.org/2025-fl3/",
    "type": "Featured Lecture",
    "instructor": "Wanja Wiese",
    "description": "It is currently an open question to what extent advances in artificial intelligence (AI) can help to understand the mind (Buckner, 2023). A striking possibility is that, to the extent that AI models do capture mechanisms underlying our own cognitive capacities, implementations of such models in concrete applications will replicate aspects of the mind. This raises the question whether at some point consciousness might emerge in such AI systems, and whether they would be capable of feeling pleasure and pain, or might even suffer (Metzinger, 2021). Evaluating these disconcerting possibilities is made difficult by the current uncertainty about consciousness: • What is consciousness? • How can we know which artificial systems are likely (or unlikely) to be conscious? • What is the ethical significance of consciousness? Answers to these questions should be based on interdisciplinary research. This talk provides an overview of some philosophical issues that need to be addressed by such efforts."
}
];
</script>
