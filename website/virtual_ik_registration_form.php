<?php
/* Template Name: Virtual IK 2021 Registration Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, March 2020 -->
			<?php

				// definition of conference fee constants
				$conference_fees = [
					"MemberStudent" => 0,
					"NonMemberStudent" => 10,
					"MemberPhd" => 50,
					"NonMemberPhd" => 70,
					"MemberFull" => 80,
					"NonMemberFull" => 100,
				];

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// salutation
				if(!isset($_POST['salute'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the salutation was not specified. Please check your form again and re-submit.';
					exit;
				}
				$salute = sanitize_text_field( $_POST['salute'] );
				if(!($salute === 'Mr' || $salute === 'Ms' || $salute === 'Mx')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the salutation had an invalid value (must be either Ms, Mr, or Mx). Please check your form again and re-submit.';
					exit;
				}

				// title
				$title = "";
				if(isset($_POST['title_prof'])) {
					$title .= "Prof.";
					if(isset($_POST['title_dr'])) {
						$title .= " Dr.";
					}
				} else if(isset($_POST['title_dr'])) {
					$title .= "Dr.";
				}

				// last name
				if(!isset($_POST['lastname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the last name was not given. Please check your form again and re-submit.';
					exit;
				}
				$last_name = sanitize_text_field( $_POST['lastname'] );

				// first name
				if(!isset($_POST['firstname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the first name was not given. Please check your form again and re-submit.';
					exit;
				}
				$first_name = sanitize_text_field( $_POST['firstname'] );

				// phone
				if(!isset($_POST['phone'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the phone number was not given. Please check your form again and re-submit.';
					exit;
				}
				$phone = sanitize_text_field( $_POST['phone'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// Registration fee
				if(!isset($_POST['fee'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the fee information was not specified. Please check your form again and re-submit.';
					exit;
				}
				$fee_string = sanitize_text_field( $_POST['fee'] );

				if(!($fee_string === 'MemberStudent' || $fee_string === 'NonMemberStudent' || $fee_string === 'MemberPhd' || $fee_string === 'NonMemberPhd' || $fee_string === 'MemberFull' || $fee_string === 'NonMemberFull')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the fee information had an invalid value (must be MemberStudent, NonMemberStudent, MemberPhd, NonMemberPhd, MemberFull, or NonMemberFull). Please check your form again and re-submit.';
					exit;
				}

				// check that a membership is specified if a corresponding fee group
				// is selected
				if($fee_string === 'MemberStudent' || $fee_string === 'MemberPhd' || $fee_string === 'MemberFull') {
					if(!isset($_POST['member_orga'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your organization (GI or GK) was missing. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
						exit;
					}
					$member_orga = sanitize_text_field( $_POST['member_orga'] );
					if(!($member_orga === 'GI' || $member_orga === 'GK')) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the organization membership had an invalid value (must be GI or GK). Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
						exit;
					}
					if($member_orga === 'GI') {
						if(!isset($_POST['gi_number'])) {
							echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your member number was missing. Please check your form again and re-submit. If this does not work, please contact us at webmaster@interdisciplinary-college.org.';
							exit;
						}
						$gi_number = sanitize_text_field( $_POST['gi_number'] );
					} else {
						$gi_number = "";
					}
				} else {
					$member_orga = "";
				}

				// process stipend reason
				if(isset($_POST['stipend'])) {
					if(!isset($_POST['stipend_reason'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you selected that you would like to apply for a stipend but no reason for the stipend was given. Please check your form again and re-submit.';
						exit;
					} else {
						$stipend_reason = sanitize_textarea_field( $_POST['stipend_reason'] );
					}
				} else {
					$stipend_reason = "";
				}

				// process billing address

				// institution
				if(!isset($_POST['billing_institution'])) {
					$billing_institution = "";
				} else {
					$billing_institution = sanitize_text_field( $_POST['billing_institution'] );
				}

				// department
				if(!isset($_POST['billing_department'])) {
					$billing_department = "";
				} else {
					$billing_department = sanitize_text_field( $_POST['billing_department'] );
				}

				// street
				if(!isset($_POST['billing_street'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the street of your billing address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_street = sanitize_text_field( $_POST['billing_street'] );

				// house no.
				if(!isset($_POST['billing_no'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the house number of your billing address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_no = sanitize_text_field( $_POST['billing_no'] );

				// zip
				if(!isset($_POST['billing_zip'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the zip code of your billing address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_zip = sanitize_text_field( $_POST['billing_zip'] );

				// city
				if(!isset($_POST['billing_city'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the city of your billing address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_city = sanitize_text_field( $_POST['billing_city'] );

				// country
				if(!isset($_POST['billing_country'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the country of your billing address was not given. Please check your form again and re-submit.';
					exit;
				}
				$billing_country = sanitize_text_field( $_POST['billing_country'] );

				// process payment type
				if(!isset($_POST['payment'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type was unexpected (must be either Bank transfer for Credit card). Please check your form again and re-submit.';
					exit;
				}
				$payment = sanitize_text_field( $_POST['payment'] );

				// sunshine/moonshine checkbox does not need to be processed

				// process sunshine lecturers
				if(isset($_POST['sunshine_lecturers'])) {
					$sunshine_lecturers = sanitize_text_field( $_POST['sunshine_lecturers'] );
				} else {
					$sunshine_lecturers = '';
				}


				// randomly generate an interaction group number
				$interaction_group_no = random_int(1, 12);

				// preprocessing ends here

				// construct registration e-mail from user data
				$registration_mail  = "Salutation:         $salute\n";
				if($title !== '') {
					$registration_mail .= "Title:              $title\n";
				}
				$registration_mail .= "Last name:          $last_name\n";
				$registration_mail .= "First name:         $first_name\n";
				$registration_mail .= "University/Company: $billing_institution\n";
				$registration_mail .= "Faculty/Department: $billing_department\n";
				$registration_mail .= "Street/PO Box:      $billing_street $billing_no\n";
				$registration_mail .= "ZIP:                $billing_zip\n";
				$registration_mail .= "City:               $billing_city\n";
				$registration_mail .= "Country:            $billing_country\n";
				$registration_mail .= "Phone:              $phone\n";
				$registration_mail .= "E-Mail:             $email\n\n";

				$conference_fee = $conference_fees[$fee_string];
				$registration_mail .= "Conference fee:     $fee_string $member_orga $gi_number $conference_fee EUR via $payment.";
				if(isset($_POST['stipend'])) {
					$registration_mail .= "\n\nI would like to apply for a stipend.";
					$registration_mail .= "\nMy reason is: $stipend_reason";
				}

				$registration_mail .= "\n\nYou will receive an invoice via e-mail. Before the event, you will receive an access link to the virtual IK.";

				$registration_mail .= "\n\nInteraction group no.: $interaction_group_no";

				if(isset($_POST['sunshine'])) {
					$registration_mail .= "\n\nI would like to support the virtual IK as a sunshine.";
					$registration_mail .= "\nThese are the lecturers I would most like to help: $sunshine_lecturers";
				}

				if(isset($_POST['moonshine'])) {
					$registration_mail .= "\n\nI would like to support the virtual IK as a moonshine.";
				}

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				if(strlen($registration_mail) > 5000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the data was too long (> 5000 characters). Please check your form again and re-submit.';
					exit;
				}

				// create a machine readable version in JSON format
				$registration_object = (object)[];
				$registration_object->salutation = $salute;
				$registration_object->title = $title;
				$registration_object->first_name = $first_name;
				$registration_object->last_name = $last_name;
				$registration_object->phone = $phone;
				$registration_object->email = $email;
				$registration_object->fee_string = $fee_string;
				$registration_object->conference_fee = $conference_fee;
				$registration_object->member_orga = $member_orga;
				$registration_object->gi_number = $gi_number;
				$registration_object->stipend = isset($_POST['stipend']);
				$registration_object->stipend_reason = $stipend_reason;
				$registration_object->billing_institution = $billing_institution;
				$registration_object->billing_department = $billing_department;
				$registration_object->billing_street = $billing_street;
				$registration_object->billing_no = $billing_no;
				$registration_object->billing_zip = $billing_zip;
				$registration_object->billing_city = $billing_city;
				$registration_object->billing_country = $billing_country;
				$registration_object->payment = $payment;
				$registration_object->sunshine = isset($_POST['sunshine']);
				$registration_object->sunshine_lecturers = $sunshine_lecturers;
				$registration_object->moonshine = isset($_POST['moonshine']);
				$registration_object->interaction_group_no = $interaction_group_no;

				// special code for debug input
				if($first_name === 'Debug') {
					echo "<p id=\"success\">The debug is complete. The following email would be send to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>:</p> <pre>$registration_mail</pre>";
					$json_data = json_encode($registration_object);
					echo "<p>JSON data</p><pre>$json_data</pre>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				if(!wp_mail('registration@interdisciplinary-college.org', 'Virtual IK 2021 Registration', '<pre>' . $registration_mail . '</pre>', $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your registration has failed. Please send your registration manually to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$registration_mail</pre>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p id=\"success\">Your registration was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that your registration is only confirmed once you received your invoice. For your own archive: The following data was transmitted:</p> <pre>$registration_mail</pre>";
				}

				$headers = array('Content-Type: text/plain; charset=UTF-8');

				// send a machine-readable backup copy to the webmaster
				wp_mail('webmaster@interdisciplinary-college.org', 'Virtual IK Registration (machine readable copy)', json_encode($registration_object), $headers)

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
