<?php
/* Template Name: Registration Form 2022 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, November 2021 -->
			<?php
			/*
			 * If you wish to change any of the variables, you will have to
			 * check five locations in this file: first, the validation stage,
			 * starting around line 80; then, the construction of the
			 * registration mail (about line 330); the object
			 * representation of the registration data (about line 427);
			 */
				// definition of conference fee constants
				$conference_fees = [
					"Student" => 30,
					"Regular" => 50
				];

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// salutation
				if(!isset($_POST['salute'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the salutation was not specified. Please check your form again and re-submit.';
					exit;
				}
				$salute = sanitize_text_field( $_POST['salute'] );
				if(!($salute === 'Mr' || $salute === 'Ms' || $salute === 'Mx')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the salutation had an invalid value (must be either Ms, Mr, or Mx). Please check your form again and re-submit.';
					exit;
				}

				// title
				$title = "";
				if(isset($_POST['title_prof'])) {
					$title .= "Prof.";
					if(isset($_POST['title_dr'])) {
						$title .= " Dr.";
					}
				} else if(isset($_POST['title_dr'])) {
					$title .= "Dr.";
				}

				// first name
				if(!isset($_POST['firstname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the first name was not given. Please check your form again and re-submit.';
					exit;
				}
				$first_name = sanitize_text_field( $_POST['firstname'] );

				// last name
				if(!isset($_POST['lastname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the last name was not given. Please check your form again and re-submit.';
					exit;
				}
				$last_name = sanitize_text_field( $_POST['lastname'] );

				// phone number
				if(!isset($_POST['phone'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the phone number was not given. Please check your form again and re-submit.';
					exit;
				}
				$phone = sanitize_text_field( $_POST['phone'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// street
				if(!isset($_POST['street'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the street of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$street = sanitize_text_field( $_POST['street'] );

				// house no.
				if(!isset($_POST['no'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the house number of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$no = sanitize_text_field( $_POST['no'] );

				// zip
				if(!isset($_POST['zip'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the zip code of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$zip = sanitize_text_field( $_POST['zip'] );

				// city
				if(!isset($_POST['city'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the city of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$city = sanitize_text_field( $_POST['city'] );

				// country
				if(!isset($_POST['country'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the country of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$country = sanitize_text_field( $_POST['country'] );

				// faculty
				if(!isset($_POST['institution'])) {
					$institution = "";
				} else {
					$institution = sanitize_text_field( $_POST['institution'] );
				}

				// department
				if(!isset($_POST['department'])) {
					$department = "";
				} else {
					$department = sanitize_text_field( $_POST['department'] );
				}

				// process billing address if checked
				if(isset($_POST['different_billing_address'])) {

					// faculty
					if(!isset($_POST['billing_institution'])) {
						$billing_institution = "";
					} else {
						$billing_institution = sanitize_text_field( $_POST['billing_institution'] );
					}

					// department
					if(!isset($_POST['billing_department'])) {
						$billing_department = "";
					} else {
						$billing_department = sanitize_text_field( $_POST['billing_department'] );
					}

					// street
					if(!isset($_POST['billing_street'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the street of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_street = sanitize_text_field( $_POST['billing_street'] );

					// house no.
					if(!isset($_POST['billing_no'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the house number of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_no = sanitize_text_field( $_POST['billing_no'] );

					// zip
					if(!isset($_POST['billing_zip'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the zip code of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_zip = sanitize_text_field( $_POST['billing_zip'] );

					// city
					if(!isset($_POST['billing_city'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the city of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_city = sanitize_text_field( $_POST['billing_city'] );

					// country
					if(!isset($_POST['billing_country'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the country of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_country = sanitize_text_field( $_POST['billing_country'] );
				} else {
					// otherwise copy the data from the regular address
					$billing_institution = $institution;
					$billing_department = $department;
					$billing_street = $street;
					$billing_no = $no;
					$billing_city = $city;
					$billing_zip = $zip;
					$billing_country = $country;
				}

				// conference_fee
				if(!isset($_POST['conference_fee'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your conference fee was not given. Please check your form again and re-submit.';
					exit;
				}
				$fee_class = sanitize_text_field( $_POST['conference_fee'] );
				if(!($fee_class === 'Student' ||
					 $fee_class === 'Regular')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the conference fee had an invalid value (must be Student or Regular). Please check your form again and re-submit.';
					exit;
				}
				$conference_fee_string = $fee_class;

				// payment type
				if(!isset($_POST['payment'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type was not given. Please check your form again and re-submit.';
					exit;
				}
				$payment = sanitize_text_field( $_POST['payment'] );
				if(!($payment === 'Bank transfer' || $payment === 'Credit card')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type had an invalid value (must be either Bank transfer or Credit card). Please check your form again and re-submit.';
					exit;
				}

				if(!isset($_POST['consent_participant_list'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you did not indicate whether you consent to your data being included in the participant list. Please check your form again and re-submit.';
					exit;
				}
				$consent_participant_list = sanitize_text_field( $_POST['consent_participant_list'] );
				if(!($consent_participant_list === 'Yes' || $consent_participant_list === 'No')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your consent value to the participant list must be either Yes or No. Please check your form again and re-submit.';
					exit;
				}

				// randomly generate an interaction group number
				$interaction_group_no = random_int(1, 12);

				// construct registration e-mail from user data
				$registration_mail  = "Salutation:         $salute\n";
				if($title !== '') {
					$registration_mail .= "Title:              $title\n";
				}
				$registration_mail .= "Last name:          $last_name\n";
				$registration_mail .= "First name:         $first_name\n\n";

				// print address
				if(!isset($_POST['different_billing_address'])) {
					// print the address table without second column
					$registration_mail .= "University/Company: $institution\n";
					$registration_mail .= "Faculty/Department: $department\n";
					$registration_mail .= "Street/PO Box:      $street $no\n";
					$registration_mail .= "ZIP:                $zip\n";
					$registration_mail .= "City:               $city\n";
					$registration_mail .= "Country:            $country\n";
					$registration_mail .= "Phone:              $phone\n";
					$registration_mail .= "E-Mail:             $email\n";
				} else {
					// print the personal and billing address side by side.
					// to achieve proper alignment of both columns, we need to
					// fill the first column with white spaces.

					// retrieve the maximum length of a field in the
					// personal address to achieve proper alignment
					$max_len = max(strlen($institution), strlen('Personal'), strlen($street . ' ' . $no), strlen($zip), strlen($city), strlen($country), strlen($department));
					// print the header for the address table
					$fill = str_repeat(' ', $max_len - strlen('Personal'));
					$registration_mail .= "                    Personal$fill Billing\n";

					$fill = str_repeat(' ', $max_len - strlen($institution));
					$registration_mail .= "University/Company: $institution$fill $billing_institution\n";
					$fill = str_repeat(' ', $max_len - strlen($department));
					$registration_mail .= "Faculty/Department: $department$fill $billing_department\n";
					$fill = str_repeat(' ', $max_len - strlen($street . ' ' . $no));
					$registration_mail .= "Street/PO Box       $street $no$fill $billing_street $billing_no\n";
					$fill = str_repeat(' ', $max_len - strlen($zip));
					$registration_mail .= "ZIP:                $zip$fill $billing_zip\n";
					$fill = str_repeat(' ', $max_len - strlen($city));
					$registration_mail .= "City:               $city$fill $billing_city\n";
					$fill = str_repeat(' ', $max_len - strlen($country));
					$registration_mail .= "Country:            $country$fill $billing_country\n";
					$registration_mail .= "Phone:              $phone\n";
					$registration_mail .= "E-Mail:             $email\n";
				}
				// print the payment information. Again, we format the
				// information in a two-column layout, such that we need to
				// fill the first column with white spaces to get the
				// alignment right
				$max_len = max(strlen($conference_fee_string), strlen($room));

				$registration_mail .= "Payment:            $payment\n";

				$conference_fee     = $conference_fees[$conference_fee_string];
				$fill = str_repeat(' ', $max_len - strlen($conference_fee_string));
				$registration_mail .= "Conference fee:     $conference_fee_string$fill $conference_fee EUR";

				if(isset($_POST['tech']) || isset($_POST['moonshine'])) {
					$registration_mail .= "\n";
				}
				if(isset($_POST['tech'])) {
					$registration_mail .= "\nI volunteer as tech support.";
				}
				if(isset($_POST['moonshine'])) {
					$registration_mail .= "\nI volunteer as a moonshine.";
				}
				$registration_mail .= "\n\n";

				if($consent_participant_list === 'Yes') {
					$registration_mail .= "I agree to be included in the participant list.";
				} else {
					$registration_mail .= "I do not agree to be included in the participant list.";
				}

				$registration_mail .= "\n\nInteraction group no.: $interaction_group_no";

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				if(strlen($registration_mail) > 5000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the registration data was too long (> 5000 characters). Please check your form again and re-submit.';
					exit;
				}

				// create a machine readable version in JSON format
				$registration_object = (object)[];
				$registration_object->salutation = $salute;
				$registration_object->title = $title;
				$registration_object->first_name = $first_name;
				$registration_object->last_name = $last_name;
				$registration_object->phone = $phone;
				$registration_object->email = $email;
				$registration_object->address = (object)[];
				$registration_object->address->institution = $institution;
				$registration_object->address->department = $department;
				$registration_object->address->street = $street;
				$registration_object->address->no = $no;
				$registration_object->address->zip = $zip;
				$registration_object->address->city = $city;
				$registration_object->address->country = $country;
				$registration_object->billing_address = (object)[];
				$registration_object->billing_address->billing_institution = $billing_institution;
				$registration_object->billing_address->billing_department = $billing_department;
				$registration_object->billing_address->billing_street = $billing_street;
				$registration_object->billing_address->billing_no = $billing_no;
				$registration_object->billing_address->billing_zip = $billing_zip;
				$registration_object->billing_address->billing_city = $billing_city;
				$registration_object->billing_address->billing_country = $billing_country;
				$registration_object->conference_fee_string = $conference_fee_string;
				$registration_object->payment = $payment;
				$registration_object->conference_fee = $conference_fee;
				$registration_object->registration_date = date("Y-m-d H:i:s");
				$registration_object->consent_participant_list = $consent_participant_list;
				$registration_object->interaction_group_no = $interaction_group_no;
				if(isset($_POST['tech'])) {
					$registration_object->tech_volunteer = True;
				} else{
					$registration_object->tech_volunteer = False;
				}
				if(isset($_POST['moonshine'])) {
					$registration_object->moonshine_volunteer = True;
				} else {
					$registration_object->moonshine_volunteer = False;
				}

				// special code for debug input
				if($first_name === 'Debug') {
					echo "<p id=\"success\">Debug successful! The following data would have been transmitted:</p> <pre>$registration_mail</pre>";
					$json_data = json_encode($registration_object);
					echo "<p>JSON data</p><pre>$json_data</pre>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				if(!wp_mail('registration@interdisciplinary-college.org', 'IK Registration', '<pre>' . $registration_mail . '</pre>', $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p id=\"success\">We are very sorry, but unfortunately sending your registration has failed. Please send your registration manually to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$registration_mail</pre>";
					exit;
				} else {
					// otherwise, display a success message
					echo "<p id=\"success\">Your registration was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that your registration is only confirmed once you received your invoice via letter. For your own archive: The following data was transmitted:</p> <pre>$registration_mail</pre>";
				}

				$headers = array('Content-Type: text/plain; charset=UTF-8');

				// send a machine-readable backup copy to the webmaster
				wp_mail('webmaster@interdisciplinary-college.org', 'IK Registration (machine readable copy)', json_encode($registration_object), $headers)

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
