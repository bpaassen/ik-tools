/*
* This file defines the schedule data for an IK conference.
* In particular, this file should contain the following data:
* start_date: start date of the conference in the format "YYYY-MM-DD"
* dinner_date:the date of the conference dinner in the format "YYYY-MM-DD"
* end_date:   end date of the conference in the format "YYYY-MM-DD"
* courses:    a list of courses, each of which is supposed to be an
*             object with the following attributes:
*   "identifier" : Some unique string identifier for the course, e.g. BC1.
*                  The identifier must start with letters and end with at least
*                  one number. whitespaces and special characters are not
*                  permitted.
*   "title" :      The string title of the course.
*   "url" :        The URL to the course page.
*   "type" :       The type of the course, either "Basic Course"/"Introductory Course",
*                  "Method Course"/"Advanced Course", "Special Course"/"Focus Course",
*                  "Rainbow Course", "Evening Talk", "Additional Event", or "Hack".
*   "instructor" : The name of the instructor(s) as string.
*   "description": The course description as HTML string.
*/
start_date = "2020-03-13";
dinner_date = "2020-03-17";
end_date = "2020-03-20";
courses = [
/* Block 1: Friday to Sunday (morning and afternoon) */
{
	"identifier" : "IC2",
	"title" : "Introduction to Psychology",
	"url" : "https://interdisciplinary-college.org/2020-ic2/",
	"type" : "Introductory Course",
	"instructor" : "Katharina Krämer",
	"description" : "<p>This course is intended for all participants (psychologists and non-psychologists alike), who are curious about the human mind and its functions. And that is basically what psychology is about: Psychological research investigates the role of mental functions in individual and social behaviour and explores the physiological and biological processes that underlie cognitive functions and behaviours. As a social science, psychology aims to understand individuals and groups by establishing general principles and researching specific cases by using quantitative and qualitative research methods.</p><p>During this introduction to psychology we will get to know the major schools of thought, including behaviourism and cognitive psychology as well as psychoanalysis and psychodynamic psychology. Psychological research encompasses many subfields and includes different approaches to the study of mental processes and behaviour. In this course, we will focus in particular on the sub-disciplines of developmental psychology, social psychology, and clinical psychology.</p>"
},
{
	"identifier" : "FC09",
	"title" : "Using Robot Models to Explore the Exploratory Behaviour of Insects",
	"short_title" : "Robot Models of Insect Exploratory Behaviour",
	"url" : "https://interdisciplinary-college.org/2020-fc09/",
	"type" : "Focus Course",
	"instructor" : "Barbara Webb",
	"description" : "<p>Insects are often thought to show only fixed ‘robotic’ behaviours but in fact exhibit substantial flexibility, from maggots exploring their world to find which odours signal risk or reward, to ants and bees discovering and efficiently navigating between food sources scattered over a large environment. Yet insects also have small brains, providing the promise that we may be able to understand and model these aspects of intelligent behaviour down to the single neuron level. This course will describe the current state of research in insect exploration, emphasising an explicitly mechanistic view of explanation: to understand a system, we should (literally) try to build it. The final lecture will reflect on this methodology of modelling and what we can learn by implementing biological explanations as robots.</p>"
},
{
	"identifier" : "FC10",
	"title" : "Mindfulness as a Method to Explore your Mind-Wandering with Curiosity",
	"short_title" : "Mindfulness and Mind-Wandering",
	"url" : "https://interdisciplinary-college.org/2020-fc10/",
	"type" : "Focus Course",
	"instructor" : "Marieke Van Vugt",
	"description" : "<p>In the first session, we will introduce the methods of mindfulness, and discuss how mindfulness differs from mind-wandering. Contrary to popular belief, mindfulness is not the opposite of mind-wandering, but rather the cultivation of mindfulness involves becoming better friends with your mind so that you learn to become less stuck in thought processes. We will also review conceptual models of mindfulness and mind-wandering together with some research underpinnings. In addition, we will introduce the first and third-person perspective on studying the mind and basics of microphenomenology. We will also start a small experiment with our own mindfulness practice, which we will analyse in the last session of the course.</p><p>In the second session, we will continue our practice of mindfulness, and review research findings on the effects of mindfulness on cognitive function and brain activity.</p><p>In the third session, we will continue our practice of mindfulness. We will place mindfulness in the context of different meditation practices, discussing similarities and differences. We will also discuss in general how we can study mindfulness scientifically and how to do so rigorously.</p><p>In the fourth session, apart from practicing mindfulness, we will discuss the findings of our little experiments. There will also be ample space for questions and additional topics to discuss.</p>"
},
{
	"identifier" : "FC13",
	"title" : "Hominum-ex-Machina: About Artificial and Real Intelligence",
	"short_title" : "Hominum-ex-Machina",
	"url" : "https://interdisciplinary-college.org/2020-fc13/",
	"type" : "Focus Course",
	"instructor" : "Markus Krause",
	"description" : "<p>Modern computational systems have amazing capabilities. They can detect a face or fingerprint in millions of samples, find a search term in a sea of billions of documents, and control the flow of trillions of dollars. Some of these abilities seem almost supernatural and even frightening. Yet, our brains are still the architects of invention and might remain to be so for aeons to come. Understanding and utilising the difference between machine und human intelligence is one of the new frontiers of computer science. With the advent of the next AI winter integrating human intervention into almost autonomous systems will gain crucial importance in the near future.</p><p>In this course we aim at lifting a bit of the mystic shroud that surrounds artificial intelligence. We will uncover its abilities, unveil short comings, and even conjure a deep neural network from (almost) thin air. You do not need to be an experienced coder or mathematical genius. Basic python understanding, and 8 grade math skills are enough to follow the course and build your own “AI”. After this hopefully disillusionary exercise we take a refreshing dive into reality. We will investigate real intelligence and how our brains talent for strategic problem solving can fuse with the sheer calculation power of machines. We will explore how these socio-technical systems will shape the future and the risks and pitfalls of the Hominum-ex-Machina.</p>"
},
{
	"identifier" : "PrfC2",
	"title" : "Ethics in Science and Good Scientific Practice ",
	"url" : "https://interdisciplinary-college.org/2020-prfc2/",
	"type" : "Professional Course",
	"instructor" : "Hans-Joachim Pflüger",
	"description" : "<p>This course will focus on aspects of ethics in science and that “scientific curiosity” is not without limits although ethical standards may vary in different cultures. We shall focus on the relatively new field of neuro-ethics. Another important topic will be concerned with good scientific practice and its implications for the performance of science, and what kind of rules apply. To follow these rules of good scientific practice is among the key requirements for scientists worldwide as well as for any autonomous artificial intelligent systems and, thus, is one of the pillars of trust into scientific results. The participants will be confronted with examples of fraud in science, ethical problems and decisions as well as good scientific practice. The outcome of such ethical decisions and questions around good scientific practice will be discussed.</p>"
},
/* Block 2: Friday to Sunday (noon and late afternoon) */
{
	"identifier" : "IC4",
	"title" : "Introduction to Ethics in AI",
	"url" : "https://interdisciplinary-college.org/2020-ic4/",
	"type" : "Introductory Course",
	"instructor" : "Heike Felzmann",
	"description" : "<p>The last few years have seen an explosion of societal uses of AI technologies, but at the same time widespread public scepticism and fear about their use have emerged. In response to these concerns, a wide range of guidance documents for good practice in AI have been published by professional and societal actors recently. Both as researchers in AI and as consumers of AI it is helpful to understand ethical concepts and concerns associated with the use of AI and to be familiar with some of these guidance documents, in order to be able to reflect carefully on their ethical and social meaning and the balance of their benefits and risks and adapt one’s practices accordingly.</p><p>This course provides a general introduction to emergent ethical issues in the field of AI. It will be suitable for anyone with an interest in reflecting on how AI impacts on contemporary life and society.</p>"
},
{
	"identifier" : "MC1",
	"title" : "Applications of Bayesian Inference and the Free Energy Principle",
	"short_title" : "Bayesian Inference and Free Energy Principle",
	"url" : "https://interdisciplinary-college.org/2020-mc1/",
	"type" : "Method Course",
	"instructor" : "Christoph Mathys",
	"description" : "<p>We will start with a look at the fundamentals of Bayesian inference, model selection, and the free energy principle. We will then look at ways to reduce Bayesian inference to simple prediction adjustments based on precision-weighted prediction errors. This will provide a natural entry point to the field of active inference, a framework for modelling and programming the behaviour of agents negotiating their continued existence in a given environment. Under active inference, an agent uses Bayesian inference to choose its actions such that they minimize the free energy of its model of the environment. We will look at how an agent can infer the state of the environment and its own internal control states in order to generate appropriate actions.</p>"
},
{
	"identifier" : "FC07",
	"title" : "A Series of Interesting Choices: Risk, Reward, and Curiosity in Video Games",
	"short_title" : "CRR in Video Games",
	"url" : "https://interdisciplinary-college.org/2020-fc07/",
	"type" : "Focus Course",
	"instructor" : "Max Birk",
	"description" : "<p>Civilization’s Sid Meier defined video games as a series of interesting choices. Game-design aims to balance risk and reward for each choice made in a game, with the goal to create compelling experiences that draw people in and keep them spellbound. In this course you will create your own game and explore how modifying formal game elements applying psychological theory affects play experience.</p>"
},
{
	"identifier" : "FC12",
	"title" : "The Development of Curiosity",
	"url" : "https://interdisciplinary-college.org/2020-fc12/",
	"type" : "Focus Course",
	"instructor" : "Gert Westermann",
	"description" : "<p>Curiosity has been described as an important driver for learning from infancy onwards. But what is curiosity? How has it been conceptualized, and how has its role in infant learning been identified and characterized? This course will describe the main theories of what curiosity is and how it affects behaviour, and how recent developmental research has studied curiosity in infants and children. Here I will address children’s active role in their learning and in their language development, as well as their preference for specific types of information. I will also touch on the role of play in infant and child development. Computational modelling can help us to develop theories of the mechanisms underlying curiosity-based exploratory behaviour, and I will discuss some of these models.</p><p>This course does not require any prior knowledge and all topics will be introduced gently.</p>"
},
{
	"identifier" : "PC2",
	"title" : "Seeking Shaky Ground",
	"url" : "https://interdisciplinary-college.org/2020-pc2/",
	"type" : "Practical Course",
	"instructor" : "Claudia Muth & Elisabeth Zimmermann",
	"description" : "<p>Curiosity entails being able to delve into the unknown, to challenge habits of thinking, of acting, of reacting, of perceiving, – of sense-making. We can decide to let ourselves be challenged, we can seek uncertainty and the risk of not knowing what will come. This for example happens, when we try out a new physical activity we don’t master yet, e.g. an adult decides to learn to ride a horse. But it also happens, when we expose ourselves to art, challenging our patterns of perceiving.</p><p>In such situations we often loose and gain or regain stability. We thereby learn. Accepting moments of instability and uncertainty as part of each learning process can provide insight and even lead to experiencing such situations as pleasurable and rewarding.</p><p>Which preconditions and circumstances have to be met in order to develop an attitude of openness, of giving up anticipation and prediction, of letting go and letting come?</p><p>Becoming aware of our own patterns of moving, of perceiving, of relating to the world is one necessity. Establishing an atmosphere of trust, where “mistakes” are invited, another.</p><p>In this course we aim to put ourselves on “shaky ground” using exercises from dance/movement/contact improvisation, as well as techniques of design/art creation. We will try to become aware of, explore and play around with our habitual ways of interacting with the world and people around us, thereby challenge our habits and raise curiosity for the unknown.</p>"
},
/* Block 3: Sunday to Tuesday (morning and afternoon) */
{
	"identifier" : "IC3",
	"title" : "Introduction to Neuroscience",
	"url" : "https://interdisciplinary-college.org/2020-ic3/",
	"type" : "Introductory Course",
	"instructor" : "Till Bockemühl & Ronald Sladky",
	"description" : "<p>The brain, the cause of – and solution to – all of life’s problems. According to our brains it is the most fascinating structure in the known universe. Consisting of about 86 billion neurons of which each can form thousands of connections to other neurons it is also the most complex structure in the known universe. In this course we would like to give you a rough guide and introduction to the basic principles, fundamental theories, and methods of neuroscience.</p><p>We will demonstrate that neuroscience can be seen as a multi-modal, multi-level, multi-disciplinary research framework that aims at addressing the challenges of this megalomaniac scientific endeavor. We will see that different frameworks and methods can lead to conflicting empirical evidence, theoretical assumptions, and heated debates. However, we argue that this might be the only way to uncover the mysteries of our brain.</p><p>In this course we will cover a variety of scopes and perspectives. We will teach some of the fundamentals of neuroscience in human and non-human animals, but we will also explore some explanatory gaps between the different levels of inference.</p><p>On a phenomenal level we will investigate the functions of individual neurons and small networks. We will discuss if and how we can learn from (genetically modified) model animals about neural functions. To what degree is this relevant for understanding human brain function, such as learning and decision making? On the other hand, we will also investigate the state of the art in human brain mapping and cognitive neuroscience. Can findings from neuroimaging tell us anything at all about neurobiology – or are they just fancy illustrations that are better suited for children’s books?</p>"
},
{
	"identifier" : "FC03",
	"title" : "Arousal Interactions with Curiosity, Risk, and Reward from a Computational Cognitive Architecture Perspective",
	"short_title" : "Computational Cognitive Architectures of CRR",
	"url" : "https://interdisciplinary-college.org/2020-fc03/",
	"type" : "Focus Course",
	"instructor" : "Christopher Dancy",
	"description" : "<p>In this focused course, we will cover some of the many ways in which stress and arousal can modulate behaviors related to curiosity, risk, and reward through the interactions between physiological, affective, and cognitive processes. We will use the ACT-R/Phi architecture to think about this from a perspective of interacting mind and body processes. The Project Malmo (Minecraft) environment will also be used to show how we might implement some of the theoretical accounts as simulated agents in a virtual environment.</p>"
},
{
	"identifier" : "PC3",
	"title" : "Juggling - experience your brain at work",
	"url" : "https://interdisciplinary-college.org/2020-pc3/",
	"type" : "Practical Course",
	"instructor" : "Susan Wache & Julia Wache",
	"description" : "<p>In this course we will teach you how to juggle. Juggling is a motor activity that requires a lot of different skills. </p><p>The activity of juggling requires a lot of different abilities. Obviously, you need to learn the movement pattern and practice a lot to get the reward – being able to juggle! To learn such specific movement patterns requires a highly complex electrical and chemical circuitry in the brain, which becomes a more and more important field of neuroscience. Juggling seems to encourage nerve fiber growth and therefore scientist believe it not only promotes brain fitness in general but could also help with debilitating illnesses.</p><p>Nevertheless, learning to juggle requires attention, focus, concentration and persistence. As every juggler would agree, the key for success is repetition. We will teach juggling mainly practical. While training you can feel constant progress independently of your previous skill level. </p><p>In the last session you will also get an introduction to site swap, a mathematical description of juggling patterns you can notate, calculate and e.g. feed into a juggling simulator.</p>"
},
{
	"identifier" : "PC4",
	"title" : "Curious Making, Taking Fabrication Risks and Crafting Rewards",
	"short_title" : "Making, Fabrication, and Crafting",
	"url" : "https://interdisciplinary-college.org/2020-pc4/",
	"type" : "Practical Course",
	"instructor" : "Janis Meißner",
	"description" : "<p>This course is about getting hands-on curious with electronics and different crafts materials. Maker toolkits are a great way to get started with designing your own interactive sensor systems – but what if these designs could also integrate other (potentially more aesthetic) materials? E-textiles and paper circuits are good examples for how functional electronic systems can be recrafted with rewarding results. In principle, any every-day materials could be used with a bit of thinking outside the (tool)box. Let’s see what you will use to hack for your ideas!</p>"
},
{
	"identifier" : "PrfC1",
	"title" : "Curiosity, Risk, and Reward in Teaching in Higher Education",
	"short_title" : "Teaching in Higher Education",
	"url" : "https://interdisciplinary-college.org/2020-prfc1/",
	"type" : "Professional Course",
	"instructor" : "Ingrid Scharlau",
	"description" : "<p>Teaching is a very complex, multilayered activity, going far beyond transfer of knowledge. The three main topics of this IK help to understand important aspects of teaching in higher education: Teaching is risky (which is not a defect but at its core), teaching is a reciprocal, social and interpersonal practice, and teaching is often a gendered practice. The course covers these three aspects with a mix of theoretical input and reflection. In the first session, we will reflect on our cultural, disciplinary, and personal understanding of teaching and then cover the three aspects risk, reward, and, finally curiosity.</p>"
},
{
	"identifier" : "Pan1",
	"title" : "Lived Curiosity in Industry and Academia; panel; title TBA",
	"short_title" : "Industry & Academia Panel Discussion",
	"url" : "https://interdisciplinary-college.org/2020-pan1/",
	"type" : "Additional Event",
	"instructor" : "Becky Inkster",
	"description" : "<p>tba</p>"
},
/* Block 4: Monday to Tuesday (noon and late afternoon) */
{
	"identifier" : "IC1",
	"title" : "Introduction to Machine Learning",
	"url" : "https://interdisciplinary-college.org/2020-ic1/",
	"type" : "Introductory Course",
	"instructor" : "Benjamin Paassen",
	"description" : "<p>This course is intended for non-machine learners with little to no prior knowledge. It will provide many examples as well as accompanying exercises and limit the number of formulae to a bare minimum, while instead maximizing the number of meaningful images.</p>"
},
{
	"identifier" : "FC02",
	"title" : "Your Wit Is My Command",
	"url" : "https://interdisciplinary-college.org/2020-fc02/",
	"type" : "Focus Course",
	"instructor" : "Tony Veale",
	"description" : "<p>Until quite recently, AI was a scientific discipline defined more by its portrayal in science fiction than by its actual technical achievements. Real AI systems are now catching up to their fictional counterparts, and are as likely to be seen in news headlines as on the big screen. Yet as AI outperforms people on tasks that were once considered yardsticks of human intelligence, one area of human experience still remains unchallenged by technology: our sense of humour.</p><p>This is not for want of trying, as this course will show. The true nature of humour has intrigued scholars for millennia, but AI researchers can now go one step further than philosophers, linguists and psychologists once could: by building computer systems with a sense of humour, capable of appreciating the jokes of human users or even of generating their own, AI researchers can turn academic theories into practical realities that amuse, explain, provoke and delight.</p>"
},
{
	"identifier" : "FC08",
	"title" : "The Motivational Power of Curiosity – Information as Reward",
	"short_title" : "The Motivational Power of Curiosity",
	"url" : "https://interdisciplinary-college.org/2020-fc08/",
	"type" : "Focus Course",
	"instructor" : "Lily FitzGibbon",
	"description" : "<p>This course will provide an overview of research from a number of fields of psychology and neuroscience pertinent to the understanding of the motivational power of curiosity. In particular, we will discuss empirical findings from across the lifespan in the context of a reward learning framework of knowledge acquisition. We will consider where the subjective experiences of curiosity and interest fit into the model and how they might be differentiated. Finally, we will discuss and develop challenges, open questions, and testable predictions from the model, setting out a programme of work for the field. The aim of this final session is to generate and develop research ideas and foster new collaborations between course participants.</p>"
},
{
	"identifier" : "Db1",
	"title" : "Curiosity as a research field; debate; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-db1/",
	"type" : "Additional Event",
	"instructor" : "Praveen Paritosh",
	"description" : "<p>tba</p>"
},
/* Block 5: Tuesday to Thursday (morning and afternoon) */
{
	"identifier" : "MC2",
	"title" : "Symbolic Reasoning within Connectionist Systems",
	"url" : "https://interdisciplinary-college.org/2020-mc2/",
	"type" : "Method Course",
	"instructor" : "Klaus Greff",
	"description" : "<p>Our brains effortlessly organize our perception into objects which it uses to compose flexible mental models of the world. Objects are fundamental to our thinking and our brains are so good at forming them from raw perception, that it is hard to notice anything special happening at all. Yet, perceptual grouping is far from trivial and has puzzled neuroscientists, psychologists and AI researchers alike.</p><p>Current neural networks show impressive capacities in learning perceptual tasks but struggle with tasks that require a symbolic understanding. This ability to form high-level symbolic representations from raw data, I believe, is going to be a key ingredient of general AI.</p><p>During this course, I will try to share my fascination with this important but often neglected topic.</p><p>Within the context of neural networks, we will discuss the key challenges and how they may be addressed. Our main focus will be the so-called Binding Problem and how it prevents current neural networks from effectively dealing with multiple objects in a symbolic fashion.</p>"
},
{
	"identifier" : "FC05",
	"title" : "Confidence and Overconfidence",
	"url" : "https://interdisciplinary-college.org/2020-fc05/",
	"type" : "Focus Course",
	"instructor" : "Vivek Nityananda",
	"description" : "<p>Decision-making in human and animal societies often uses a confidence heuristic – trusting the decisions made by confident individuals. This could have the benefit of quick decision-making without having to explore risky options yourself. However, confidence is a good guide to decisions only if it reflects accuracy. When the trusted individuals are overconfident, this results in risky and often catastrophic decisions. Despite the possibility of these negative outcomes, overconfidence persists and is widespread. What are then the advantages of overconfidence? Using an evolutionary perspective demonstrates the individual and social rewards of overconfidence. This also helps us understand how we can make the most of confidence while avoiding the obvious costs of overconfidence.</p>"
},
/*{
	"identifier" : "FC06",
	"title" : "Cybersecurity/Curiosity, Risk, and Reward in Hacking; title TBA",
	"short_title" : "Cybersecurity",
	"url" : "https://interdisciplinary-college.org/2020-fc06/",
	"type" : "Focus Course",
	"instructor" : "Sadia Afroz</p>"
},*/
{
	"identifier" : "FC11",
	"title" : "Artificial curiosity for robot learning",
	"url" : "https://interdisciplinary-college.org/2020-fc11/",
	"type" : "Focus Course",
	"instructor" : "Nguyen Sao Mai",
	"description" : "<p>This course will provide an overview of research in machine learning and robotics of artificial curiosity. Also referred to as intrinsic motivation, this stream of algorithms inspired by theories of developmental psychology allow artificial agents to learn more autonomously, especially in stochastic high-dimensional environments, for redundant tasks, for multi-task, life-long or curriculum learning.</p>"
},
{
	"identifier" : "PC1",
	"title" : "Perceiving the World through Introspection",
	"url" : "https://interdisciplinary-college.org/2020-pc1/",
	"type" : "Practical Course",
	"instructor" : "Annekatrin Vetter & Sophia Reul",
	"description" : "<p>How do I experience the world around me? What might influence my decisions and actions in everyday life? How do I feel?  If you are curious to answer these questions we invite you to come to our course. In our four sessions we will focus on the broad field of self-experience. We are going to introduce you to different exercises and tools out of the range of self-awareness, mindfulness, body perception, biography reflection and interpersonal and intrapersonal communication. A curios mind is the only requirement to join our experiential-group and we are looking forward to welcome you at IK.</p>"
},
/* Block 6: Tuesday to Friday (noon and late afternoon) */
{
	"identifier" : "PrfC3",
	"title" : "Curiosity, Risk, and Reward in the Academic Job Search",
	"short_title" : "Academic Job Search",
	"url" : "https://interdisciplinary-college.org/2020-prfc3/",
	"type" : "Professional Course",
	"instructor" : "Emily King",
	"description" : "<p>So you’ve decided that you want a career or at least a job in academia; what’s next?  Imposter Syndrome is something that makes applying to lots of interesting jobs seem like a risk; when should you take the time to apply?  How can you leverage curiosity to broaden your job search? This single session course will cover some of the basics of the academic job search: how to decide whether to apply for a particular job and then how to make your application pop.  As the cover letter is the first thing that most hiring committees see, we will focus on how to make yours strong.  CVs, research statements, teaching portfolios, and interview questions will also be touched on.</p><p>This class will be discussion-based.  Please come with your questions! Also, if you want to submit a cover letter to be (constructively!) critiqued by the participants, please send it to me over email.</p>"
},
{
	"identifier" : "MC3",
	"title" : "Embodied Symbol Emergence",
	"url" : "https://interdisciplinary-college.org/2020-mc3/",
	"type" : "Method Course",
	"instructor" : "Malte Schilling & Michael Spranger",
	"description" : "<p>Symbols are the bedrock of human cognition. They play a role in planning, but are also crucial to understanding and modeling language. Since they are so important for human cognition, they are likely also vital for implementing similar abilities in software agents and robots.</p><p>The course will focus on symbols from two integrated perspectives. On the one hand, we look at the emergence of internal models through interaction with the environment and their role in sensorimotor behavior. This perspective is the embodied perspective. The first two lectures of the course concentrate on the emergence of internal models and grounded symbols in simple animals and agents and show how interaction with an environment requires internal models and how these are structured. Here we use robots to show how effective the discussed mechanisms are.</p><p>The second perspective is that symbols can also be socially constructed. In particular, we will focus on language and how it is grounded in embodiment but also social interaction. This will be the topic of the third and fourth lecture. We first investigate the emergence of grounded names and categories (and their terms) in social interactions between robots. The second two lectures of the course will focus on compositionality – that is the interaction of embodied categories in larger phrases or sentences and grammar.</p>"
},
/*{
	"identifier" : "MC4",
	"title" : "Learning Mappings via Symbolic, Probabilistic, and Connectionist Modeling",
	"short_title" : "Learning Mappings",
	"url" : "https://interdisciplinary-college.org/2020-mc4/",
	"type" : "Method Course",
	"instructor" : "Afsaneh Fazly</p>"
},*/
{
	"identifier" : "MC5",
	"title" : "Low Complexity Modeling in Data Analysis and Image Processing",
	"short_title" : "Low Complexity Modeling",
	"url" : "https://interdisciplinary-college.org/2020-mc5/",
	"type" : "Method Course",
	"instructor" : "Emily King",
	"description" : "<p>Are you curious about how to extract important information from a data set?  Very likely, you will be rewarded if you use some sort of low complexity model in your analysis and processing.  A low complexity model is a representation of data which is in some sense much simpler than what the original format of the data would suggest. For example, every time you take a picture with a phone, about 80% of the data is discarded when the image is saved as a JPEG file. The JPEG compression algorithm works due to the fact that discrete cosine functions yield a low complexity model for natural images that tricks human perception. As another example, linear bottlenecks, pooling, pruning, and dropout are all examples of enforcing a low complexity model on neural networks to prevent overfitting. </p>"
},
{
	"identifier" : "FC01",
	"title" : "Motifs for Neurocognitive Challenges from Individual to Evolutionary Time Scales",
	"short_title" : "Neurocognitive Challenges",
	"url" : "https://interdisciplinary-college.org/2020-fc01/",
	"type" : "Focus Course",
	"instructor" : "Wulf Haubensak",
	"description" : "<p>Brains are built to avoid threats and seize rewards – the basic behavioral challenges from survival in the wild to navigating complex societies. Here, we explore the underlying neuronal circuit motifs, from individual to evolutionary time scales. We will also discuss how network function is genetically programmed and how dysfunction might lead to psychiatric symptoms.</p>"
},
{
	"identifier" : "FC04",
	"title" : "Behaviour Modelling; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-fc04/",
	"type" : "Focus Course",
	"instructor" : "Fahim Kawsar",
	"description" : "<p>tba</p>"
},
/* Evening and Additional Events */
{
	"identifier" : "WA1",
	"title" : "Welcome Address",
	"url" : "https://interdisciplinary-college.org/program/#events",
	"type" : "Evening Talk",
	"instructor" : "Smeddinck, Rohlfing & Stewart"
},
{
	"identifier" : "ET1",
	"title" : "How to Know",
	"url" : "https://interdisciplinary-college.org/2020-et1/",
	"type" : "Evening Talk",
	"instructor" : "Celeste Kidd",
	"description" : "<p>This evening lecture will discuss Kidd’s research about how people come to know what they know. The world is a sea of information too vast for any one person to acquire entirely. How then do people navigate the information overload, and how do their decisions shape their knowledge and beliefs? In this talk, Kidd will discuss research from her lab about the core cognitive systems people use to guide their learning about the world—including attention, curiosity, and metacognition (thinking about thinking). The talk will discuss the evidence that people play an active role in their own learning, starting in infancy and continuing through adulthood. Kidd will explain why we are curious about some things but not others, and how our past experiences and existing knowledge shape our future interests. She will also discuss why people sometimes hold beliefs that are inconsistent with evidence available in the world, and how we might leverage our knowledge of human curiosity and learning to design systems that better support access to truth and reality.</p>"
},
{
	"identifier" : "PS1",
	"title" : "Poster Session",
	"url" : "https://interdisciplinary-college.org/contribute/#poster",
	"type" : "Evening Talk",
	"instructor" : ""
},
{
	"identifier" : "ET2",
	"title" : "Data-Driven Dynamical Models for Neuroscience and Neuroengineering",
	"url" : "https://interdisciplinary-college.org/2020-et2/",
	"type" : "Evening Talk",
	"instructor" : "Bing W. Brunton",
	"description" : "<p>Discoveries in modern neuroscience are increasingly driven by quantitative understanding of complex data. The work in my lab lies at an emerging, fertile intersection of computation and biology. I develop data-driven analytic methods that are applied to, and are inspired by, neuroscience questions. Projects in my lab explore neural computations in diverse organisms.  We work with theoretical collaborators on developing methods, and with experimental collaborators studying insects, rodents, and primates. The common theme in our work is the development of methods that leverage the escalating scale and complexity of neural and behavioural data to find interpretable patterns.</p>"
},
{
	"identifier" : "ET3",
	"title" : "Information as a Resource: How Organisms Deal with Uncertainty",
	"url" : "https://interdisciplinary-college.org/2020-et3/",
	"type" : "Evening Talk",
	"instructor" : "Alex Kacelnik",
	"description" : "<p>Organisms nearly always act with incomplete information about the outcome of possible actions. They can include unpredictability into their decision process (risk sensitivity), or allocate effort to reduce uncertainty (learning, sampling). In all cases, the consequences of uncertainty, and the cost of reducing it, affect the expected payoffs, and hence can be expected to play a role in the decision mechanisms. Similarly, designers of synthetic intelligences are starting to include information-seeking (i.e. curiosity) in the behaviour of autonomous artificial systems, including problem-solving robots. I will present several lines of behavioural research in this area.</p>"
},
{
	"identifier" : "ET4",
	"title" : "Cognitive Systems; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-et4/",
	"type" : "Evening Talk",
	"instructor" : "Tanja Schultz",
	"description" : "<p>tba</p>"
},
/* Rainbow Courses */
{
	"identifier" : "RC1",
	"title" : "Homeostatically-driven behavioral architectures: How to model biological organisms throughout their life-cycle",
	"short_title" : "Homeostatically-driven behavioral architectures",
	"url" : "https://interdisciplinary-college.org/2020-rc1/",
	"type" : "Rainbow Course",
	"instructor" : "Panagiotis Sakagiannis",
	"description" : "<p>Why do organisms behave? When do they take risks and when do rewards matter to them? What is the nervous system's role in a successful life cycle and how does it relate to its evolutionary origins? In this course, we adopt a behavioral modeler's view integrating insights from systems neuroscience, ecological energetics, and layered robotic architectures in order to sketch a framework for dynamic mechanistic models of biological behavior. We address the advantages and shortcomings of region-specific biologically realistic neurocomputational models, of agent-based ecological simulations and of optimality-driven intelligent artificial agents and discuss ways of combining these powerful computational tools with a focus on the persisting individual homeostasis. Nested behaviors, recurrent neural networks, and entangled spatiotemporal scales are our main modeling challenges. An intensively studied organism, the drosophila fruit fly larva, will serve as our model agent for the whole course.</p>"
},
{
	"identifier" : "RC2",
	"title" : "Can patterns of word usage tell us what lemon and moon have in common? Analyzing the semantic content of distributional semantic models",
	"short_title" : "Analyzing the semantic content of distributional semantic models",
	"url" : "https://interdisciplinary-college.org/2020-rc2/",
	"type" : "Rainbow Course",
	"instructor" : "Pia Sommerauer",
	"description" : "<p>Can patterns of textual contexts in which words appear tell you (or your model) that both, a lemon and the moon are described as yellow and round but differ with respect to (almost) everything else? In other words: How much information about concepts is encoded in patterns of word usage (i.e. distributional data)?</p><p>In this course, I will take stock of what we know about the semantic content encoded in data-derrived meaning representations (e.g Word2Vec), which are commonly used in Natural Language Processing and cognitive modelling (e.g. metaphor interpretation).</p><p>I will focus on how we can find out whether (and what) semantic knowledge they represent (beyond a general sense of semantic word similarity and relatedness). Drawing on methods in the area of neural network interpretability, I will discuss how we can “diagnose” semantic knowledge to find out whether a model can in fact distinguish flying from non-flying birds or tell you what lemons and the moon have in common.</p>"
},
{
	"identifier" : "RC3",
	"title" : "Representing Uncertainties in Artificial Neural Networks",
	"url" : "https://interdisciplinary-college.org/2020-rc3/",
	"type" : "Rainbow Course",
	"instructor" : "Kai Standvoss",
	"description" : "<p>Tracking uncertainties is a key capacity of an intelligent system for successfully interacting within a changing environment. Representations of uncertainty are relevant to optimally weigh sensory evidence against prior expectations, to adjust learning rates accordingly, and importantly to trade-off exploitation and exploration. Thus, uncertainty is a crucial component of curiosity and reward driven behavior. Additionally, calibrated uncertainty estimates are relevant for human interaction as well as reliable artificial systems. However, it is not yet well understood how uncertainties are tracked in the brain.</p><p>Bayesian views on Deep Learning offer a way to specify distributions over model parameters and to learn generative models of data generation processes. Thereby, different levels and kind of uncertainties can be represented. In this course, we will discuss different Bayesian methods to track uncertainties in neural networks and speculate about possible links to neuroscience.</p>"
}
];
calendar_data = `BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:IK 2020
X-WR-TIMEZONE:UTC
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:186mnqgahg7ui1up6nktek3et1@google.com
CREATED:20191227T135711Z
DESCRIPTION:
LAST-MODIFIED:20200117T102309Z
LOCATION:H4
SEQUENCE:6
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T163000Z
DTEND:20200315T180000Z
DTSTAMP:20200204T230237Z
UID:77p5a6sooodjgg1trqonmosngn@google.com
CREATED:20191227T123613Z
DESCRIPTION:
LAST-MODIFIED:20200117T102300Z
LOCATION:H4
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:48qhqpaon9kb2dffqq80coc2ja@google.com
CREATED:20191227T120138Z
DESCRIPTION:
LAST-MODIFIED:20200116T103622Z
LOCATION:H3
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:7muai2i09182rgpfr7iq77iiv9@google.com
CREATED:20191227T120508Z
DESCRIPTION:
LAST-MODIFIED:20200116T103531Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:01aeog5p1k8v52v5rk3lsoh9ih@google.com
CREATED:20191227T123619Z
DESCRIPTION:
LAST-MODIFIED:20200116T103506Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:14qmst7dpjo2lm457krc7dhndj@google.com
CREATED:20191227T121320Z
DESCRIPTION:
LAST-MODIFIED:20200116T103457Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC12: The Development of Curiosity (Westermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:7tsard6ogjk4j43ipioagi8ctl@google.com
CREATED:20191227T121141Z
DESCRIPTION:
LAST-MODIFIED:20200116T103443Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:1dmceia3gq57s7s4dv7c6tipeb@google.com
CREATED:20191227T121313Z
DESCRIPTION:
LAST-MODIFIED:20200116T103438Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC12: The Development of Curiosity (Westermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:5idncchra324609q03htcru7gn@google.com
CREATED:20191227T121328Z
DESCRIPTION:
LAST-MODIFIED:20200116T103431Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC12: The Development of Curiosity (Westermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:5c8ph9obqn1lek6e137nq4vej1@google.com
CREATED:20191227T121235Z
DESCRIPTION:
LAST-MODIFIED:20200116T103427Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:1pt58f0ljq8702kvndlf8mbi8v@google.com
CREATED:20191227T120426Z
DESCRIPTION:
LAST-MODIFIED:20200116T103410Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:58621aeaac3bk8cegnvaqhhdgb@google.com
CREATED:20191227T121241Z
DESCRIPTION:
LAST-MODIFIED:20200116T103406Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:095kh7rmgh1a59ivvt5i3of1u9@google.com
CREATED:20191227T123547Z
DESCRIPTION:
LAST-MODIFIED:20200116T103351Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:4fgfrqmbcrmeotu3pi48scja79@google.com
CREATED:20191227T120514Z
DESCRIPTION:
LAST-MODIFIED:20200116T103310Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:3otqa9l60k1ehf72l3rm254o0o@google.com
CREATED:20191227T121148Z
DESCRIPTION:
LAST-MODIFIED:20200116T103306Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:3nelli45keqcbpumvscc5ojfj6@google.com
CREATED:20191227T120400Z
DESCRIPTION:
LAST-MODIFIED:20200116T103147Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:0dpp05da6vu5v4ddtq0u1qdt9t@google.com
CREATED:20191227T123532Z
DESCRIPTION:
LAST-MODIFIED:20200116T103141Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:2fjv0q40oj0qj61cv6g330fvpa@google.com
CREATED:20191227T123326Z
DESCRIPTION:
LAST-MODIFIED:20200116T101305Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:4h5cid91lefo3sjfngoqmphek5@google.com
CREATED:20191227T123244Z
DESCRIPTION:
LAST-MODIFIED:20200116T101258Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:2vi84lqof9r0gj6p0970sf3b9h@google.com
CREATED:20191227T123202Z
DESCRIPTION:
LAST-MODIFIED:20200116T101248Z
LOCATION:H2
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:1mssnlldu3nunl6k26mq24vnks@google.com
CREATED:20191227T123333Z
DESCRIPTION:
LAST-MODIFIED:20200116T101242Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:6si7cuht1lno1p6l4d5f7dq4mp@google.com
CREATED:20191227T135703Z
DESCRIPTION:
LAST-MODIFIED:20200116T101143Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:25t1f3i4i7eodt7vhmlj7kea70@google.com
CREATED:20191227T135650Z
DESCRIPTION:
LAST-MODIFIED:20200116T101136Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:70qu1vkfcbqes0qufqb5rcdb43@google.com
CREATED:20191227T135633Z
DESCRIPTION:
LAST-MODIFIED:20200116T101129Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:25r8ltimv0ptbjn8lnac2g19sf@google.com
CREATED:20191227T103533Z
DESCRIPTION:
LAST-MODIFIED:20200116T101113Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:0tpe9r0r11l99ej6b3d5p4s9al@google.com
CREATED:20191227T103605Z
DESCRIPTION:
LAST-MODIFIED:20200116T101109Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:39u8ubu2ids6og6jk73vu1ujss@google.com
CREATED:20191227T103613Z
DESCRIPTION:
LAST-MODIFIED:20200116T101103Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:0ulori0vkgglq9d8kg5bk702tt@google.com
CREATED:20191227T103548Z
DESCRIPTION:
LAST-MODIFIED:20200116T101058Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:0dthk2h0k7ggpocm691amomdrt@google.com
CREATED:20191227T122743Z
DESCRIPTION:
LAST-MODIFIED:20200116T032922Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC01: Motifs for Neurocognitive Challenges from Individual to Evolu
 tionary Time Scales (Haubensack)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:2md2964b2b5apfratmn9u0uv3l@google.com
CREATED:20191227T122730Z
DESCRIPTION:
LAST-MODIFIED:20200116T032917Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC01: Motifs for Neurocognitive Challenges from Individual to Evolu
 tionary Time Scales (Haubensack)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:4d0qp6ekepgjjmp10er8oihgab@google.com
CREATED:20191227T133649Z
DESCRIPTION:
LAST-MODIFIED:20200116T032911Z
LOCATION:H2
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:5v73v4li552bo8e2tnu7gtagea@google.com
CREATED:20191227T135740Z
DESCRIPTION:
LAST-MODIFIED:20200116T032904Z
LOCATION:H2
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:0g9ji5cgc4iig0qkvpdbgo1mk0@google.com
CREATED:20191227T135730Z
DESCRIPTION:
LAST-MODIFIED:20200116T032857Z
LOCATION:H2
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:53j7lcqdni8grnra8pjlrg953f@google.com
CREATED:20191227T135754Z
DESCRIPTION:
LAST-MODIFIED:20200116T032853Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:157mgalmdiocsurqd701glsm5u@google.com
CREATED:20191227T133536Z
DESCRIPTION:
LAST-MODIFIED:20200116T032817Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC5: Low Complexity Modeling in Data Analysis and Image Processing 
 (King)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:43j65kct1n8d67dmk1ug97e77o@google.com
CREATED:20191227T133541Z
DESCRIPTION:
LAST-MODIFIED:20200116T032814Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:MC5: Low Complexity Modeling in Data Analysis and Image Processing 
 (King)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:52bdm8vluetvf8hrh54s8qbkr9@google.com
CREATED:20191227T124435Z
DESCRIPTION:
LAST-MODIFIED:20200116T032803Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC5: Low Complexity Modeling in Data Analysis and Image Processing 
 (King)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:72mjlv6hg3pcoq2iblt3odsfua@google.com
CREATED:20191227T124424Z
DESCRIPTION:
LAST-MODIFIED:20200116T032758Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PrfC3: Curiosity\, Risk\, and Reward in the Academic Job Search (Ki
 ng)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:7k9oq7sbrp0p94vddjs4b83k1s@google.com
CREATED:20191227T122722Z
DESCRIPTION:
LAST-MODIFIED:20200116T032737Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC01: Motifs for Neurocognitive Challenges from Individual to Evolu
 tionary Time Scales (Haubensack)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:0i779g606hl4cj5u10id3dgepm@google.com
CREATED:20200107T190609Z
DESCRIPTION:
LAST-MODIFIED:20200116T032637Z
LOCATION:F2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:17l8h9gljrclim87ok8srl401f@google.com
CREATED:20200107T190555Z
DESCRIPTION:
LAST-MODIFIED:20200116T032627Z
LOCATION:F2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:16kra8qti4l765n2phhqgl9r45@google.com
CREATED:20191227T151007Z
DESCRIPTION:
LAST-MODIFIED:20200116T032620Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:59e3ta4o8pdukdilipamka954q@google.com
CREATED:20200107T190632Z
DESCRIPTION:
LAST-MODIFIED:20200116T032616Z
LOCATION:F2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:619vq6ib346lq2pmqcvoj4umn0@google.com
CREATED:20191227T114146Z
DESCRIPTION:
LAST-MODIFIED:20200116T032557Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:7ni4n4pk7sps2gkkplmsmav207@google.com
CREATED:20191227T123448Z
DESCRIPTION:
LAST-MODIFIED:20200116T032553Z
LOCATION:F3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:69c1lgo69balpagt4heav18n4g@google.com
CREATED:20191227T123906Z
DESCRIPTION:
LAST-MODIFIED:20200116T032510Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:29523prlobc6mr6tmaubp97s46@google.com
CREATED:20191227T124134Z
DESCRIPTION:
LAST-MODIFIED:20200116T032506Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T110000Z
DTEND:20200317T123000Z
DTSTAMP:20200204T230237Z
UID:66es4ekb3n8vjuanvfvkilfep7@google.com
CREATED:20191227T114026Z
DESCRIPTION:
LAST-MODIFIED:20200116T032437Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T110000Z
DTEND:20200317T123000Z
DTSTAMP:20200204T230237Z
UID:3t5o7ucanf0f4bi62h82foddmo@google.com
CREATED:20191227T123438Z
DESCRIPTION:
LAST-MODIFIED:20200116T032432Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:520qh2e246mabtij9r9ksh7fqa@google.com
CREATED:20191227T123726Z
DESCRIPTION:
LAST-MODIFIED:20200116T032416Z
LOCATION:H2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:3b271l5kcc6qckktq9ubbeluc9@google.com
CREATED:20191227T115633Z
DESCRIPTION:
LAST-MODIFIED:20200116T032402Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:1cfqjatbg5s5lja5oguth417k5@google.com
CREATED:20191227T115609Z
DESCRIPTION:
LAST-MODIFIED:20200116T032354Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:3hedso6callekbsbg48indkdlv@google.com
CREATED:20191227T120200Z
DESCRIPTION:
LAST-MODIFIED:20200116T032345Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:5aa0h6c5dbeeahij1h0vkri30t@google.com
CREATED:20191227T123731Z
DESCRIPTION:
LAST-MODIFIED:20200116T032340Z
LOCATION:H2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:25sajnfmjeeucbk80m7mko48k2@google.com
CREATED:20191227T123717Z
DESCRIPTION:
LAST-MODIFIED:20200116T032335Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:4en2ag4ntpgpf850adjmrdgkl5@google.com
CREATED:20191227T120154Z
DESCRIPTION:
LAST-MODIFIED:20200116T032328Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:6snjsgsraahkoes3k8cdkqlei9@google.com
CREATED:20191227T115539Z
DESCRIPTION:
LAST-MODIFIED:20200116T032323Z
LOCATION:H3
SEQUENCE:7
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:3l8ilpt3ri81ra265ab7bvq21a@google.com
CREATED:20191227T114345Z
DESCRIPTION:
LAST-MODIFIED:20200116T030639Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:64gh0f652p4nooru0hhkgllctu@google.com
CREATED:20191227T123455Z
DESCRIPTION:
LAST-MODIFIED:20200116T030631Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T110000Z
DTEND:20200316T123000Z
DTSTAMP:20200204T230237Z
UID:5272vqtecim9u0vrdgk3ll9nfn@google.com
CREATED:20191227T114049Z
DESCRIPTION:
LAST-MODIFIED:20200116T030621Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T110000Z
DTEND:20200316T123000Z
DTSTAMP:20200204T230237Z
UID:3drg0cd5g4kkg2li1rr3s0h5o1@google.com
CREATED:20191227T123431Z
DESCRIPTION:
LAST-MODIFIED:20200116T030559Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:6nsf9qtn87mssamg017dapqd2e@google.com
CREATED:20191227T120146Z
DESCRIPTION:
LAST-MODIFIED:20200116T030437Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:35hj4s4962cqf9s4a1g3n5ihfl@google.com
CREATED:20191227T123709Z
DESCRIPTION:
LAST-MODIFIED:20200116T030432Z
LOCATION:H2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:2vpgbs3or4fru3ohh0jrmef5kn@google.com
CREATED:20191227T115621Z
DESCRIPTION:
LAST-MODIFIED:20200116T030319Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:5mbb1dms92nrbhtumtooaevp0j@google.com
CREATED:20191227T103327Z
DESCRIPTION:
LAST-MODIFIED:20200116T025740Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:48asld3utm78mih95kdqhc4060@google.com
CREATED:20191227T121856Z
DESCRIPTION:
LAST-MODIFIED:20200116T025547Z
LOCATION:F1
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:64dv40av1bu9fo96gd1loia2h1@google.com
CREATED:20191227T121834Z
DESCRIPTION:
LAST-MODIFIED:20200116T025536Z
LOCATION:F1
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:40tjted2h15cg5mv8evhl9cppe@google.com
CREATED:20191227T121849Z
DESCRIPTION:
LAST-MODIFIED:20200116T025531Z
LOCATION:F1
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:5orejl6jk06hdf3be15h54ido6@google.com
CREATED:20191227T121825Z
DESCRIPTION:
LAST-MODIFIED:20200116T025525Z
LOCATION:F1
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:3s2rmfs64dr8ou24po4mjkmh02@google.com
CREATED:20191227T103318Z
DESCRIPTION:
LAST-MODIFIED:20200116T025404Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:79b1smm9umnqce8c786lvhsrdk@google.com
CREATED:20191227T103305Z
DESCRIPTION:
LAST-MODIFIED:20200116T025347Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:4j15phjk6vm96ibr9pic58k12s@google.com
CREATED:20191227T103159Z
DESCRIPTION:
LAST-MODIFIED:20200116T025337Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:17t9p9anjhe9btmfi5lv2g01n0@google.com
CREATED:20191227T120614Z
DESCRIPTION:
LAST-MODIFIED:20200116T025212Z
LOCATION:F1
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:0d7d5kntl2p67jrf63h0ncs3bm@google.com
CREATED:20191227T120620Z
DESCRIPTION:
LAST-MODIFIED:20200116T025200Z
LOCATION:F1
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:7j670ou75d6edatemml9qrppkr@google.com
CREATED:20191227T120551Z
DESCRIPTION:
LAST-MODIFIED:20200116T025154Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:0siussur61qmgklv06cpbujscg@google.com
CREATED:20191227T120603Z
DESCRIPTION:
LAST-MODIFIED:20200116T025147Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:7m7vdecaboop00veq0m7sgb5k5@google.com
CREATED:20191227T115101Z
DESCRIPTION:
LAST-MODIFIED:20200116T012639Z
LOCATION:H4
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC4: Learning Mappings via Symbolic\, Probabilistic\, and Connectio
 nist Modeling (Fazly)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:0vrafdu4rh381odci2cpktjeif@google.com
CREATED:20191227T115028Z
DESCRIPTION:
LAST-MODIFIED:20200116T012631Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC4: Learning Mappings via Symbolic\, Probabilistic\, and Connectio
 nist Modeling (Fazly)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:6l8cqqjej0lt16ele03238or9q@google.com
CREATED:20191227T115036Z
DESCRIPTION:
LAST-MODIFIED:20200116T012627Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC4: Learning Mappings via Symbolic\, Probabilistic\, and Connectio
 nist Modeling (Fazly)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:77sagh9sdnk6m0vf6k0k95a8lv@google.com
CREATED:20191227T124034Z
DESCRIPTION:
LAST-MODIFIED:20200116T005913Z
LOCATION:H2
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:6gqe1i6qria1q0d7esb97d5l8m@google.com
CREATED:20191227T124058Z
DESCRIPTION:
LAST-MODIFIED:20200116T005903Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:4cdfi9d32sopctdimmfhmdve2b@google.com
CREATED:20191227T124106Z
DESCRIPTION:
LAST-MODIFIED:20200116T005842Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:2c42rlhkbhkp8s93cu7cu0plu0@google.com
CREATED:20191227T124046Z
DESCRIPTION:
LAST-MODIFIED:20200116T005833Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:1etr9djtd40amje6fd0nieh7b8@google.com
CREATED:20200115T231059Z
DESCRIPTION:
LAST-MODIFIED:20200115T231101Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:31apcqakpt556ha64e21q4h3mo@google.com
CREATED:20191227T123036Z
DESCRIPTION:
LAST-MODIFIED:20200103T134800Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:72isal8l7n04r1mqsnrg2vf95j@google.com
CREATED:20191227T123020Z
DESCRIPTION:
LAST-MODIFIED:20200103T134756Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:76g7utamcgukq1omouhdiefaf5@google.com
CREATED:20191227T123028Z
DESCRIPTION:
LAST-MODIFIED:20200103T134751Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:441cfbq0dhqmonaihf3m8jop6g@google.com
CREATED:20191227T123011Z
DESCRIPTION:
LAST-MODIFIED:20200103T134745Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:0no8tdqnvljbc7jvgad2ldbmge@google.com
CREATED:20191227T120254Z
DESCRIPTION:
LAST-MODIFIED:20200103T134639Z
LOCATION:F2
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:7pg7autvefut9le8r3lea587v5@google.com
CREATED:20191227T122823Z
DESCRIPTION:
LAST-MODIFIED:20200103T134558Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T110000Z
DTEND:20200317T123000Z
DTSTAMP:20200204T230237Z
UID:67h5d42vdri8o3b1ge7v8acirp@google.com
CREATED:20191227T122839Z
DESCRIPTION:
LAST-MODIFIED:20200103T134550Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:013ktmqsnpluh59lg5fujh8m6k@google.com
CREATED:20191227T122833Z
DESCRIPTION:
LAST-MODIFIED:20200103T134546Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T110000Z
DTEND:20200316T123000Z
DTSTAMP:20200204T230237Z
UID:6kfbbi35gdiu8gh89p2rii823g@google.com
CREATED:20191227T122815Z
DESCRIPTION:
LAST-MODIFIED:20200103T134530Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:6go79ua50hp6dbbaii17likfs5@google.com
CREATED:20191227T120317Z
DESCRIPTION:
LAST-MODIFIED:20200103T134504Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:0pit14h5csunjclb6qtuui2klf@google.com
CREATED:20191227T120308Z
DESCRIPTION:
LAST-MODIFIED:20200103T134501Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:23bku6miqqvhskg32qqcsvos9v@google.com
CREATED:20191227T120301Z
DESCRIPTION:
LAST-MODIFIED:20200103T134459Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:0qrqs5hjqdon53j0763sotb8ae@google.com
CREATED:20191227T114558Z
DESCRIPTION:
LAST-MODIFIED:20200103T131635Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:5nfpq02g4qqgnopdkip91r3ts7@google.com
CREATED:20191227T114544Z
DESCRIPTION:
LAST-MODIFIED:20200103T131630Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:4vcsce5ds83kbou6hp4lgqkqrm@google.com
CREATED:20191227T114129Z
DESCRIPTION:
LAST-MODIFIED:20200103T131625Z
LOCATION:F2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:13oubqg4tshsbu2l5ci79kal4t@google.com
CREATED:20191227T114514Z
DESCRIPTION:
LAST-MODIFIED:20200103T131623Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:7hu8ek5v6sl7g6b690r6m5qmqb@google.com
CREATED:20191227T121559Z
DESCRIPTION:
LAST-MODIFIED:20200103T131555Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:3502m9d38fsnuicbo3hfj5rta3@google.com
CREATED:20191227T121428Z
DESCRIPTION:
LAST-MODIFIED:20200103T131553Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:7ioi0uvbamaalc92uqp15d0n6m@google.com
CREATED:20191227T121552Z
DESCRIPTION:
LAST-MODIFIED:20200103T131551Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T163000Z
DTEND:20200315T180000Z
DTSTAMP:20200204T230237Z
UID:1cbgmii2h3q0qbgs3siosbhsbj@google.com
CREATED:20191227T095641Z
DESCRIPTION:
LAST-MODIFIED:20191227T150729Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Pan1: Academia &amp; Industry Panel (Inkster)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T200000Z
DTEND:20200316T213000Z
DTSTAMP:20200204T230237Z
UID:5fes2q1runtvkaslmkpdd16qnv@google.com
CREATED:20191227T105028Z
DESCRIPTION:
LAST-MODIFIED:20191227T150608Z
LOCATION:Dining Hall
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:ET2: Data-Driven Dynamical Models for Neuroscience and Neuroenginee
 ring (Brunton)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T200000Z
DTEND:20200314T213000Z
DTSTAMP:20200204T230237Z
UID:3om15rdo11mkug8eei02lrfue1@google.com
CREATED:20191227T103753Z
DESCRIPTION:
LAST-MODIFIED:20191227T150604Z
LOCATION:Dining Hall
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:ET1: How to know (Kidd)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:044g33t1vjjo8fkhur8dme2g7e@google.com
CREATED:20191227T124200Z
DESCRIPTION:
LAST-MODIFIED:20191227T124200Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:29k4brksqb3n3brnus7g4a66e1@google.com
CREATED:20191227T124151Z
DESCRIPTION:
LAST-MODIFIED:20191227T124151Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:3o464kv33vvct2qkpafd0r69i4@google.com
CREATED:20191227T124143Z
DESCRIPTION:
LAST-MODIFIED:20191227T124143Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:7gjm71e76ojbas79t3d101pihe@google.com
CREATED:20191227T123949Z
DESCRIPTION:
LAST-MODIFIED:20191227T123949Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:2nvddratom34sd4ar82k1ak3ga@google.com
CREATED:20191227T123941Z
DESCRIPTION:
LAST-MODIFIED:20191227T123941Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:4mhmqsfn3c57ds8g04fisasrn9@google.com
CREATED:20191227T123933Z
DESCRIPTION:
LAST-MODIFIED:20191227T123934Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:5v1armurdvtct1jv8f1dov6tt9@google.com
CREATED:20191227T115447Z
DESCRIPTION:
LAST-MODIFIED:20191227T115448Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:5fleo3gadfif8229jbj7ki1hse@google.com
CREATED:20191227T115439Z
DESCRIPTION:
LAST-MODIFIED:20191227T115439Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:7msrftqcil57e4k74f9hrkked5@google.com
CREATED:20191227T115432Z
DESCRIPTION:
LAST-MODIFIED:20191227T115432Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:252liug2nf9g18lq7m1jeac40c@google.com
CREATED:20191227T115422Z
DESCRIPTION:
LAST-MODIFIED:20191227T115426Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T200000Z
DTEND:20200318T213000Z
DTSTAMP:20200204T230237Z
UID:2oterd7css1ie2ahgccpooi1j8@google.com
CREATED:20191227T114846Z
DESCRIPTION:
LAST-MODIFIED:20191227T114846Z
LOCATION:Dining Hall
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:ET4: Cognitive Systems (Schultz)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T193000Z
DTEND:20200317T220000Z
DTSTAMP:20200204T230237Z
UID:51gneemai7pgnulg7oeefnqtkn@google.com
CREATED:20191227T114756Z
DESCRIPTION:
LAST-MODIFIED:20191227T114756Z
LOCATION:Dining Hall
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Conference Dinner
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T180000Z
DTEND:20200317T183000Z
DTSTAMP:20200204T230237Z
UID:7emeb2636l7pmu3irshvvtjm0p@google.com
CREATED:20191227T114654Z
DESCRIPTION:
LAST-MODIFIED:20191227T114734Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:ET3: Information as a Resource: How Organisms Deal with Uncertainty
  (Kacelnik)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T200000Z
DTEND:20200315T223000Z
DTSTAMP:20200204T230237Z
UID:2v0d82t4j6t28gpusp0qbjjpk7@google.com
CREATED:20191227T103914Z
DESCRIPTION:
LAST-MODIFIED:20191227T105106Z
LOCATION:Forum
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:Poster Session
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T200000Z
DTEND:20200313T213000Z
DTSTAMP:20200204T230237Z
UID:4veloq7gfqlbn71p9oec2uq5dd@google.com
CREATED:20191227T103653Z
DESCRIPTION:
LAST-MODIFIED:20191227T103828Z
LOCATION:
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:Welcome Event
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
LOCATION:H1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:RC1: Title tba
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR`;
