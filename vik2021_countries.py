from extract_field import extract_field

rows = extract_field('vik2021_registrations.txt', ['billing_country'])

synonyms = {
    "Deutschland" : "Germany",
    "Almanya" : "Germany",
    "DeutschlandDeutschland" : "Germany",
    "Germania" : "Germany",
    "Ggermany" : "Germany",
    "DE" : "Germany",
    "The Netherlands" : "Netherlands",
    "Holandia" : "Netherlands",
    "USA" : "United States",
    "United States of America" : "United States",
    "Österreich" : "Austria",
    "UK" : "United Kingdom",
    "france" : "France",
    "india" : "India",
    "Slovenija" : "Slovenia"
}

from collections import Counter

c = Counter()

for lst in rows:
    country = lst[0]
    if country == '':
        continue
    if country in synonyms:
        country = synonyms[country]
    c[country] += 1

# add Bucknell students
c["United States"] += 20

# make statistics
total = 0
for country in c:
    total += c[country]

for country in c:
    print('%s: %g' % (country, c[country] / total))
