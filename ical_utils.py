"""
Provides utility functions to read ical data

Copyright (C) 2020
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2020 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import re
import urllib.request

time_regex = re.compile('(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)')
course_id_regex = re.compile('SUMMARY:(\w+\d+).*')

def read_sessions_from_ical(ical_url, without_locations = False):
    # read the data as string
    try:
        http_request = urllib.request.urlopen(ical_url)
    except ValueError:
        global json_path
        if(not json_path.endswith('/')):
            json_path += '/'
        http_request = urllib.request.urlopen(json_path + ical_url)
    ical_data = http_request.read().decode('utf-8')
    # initialize an empty sessions array
    sessions = []
    # iterate over all lines in the ical data and process events in a
    # finite-state-machine fashion. The state machine has only two states,
    # depending on whether we are inside an event or not
    in_event = False
    for ical_line in ical_data.splitlines():
        if(not in_event):
            if(ical_line.startswith('BEGIN:VEVENT')):
                # begin a new event
                in_event = True
                current_session = {}
        else:
            if(ical_line.startswith('END:VEVENT')):
                # end the current event
                in_event = False
                # store the current event
                sessions.append(current_session)
            elif(ical_line.startswith('SUMMARY')):
                m = course_id_regex.match(ical_line)
                if m is not None:
                    current_session['course_id'] = m.group(1)
            elif(ical_line.startswith('DTSTART')):
                # parse the time
                time = time_regex.search(ical_line)
                current_session['date'] = time.group(1) + '-' + time.group(2) + '-' + time.group(3)
                # find the corresponding slot
                if(time.group(4) == '09'):
                    current_session['slot'] = 'morning'
                elif(time.group(4) == '11'):
                    current_session['slot'] = 'noon'
                elif(time.group(4) == '14'):
                    current_session['slot'] = 'afternoon'
                elif(time.group(4) == '16'):
                    current_session['slot'] = 'late-afternoon'
                else:
                    current_session['slot'] = 'evening'
                current_session['start_time'] = time.group(4) + ':' + time.group(5)
            elif(ical_line.startswith('DTEND')):
                time = time_regex.search(ical_line)
                current_session['end_time'] = time.group(4) + ':' + time.group(5)
            elif(ical_line.startswith('LOCATION') and not without_locations):
                current_session['location'] = ical_line[len('LOCATION:'):]
    return sessions
