import json
import sys
import csv
import os

def load_json(file_path):
    """Load a JSON file and return the parsed data."""
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            data = json.load(file)
        return data
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        sys.exit(1)
    except json.JSONDecodeError:
        print(f"Error: File '{file_path}' is not a valid JSON file.")
        sys.exit(1)

def write_csv(data, output_file):
    """Write JSON data to a CSV file."""
    if not data:
        print("Error: No data to write to CSV.")
        sys.exit(1)
    
    fieldnames = data[0].keys()
    
    with open(output_file, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)
    
    print(f"CSV file '{output_file}' has been created successfully.")

def process_poster(input_data, working_directory, year):
    csv_data = []
    html = "<html>\n<head>\n<title>Poster abstracts</title>\n</head>\n<body>\n"
    for index, entry in enumerate(input_data):
        number = index
        presenter = entry.get('presenting_author')
        author = entry.get('authors')
        title = entry.get('title').replace('\\"','"') # replace escaped ""
        affiliation = entry.get('affiliations')
        abstract = entry.get('abstract')
        abstract = abstract.replace('\n','\n<br>') # replace line breaks with <br>
        abstract = abstract.replace('\\"','"') # replace escaped ""
        author = author.replace('\\"','"') # replace escaped ""

        csv_data.append({
            'Number': "{0!s}".format(number),
            'Presenter': presenter,
            'Author': author,
            'Title': title
        })

        html += "<h3>{0!s} - {1!s}</h3>\n".format(number, title)
        html += "<p><em>{}</em></p>\n".format(author)
        html += "<p>{}</p>\n".format(affiliation)
        html +="<details>\n<p>{}</p>\n</details>\n".format(abstract)

    html += "</body>\n</html>"

    with open(working_directory + '/{0!s}-poster.html'.format(year), 'w', encoding='utf-8') as htmlfile:
        htmlfile.write(html)

    write_csv(csv_data, working_directory + '/{0!s}-poster.csv'.format(year))

def process_spotlight(input_data, working_directory, year):
    output_data = []
    html = "<html>\n<head>\n<title>Research Spotlight abstracts</title>\n</head>\n<body>\n"
    for index, entry in enumerate(input_data):
        number = index
        instructor = entry.get('instructor')
        title = entry.get('title')
        affiliation = entry.get('affiliations')
        abstract = entry.get('abstract')
        abstract = abstract.replace('\n','\n<br>') # replace line breaks with <br>
        abstract = abstract.replace('\\"','"') # replace escaped ""
        instructor = instructor.replace('\\"','"') # replace escaped ""

        output_data.append({
            'Number': "{0!s}".format(number),
            'Presenter': instructor,
            'Title': title
        })

        html += "<h3>{}</h3>\n".format(title)
        html += "<p><em>{}</em></p>\n".format(instructor)
        html += "<p>{}</p>\n".format(affiliation)
        html +="<details>\n<p>{}</p>\n</details>\n".format(abstract)

    html += "</body>\n</html>"
    
    with open(working_directory + '/{0!s}-spotlights.html'.format(year), 'w', encoding='utf-8') as htmlfile:
        htmlfile.write(html)
    write_csv(output_data, working_directory + '/{0!s}-spotlights.csv'.format(year))

def process_featuredlecture(input_data, working_directory, year):
    output_data = []
    for index, entry in enumerate(input_data):
        number = index
        instructor = entry.get('instructor')
        title = entry.get('title')

        output_data.append({
            'Number': "{0!s}".format(number),
            'Presenter': instructor,
            'Title': title
        })
    
    write_csv(output_data, working_directory + '/{0!s}-featuredlectures.csv'.format(year))

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python script.py <json_file> <year> [poster,spotlight,featuredlecture]")
        sys.exit(1)
    
    json_file = sys.argv[1]
    year = sys.argv[2]
    type = sys.argv[3]
    input_data = load_json(json_file)
    
    working_directory = os.path.dirname(json_file)
    
    print("We have {0!s} entries".format(len(input_data)))
    if type == 'poster':
        process_poster(input_data, working_directory, year)
    elif type == 'spotlight':
        process_spotlight(input_data, working_directory, year)
    elif type == 'featuredlecture':
        process_featuredlecture(input_data, working_directory, year)

    