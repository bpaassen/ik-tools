#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Distributes IK participants into interaction groups.

Copyright (C) 2019
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import print_function
import configparser
import csv
import math
import numpy as np
import sys

__author__ = 'Zam Franz and Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2018-2019 Zam Franz, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '2.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

# Print help if the user tries to provide command line arguments

def print_help():
	print("""Interdisciplinary College Interaction Group Generation Tool
Copyright (C) 2018-2019 Zam Franz, Benjamin Paaßen

This tool is intended to group the participants of the interdisciplinary
college (IK) into interaction groups. This distribution is done as to maximize
diversity within interaction groups. Diversity is measured in terms of
distance between participants, and the distance is quantified in terms of
the participants registration data. For example, if two participants are
from a different academic field they have a higher distance, etc.

This tool requires no command line arguments. All parameters are specified
via the config.ini file. Please refer to that file for further information.""")

if(len(sys.argv) > 1):
	print_help()
	sys.exit()

# Get the filename of the participant table from the config
config = configparser.ConfigParser()
config.read('config.ini')
table_filename = config['DEFAULT']['input_file']

# Read the participant table data
# After this section, X will be a list, where each entry
# is a row of our table. The columns contain the data about
# the participants.
X = []

# We load the CSV delimiter and quote char from the config
delimiter = config['DEFAULT'].get('delimiter', ',')
quotechar = config['DEFAULT'].get('quotechar', '\"')

with open(table_filename, newline='') as csvfile:
	reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
	for row in reader:
		X.append(row)

# We discard the first row because it contains the headers
X = X[1:]

print('read data from %d participants.' % len(X))

# Now, start to calculate the pairwise distances between all participants.
# The distances are computed as a weighted sum of distances for all considered
# columns, where the weights are specified by the config.ini file.
# We now process each column of the input file in turn and add the pairwise
# distances for the respective column to the overall pairwise distance
# matrix

# We initialize the pairwise distance matrix
m = len(X)
D = np.zeros((m, m))

# We load the weights for all columns and normalize them to 1.
title_weight       = float(config['WEIGHTS']['title_weight'])
city_weight        = float(config['WEIGHTS']['register_city_weight'])
country_weight     = float(config['WEIGHTS']['register_country_weight'])
department_weight  = float(config['WEIGHTS']['register_department_weight'])
faculty_weight     = float(config['WEIGHTS']['register_faculty_weight'])
work_field_weight  = float(config['WEIGHTS']['register_work_field_weight'])
frequency_weight   = float(config['WEIGHTS']['frequency_weight'])

weight_sum = title_weight + city_weight + country_weight + department_weight + faculty_weight + work_field_weight + frequency_weight

title_weight       /= weight_sum
city_weight        /= weight_sum
country_weight     /= weight_sum
department_weight  /= weight_sum
faculty_weight     /= weight_sum
work_field_weight  /= weight_sum
frequency_weight   /= weight_sum

# Process the titles of participants. We say that participants have
# a title distance of 1 if they have the same title and zero otherwise.
# If a participant has not provided a title, we will assume that they don't
# have the same title as any other participant (even others who have no title).

title_column_index = int(config['COLUMNS']['title_column'])

for i in range(0, m):
	title_i = X[i][title_column_index]
	for j in range(i+1, m):
		title_j = X[j][title_column_index]
		if(title_i == '' or title_j == '' or title_i != title_j):
			D[i, j] += title_weight * 1
			D[j, i] += title_weight * 1

# Process the participants' city. As with the title, we define the distance
# between cities as 1 if the city is not the same or the city information
# is missing

city_column_index = int(config['COLUMNS']['register_city_column'])

for i in range(0, m):
	city_i = X[i][city_column_index]
	for j in range(i+1, m):
		city_j = X[j][city_column_index]
		if(city_i == '' or city_j == '' or city_i != city_j):
			D[i, j] += city_weight * 1
			D[j, i] += city_weight * 1

# Process the participants' country. As with the title, we define the distance
# between cities as 1 if the country is not the same or the country information
# is missing

country_column_index = int(config['COLUMNS']['register_country_column'])

for i in range(0, m):
	country_i = X[i][country_column_index]
	for j in range(i+1, m):
		country_j = X[j][country_column_index]
		if(city_i == '' or country_j == '' or country_i != country_j):
			D[i, j] += country_weight * 1
			D[j, i] += country_weight * 1

# Process the participants' department. As with the title, we define the distance
# between cities as 1 if the department is not the same or the department information
# is missing

department_column_index = int(config['COLUMNS']['register_department_column'])

for i in range(0, m):
	department_i = X[i][department_column_index]
	for j in range(i+1, m):
		department_j = X[j][department_column_index]
		if(department_i == '' or department_j == '' or department_i != department_j):
			D[i, j] += department_weight * 1
			D[j, i] += department_weight * 1

# Process the participants' faculty. As with the title, we define the distance
# between cities as 1 if the faculty is not the same or the faculty information
# is missing

faculty_column_index = int(config['COLUMNS']['register_faculty_column'])

for i in range(0, m):
	faculty_i = X[i][faculty_column_index]
	for j in range(i+1, m):
		faculty_j = X[j][faculty_column_index]
		if(faculty_i == '' or faculty_j == '' or faculty_i != faculty_j):
			D[i, j] += faculty_weight * 1
			D[j, i] += faculty_weight * 1

# Process the participants' work field. Here, we assign distances which are
# stored in a specific distance matrix file for work fields. If a participant
# has provided no work field information or a work field which is not covered
# by the distance matrix, we assign a distance of 1.

# Load the work field distance matrix

work_field_distance_matrix_filename = config['DEFAULT']['work_field_distance_matrix_file']

with open(work_field_distance_matrix_filename, newline='') as csvfile:
	reader   = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
	# The first row of the CSV file should contain all available fields
	# as column headers
	fields = reader.__next__()[1:]
	# Initialize the work_field distance matrix
	D_fields = np.zeros((len(fields), len(fields)))
	for i in range(0, len(fields)-1):
		# We assume the matrix to be symmetric, which is why we only load
		# the upper triangle of it, i.e. we ignore all columns up to i+1.
		d = [float(c) for c in reader.__next__()[i+2:]]
		# copy the file content to the matrix
		for j in range(i+1, len(fields)):
			D_fields[i, j] = d[j-i-1]

# Add the distances to the transposed distance matrix to get the (symmetric)
# lower triangle of the matrix
D_fields = D_fields + np.transpose(D_fields)

# For each participant, retreive the index of their work field according to the
# distance matrix file

work_field_column_index = int(config['COLUMNS']['register_work_field_column'])

field_indices = np.zeros(m, dtype=np.int)
for i in range(0, m):
	try:
		field_indices[i] = fields.index(X[i][work_field_column_index])
	except ValueError:
		# If a participants' field is not listed in the distance matrix file,
		# we assign a -1.
		field_indices[i] = -1

# add the work field distance to the overall distance

for i in range(0, m):
	for j in range(i+1, m):
		if(field_indices[i] < 0 or field_indices[j] < 0):
			# If participant i or participant j has an unlisted work field,
			# we quantify the distance between their fields as 1.
			D[i, j] += work_field_weight * 1;
			D[j, i] += work_field_weight * 1;
		else:
			# Otherwise, we use the corresponding distance matrix entry
			D[i, j] += work_field_weight * D_fields[field_indices[i], field_indices[j]]
			D[j, i] += work_field_weight * D_fields[field_indices[i], field_indices[j]]

# Process how many times each participant has been at IK before. This information
# is stored in a separate CSV file containing the frequent participants of IK.
# To keep it simple, we just define the distance between frequent and non-frequent
# IK participants as one and as zero otherwise.

# Import the list of frequent participants

frequent_participants_filename = config['DEFAULT']['frequent_participants_file']

frequent_participants = []

with open(frequent_participants_filename, newline='') as csvfile:
	reader   = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
	for row in reader:
		frequent_participants.append(row[0] + ' ' + row[1]) 

frequent_participants = frozenset(frequent_participants)

# Check whether each participant is frequent or not
first_name_column_index = int(config['COLUMNS']['first_name_column'])
last_name_column_index = int(config['COLUMNS']['last_name_column'])
Is_frequent            = np.zeros(m, dtype=np.bool)

for i in range(0, m):
	Is_frequent[i] = (X[i][first_name_column_index] + ' ' + X[i][last_name_column_index]) in frequent_participants

# compute the distance between participants in terms of frequency
for i in range(0, m):
	for j in range(i+1, m):
		if((not Is_frequent[i] and Is_frequent[j]) or (Is_frequent[i] and not Is_frequent[j])):
			D[i, j] += frequency_weight * 1
			D[j, i] += frequency_weight * 1

# Now that the pairwise distances are computed, we can start with assembling
# the actual interaction groups. We first read how many interaction groups
# should be constructed from the config file. This parameter is called K.

K = int(config['DEFAULT']['num_groups'])

if(K < 1):
	raise ValueError('We have to construct at least one interaction group!')
if(K > m):
	raise ValueError('We can not construct more interaction groups than participants are available!');

# We then read how large the interaction groups should be at least from the
# config file. This parameter is called n.

n = int(config['DEFAULT']['min_group_size'])

if(n < 1):
	raise ValueError('The minimum group size has to be at least 1!')
if(K * n > m):
	raise ValueError('If we would construct {} interaction groups of size {} each, this would require {} participants. But only {} participants are available!'.format( K, n, K*n, m ) );

# Initialize the interaction groups with the first K participants in the participants table, who
# should be the interaction group leaders
groups = []

for k in range(0, K):
	groups.append([k])

# If we also have moonshines, we expect the next K rows of the participants file to be
# moonshines, and we assign to each interaction group leader the most distant moonshine
# greedily

# this vector contains the diversity in each group
group_divs = np.zeros(K)

if(config['DEFAULT']['assign_moonshines'].lower() == 'true'):
	remaining_moonshines = set(range(K, 2*K))
	print('Assigning moonshines to interaction groups')
	for k in range(0, K):
		# select the moonshine which is as distant as possible to the group leader
		best_moonshine = -1;
		max_d          = -1;
		for j in remaining_moonshines:
			if(D[k, j] > max_d):
				max_d = D[k, j]
				best_moonshine = j
		groups[k].append(best_moonshine)
		remaining_moonshines.remove(best_moonshine)
		group_divs[k] += max_d

	i_start = 2*K
	# Initialize the number of points we still need to ensure that all groups are big enough
	delta_n = (n-2) * np.ones(K)
	k = np.argmin(group_divs)
else:
	# Initialize the number of points we still need to ensure that all groups are big enough
	i_start = K
	delta_n = (n-1) * np.ones(K)
	k = 0

# Initialize the set of remaining points
Phi = set(range(i_start, m))

# Start the greedy max diversity algorithm. We iterate until all data points are assigned
for i in range(i_start, m):
	# select the data point which most increases the diversity in group k
	best_i = -1
	max_d  = -1
	for j in Phi:
		# compute the distance of j to all data points in the group. This is the contribution
		# to group diversity of the new data point
		d = 2 * np.sum(D[j, groups[k]])
		# check if it is better than the current diversity advantage
		if(d > max_d):
			max_d  = d
			best_i = j
	# remove the selected point from Phi and add it to the kth group
	Phi.remove(best_i)
	groups[k].append(best_i)
	group_divs[k] += max_d
	if(delta_n[k] > 0):
		delta_n[k] = delta_n[k] - 1
	# Let the group with the least current diversity select the next member, except if we
	# need to distribute the remaining members such that all groups are big enough
	if(np.sum(delta_n) == len(Phi)):
		# In that case, we need to add to the smallest group next
		k = np.argmax(delta_n)
	else:
		k = np.argmin(group_divs)

# Print out the resulting groups
for k in range(0, K):
	print('group {} with average diversity = {}'.format(k, group_divs[k] / (len(groups[k]) * len(groups[k]))))
	for i in groups[k]:
		print('{}: {} {}'.format(i, X[i][first_name_column_index], X[i][last_name_column_index]))

# also print out a table mapping each participant to their interaction group
participant_to_group_file = config['DEFAULT']['participant_to_group_file']
participant_to_group = -np.ones(len(X), dtype=int)
for k in range(K):
	for i in groups[k]:
		if(participant_to_group[i] >= 0):
			raise ValueError('Participant %s %s was distributed into multiple interaction groups!' % (X[i][first_name_column_index], X[i][last_name_column_index]))
		participant_to_group[i] = k

# make sure that everyone was distributed
for i in range(len(X)):
	if(participant_to_group[i] < 0):
		raise ValueError('Participant %s %s was not distribute!' % (X[i][first_name_column_index], X[i][last_name_column_index]))

# convert to a list of lists containing last and first names, according to which we can sort
table_out = []
for i in range(len(X)):
	table_out.append([X[i][last_name_column_index], X[i][first_name_column_index], participant_to_group[i] + 1])

# sort
table_out.sort()

# write out
with open(participant_to_group_file, 'w', newline = '') as csvfile:
	csv_writer = csv.writer(csvfile, delimiter = delimiter, quotechar = quotechar)
	for i in range(len(X)):
		csv_writer.writerow(table_out[i])
