#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generates a PDF version of the schedule for the Interdisciplinary College.
The basis for the schedule generation is a javascript file from the website
which can be input as URL or file path on disk.

Note that this script has the following dependencies to work properly:
* cairosvg
* cssselect
* lxml
* svgwrite
* tinycss2

If at least svgwrite is available this script will at least generate an SVG
output, which you can then manually convert to pdf via inkscape or some other
software.

Copyright (C) 2019-2025
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2019-2024 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.2.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys
from collections import Counter
import copy
import calendar
import datetime
import json
import urllib.request
import svgwrite
import re
try:
    import cairosvg
    import cssselect
    import lxml
    import tinycss2
    no_pdf = False
except ImportError:
    no_pdf = True
from ical_utils import read_sessions_from_ical

# retrieve the path to the javascript file from the command line
if len(sys.argv) < 2 or sys.argv[1] == '--without-locations':
    json_path = 'https://interdisciplinary-college.org/schedule/courses_metadata.php'
    print('Note: no command line argument for a json file was given, therefore this program will choose the current data on the IK website under %s' % json_path)
    # load the website data
    http_request = urllib.request.urlopen(json_path)
    json_data = http_request.read().decode('utf-8')
else:
    json_path = sys.argv[1]
    if(json_path.startswith('http')):
        # if the path is a URL, read the json data from url
        http_request = urllib.request.urlopen(json_path)
        json_data = http_request.read().decode('utf-8')
    else:
        # otherwise, assume that json_path is a file path on disk and treat it
        # as such
        with open(json_path) as json_file:
            json_data = json_file.read()

# then, parse the JSON data
json_data = json.loads(json_data)

# retrieve the start, dinner, and end date
start_date  = datetime.datetime.strptime(json_data['start_date'], '%Y-%m-%d')
dinner_date = datetime.datetime.strptime(json_data['dinner_date'], '%Y-%m-%d')
end_date    = datetime.datetime.strptime(json_data['end_date'], '%Y-%m-%d')

# build a dictionary mapping from course IDs to courses
course_dict = {}
for course in json_data['courses']:
    course_dict[course['identifier']] = course

# load the current calendar data
sessions = read_sessions_from_ical('https://interdisciplinary-college.org/schedule/calendar_data.php', without_locations = ('--without-locations' in sys.argv))
# then sort the sessions into the courses based on course identifiers
for session in sessions:
    if('course_id' not in session or session['course_id'] not in course_dict):
        continue
    course = course_dict[session['course_id']]
    course_sessions = course.setdefault('sessions', [])
    course_sessions.append(session)

# after all data loading, sort the sessions for all courses
def session_sort_keys(session):
    if session['slot'] == 'morning':
        slot_idx = 0
    elif session['slot'] == 'noon':
        slot_idx = 1
    elif session['slot'] == 'afternoon':
        slot_idx = 2
    elif session['slot'] == 'late-afternoon':
        slot_idx = 3
    elif session['slot'] == 'evening':
        slot_idx = 4
    else:
        raise ValueError('unknown time slot!')
    return (session['date'], slot_idx)

for course in json_data['courses']:
    if 'sessions' not in course:
        continue
    course['sessions'].sort(key = session_sort_keys)

# generate a list of courses with sessions
# identify the number of sessions in each slot to know if a course covers a slot entirely
slot_counts = Counter()
for course in json_data['courses']:
    if 'sessions' not in course:
        continue
    print('%s: %s (%s)' % (course['identifier'], course['title'], course['instructor']))
    for session in course['sessions']:
      print('%s: %s-%s' % (session['date'], session['start_time'], session['end_time']))
      slot = session['date'] + ' ' + session['slot']
      slot_counts[slot] += 1


# generate a list of all conference days with their according weekdays
# and a map from day representations to indices
conference_days = []
for d in range((end_date - start_date).days + 1):
    date = start_date + datetime.timedelta(days = d)
    conference_days.append(date)
# generate the time slots for all conference days
slots = []
# the first day only has three slots, starting in the afternoon
slots.append(['afternoon', 'late-afternoon', 'evening'])
S = 3
# all intermediate conference days have all five slots
for d in range(1, len(conference_days) - 1):
    slots.append(['morning', 'noon', 'afternoon', 'late-afternoon', 'evening'])
    S += 5
# the last conference day only has two slots
S += 2
slots.append(['morning', 'noon'])

# sort courses according to regular courses, joint events with no parallel other courses, and rainbow courses
regular_courses  = []
spotlight_sessions = []
rainbow_sessions = []
joint_events     = {}

for course in json_data['courses']:
    if course['type'] == 'Joint Event' or course['type'] == 'Evening Talk':
      if len(course['sessions']) != 1:
        raise ValueError('course %s was a Joint Event but had more than one slot' % course['identifier'])
      session = course['sessions'][0]
      slot = session['date'] + ' ' + session['slot']
      joint_events[slot] = course
    elif(course['type'] == 'Research Spotlight'):
        # copy the session and append a reference to the course identifier
        for session in course['sessions']:
            session_copy = copy.copy(session)
            session_copy['identifier'] = course['identifier']
            spotlight_sessions.append(session_copy)
    elif(course['type'] == 'Rainbow Course' or course['type'] == 'Featured Lecture'):
        # copy the session and append a reference to the course identifier
        for session in course['sessions']:
            session_copy = copy.copy(session)
            session_copy['identifier'] = course['identifier']
            rainbow_sessions.append(session_copy)
    elif course['type'] not in ['Introductory Course', 'Basic Course', 'Advanced Course', 'Method Course', 'Focus Course', 'Special Course', 'Practical Course']:
        print(f'ignored course {course["identifier"]} {course["title"]} due to unknown type: {course["type"]}')
        continue
    else:
        # correct course type for certain common synonyms
        if course['type'] == 'Introductory Course':
            course['type'] = 'Basic Course'
        elif course['type'] == 'Advanced Course':
            course['type'] = 'Method Course'
        elif course['type'] == 'Focus Course':
            course['type'] = 'Special Course'
        # replace title with short title if available
        if 'short_title' in course:
            course['title'] = course['short_title']
        regular_courses.append(course)

# sort the rainbow sessions
rainbow_sessions.sort(key = session_sort_keys)
spotlight_sessions.sort(key = session_sort_keys)

num_rainbow_rows = 0
if len(spotlight_sessions) > 0:
  num_rainbow_rows += 1
if len(rainbow_sessions) > 0:
  num_rainbow_rows += 1

# set up the CSS styling for the calendar
CSS_STYLES = """
    line { stroke: white; stroke-width: 2px; }
    line.thick { stroke: white; stroke-width: 4px; }
    text { font-size: 16px; text-align: center; text-anchor: middle; }

    .day-header > rect { fill: #555753; }
    .day-header > text { fill: white; font-weight: bold; }

    rect.morning { fill: #d3d7cf; }
    rect.noon { fill: #eeeeec; }
    rect.afternoon { fill: #d3d7cf; }
    rect.late-afternoon { fill: #eeeeec; }
    rect.evening { fill: #d3d7cf; }

    rect.basic-course.id { fill: #009939; }
    rect.method-course.id { fill: #e21317; }
    rect.special-course.id { fill: #f7a70e; }
    rect.practical-course.id { fill: #1d8ece; }
    rect.professional-course.id { fill: #1d8ece; }
    rect.rainbow-course.id { fill: #674ea7; }
    rect.additional-event.id { fill: #674ea7; }
    text.id { font-weight: bold; }

    rect.basic-course.title { fill: #d9ead3; }
    rect.method-course.title { fill: #f4cccc; }
    rect.special-course.title { fill: #fff2cc; }
    rect.practical-course.title { fill: #d0e0e3; }
    rect.professional-course.title { fill: #d0e0e3; }
    rect.rainbow-course.title { fill: #d9d2e9; }
    rect.additional-event.title { fill: #d9d2e9; }
    text.title { font-size: 12px; font-weight: bold; text-anchor: left; }
    text.instructor { font-size: 12px; text-anchor: left; }

    rect.basic-course.slot-box { fill: #009939; }
    rect.method-course.slot-box { fill: #e21317; }
    rect.special-course.slot-box { fill: #f7a70e; }
    rect.practical-course.slot-box { fill: #1d8ece; }
    rect.professional-course.slot-box { fill: #1d8ece; }
    rect.rainbow-course.slot-box { fill: #674ea7; }
    rect.additional-event.slot-box { fill: #674ea7; }
    text.location { }

    text.evening.instructor { text-anchor: middle; }
"""
# the box size in pixels
BOX_SIZE = 50
# the course title box width
TITLE_WIDTH = 350
# the day header height
DAY_HEADER_HEIGHT = 25
# The y-offset for text in pixels
TEXT_OFFSET = 8

TABLE_WIDTH = (S+1) * BOX_SIZE + TITLE_WIDTH
TABLE_HEIGHT = DAY_HEADER_HEIGHT + BOX_SIZE + len(regular_courses) * BOX_SIZE + num_rainbow_rows * BOX_SIZE

# After this preparation, start generating the actual table as a scalable
# vector graphic, using the svgwrite library

# Initialize the SVG drawing on DIN A2 paper
PAPER_WIDTH  = 594
PAPER_HEIGHT = 420
dwg    = svgwrite.Drawing('schedule.svg', size=('%dmm' % PAPER_WIDTH, '%dmm' % PAPER_HEIGHT))
# append the CSS styles
dwg.defs.add(dwg.style(CSS_STYLES))
# set the size of the viewbox in pixels, which also sets the resolution.
# We want a resolution such that 2cm of the paper remain free in each direction.
PAPER_DELTA = 20
resolution = TABLE_WIDTH / float(PAPER_WIDTH - 2 * PAPER_DELTA)
delta = int(resolution * PAPER_DELTA)
dwg.viewbox(-delta, -delta, TABLE_WIDTH + 2*delta, TABLE_HEIGHT + 2*delta)

# start with the timeslot headers. In a first row, we write a header for each
# day
TABLE_CONTENT_WIDTH = TABLE_WIDTH - BOX_SIZE - TITLE_WIDTH
day_header_group = dwg.add(dwg.g(class_='day-header'))
# draw a big background rectangle for the entire row
day_header_group.add(dwg.rect(insert=(BOX_SIZE + TITLE_WIDTH, 0), size=(TABLE_CONTENT_WIDTH, DAY_HEADER_HEIGHT)))

# write the day for each header
x = BOX_SIZE + TITLE_WIDTH
y = DAY_HEADER_HEIGHT - TEXT_OFFSET
for d in range(len(conference_days)):
    num_slots = len(slots[d])
    width = num_slots * BOX_SIZE
    # retrieve the weekday string
    weekday = calendar.day_name[conference_days[d].weekday()]
    if(num_slots < 3):
        # use abbreviated version of week day if there are few slots
        weekday = weekday[:3]
    # add it as text element
    day_header_group.add(dwg.text(weekday, (x + int(0.5 * width), y)))
    # increment x coordinate
    x += width

# now, draw background rectangles for each timeslot
slot_group = dwg.add(dwg.g(class_='slot_backgrounds'))
x = BOX_SIZE + TITLE_WIDTH
y = DAY_HEADER_HEIGHT
for d in range(len(conference_days)):
    for slot in slots[d]:
        slot_group.add(dwg.rect(insert=(x, y), size=(BOX_SIZE, TABLE_HEIGHT - DAY_HEADER_HEIGHT), class_=slot))
        x += BOX_SIZE

# In a second row, we write a header for each timeslot
timeslot_header_group = dwg.add(dwg.g(class_='timeslot-header'))
# write the times for each slot
x = BOX_SIZE + TITLE_WIDTH
y_start = DAY_HEADER_HEIGHT + 0.5 * BOX_SIZE - TEXT_OFFSET
y_end = DAY_HEADER_HEIGHT + BOX_SIZE - TEXT_OFFSET
for d in range(len(conference_days)):
    for slot in slots[d]:
        # retrieve the start and end time for the current slot
        if(slot == 'morning'):
            start_time = '09:00'
            end_time   = '10:30'
        elif(slot == 'noon'):
            start_time = '11:00'
            end_time   = '12:30'
        elif(slot == 'afternoon'):
            start_time = '14:30'
            end_time   = '16:00'
        elif(slot == 'late-afternoon'):
            start_time = '16:30'
            end_time   = '18:00'
        elif(slot == 'evening'):
            # retrieve start and end time from the stored evening talks
            date = conference_days[d].strftime('%Y-%m-%d')
            slot_id = date + ' evening'
            if slot_id in joint_events:
              course = joint_events[slot_id]
              start_time = course['sessions'][0]['start_time']
              end_time   = course['sessions'][0]['end_time']
        else:
            raise ValueError('Unknown slot: %s' % slot)
        # write the start time into the top of the box
        timeslot_header_group.add(dwg.text(start_time, (x + int(0.5 * BOX_SIZE), y_start)))
        # write the start time into the bottom of the box
        timeslot_header_group.add(dwg.text(end_time, (x + int(0.5 * BOX_SIZE), y_end)))
        # increment the x position
        x += BOX_SIZE

# now, write one course per row
courses_group = dwg.add(dwg.g(class_='courses'))
courses_lines_group = dwg.add(dwg.g(class_='courses-lines'))
y = DAY_HEADER_HEIGHT + BOX_SIZE
for course in regular_courses:
    style_class = course['type'].replace(' ', '-').lower()
    # draw a colored rectangle for the course identifier
    courses_group.add(dwg.rect(insert=(0, y), size=(BOX_SIZE, BOX_SIZE), class_=style_class + ' id'))
    # write the course identifier into it
    courses_group.add(dwg.text(course['identifier'], (int(0.5 * BOX_SIZE), y + int(0.75*BOX_SIZE) - TEXT_OFFSET), class_='id'))
    # draw a colored rectangle for the course title
    courses_group.add(dwg.rect(insert=(BOX_SIZE, y), size=(TITLE_WIDTH, BOX_SIZE), class_=style_class + ' title'))
    # write the course title into the top part
    courses_group.add(dwg.text(course['title'], (BOX_SIZE + TEXT_OFFSET, y + int(0.5*BOX_SIZE) - TEXT_OFFSET), class_='title wrap'))
    # write the course instructor into the bottom part
    courses_group.add(dwg.text(course['instructor'], (BOX_SIZE + TEXT_OFFSET, y + BOX_SIZE - TEXT_OFFSET), class_='instructor'))
    # iterate over all sessions and color the slots where the course takes place
    for session in course['sessions']:
        # identify the correct conference day
        d = conference_days.index(datetime.datetime.strptime(session['date'], '%Y-%m-%d'))
        # identify the correct slot
        s = slots[d].index(session['slot'])
        # then increment s by the sessions of all previous conference days
        for j in range(d):
            s += len(slots[j])
        # draw a colored rectangle
        x = (s+1) * BOX_SIZE + TITLE_WIDTH
        courses_group.add(dwg.rect(insert=(x, y), size=(BOX_SIZE, BOX_SIZE), class_=style_class + ' slot-box'))
        # write the location into it
        if('location' in session):
            courses_group.add(dwg.text(session['location'], (x + int(0.5 * BOX_SIZE), y + int(0.75*BOX_SIZE) - TEXT_OFFSET), class_='location'))

    # increment y
    y += BOX_SIZE

    # draw a line to separate this course row from the next one
    courses_lines_group.add(dwg.line(start=(0, y), end=(TABLE_WIDTH, y)))

# write the research spotlights
if len(spotlight_sessions) > 0:
  # draw a colored rectangle for the course identifier
  courses_group.add(dwg.rect(insert=(0, y), size=(BOX_SIZE, BOX_SIZE), class_='rainbow-course id'))
  # write the course identifier into it
  courses_group.add(dwg.text('', (int(0.5 * BOX_SIZE), y + int(0.75*BOX_SIZE) - TEXT_OFFSET), class_='id'))
  # draw a colored rectangle for the course title
  courses_group.add(dwg.rect(insert=(BOX_SIZE, y), size=(TITLE_WIDTH, BOX_SIZE), class_='rainbow-course title'))
  # write the course title into it
  courses_group.add(dwg.text('Research Spotlights', (BOX_SIZE + TEXT_OFFSET, y + int(0.75*BOX_SIZE) - TEXT_OFFSET), class_='title'))
  for session in spotlight_sessions:
      d = conference_days.index(datetime.datetime.strptime(session['date'], '%Y-%m-%d'))
      # identify the correct slot
      s = slots[d].index(session['slot'])
      # then increment s by the sessions of all previous conference days
      for j in range(d):
          s += len(slots[j])
      # draw a colored rectangle
      x = (s+1) * BOX_SIZE + TITLE_WIDTH
      courses_group.add(dwg.rect(insert=(x, y), size=(BOX_SIZE, BOX_SIZE), class_='rainbow-course slot-box'))
      # write the course identifier in the top part of the box
      courses_group.add(dwg.text(session['identifier'], (x + int(0.5 * BOX_SIZE), y + int(0.5*BOX_SIZE) - TEXT_OFFSET), class_='id'))
      # write the location into it
      if('location' in session):
          courses_group.add(dwg.text(session['location'], (x + int(0.5 * BOX_SIZE), y + BOX_SIZE - TEXT_OFFSET), class_='location'))

  # increment y
  y += BOX_SIZE
  # draw a line to separate this course row from the next one
  courses_lines_group.add(dwg.line(start=(0, y), end=(TABLE_WIDTH, y)))

# write the rainbow talks/featured lectures
if len(rainbow_sessions) > 0:
  # draw a colored rectangle for the course identifier
  courses_group.add(dwg.rect(insert=(0, y), size=(BOX_SIZE, BOX_SIZE), class_='rainbow-course id'))
  # write the course identifier into it
  courses_group.add(dwg.text('', (int(0.5 * BOX_SIZE), y + int(0.75*BOX_SIZE) - TEXT_OFFSET), class_='id'))
  # draw a colored rectangle for the course title
  courses_group.add(dwg.rect(insert=(BOX_SIZE, y), size=(TITLE_WIDTH, BOX_SIZE), class_='rainbow-course title'))
  # write the course title into it
  courses_group.add(dwg.text('Featured Lectures', (BOX_SIZE + TEXT_OFFSET, y + int(0.75*BOX_SIZE) - TEXT_OFFSET), class_='title'))
  for session in rainbow_sessions:
      d = conference_days.index(datetime.datetime.strptime(session['date'], '%Y-%m-%d'))
      # identify the correct slot
      s = slots[d].index(session['slot'])
      # then increment s by the sessions of all previous conference days
      for j in range(d):
          s += len(slots[j])
      # draw a colored rectangle
      x = (s+1) * BOX_SIZE + TITLE_WIDTH
      courses_group.add(dwg.rect(insert=(x, y), size=(BOX_SIZE, BOX_SIZE), class_='rainbow-course slot-box'))
      # write the course identifier in the top part of the box
      courses_group.add(dwg.text(session['identifier'], (x + int(0.5 * BOX_SIZE), y + int(0.5*BOX_SIZE) - TEXT_OFFSET), class_='id'))
      # write the location into it
      if('location' in session):
          courses_group.add(dwg.text(session['location'], (x + int(0.5 * BOX_SIZE), y + BOX_SIZE - TEXT_OFFSET), class_='location'))

# write the joint events
evening_talk_group = dwg.add(dwg.g(class_='evening-talks'))
for slot_id in joint_events:
    date   = slot_id[:10]
    slot   = slot_id[11:]
    course = joint_events[slot_id]
    # identify the correct conference day
    d = conference_days.index(datetime.datetime.strptime(date, '%Y-%m-%d'))
    # identify the correct time slot
    s = slots[d].index(slot)
    # then increment s by the sessions of all previous conference days
    for j in range(d):
        s += len(slots[j])
    # draw a new background rectangle over the row separator lines
    x = (s+1) * BOX_SIZE + TITLE_WIDTH
    y = DAY_HEADER_HEIGHT + BOX_SIZE
    height = TABLE_HEIGHT - DAY_HEADER_HEIGHT - BOX_SIZE
    evening_talk_group.add(dwg.rect(insert=(x, y), size=(BOX_SIZE, height), class_='evening'))
    # write the evening talk into it.
    # The course identifier and title into a first line
    x += int(0.8 * BOX_SIZE) - TEXT_OFFSET
    y += int(0.5 * TABLE_HEIGHT) - TEXT_OFFSET
    if(course['identifier'] == ''):
        evening_header = course['title']
    else:
        evening_header = course['identifier'] + ': ' + course['title']
    evening_talk_group.add(dwg.text(evening_header, (x, y), transform='rotate(90 %d, %d)' % (x, y), class_='id'))
    # and the instructor into a second line
    x -= int(0.5 * BOX_SIZE)
    evening_talk_group.add(dwg.text(course['instructor'], (x, y), transform='rotate(90 %d, %d)' % (x, y), class_='instructor evening'))

# In a final step, draw separator lines between rows and columns
outlines_group = dwg.add(dwg.g(class_='outlines'))

# draw a line, separating the bottom of the day header row from the next row
outlines_group.add(dwg.line(start=(BOX_SIZE + TITLE_WIDTH, DAY_HEADER_HEIGHT), end=(TABLE_WIDTH, DAY_HEADER_HEIGHT)))

# draw a line, separating the bottom of the timeslot header from the next row
outlines_group.add(dwg.line(start=(0, DAY_HEADER_HEIGHT + BOX_SIZE), end=(TABLE_WIDTH, DAY_HEADER_HEIGHT + BOX_SIZE), class_='thick'))

# draw the column separator lines
# separate the course ID column from the course title column
outlines_group.add(dwg.line(start=(BOX_SIZE, DAY_HEADER_HEIGHT + BOX_SIZE), end=(BOX_SIZE, TABLE_HEIGHT)))
# separate the course title column from the main table
outlines_group.add(dwg.line(start=(BOX_SIZE + TITLE_WIDTH, 0), end=(BOX_SIZE + TITLE_WIDTH, TABLE_HEIGHT), class_='thick'))
# iterate over the slots
x = BOX_SIZE + TITLE_WIDTH
for d in range(len(conference_days)):
    # separate slots within one conference day
    for s in range(len(slots[d])):
        x += BOX_SIZE
        outlines_group.add(dwg.line(start=(x, DAY_HEADER_HEIGHT), end=(x, TABLE_HEIGHT)))
    # separate conference days
    outlines_group.add(dwg.line(start=(x, 0), end=(x, TABLE_HEIGHT), class_='thick'))

# write SVG out
dwg.save()
# convert to PDF
if(no_pdf):
    print('Warning: This script only creates the schedule.svg output because not all dependencies for PDF conversion are available')
else:
    cairosvg.svg2pdf(url='schedule.svg', write_to='schedule.pdf')
