#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Extracts one or several fields for all participants in a data file.

Copyright (C) 2020
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2020 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys
import json

def extract_field(data_file, columns, ignore_warnings = False):
    # read the registration data backup file
    # try to open the data file
    rows = []
    with open(data_file) as f:
        l = 1
        for line in f:
            # check if the current line is JSON data
            if not line.startswith('{'):
                continue
            # if so, parse the json content
            try:
              datum = json.loads(line)
            except json.decoder.JSONDecodeError as ex:
              print('entry %d: %s' % (l, line))
              raise ex
            # extract the desired columns and print the data
            datum_cols = []
            for col in columns:
                try:
                    entry = datum
                    for key in col.split('.'):
                        entry = entry[key]
                    datum_cols.append(entry)
                except KeyError as ex:
                    if not ignore_warnings:
                        print('warning: entry %d entry did not have an %s field' % (l, key))
                    datum_cols.append('')
                    continue
            rows.append(datum_cols)
            l += 1
    return rows

if __name__== "__main__" :
    # retrieve the path to the backup registration data file on the command line
    if(len(sys.argv) < 2):
        data_file = 'registration_data.txt'
        print('Note: no command line argument for a data file was given, therefore this program will choose the file %s' % data_file)
    else:
        data_file = sys.argv[1]

    if(len(sys.argv) < 3):
        columns = ['email']
        print('Note: no column was given, therefore this program will choose the columns %s' % str(columns))
    else:
        columns = sys.argv[2].split(',')
    rows = extract_field(data_file, columns)
    for row in rows:
        print('\t'.join([str(entry) for entry in row]))
