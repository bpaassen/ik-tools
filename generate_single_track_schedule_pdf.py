#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generates a PDF version of the schedule for the Interdisciplinary College.
The basis for the schedule generation is a javascript file from the website
which can be input as URL or file path on disk.

Note that this script has the following dependencies to work properly:
* cairosvg
* cssselect
* lxml
* svgwrite
* tinycss

If at least svgwrite is available this script will at least generate an SVG
output, which you can then manually convert to pdf via inkscape or some other
software.

Copyright (C) 2019
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2019 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys
import copy
import calendar
import datetime
import json
import svgwrite
import urllib.request
try:
    import cairosvg
    import cssselect
    import lxml
    import tinycss
    no_pdf = False
except ImportError:
    no_pdf = True
from ical_utils import read_sessions_from_ical

# retrieve the path to the javascript file from the command line
if(len(sys.argv) < 2 or sys.argv[1] == '--without-locations'):
    json_path = 'https://interdisciplinary-college.org/schedule/courses_metadata.php'
    print('Note: no command line argument for a json file was given, therefore this program will choose the current data on the IK website under %s' % json_path)
    # load the website data
    http_request = urllib.request.urlopen(json_path)
    json_data = http_request.read().decode('utf-8')
else:
    json_path = sys.argv[1]
    if(json_path.startswith('http')):
        # if the path is a URL, read the json data from url
        http_request = urllib.request.urlopen(json_path)
        json_data = http_request.read().decode('utf-8')
    else:
        # otherwise, assume that json_path is a file path on disk and treat it
        # as such
        with open(json_path) as json_file:
            json_data = json_file.read()

# then, parse the JSON data
json_data = json.loads(json_data)

# retrieve the start and end date
start_date = datetime.datetime.strptime(json_data['start_date'], '%Y-%m-%d')
end_date   = datetime.datetime.strptime(json_data['end_date'], '%Y-%m-%d')

# build a dictionary mapping from course IDs to courses
course_dict = {}
for course in json_data['courses']:
    course_dict[course['identifier']] = course

# load the current calendar data
sessions = read_sessions_from_ical('https://interdisciplinary-college.org/schedule/calendar_data.php', without_locations = ('--without-locations' in sys.argv))

# sort according to date and then start time
sessions.sort(key = lambda session : (session['date'], session['start_time']))

# generate a list of all conference days with their according weekdays
conference_days = []
for d in range(min(8, (end_date - start_date).days + 1)):
    date = start_date + datetime.timedelta(days = d)
    conference_days.append(date)

# set up the CSS styling for the calendar
CSS_STYLES = """
    line.thin { stroke: black; stroke-linecap: round; stroke-dasharray: 1, 10; stroke-width: 1px; fill: none; }
    line.thick { stroke: black; stroke-width: 4px; stroke-dasharray: 0; }
    text { font-size: 14px; text-align: left; text-anchor: start; }

    .day-header > rect { fill: #555753; }
    .day-header > text { fill: white; font-weight: bold; }
    text.date { text-anchor: end; text-align: right; }

    text.id { font-weight: bold; }
    text.title { font-size: 12px; font-weight: bold; }
    text.instructor {  }
    text.location {  }

    rect.bc { fill: #52cc7f; }
    rect.mc { fill: #ec5b5d; }
    rect.sc { fill: #f7c058; }
    rect.pc { fill: #60afdb; }
    rect.prc { fill: #60afdb; }
    rect.rc { fill: #9280c1; }
    rect.evnt { fill: #9280c1; }
    rect.et { fill: #9280c1; }
"""

#    rect.morning { fill: #d3d7cf; }
#    rect.noon { fill: #eeeeec; }
#    rect.afternoon { fill: #d3d7cf; }
#    rect.late-afternoon { fill: #eeeeec; }
#    rect.evening { fill: #d3d7cf; }

#    rect.basic-course.id { fill: #009939; }
#    rect.method-course.id { fill: #e21317; }
#    rect.special-course.id { fill: #f7a70e; }
#    rect.practical-course.id { fill: #1d8ece; }
#    rect.professional-course.id { fill: #1d8ece; }
#    rect.rainbow-course.id { fill: #674ea7; }
#    rect.additional-event.id { fill: #674ea7; }
#    text.id { font-weight: bold; }

#    rect.basic-course.title { fill: #d9ead3; }
#    rect.method-course.title { fill: #f4cccc; }
#    rect.special-course.title { fill: #fff2cc; }
#    rect.practical-course.title { fill: #d0e0e3; }
#    rect.professional-course.title { fill: #d0e0e3; }
#    rect.rainbow-course.title { fill: #d9d2e9; }
#    rect.additional-event.title { fill: #d9d2e9; }
#    text.title { font-size: 12px; font-weight: bold; text-anchor: left; }
#    text.instructor { font-size: 12px; text-anchor: left; }

#    rect.basic-course.slot-box { fill: #009939; }
#    rect.method-course.slot-box { fill: #e21317; }
#    rect.special-course.slot-box { fill: #f7a70e; }
#    rect.practical-course.slot-box { fill: #1d8ece; }
#    rect.professional-course.slot-box { fill: #1d8ece; }
#    rect.rainbow-course.slot-box { fill: #674ea7; }
#    rect.additional-event.slot-box { fill: #674ea7; }
#    text.location { }

#    text.evening.instructor { text-anchor: middle; }

# mapping from course types to css styles
type_to_css = {
    "Basic Course" : "bc",
    "Introductory Course" : "bc",
    "Method Course" : "mc",
    "Advanced Course" : "mc",
    "Special Course" : "sc",
    "Focus Course" : "fc",
    "Practical Course" : "pc",
    "Professional Course" : "prc",
    "Rainbow Course" : "rc",
    "Evening Talk" : "et",
    "Additional Event" : "evnt"
}


# the box size in pixels
BOX_WIDTH = 300
BOX_HEIGHT = 75
# the line height
LINE_HEIGHT = BOX_HEIGHT / 3
# The y-offset for text in pixels
TEXT_OFFSET = 8
# The maximum number of characters in a line
MAX_CHARS = 40

TABLE_WIDTH = len(conference_days) * BOX_WIDTH + BOX_WIDTH // 2
TABLE_HEIGHT = LINE_HEIGHT * 1 + BOX_HEIGHT * 4

# After this preparation, start generating the actual table as a scalable
# vector graphic, using the svgwrite library

# Initialize the SVG drawing in 4:1 ratio
PAPER_WIDTH  = 800
PAPER_HEIGHT = 200
dwg    = svgwrite.Drawing('schedule_single_track.svg', size=('%dmm' % PAPER_WIDTH, '%dmm' % PAPER_HEIGHT))
# append the CSS styles
dwg.defs.add(dwg.style(CSS_STYLES))

# set the size of the viewbox in pixels, which also sets the resolution.
# We want a resolution such that 2cm of the paper remain free in each direction.
PAPER_DELTA = 20
resolution = TABLE_WIDTH / float(PAPER_WIDTH - 2 * PAPER_DELTA)
delta = int(resolution * PAPER_DELTA)
dwg.viewbox(-delta, -delta, TABLE_WIDTH + 2*delta, TABLE_HEIGHT + 2*delta)

# In a first row, we write a header for each day
day_header_group = dwg.add(dwg.g(class_='day-header'))
# draw a big background rectangle for the entire row
day_header_group.add(dwg.rect(insert=(0, 0), size=(TABLE_WIDTH, LINE_HEIGHT)))
# write the column/row header
day_header_group.add(dwg.text('Time \ Date', (TEXT_OFFSET, LINE_HEIGHT - TEXT_OFFSET)))
# write a header for each day
x = BOX_WIDTH // 2
for day in conference_days:
    # write the weekday
    weekday = calendar.day_name[day.weekday()] + ', ' + day.strftime('%Y-%m-%d')
    day_header_group.add(dwg.text(weekday, (x + TEXT_OFFSET, LINE_HEIGHT - TEXT_OFFSET)))
    x += BOX_WIDTH

# draw a big background rectangle for the entire column
day_header_group.add(dwg.rect(insert=(0, LINE_HEIGHT), size=(BOX_WIDTH // 2, TABLE_HEIGHT - LINE_HEIGHT)))
# write a header for each time slot
start_times = ['16:00', '17:00', '18:00', '19:00']
end_times   = ['17:00', '18:00', '19:00', '20:00']
y = LINE_HEIGHT
for s in range(len(start_times)):
    # write the start time at the beginning of the time slot
    day_header_group.add(dwg.text(start_times[s], (TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET)))
    # write the end time at the end of the time slot
    day_header_group.add(dwg.text(end_times[s], (TEXT_OFFSET, y + 2 * LINE_HEIGHT - TEXT_OFFSET)))
    # increment y
    y += BOX_HEIGHT
## write the dinner times
#day_header_group.add(dwg.text('18:15-19:30', (TEXT_OFFSET, y - TEXT_OFFSET)))
#y += LINE_HEIGHT
## write the evening talk start time
#day_header_group.add(dwg.text('20:00', (TEXT_OFFSET, y - TEXT_OFFSET)))

# iterate over all time slots and print all sessions in that time slot
sessions_group = dwg.add(dwg.g(class_='sessions'))
x = BOX_WIDTH // 2
s = 0
for d in range(len(conference_days)):
    y = LINE_HEIGHT
    date = conference_days[d].strftime('%Y-%m-%d')
    # print all sessions for the current day
    while s < len(sessions) and sessions[s]['date'] == date:
        session = sessions[s]

        if session['course_id'] not in course_dict:
            print('Course %s was not found' % session['course_id'])
            s += 1
            continue

        course  = course_dict[session['course_id']]

        # ignore hacks in the PDF schedule
        if course['type'] == 'Hack':
            continue

        # map course type to CSS class
        css_class = type_to_css[course['type']]

        # draw a box for the course
        sessions_group.add(dwg.rect(insert=(x, y), size=(BOX_WIDTH, BOX_HEIGHT), class_=css_class))

        # write the course title in the first two lines of the box
        if len(course['title']) < MAX_CHARS:
            sessions_group.add(dwg.text(course['title'], (x + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='title'))
        else:
            words = course['title'].split(' ')
            first_line = words[0]
            i = 1
            while len(first_line) + len(words[i]) < MAX_CHARS:
                first_line += ' ' + words[i]
                i += 1
            sessions_group.add(dwg.text(first_line, (x + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='title'))
            snd_line = words[i]
            i += 1
            while i < len(words) and len(snd_line) + len(words[i]) < MAX_CHARS:
                snd_line += ' ' + words[i]
                i += 1
            sessions_group.add(dwg.text(snd_line, (x + TEXT_OFFSET, y + 2 * LINE_HEIGHT - TEXT_OFFSET), class_='title'))
        # write the instructor in the second line
        sessions_group.add(dwg.text(course['instructor'], (x + TEXT_OFFSET, y + 3*LINE_HEIGHT - TEXT_OFFSET), class_='instructor'))

        # increment y
        y += BOX_HEIGHT

        s += 1
    # when a day is ended, increase y
    x += BOX_WIDTH

# In a final step, draw separator lines between rows and columns
outlines_group = dwg.add(dwg.g(class_='outlines'))

# draw a thick line, separating the bottom of the day header row from the next row
outlines_group.add(dwg.line(start=(0, LINE_HEIGHT), end=(TABLE_WIDTH, LINE_HEIGHT), class_='thick'))

# draw lines separating each time slot from the next
y = LINE_HEIGHT
for s in range(len(start_times)):
    # draw a line between the current time slot and the next
    y += BOX_HEIGHT
    outlines_group.add(dwg.line(start=(0, y), end=(TABLE_WIDTH, y), class_='thin'))

# draw a thick line between each conference day
x = BOX_WIDTH // 2
for d in range(len(conference_days)):
    outlines_group.add(dwg.line(start=(x, 0), end=(x, TABLE_HEIGHT), class_='thick'))
    x += BOX_WIDTH

# write SVG out
dwg.save()
# convert to PDF
if(no_pdf):
    print('Warning: This script only creates the schedule.svg output because not all dependencies for PDF conversion are available')
else:
    cairosvg.svg2pdf(url='schedule_single_track.svg', write_to='schedule_single_track.pdf')
