# IK Tools

This package contains a collection of tools for the [interdisciplinary college (IK)][1].
I expect that the tools presented here are interesting mainly for future conference managers
of the IK. However, other people are welcome to utilize these tools as well and adapt them
to their liking under the conditions of the GPLv3.

In more detail, this package contains:

* `generate_schedule_pdf.py`: a Python3 script to convert the current scheduling data from the
    IK website under <https://interdisciplinary-college.org/program/week-schedule/> into a PDF
    version. Note that this script utilizes SVG drawing, such that the resulting PDF is a proper
    vector graphic that can be scaled up arbitrarily for print.
* `generate_traditional_schedule_pdf.py`: basically the same as `generate_schedule_pdf.py`, but
    generates the schedule in the traditional IK format.
* `generate_ik_participation_groups.py`: Distributes the participants in `ik_participants.csv`
    into interaction groups as diverse as possible using a greedy assignment scheme. For more
    information, please refer to the script itself and to the `config.ini`. Other resources used
    by this script are `frequent_participants.csv` and `work_field_distances.csv`.

[1]:https://interdisciplinary-college.org
