from extract_field import extract_field

fields = ['billing_country', 'salutation', 'fee_string']
rows = extract_field('vik2021_registrations.txt', fields)

from collections import Counter

counters = []
for field in fields:
    counters.append(Counter())

for row in rows:
    for i in range(len(fields)):
        counters[i].update([row[i]])

for i in range(len(fields)):
    print('\n\n----- %s -----' % fields[i])
    for key in counters[i]:
        print('%s: %d' % (key, counters[i][key]))
