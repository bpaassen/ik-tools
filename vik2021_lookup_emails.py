import csv
import json
from edist.sed import sed_string

emails = set()

from collections import Counter
gender_count = Counter()

data_file = 'vik2021_registrations.txt'
with open(data_file) as f:
    for line in f:
        # check if the current line is JSON data
        if not line.startswith('{'):
            continue
        # if so, parse the json content
        datum = json.loads(line)
        emails.add(datum['email'])
        if 'salutation' in datum:
            gender_count[datum['salutation']] += 1


# students form bucknell
bucknell_file = 'vik2021_bucknell.csv'
with open(bucknell_file) as f:
    reader = csv.reader(f, delimiter = '\t')
    for row in reader:
        emails.add(row[2])

print(len(emails))
print(gender_count)


#data_file = 'vik2021_registrations.txt'
## try to open the data file
#name_to_email = {}
#with open(data_file) as f:
#    for line in f:
#        # check if the current line is JSON data
#        if not line.startswith('{'):
#            continue
#        # if so, parse the json content
#        datum = json.loads(line)

#        name_to_email[datum['first_name'] + ' ' + datum['last_name']] = datum['email']
#        name_to_email[datum['last_name'] + ' ' + datum['first_name']] = datum['email']

#lecturer_file = 'vik2021_lecturers.csv'
#with open(lecturer_file) as f:
#    reader = csv.reader(f, delimiter = '\t')
#    for row in reader:
#        name_to_email[row[0] + ' ' + row[1]] = row[2]
#        name_to_email[row[1] + ' ' + row[0]] = row[2]

## students form bucknell
#bucknell_file = 'vik2021_bucknell.csv'
#with open(bucknell_file) as f:
#    reader = csv.reader(f, delimiter = '\t')
#    for row in reader:
#        name_to_email[row[0] + ' ' + row[1]] = row[2]
#        name_to_email[row[1] + ' ' + row[0]] = row[2]

## load interaction group file
#interaction_group_file = 'pc3_signups.csv'

#out = []

#with open(interaction_group_file) as f:
#    reader = csv.reader(f, delimiter = '\t')
#    for row in reader:
#        name = row[1] + ' ' + row[0]
#        if name in name_to_email:
#            email = name_to_email[name]
#        else:
#            closest = None
#            d_min   = 1000
#            for name2 in name_to_email:
#                d = sed_string(name, name2)
#                if d < d_min:
#                    d_min = d
#                    closest = name2
#            
#            email = name_to_email[closest]
#            print('mapped %s to %s' % (name, closest))
#            name = closest
#        out.append('\t'.join([row[0], row[1], email]))

#print('\n'.join(out))
#print('\n\n')
