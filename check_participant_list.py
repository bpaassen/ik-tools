#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Checks a participant list in CSV format against the backup database.

Copyright (C) 2020
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2020 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys
import json
from edist import sed

# retrieve the path to the backup registration data file on the command line
if(len(sys.argv) < 2):
    data_file = 'registration_data.txt'
    print('Note: no command line argument for a data file was given, therefore this program will choose the file %s' % data_file)
else:
    data_file = sys.argv[1]

# retrieve the path to the reference participant list from the command line
if(len(sys.argv) < 3):
    ref_file = 'participants.csv'
    print('Note: no command line argument for a reference file was given, therefore this program will choose the file %s' % ref_file)
else:
    ref_file = sys.argv[2]

# read the reference CSV file
ref_rows = []
with open(ref_file) as f:
    for line in f:
        ref_rows.append(line.replace('\n', '').replace('\"', ''))
ref_rows = set(ref_rows)

# read the registration data backup file
backup_rows = []
# try to open the data file
with open(data_file) as f:
    for line in f:
        # check if the current line is JSON data
        if not line.startswith('{'):
            continue
        # if so, parse the json content
        datum = json.loads(line)
        # extract all stipend application relevant data and transform it to CSV
        backup_rows.append(datum['first_name'] + ' ' + datum['last_name'])

# check rows that are contained in the backup data but _not_ in the reference file
for row in backup_rows:
    if row not in ref_rows:
        print('Found missing: %s' % row)
        # look for near matches via edit distance
        found_near_match = False
        for row2 in ref_rows:
            d = sed.sed_string(row, row2) / max(len(row), len(row2))
            if d < 0.3:
                found_near_match = True
                print('Found near match: %s' % row2)
        if not found_near_match:
            rev_row = row.split()
            if len(rev_row) == 2:
                rev_row = rev_row[1] + ' ' + rev_row[0]
                for row2 in ref_rows:
                    d = sed.sed_string(rev_row, row2) / max(len(rev_row), len(row2))
                    if d < 0.3:
                        print('Found near match: %s' % row2)
