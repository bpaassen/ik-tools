import csv

NO_COL_     = 0
IND_COL_    = 1
AUTHOR_COL_ = 2
AFFIL_COL_  = 3
TITLE_COL_  = 4
DESC_COL_   = 5

poster_file = 'vik2021_poster.csv'

posters = []
industry_posters = []

with open(poster_file) as f:
    read = csv.reader(f, delimiter = ',', quotechar = '\"')
    read.__next__()
    for row in read:
        no      = int(row[NO_COL_])
        is_industry = row[IND_COL_] == 'yes'
        authors = row[AUTHOR_COL_]
        affil   = row[AFFIL_COL_]
        title   = row[TITLE_COL_]
        desc    = row[DESC_COL_]

        if is_industry:
            industry_posters.append((no, authors, affil, title, desc))
        else:
            posters.append((no, authors, affil, title, desc))

# print style block
print('<style>')
print('p.authors { font-weight: bold; }')
print('p.affiliation { font-style: italic; font-size: 80% }')
print('</style>\n')

# print out a table of contents first
print('<ol>')
i = 1
for (no, authors, affil, title, desc) in posters:
    if no != i:
        raise ValueError('Order was wrong!')
    print('<li><a href="#%d">%s</a></li>' % (no, title))
    i += 1
print('</ol>')
print('\n<h3>Industry posters</h3>')
print('<ul>')
for (no, authors, affil, title, desc) in industry_posters:
    print('<li><a href="#%d">%s</a></li>' % (no, title))
print('</ul>')

# print the posters themselves
for (no, authors, affil, title, desc) in posters:
    # print title first
    print('\n<h3 id=\"%d\">%d: %s</h3>\n' % (no, no, title))
    # print authors
    print('<p class="authors">%s</p>\n' % authors)
    # print affiliation
    print('<p class="affiliation">%s</p>\n' % affil)
    # print description
    print('<p class="description">%s</p>\n' % desc)

# print the industry posters
for (no, authors, affil, title, desc) in industry_posters:
    # print title first
    print('\n<h3 id=\"%d\">%s</h3>\n' % (no, title))
    # print authors
    print('<p class="authors">%s</p>\n' % authors)
    # print description
    print('<p class="description">%s</p>\n' % desc)
