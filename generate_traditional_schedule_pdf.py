#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generates a PDF version of the schedule for the Interdisciplinary College
in the traditional IK format (pre 2018).
The basis for the schedule generation is a javascript file from the website
which can be input as URL or file path on disk.

Note that this script has the following dependencies to work properly:
* cairosvg
* cssselect
* lxml
* svgwrite
* tinycss2

If at least svgwrite is available this script will at least generate an SVG
output, which you can then manually convert to pdf via inkscape or some other
software.

Copyright (C) 2019
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2019-2025 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.2.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys
import copy
import calendar
import datetime
import json
import urllib.request
import svgwrite
import re
try:
    import cairosvg
    import cssselect
    import lxml
    import tinycss2
    no_pdf = False
except ImportError:
    no_pdf = True
from ical_utils import read_sessions_from_ical

# retrieve the path to the javascript file from the command line
if len(sys.argv) < 2 or sys.argv[1] == '--without-locations':
    json_path = 'https://interdisciplinary-college.org/schedule/courses_metadata.php'
    print('Note: no command line argument for a json file was given, therefore this program will choose the current data on the IK website under %s' % json_path)
    # load the website data
    http_request = urllib.request.urlopen(json_path)
    json_data = http_request.read().decode('utf-8')
else:
    json_path = sys.argv[1]
    if json_data.startswith('http'):
        # if the path is a URL, read the json data from url
        http_request = urllib.request.urlopen(json_path)
        json_data = http_request.read().decode('utf-8')
    else:
        # otherwise, assume that json_path is a file path on disk and treat it
        # as such
        with open(json_path) as json_file:
            json_data = json_file.read()

# then, parse the JSON data
json_data = json.loads(json_data)

# retrieve the start, dinner, and end date
start_date  = datetime.datetime.strptime(json_data['start_date'], '%Y-%m-%d')
dinner_date = datetime.datetime.strptime(json_data['dinner_date'], '%Y-%m-%d')
end_date    = datetime.datetime.strptime(json_data['end_date'], '%Y-%m-%d')

# build a dictionary mapping from course IDs to courses
course_dict = {}
for course in json_data['courses']:
    course_dict[course['identifier']] = course

# load the current calendar data
sessions = read_sessions_from_ical('https://interdisciplinary-college.org/schedule/calendar_data.php', without_locations = ('--without-locations' in sys.argv))
# then sort the sessions into the courses based on course identifiers
for session in sessions:
    if('course_id' not in session or session['course_id'] not in course_dict):
        continue
    course = course_dict[session['course_id']]
    course_sessions = course.setdefault('sessions', [])
    course_sessions.append(session)

# after all data loading, sort the sessions for all courses
def session_sort_keys(session):
    if session['slot'] == 'morning':
        slot_idx = 0
    elif session['slot'] == 'noon':
        slot_idx = 1
    elif session['slot'] == 'afternoon':
        slot_idx = 2
    elif session['slot'] == 'late-afternoon':
        slot_idx = 3
    elif session['slot'] == 'evening':
        slot_idx = 4
    else:
        raise ValueError('unknown time slot!')
    return (session['date'], slot_idx)

for course in json_data['courses']:
    if 'sessions' not in course:
        continue
    course['sessions'].sort(key = session_sort_keys)

# generate a list of courses with sessions
for course in json_data['courses']:
    if 'sessions' not in course:
        continue
    print('%s: %s (%s)' % (course['identifier'], course['title'], course['instructor']))
    for session in course['sessions']:
        print('%s: %s-%s' % (session['date'], session['start_time'], session['end_time']))


# generate a list of all conference days with their according weekdays
# and a map from day representations to indices
conference_days = []
for d in range((end_date - start_date).days + 1):
    date = start_date + datetime.timedelta(days = d)
    conference_days.append(date)
# generate the time slots for all conference days
slots = []
# the first day only has three slots, starting in the afternoon
slots.append(['afternoon', 'late-afternoon', 'evening'])
# all intermediate conference days have all five slots
for d in range(1, len(conference_days) - 1):
    slots.append(['morning', 'noon', 'afternoon', 'late-afternoon', 'evening'])
# the last conference day only has the morning slot
slots.append(['morning', 'noon'])

# initialize an array which shall store the sessions in all time slots
sessions = []
for day in slots:
    sessions_day = []
    for slot in day:
        sessions_day.append([])
    sessions.append(sessions_day)

# iterate over all courses and sort the sessions into the respective slots
for c in range(len(json_data['courses'])):
    course = json_data['courses'][c]
    if course['type'] == 'Hack':
        continue
    # iterate over all sessions in this course
    if 'sessions' not in course:
        continue
    for session in course['sessions']:
        # append a reference to the course
        session['course_id'] = c
        # identify the correct day
        d = conference_days.index(datetime.datetime.strptime(session['date'], '%Y-%m-%d'))
        # identify the correct slot
        s = slots[d].index(session['slot'])
        # sort the session into the correct slot
        sessions[d][s].append(session)

# assign courses that happen at the same time to the same track
assigned = set()
tracks = []
for d in range(len(conference_days)):
    for s in range(len(slots[d])):
        if slots[d][s] == 'evening' or not sessions[d][s]:
            continue
        track = []
        for session in sessions[d][s]:
            course = json_data['courses'][session['course_id']]
            if course['identifier'] in assigned:
                continue
            assigned.add(course['identifier'])
            track.append(course['identifier'])
        if len(track) > 0:
            tracks.append(track)

# assign colors to courses based on which courses happen in the same track
color_assignment = {}
col = 1
for track in tracks:
    if(len(track) > 12):
        raise ValueError('More courses than available colors in track! %s' % str(track))
    if col + len(track) > 12:
        col = 1
    # ensure unique colors in each track but allow repitions across tracks
    for course_id in track:
        color_assignment[course_id] = col
        col += 1

# set up the CSS styling for the calendar
CSS_STYLES = """
    line.thin { stroke: black; stroke-linecap: round; stroke-dasharray: 1, 10; stroke-width: 1px; fill: none; }
    line.thick { stroke: black; stroke-width: 4px; stroke-dasharray: 0; }
    text { font_size: 16px; text-align: left; text-anchor: start; }

    .day-header > rect { fill: #ff99cc; }
    .day-header > text { font-weight: bold; }
    text.date { text-anchor: end; text-align: right; }

    .food > rect { fill: ffcc99; }

    rect.course1 { fill: #ffff99; }
    rect.course2 { fill: #99ccff; }
    rect.course3 { fill: #ccffcc; }
    rect.course4 { fill: #e6e6ff; }
    rect.course5 { fill: #fff2cb; }

    rect.course6 { fill: #e2efd9; }
    rect.course7 { fill: #fbe4d5; }
    rect.course8 { fill: #e6e6ff; }
    rect.course9 { fill: #ffe598; }
    rect.course10 { fill: #cc99ff; }
    rect.course11 { fill: #ffccff; }
    rect.course12 { fill: #ffff99; }
    rect.rainbow { fill: url(#vert_lin_grad); }
    rect.evening1 { fill: #34cc66; }
    rect.evening2 { fill: #94bd5e; }

    text.id { font-weight: bold; }
    text.title { font-weight: bold; }
    text.instructor { font-weight: bold; }
    text.location { font-weight: bold; }
"""
# the box size in pixels
BOX_WIDTH = 160
BOX_HEIGHT = 75
# the line height
LINE_HEIGHT = BOX_HEIGHT / 3
# The y-offset for text in pixels
TEXT_OFFSET = 8

TABLE_WIDTH = (len(conference_days)*2+1) * BOX_WIDTH
TABLE_HEIGHT = LINE_HEIGHT * 5 + BOX_HEIGHT * 9

# After this preparation, start generating the actual table as a scalable
# vector graphic, using the svgwrite library

# Initialize the SVG drawing on DIN A2 paper
PAPER_WIDTH  = 594
PAPER_HEIGHT = 420
dwg    = svgwrite.Drawing('schedule_traditional.svg', size=('%dmm' % PAPER_WIDTH, '%dmm' % PAPER_HEIGHT))
# append the CSS styles
dwg.defs.add(dwg.style(CSS_STYLES))
# define a nice color gradient for rainbow courses
vert_grad = svgwrite.gradients.LinearGradient(start=(0, 0), end=(1,0), id="vert_lin_grad")
vert_grad.add_stop_color(offset='0%', color='red', opacity=0.5)
vert_grad.add_stop_color(offset='24%', color='orange', opacity=0.5)
vert_grad.add_stop_color(offset='48%', color='yellow', opacity=0.5)
vert_grad.add_stop_color(offset='70%', color='green', opacity=0.5)
vert_grad.add_stop_color(offset='80%', color='blue', opacity=0.5)
vert_grad.add_stop_color(offset='90%', color='indigo', opacity=0.5)
vert_grad.add_stop_color(offset='100%', color='violet', opacity=0.5)
dwg.defs.add(vert_grad)

# set the size of the viewbox in pixels, which also sets the resolution.
# We want a resolution such that 2cm of the paper remain free in each direction.
PAPER_DELTA = 20
resolution = TABLE_WIDTH / float(PAPER_WIDTH - 2 * PAPER_DELTA)
delta = int(resolution * PAPER_DELTA)
dwg.viewbox(-delta, -delta, TABLE_WIDTH + 2*delta, TABLE_HEIGHT + 2*delta)

# In a first row, we write a header for each day
day_header_group = dwg.add(dwg.g(class_='day-header'))
# draw a big background rectangle for the entire row
day_header_group.add(dwg.rect(insert=(0, 0), size=(TABLE_WIDTH, LINE_HEIGHT)))
# write the column/row header
day_header_group.add(dwg.text('Time \ Date', (TEXT_OFFSET, LINE_HEIGHT - TEXT_OFFSET)))
# write a header for each day
x = BOX_WIDTH
for day in conference_days:
    # write the weekday
    weekday = calendar.day_name[day.weekday()]
    day_header_group.add(dwg.text(weekday, (x + TEXT_OFFSET, LINE_HEIGHT - TEXT_OFFSET)))
    # write the date
    date = day.strftime('%d/%m/%Y')
    day_header_group.add(dwg.text(date, (x + 2*BOX_WIDTH - TEXT_OFFSET, LINE_HEIGHT - TEXT_OFFSET), class_='date'))
    x += 2*BOX_WIDTH

# draw a big background rectangle for the entire column
day_header_group.add(dwg.rect(insert=(0, LINE_HEIGHT), size=(BOX_WIDTH, TABLE_HEIGHT - LINE_HEIGHT)))
# write a header for each time slot
start_times = ['09:00', '11:00', '14:30', '16:30']
end_times   = ['10:30', '12:30', '16:00', '18:00']
y = LINE_HEIGHT
for s in range(len(start_times)):
    # write the start time at the beginning of the time slot
    day_header_group.add(dwg.text(start_times[s], (TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET)))
    # write the end time at the end of the time slot
    day_header_group.add(dwg.text(end_times[s], (TEXT_OFFSET, y + 2 * BOX_HEIGHT - TEXT_OFFSET)))
    # increment y
    y += 2 * BOX_HEIGHT + LINE_HEIGHT
# write the dinner times
day_header_group.add(dwg.text('18:15-19:30', (TEXT_OFFSET, y - TEXT_OFFSET)))
y += LINE_HEIGHT
# write the evening talk start time
day_header_group.add(dwg.text('20:00', (TEXT_OFFSET, y - TEXT_OFFSET)))

# draw the food breaks
food_group = dwg.add(dwg.g(class_='food'))
y = 0
breaks = ['Coffee break', 'Lunch break', 'Coffee break', 'Dinner break']
for s in range(len(breaks)):
    # increment y
    y += LINE_HEIGHT + 2 * BOX_HEIGHT
    # draw a big rectangle for each food break
    food_group.add(dwg.rect(insert=(BOX_WIDTH, y), size=(TABLE_WIDTH - BOX_WIDTH, LINE_HEIGHT)))
    # draw a header for each food break at each day
    x = BOX_WIDTH
    for d in range(len(conference_days)):
        if breaks[s] == 'Dinner break' and conference_days[d] == dinner_date:
            continue
        food_group.add(dwg.text(breaks[s], (x + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET)))
        # increment x
        x += 2 * BOX_WIDTH

# iterate over all time slots and print all sessions in that time slot
sessions_group = dwg.add(dwg.g(class_='sessions'))
x = BOX_WIDTH
for d in range(len(conference_days)):
    y = LINE_HEIGHT
    if(d == 0):
        y += 4 * BOX_HEIGHT + 2 * LINE_HEIGHT
    # then draw the sessions
    for s in range(len(slots[d])):
        for se in range(len(sessions[d][s])):
            session = sessions[d][s][se]
            course  = json_data['courses'][session['course_id']]
            if course['type'] == 'Hack':
                continue
            elif course['type'] in ['Rainbow Course', 'Research Spotlight', 'Featured Lecture']:
                color = 'rainbow'
            elif slots[d][s] == "evening":
                color = f'evening{(d % 2) + 1}'
            else:
                color = 'course%d' % color_assignment[course['identifier']]
            if 'short_title' in course:
                course['title'] = course['short_title']
            # get a list of instructors
            instructors = re.split('(,|&| and )', course['instructor'])
            # extract the last name of each instructor
            instructors = [instructor.split(' ')[-1] for instructor in instructors[::2]]

            if len(sessions[d][s]) == 1:
                # if we have one session in this slot, we fill the entire box
                if slots[d][s] == "evening":
                  session_height = BOX_HEIGHT
                else:
                  session_height = 2 * BOX_HEIGHT
                sessions_group.add(dwg.rect(insert=(x, y), size=(2 * BOX_WIDTH, session_height), class_='evening%d' % (d % 2 + 1)))
                # write the course identifier in the first line of the box
                if course['type'] != 'Joint Event':
                    sessions_group.add(dwg.text(course['identifier'], (x + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='id'))
                else:
                    sessions_group.add(dwg.text(course['title'], (x + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='id'))
                # write the instructor in the second line
                sessions_group.add(dwg.text(', '.join(instructors), (x + TEXT_OFFSET, y + 2*LINE_HEIGHT - TEXT_OFFSET), class_='instructor'))
                # write the location in the third line
                if 'location' in session:
                    sessions_group.add(dwg.text(session['location'], (x + TEXT_OFFSET, y + 3*LINE_HEIGHT - TEXT_OFFSET), class_='location'))
            elif len(sessions[d][s]) < 5:
                # if we have at most four sessions in this slot, we have one
                # box per course
                # draw a colored box for the current session
                sessions_group.add(dwg.rect(insert=(x, y), size=(BOX_WIDTH, BOX_HEIGHT), class_=color))
                # write the course identifier in the first line of the box
                sessions_group.add(dwg.text(course['identifier'], (x + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='id'))
                # write the location to its right
                if('location' in session):
                    sessions_group.add(dwg.text(session['location'], (x + TEXT_OFFSET + 0.5 * BOX_WIDTH, y + LINE_HEIGHT - TEXT_OFFSET), class_='location'))
                # write the first instructor in the second line
                sessions_group.add(dwg.text(instructors[0], (x + TEXT_OFFSET, y + 2*LINE_HEIGHT - TEXT_OFFSET), class_='instructor'))
                # write the second instructor in the third line
                if len(instructors) > 1:
                  sessions_group.add(dwg.text(instructors[1], (x + TEXT_OFFSET, y + 3*LINE_HEIGHT - TEXT_OFFSET), class_='instructor'))
                # increment x and y
                x -= (2 * (se % 2) - 1) * BOX_WIDTH
                y += (se % 2) * BOX_HEIGHT
            else:
                # if we have five our six sessions in this slot, we have only
                # one line per course
                # draw a colored box for the current session
                sessions_group.add(dwg.rect(insert=(x, y), size=(2*BOX_WIDTH, LINE_HEIGHT), class_=color))
                # write the course identifier first into the box
                sessions_group.add(dwg.text(course['identifier'], (x + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='id'))
                # then the location
                if('location' in session):
                    sessions_group.add(dwg.text(session['location'], (x + int(0.4 * BOX_WIDTH) + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='location'))
                # and finally the (first) instructor
                sessions_group.add(dwg.text(instructors[0], (x + int(0.5 * BOX_WIDTH) + TEXT_OFFSET, y + LINE_HEIGHT - TEXT_OFFSET), class_='instructor'))
                # increment y
                y += LINE_HEIGHT
        # increment x and y
        if len(sessions[d][s]) == 1:
            if slots[d][s] == "evening":
              y += BOX_HEIGHT
            else:
              y += 2 * BOX_HEIGHT
            y += LINE_HEIGHT
        elif len(sessions[d][s]) < 5:
            if len(sessions[d][s]) % 2 == 1:
                x -= BOX_WIDTH
            if len(sessions[d][s]) < 2:
                y += BOX_HEIGHT
            if len(sessions[d][s]) < 4:
                y += BOX_HEIGHT
            y += LINE_HEIGHT
        else:
            y += (7 - len(sessions[d][s])) * LINE_HEIGHT
    # when a day is ended, increase y
    x += 2* BOX_WIDTH

# In a final step, draw separator lines between rows and columns
outlines_group = dwg.add(dwg.g(class_='outlines'))

# draw a thick line, separating the bottom of the day header row from the next row
outlines_group.add(dwg.line(start=(0, LINE_HEIGHT), end=(TABLE_WIDTH, LINE_HEIGHT), class_='thick'))

# draw lines separating each time slot from the next
y = LINE_HEIGHT
for s in range(len(start_times)):
    # draw a line between the current time slot and the pause
    y += 2 * BOX_HEIGHT
    outlines_group.add(dwg.line(start=(0, y), end=(TABLE_WIDTH, y), class_='thin'))
    # and draw an additional line between the pause and the next slot
    y += LINE_HEIGHT
    outlines_group.add(dwg.line(start=(0, y), end=(TABLE_WIDTH, y), class_='thin'))

# draw a thick line between each conference day
x = BOX_WIDTH
for d in range(len(conference_days)):
    outlines_group.add(dwg.line(start=(x, 0), end=(x, TABLE_HEIGHT), class_='thick'))
    x += 2 * BOX_WIDTH

# write SVG out
dwg.save()
# convert to PDF
if(no_pdf):
    print('Warning: This script only creates the schedule.svg output because not all dependencies for PDF conversion are available')
else:
    cairosvg.svg2pdf(url='schedule_traditional.svg', write_to='schedule_traditional.pdf')
