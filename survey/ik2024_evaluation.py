import csv
import numpy as np
import itertools
from statistics import median, quantiles, mean, stdev

likert_scale = ['I did not attend', 'not at all', 'not too much', 'it was okay', 'it was great', 'it was fantastic']

attendance_scale = ['No thanks', 'maybe', 'probably', 'once IK, forever IK']

field_values = ['Artificial Intelligence', 'Computer Science', 'Neurobiology', 'Cognitive Science', 'Philosophy', 'Linguistics', 'Psychology', 'Other']

nationality_values = ['German', 'Austrian', 'Dutch', 'Spanish', 'Portuguese', 'Other']

status_values = ['bachelor student', 'master student', 'PhD student', 'Postdoc', 'Faculty', 'Industry', 'Other']

def generate_pgfplot(title, column, scale_type = 'numeric'):
  print('\\subsection*{%s}' % title.replace('&', '\\&'))
  if scale_type == 'numeric':
    values = [str(j) for j in range(1, 11)]
  elif scale_type == 'likert':
    values = likert_scale
  elif scale_type == 'attendance':
    values = attendance_scale
  elif scale_type == 'field':
    values = field_values
    column = list(itertools.chain.from_iterable([val.split(';') for val in column]))
  elif scale_type == 'nationality':
    values = nationality_values
    column = [val.lower().capitalize() for val in column]
  elif scale_type == 'status':
    values = status_values
  else:
    raise ValueError('unknown scale: %s' % scale_type)

  counts = np.zeros(len(values), dtype=int)
  if 'Other' in values:
    other_index = values.index('Other')
  else:
    other_index = -1

  for val in column:
    if val not in values:
      if other_index >= 0:
        counts[other_index] += 1
        continue
      else:
        raise ValueError('unexpected value in column %s for type %s: %s' % (title, scale_type, val))
    counts[values.index(val)] += 1

  xticks = '{%s}' % ','.join([str(j) for j in range(len(values))])
  xticklabels = '{%s}' % ','.join([val.replace(',', '') for val in values])

  if len(column) > 50:
    res = 10
  elif len(column) > 20:
    res = 5
  else:
    res = 1

  step = int(np.ceil(len(column) / (4 * res)) * res)

  yticks = list(range(0, int(np.floor(len(column) / res) * res)+1, step))
  if yticks[-1] < len(column) * 0.9:
    yticks.append(len(column))
  else:
    yticks[-1] = len(column)
  yticklabels = ['%g / %g \\%%' % (ytick, round(ytick / len(column) * 100)) for ytick in yticks]
  yticks = '{%s}' % ','.join([str(ytick) for ytick in yticks])
  yticklabels = '{%s}' % ', '.join(yticklabels)

  ymax = len(column)

  print('\\begin{center}')
  print('\\begin{tikzpicture}')
  print('\\begin{axis}[ybar, height=5cm, width=12cm, ymin=0, ymax=%d, xtick=%s, xticklabels=%s, x tick label style={rotate=45, anchor=north east}, ytick=%s, yticklabels=%s, ylabel={no.\ respondents / \\%%}]' % (ymax, xticks, xticklabels, yticks, yticklabels))
  print('\\addplot[fill=skyblue1, draw=skyblue3] coordinates {')

  for j in range(len(values)):
    print('(%d, %d)' % (j, counts[j]))

  print('};')
  print('\\end{axis}')
  print('\\end{tikzpicture}')
  print('\\end{center}')

def generate_textcollection(title, column):
    print('\\subsection*{%s}\n\n' % title.replace('&', '\\&'))
    if len(column) == 0:
      print('no one answered this question.')
    else:
      print('\\begin{itemize}')
      for text in column:
          text = text.replace('\\"', '\'').replace('#', '\\#').replace('&', '\\&').replace('%', '\\%')
          print('\\item \\enquote{%s}' % text)
      print('\\end{itemize}')

def statistics(column):
  int_vals = [float(val) for val in column]
  q   = quantiles(int_vals, n = 4)
  iqr = (q[2] - q[0]) / 2.
  median = q[1]
  return mean(int_vals), stdev(int_vals), median, iqr

# read actual data from CSV
# Accumulate data over columns
columns = []

with open('IK 2024 Evaluation.csv') as f:
  reader = csv.reader(f)
  header = reader.__next__()
  for col in range(len(header)):
    columns.append([])
  for row in reader:
    for j in range(1, len(row)):
      if row[j] == '':
        continue
      columns[j].append(row[j])

# define the type of question
scale_types = ['none', 'likert', 'likert', 'likert', 'likert', 'text', 'attendance', # first page
  'numeric', # poster session
  'numeric', 'text', # ET1
  'numeric', 'text', # ET2
  'numeric', 'text', # ET3
  'numeric', 'text', # BC1
  'numeric', 'text', # BC2
  'numeric', 'text', # BC4
  'numeric', 'text', # MC1
  'numeric', 'text', # MC2
  'numeric', 'text', # MC3
  'numeric', 'text', # MC4
  'numeric', 'text', # SC1
  'numeric', 'text', # SC2
  'numeric', 'text', # SC3
  'numeric', 'text', # SC4
  'numeric', 'text', # SC5
  'numeric', 'text', # SC6
  'numeric', 'text', # SC7
  'numeric', 'text', # SC8
  'numeric', 'text', # SC9
  'numeric', 'text', # SC10
  'numeric', 'text', # SC11
  'numeric', 'text', # SC12
  'numeric', 'text', # SC13
  'numeric', 'text', # SC14
  'numeric', 'text', # SC15
  'numeric', 'text', # PC1
  'numeric', 'text', # PC2
  'numeric', 'text', # PC3
  'numeric', 'text', # PC4
  'numeric', 'text', # RC1
  'numeric', 'text', # RC2
  'numeric', 'text', # RC3
  'text', # reasons for non-attendance
  'field', 'nationality', 'status'
]

# compute average for all numeric fields and average of averages
medians = []
means   = []
for i in range(len(header)):
  if scale_types[i] == 'numeric':
    mean_i, _, median_i, _ = statistics(columns[i])
    means.append(mean_i)
    medians.append(median_i)

global_mean, global_std, _, _   = statistics(means)
_, _, global_median, global_iqr = statistics(medians)

# print the results

print('\\section{Overall evaluation of IK 2024}')

for i in range(len(header)):
  if i == 7:
    print('\\clearpage')
    print('\\section{Course Evaluations}')
  elif i == len(header) - 3:
    print('\\section{Demographics}')
  print()
  if scale_types[i] == 'none':
    continue
  elif scale_types[i] == 'text':
    generate_textcollection(header[i], columns[i])
  else:
    generate_pgfplot(header[i], columns[i], scale_type = scale_types[i])

  if scale_types[i] == 'numeric':
    print('Mean: $%.2f$ ($\pm %.2f$) $\quad$ Median: $%.2f$ ($\pm %.2f$)\\\\' % statistics(columns[i]))
    print('Mean across courses: $%.2f$ ($\pm %.2f$) $\quad$ Median across courses: $%.2f$ ($\pm %.2f$)' % (global_mean, global_std, global_median, global_iqr))
  elif scale_types[i] == 'text':
    print('\\clearpage')

  if i == 7:
    print('\\clearpage')


