pages = [
  'ET1',
  'ET2',
  'ET3',
  'BC1',
  'BC2',
  'BC3',
  'BC4',
  'MC1',
  'MC2',
  'MC3',
  'MC4',
  'SC1',
  'SC2',
  'SC3',
  'SC4',
  'SC5',
  'SC6',
  'SC7',
  'SC8',
  'SC9',
  'SC10',
  'SC11',
  'SC12',
  'SC13',
  'SC14',
  'SC15',
  'PC1',
  'PC2',
  'PC3',
  'PC4',
  'RC1',
  'RC2',
  'RC3'
]

init_page = 13

import os

os.system('pdfseparate -f %d -l %d ik2024_evaluation_report.pdf ik2024_evaluation_report_page_00%%d.pdf' % (init_page, init_page + len(pages)))

j = 0
while j < len(pages):
  identifier = pages[j]
  page_list = []
  while j < len(pages) and identifier == pages[j]:
    page_list.append('ik2024_evaluation_report_page_00%d.pdf' % (init_page + j))
    j += 1
  os.system('pdfunite %s ik2024_evaluation_report_%s.pdf' % (' '.join(page_list), identifier.lower()))

os.system('rm ik2024_evaluation_report_page_00*.pdf')
