import json
import sys

if(len(sys.argv) < 2):
    data_file = 'registration_data.txt'
    print('Note: no command line argument for a data file was given, therefore this program will choose the file %s' % data_file)
else:
    data_file = sys.argv[1]

sunshines  = []
moonshines = []

with open(data_file) as f:
    for line in f:
        # check if the current line is JSON data
        if not line.startswith('{'):
            continue
        # if so, parse the json content
        datum = json.loads(line)
        # check if the person wants to be a sunshine
        if datum['sunshine']:
            sunshines.append('\t'.join([datum['first_name'], datum['last_name'], datum['email'], datum['sunshine_lecturers']]))
        if datum['moonshine']:
            moonshines.append('\t'.join([datum['first_name'], datum['last_name'], datum['email']]))


print('--- sunshines ---')
print('\n'.join(sunshines))
print('\n\n--- moonshines ---')
print('\n'.join(moonshines))
