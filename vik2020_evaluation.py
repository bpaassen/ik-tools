import json

elements = ['slides', 'videos', 'streams', 'lecture_discord', 'poster_session', 'poster_discord', 'hacks', 'welcome', 'fireplace', 'pub']
likert_scale = ['very much', 'somewhat', 'not really', 'not at all', 'not used', 'unaware', 'na']

# load the evaluation responses
with open('vik2020_evaluation_data.json') as f:
    data = json.load(f)

print('\\section{Events}\n\nAt the virtual IK 2020, which of the following offers did you use and how much did you like them?\n')

# start collecting data in a matrix form
import numpy as np

X = np.zeros((len(elements), len(likert_scale)), dtype=int)
for response in data:
    for i in range(len(elements)):
        j = likert_scale.index(response[elements[i]])
        X[i, j] += 1

style = 'ybar, height=4cm, width=8cm, ymin=0, ymax=%d, xtick={%s}, xticklabels={%s}, x tick label style={rotate=45, anchor=north east}' % (len(data), ','.join([str(j) for j in range(len(likert_scale))]), ','.join(likert_scale))

# construct a single groupplot with pgfplots
print('\\begin{center}')
print('\\begin{tikzpicture}')
print('\\begin{groupplot}[group style={group size=2 by 5,x descriptions at=edge bottom,y descriptions at=edge left}, ybar, height=4cm, width=8cm, ymin=0, ymax=%d, xtick={%s}, xticklabels={%s}, x tick label style={rotate=45, anchor=north east}]' % (len(data), ','.join([str(j) for j in range(len(likert_scale))]), ','.join(likert_scale)))
for i in range(len(elements)):
    print('\\nextgroupplot[title={%s}]' % (elements[i].replace('_', ' ')))
    print('\\addplot[fill=ik-blue-bright, draw=ik-blue] coordinates')
    coords = '{'
    for j in range(len(likert_scale)):
        coords += ' (%d, %d)' % (j, X[i, j])
    coords += '};'
    print(coords)
print('\\end{groupplot}')
print('\\end{tikzpicture}')
print('\\end{center}\n')

# show background bar chart
backgrounds = ['undergrad', 'grad', 'phd', 'postdoc', 'prof', 'industry', 'other', 'na']

y = np.zeros(len(backgrounds), dtype=int)
for response in data:
    j = backgrounds.index(response['background'])
    y[j] += 1

style = 'ybar, height=4cm, width=8cm, ymin=0, ymax=%d, xtick={%s}, xticklabels={%s}, x tick label style={rotate=45, anchor=north east}' % (len(data), ','.join([str(j) for j in range(len(backgrounds))]), ','.join(backgrounds))

print('\\section{Background}\n\nMy background while participating at the virtual IK 2020 was:\n')

print('\\begin{center}')
print('\\begin{tikzpicture}')
print('\\begin{axis}[%s]' % style)
print('\\addplot[fill=ik-blue-bright, draw=ik-blue] coordinates')
coords = '{'
for j in range(len(backgrounds)):
    coords += ' (%d, %d)' % (j, y[j])
coords += '};'
print(coords)
print('\\end{axis}')
print('\\end{tikzpicture}')
print('\\end{center}\n')

# accumulate free text responses

print('\\section{Free text fields}\n\n')

questions = ['What I liked best about the virtual IK 2020', 'What I would have done different about the virtual IK 2020', 'Further comments, e.g. special wishes for a virtual IK 2021']

field_names = ['like_free_text', 'improve_free_text', 'comments_free_text']

for q in range(len(questions)):
    print('\\subsection{%s}\n\n' % questions[q])
    print('\\begin{itemize}')
    for response in data:
        text = response[field_names[q]]
        if text == '':
            continue
        text = text.replace('\\"', '\'').replace('#', '\\#')
        print('\\item \\enquote{%s}\n' % text)
    print('\\end{itemize}')
