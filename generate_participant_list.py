#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generates a CSV file (with comma delimiter) containing the following columns
for all participants in the following order:

1. title
2. first_name
3. last_name
4. address -> city
5. address -> country
6. address -> institution
7. address -> department
8. studyfield

The result is written to 'ik_participants.csv'.

Copyright (C) 2020
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2020 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys
import json
from extract_field import extract_field

# retrieve the path to the backup registration data file on the command line
if(len(sys.argv) < 2):
    data_file = 'registration_data.txt'
    print('Note: no command line argument for a data file was given, therefore this program will choose the file %s' % data_file)
else:
    data_file = sys.argv[1]

# retrieve the path to the output file on the command line
if(len(sys.argv) < 3):
    out_file = 'ik_participants.csv'
    print('Note: no command line argument for an output file was given, therefore this program will choose the file %s' % out_file)
else:
    out_file = sys.argv[2]

if(len(sys.argv) < 4):
    delimiter = '\t'
    print('Note: no command line argument for a delimiter was given, therefore this program will choose tab')
else:
    delimiter = sys.argv[3]

columns = ['title', 'first_name', 'last_name', 'address.city', 'address.country', 'address.institution', 'address.department', 'studyfield']

rows = extract_field(data_file, columns)

with open(out_file, 'w') as f:
    for row in rows:
        f.write(delimiter.join(row) + '\n')
